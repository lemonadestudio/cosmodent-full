<?php
include 'lib/functions.php';

$title = 'Cosmodent - nowoczesna stomatologia w Rzeszowie i Łańcucie';
$keywords = 'stomatologia, stomatolog Rzeszów, stomatolog Łańcut, dentysta Rzeszów, dentysta Łańcut, praktyka stomatologiczna Cosmodent';
$description = 'Nowoczesna praktyka stomatologiczna Cosmodent zaprasza do gabinetów w Rzeszowie i Łańcucie';

require_once 'templates/head.php';
require_once 'templates/top.html';
require_once 'templates/base.html';
require_once 'templates/footer.html';
require_once 'templates/end.html';
?>
        
        
