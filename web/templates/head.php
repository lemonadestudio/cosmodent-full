<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title ?></title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="keywords" content="<?php echo $keywords ?>"/>
        <meta name="description" content="<?php echo $description ?>"/>
        <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
        
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        <link type="text/css" href="js/theme/jquery.ui.core.css" rel="stylesheet" />
        <link type="text/css" href="js/theme/jquery.ui.theme.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <!--[if lte IE 9]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <![endif]-->
    </head>
    <body>
        <div id="container">