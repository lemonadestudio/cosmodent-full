<?php

namespace XD\CmsBundle\Entity;



use Doctrine\ORM\Mapping as ORM;
use XD\CmsBundle\Helper\Cms;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * XD\CmsBundle\Entity\UploadedFile
 */
class UploadedFile
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $title
     * @Assert\NotNull
     */
    private $title;

    /**
     * @var boolean $published
     */
    private $published;

    /**
     * @var datetime $publishDate
     */
    private $publishDate;

    /**
     * @var string $file
     */
    private $file;

    /**
     * @var boolean $showOnList
     */
    private $showOnList;
    
    public $_file;
    
    public function __construct() {
    	$this->isPublished = true;
    	$this->setPublishDate(new \DateTime());
    	$this->showOnList = true;
    	$this->published = true;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set published
     *
     * @param boolean $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param datetime $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * Get publishDate
     *
     * @return datetime 
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set file
     *
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set showOnList
     *
     * @param boolean $showOnList
     */
    public function setShowOnList($showOnList)
    {
        $this->showOnList = $showOnList;
    }

    /**
     * Get showOnList
     *
     * @return boolean 
     */
    public function getShowOnList()
    {
        return $this->showOnList;
    }
    /**
     * @ORM\prePersist
     */
    public function setCreatedValue()
    {

    }

    /**
     * @ORM\prePersist
     */
    public function preUpload()
    {
        // Add your code here
        
    	$cms_helper = new Cms();
    	
    	if (null !== $this->_file) {
    		
    		
    		// do whatever you want to generate a unique name
    		$this->oldFile = $this->getAbsolutePath();
    		
    		$this->file = $cms_helper->make_url($this->getTitle()).'.'.$this->_file->guessExtension();
    		$this->file = $this->_file->getClientOriginalName();
    	}
    	
    }

    /**
     * @ORM\postPersist
     */
    public function upload()
    {
        if (null === $this->_file) {
    		return;
    	}
    
    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->_file->move($this->getUploadRootDir(), $this->file);
    	
    	if($this->oldFile) {
    		unlink($this->oldFile);
    	}
    
    	unset($this->_file);
    }

    /**
     * @ORM\postRemove
     */
    public function removeUpload()
    {
    	if ($file = $this->getAbsolutePath()) {
    		unlink($file);
    	}
    }
    
    public function getAbsolutePath()
    {
    	return !$this->getFile() ? null : $this->getUploadRootDir().'/'.$this->getFile();
    }
    
    public function getWebPath()
    {
    	return !$this->getFile() ? null : $this->getUploadDir().'/'.$this->getFile();
    }
    
    protected function getUploadRootDir()
    {
    	 
    	// the absolute directory path where uploaded documents should be saved
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
    	return 'uploads/pliki';
    }
    
    public function fileSize() {
    	
    	if($this->getAbsolutePath() && file_exists($this->getAbsolutePath())) {
    		return round( filesize($this->getAbsolutePath()) / 1024);
    	}
    	
    	return '0';
    	
    }
    
    public function __toString() {
    	
    	return $this->getTitle().' - ('.$this->getFile().')';
    }
   
    
    
}