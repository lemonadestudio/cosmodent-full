<?php

namespace XD\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * XD\CmsBundle\Entity\GalleryPhoto
 */
class GalleryPhoto
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $file
     */
    private $file;
    
    public $_file;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set file
     *
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
    /**
     * @ORM\prePersist
     */
    public function setCreatedValue()
    {
        // Add your code here
    }

    /**
     * @ORM\prePersist
     */
    public function preUpload()
    {
    	
    	
    	if (null !== $this->_file) {
    		
    		
    		// do whatever you want to generate a unique name
    		$this->oldFile = $this->getAbsolutePath();
    		
    		$this->file = uniqid().'.'.$this->_file->guessExtension();
    		
    	}
    }

    /**
     * @ORM\postPersist
     */
    public function upload()
    {
       if (null === $this->_file) {
    		return;
    	}
    
    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->_file->move($this->getUploadRootDir(), $this->file);
    	
    	if($this->oldFile) {
    		unlink($this->oldFile);
    	}
    
    	unset($this->_file);
    }

    /**
     * @ORM\postRemove
     */
    public function removeUpload()
    {
    if ($file = $this->getAbsolutePath()) {
    		unlink($file);
    	}
    }
    
    
    public function getAbsolutePath()
    {
    	return !$this->getFile() ? null : $this->getUploadRootDir().'/'.$this->getFile();
    }
    
    public function getWebPath()
    {
    	return !$this->getFile() ? null : $this->getUploadDir().'/'.$this->getFile();
    }
    
    protected function getUploadRootDir()
    {
    
    	// the absolute directory path where uploaded documents should be saved
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
    	return 'uploads/photos';
    }
    /**
     * @var XD\CmsBundle\Entity\Page
     */
    private $page;


    /**
     * Set page
     *
     * @param XD\CmsBundle\Entity\Page $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * Get page
     *
     * @return XD\CmsBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }
}