<?php

namespace XD\CmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('xd_cms');

         $rootNode
            ->addDefaultsIfNotSet()
            ->children()
            	 ->arrayNode('menu')
	             	->children()
	             		->arrayNode('modules')
	             			->prototype('array')
	             				->children()
		             				->scalarNode('label')->end()
		             				->scalarNode('route')->end()
		             				
		             				->variableNode('route_parameters')->defaultValue(array())->end()
		             				->scalarNode('get_elements_service')->defaultNull()->end()
	             				->end()
	             			->end()
	             		->end()
	             		->arrayNode('locations')
	             			->useAttributeAsKey('type')
	             			->prototype('array')
	             				->children()
	             					->scalarNode('label')->end()
	             					->scalarNode('cache')->defaultValue(0)->end()
	             				->end()
	             			->end()
	             		->end()
	             	->end()
	             ->end()
	         ->end()
	     ;    
            	 
            	 
// 	                ->prototype('array')
// 	                    ->children()
// 	                        ->arrayNode('contexts')
// 	                            ->prototype('scalar')->end()
// 	                        ->end()
// 	                        ->scalarNode('cache')->defaultValue('sonata.cache.noop')->end()
// 	                        ->arrayNode('settings')
// 	                            ->useAttributeAsKey('id')
// 	                            ->prototype('scalar')->end()
// 	                        ->end()
// 	                    ->end()
// 	                ->end()
// 	            ->end()
//                     ->prototype('array')
//                         ->fixXmlConfig('filter', 'filters')
//                         ->children()
//                             ->scalarNode('path')->end()
//                             ->scalarNode('quality')->defaultValue(100)->end()
//                             ->scalarNode('format')->defaultNull()->end()
//                             ->scalarNode('cache')->defaultNull()->end()
//                             ->scalarNode('data_loader')->defaultNull()->end()
//                             ->scalarNode('controller_action')->defaultNull()->end()
//                             ->arrayNode('filters')
//                                 ->useAttributeAsKey('name')
//                                 ->prototype('array')
//                                     ->useAttributeAsKey('name')
//                                     ->prototype('variable')->end()
//                                 ->end()
//                             ->end()
//                         ->end()
//                     ->end()
     

        return $treeBuilder;
    }
}
