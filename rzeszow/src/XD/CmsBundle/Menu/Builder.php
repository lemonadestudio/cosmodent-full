<?php

namespace XD\CmsBundle\Menu;

use XD\CmsBundle\Entity\MenuItem;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Description of Builder
 *
 * @author przemq
 */
class Builder extends ContainerAware {




    public function managedMenu(FactoryInterface $factory, $options = array()) {

        $menu = $factory->createItem('root');



        $request = $this->container->get('request');
        $translator = $this->container->get('translator');
        $doctrine = $this->container->get('doctrine');
        $router = $this->container->get('router');




        $menu->setCurrentUri($this->container->get('request')->getRequestUri());


        $em = $this->container->get('doctrine')->getEntityManager();

         $location = $options['location'];

        $menu_items = $em->createQuery("
        		SELECT m
        		FROM XDCmsBundle:MenuItem m
        		WHERE
        			m.location = :location
        		AND m.depth <= 2

        		ORDER BY m.lft ASC, m.title ASC")
        ->setParameter('location', $location)
        ->execute();






  		foreach($menu_items as $item) {



  			$item_arr = $this->getMenuItemArray($item);

  			if(!$item_arr) {
  				continue;
  			}

  			$new_menu_item = null;
  			if($item->getDepth() == 1) {
	  			switch($item->getType()) {

	  				case 'url':

	  					$new_menu_item = $menu -> addChild($item_arr['title'], array(
	  						'uri' => $item_arr['uri'],
	  						'linkAttributes' => $item_arr['linkAttributes']));
	  					break;

	  				case 'route':

	  						$new_menu_item = $menu->addChild($item_arr['title'], array(
	  								'route' => $item_arr['route'],
	  								'linkAttributes' => $item_arr['linkAttributes'],
	  								'routeParameters' => $item_arr['routeParameters'],
	  						));

			            break;
	  				default:
	  					break;

	  			}
  			}
  			if($new_menu_item) {

	  			foreach($menu_items as $item2) {



	  				if($item2->getParentItem()) {
	  					if($item2->getParentItem()->getId() == $item->getId()) {

	  						$item2_arr = $this->getMenuItemArray($item2);

	  						if(!$item2_arr) {
	  							continue;
	  						}

	  						switch($item2->getType()) {

	  							case 'url':

	  								$new_menu_item -> addChild($item2_arr['title'], array(
	  								'uri' => $item2_arr['uri'],
	  								'linkAttributes' => $item2_arr['linkAttributes']));
	  								break;

	  							case 'route':

	  								$new_menu_item ->addChild($item2_arr['title'], array(
	  								'route' => $item2_arr['route'],
	  								'linkAttributes' => $item2_arr['linkAttributes'],
	  								'routeParameters' => $item2_arr['routeParameters'],
	  								));

	  								break;
	  							default:
	  								break;

	  						}
	  					}
	  				}

	  			}
  			}



  		}


//         foreach ($pages as $page) {

//             $menu->addChild($page->getTitle(), array(
//                 'route' => 'page_show',
//                 'routeParameters' => array('slug' => $page->getSlug())
//             ));

//         }

        return $menu;
    }

    public function getMenuItemArray(MenuItem $item) {

    	$router = $this->container->get('router');

    	switch($item->getType()) {

    		case 'url':

    			return array(
    				'title' => $item->getTitle(),
    				'uri' => $item->getUrl(),
    				'linkAttributes' => $item->getAttributesArray(),

    			);


    		case 'route':

    			$parameters = $item->getRouteParametersArray();
    			try{
    				$router->generate($item->getRoute(), $parameters );

    			} catch(\Exception $e) {

    				// brak możliwości wygenerowania trasy
    				break;

    			}

    			return array(
    					'title' => $item->getTitle(),
    					'linkAttributes' => $item->getAttributesArray(),
    					'route' => $item->getRoute(),
    					'routeParameters' => $parameters,
    			);

    		default:

    	}

    	return false;

    }

}
