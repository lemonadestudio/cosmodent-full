<?php 

namespace XD\CmsBundle\Extensions;



use Craue\ConfigBundle\Util\Config;


class SettingsExtension extends \Twig_Extension
{
    protected $craue_config;

    function __construct(Config $config) {
        $this->craue_config = $config;
    }

    public function getGlobals() {
        return array(
            'config' => $this->craue_config->all(),
        	// 'craue_config_all' => $this->craue_config->all()
        );
    }

    public function getName()
    {
        return 'craue_config';
    }

}