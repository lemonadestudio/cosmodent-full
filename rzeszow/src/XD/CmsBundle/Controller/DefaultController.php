<?php

namespace XD\CmsBundle\Controller;


use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
//      * @Route("/")
     */
    public function indexAction()
    {
        $response = $this->render('XDCmsBundle:Default:index.html.twig');

        $response->setSharedMaxAge(20);

        return $response;
    }
    
    public function settingsAction() {
    	
    	
    	$this->container->get('twig')->addGlobal('ustawienia', ' bla bla');
    	
    	return new Response();
    	
    	
    }

}
