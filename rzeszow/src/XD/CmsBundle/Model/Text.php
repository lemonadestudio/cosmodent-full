<?php 

namespace XD\CmsBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSupeMappedSuperclass;
use XD\CmsBundle\Helper\Cms;

/**
 * Superklasa dla tekstówek / aktualności itp
 * 
 * @ORM\MappedSuperclass;
 * @ORM\HasLifecycleCallbacks(); 
 * 
 */
abstract class Text {

	/**
	 * @var string $title
	 * 
	 * @ORM\Column(type="string", length="255");
	 * @Assert\NotNull();
	 * @Assert\MaxLength(255);
	 */
	protected $title;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $published = true;
	
	/**
	 *
	 * @var \DateTime  $publishDate;
	 * @ORM\Column(type="datetime", nullable="true");
	 */
	protected $publishDate;
	
	/**
	 * @var string $keywords
	 * 
	 * @ORM\Column(type="string", length="1024", nullable=true);
	 * @Assert\MaxLength(1024);
	 */
	protected $keywords;
	
	/**
	 * @var string $description
	 *
	 * @ORM\Column(type="string", length=2048, nullable=true);
	 * @Assert\MaxLength(2048)
	 */
	protected $description;
	

	/**
	 * @ORM\Column(type="string", length="255", nullable="true" );
	 * @Assert\MaxLength(255);
	 */
	protected $slug;
	
	/**
	 * @var text $content
	 * @ORM\Column(type="text", nullable=true )
	 */
	protected $content;
	
	
	/**
	 * 
	 * @var unknown_type
	 * @ORM\Column(type="boolean", nullable=true );
	 */
	protected $automaticSeo;
	
	
	
	
	public function __construct() {
	
	
		$this->published = true;
		$this->automaticSeo  = true;
		$this->publishDate = new \DateTime();

	
	}
	
	public function __toString() {
		
		return ''.$this->title;
		
	}
	
	
	/**
	 * Set title
	 *
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Set published
	 *
	 * @param boolean $published
	 */
	public function setPublished($published)
	{
		$this->published = $published;
	}
	
	/**
	 * Get published
	 *
	 * @return boolean
	 */
	public function getPublished()
	{
		return $this->published;
	}
	
	/**
	 * Set publishDate
	 *
	 * @param datetime $publishDate
	 */
	public function setPublishDate($publishDate)
	{
		$this->publishDate = $publishDate;
	}
	
	/**
	 * Get publishDate
	 *
	 * @return datetime
	 */
	public function getPublishDate()
	{
		return $this->publishDate;
	}
	
	/**
	 * Set keywords
	 *
	 * @param string $keywords
	 */
	public function setKeywords($keywords)
	{
		$this->keywords = $keywords;
	}
	
	/**
	 * Get keywords
	 *
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	/**
	 * Set slug
	 *
	 * @param string $slug
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;
	}
	
	/**
	 * Get slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}
	
	/**
	 * Set content
	 *
	 * @param text $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}
	
	/**
	 * Get content
	 *
	 * @return text
	 */
	public function getContent()
	{
		return $this->content;
	}
	
	/**
	 * Set automaticSeo
	 *
	 * @param boolean $automaticSeo
	 */
	public function setAutomaticSeo($automaticSeo)
	{
		$this->automaticSeo = $automaticSeo;
	}
	
	/**
	 * Get automaticSeo
	 *
	 * @return boolean
	 */
	public function getAutomaticSeo()
	{
		return $this->automaticSeo;
	}
	
    /**
	 * @ORM\PrePersist();
	 * @ORM\PreUpdate();
	 */ 
	public function setCreatedValue()
	{
		
		$cms_helper = new Cms();
		 
		if($this->getAutomaticSeo()) {
	
			// urla
			$this->setSlug($cms_helper->make_url($this->getTitle()));
	
			// description
			$description = strip_tags($this->getContent());
			$description = mb_substr(html_entity_decode($description), 0, 2048);

			// usunięcie ostatniego, niedokończonego zdania
	
			$description = preg_replace('@(.*)\..*@', '\1', $description);
	
			$this->setDescription($description);
	
			// keywords
			$keywords_arr = explode(' ', $this->getTitle().' '.$this->getDescription());
	
			$keywords = array();
			if(is_array($keywords_arr)) {
				foreach($keywords_arr as $kw) {
					$kw = trim($kw);
					$kw = preg_replace('@\.,;\'\"@', '', $kw);
					if(strlen($kw) >= 4 && !in_array($kw, $keywords)) {
						$keywords[] = $kw;
					}
					if(count($keywords) >= 10) {
						break;
					}
				}
			}
			
			$this->setKeywords(implode(', ', $keywords));
			$this->setAutomaticSeo(false);
			
		}
	}
}
