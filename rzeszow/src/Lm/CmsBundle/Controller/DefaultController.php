<?php

namespace Lm\CmsBundle\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Lm\CmsBundle\Entity\Strona;
use XD\CmsBundle\Entity\ContactFormMessage;
use Lm\CmsBundle\Entity\Galeria;
use Lm\CmsBundle\Entity\Specjalizacja;
use Lm\CmsBundle\Entity\GaleriaZdjecie;
use Lm\CmsBundle\Form\Type\ContactKonsultacjeType;
use Lm\CmsBundle\Form\Type\ContactZapytanieType;
use Lm\CmsBundle\Form\Type\ContactPracaType;
use Lm\CmsBundle\Form\Type\ContactReferencjeType;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\File\File;
use Lm\CmsBundle\Entity\SpecjalizacjaOpis;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller {

	/**
	 * @Route("/")
	 * @Template()
	 */
	public function mainAction() {

		$zdjecia_zmieniarki = $this->getDoctrine()->getRepository('LmCmsBundle:ZdjecieZmieniarki')
			->findBy(array(), array('kolejnosc' => 'ASC'));
        
        $main_photo_dentistry_query = $this->getDoctrine()->getEntityManager()
		->createQuery(
				"
				SELECT g FROM LmCmsBundle:GaleriaZdjecie g
				WHERE g.podpis = 'Stomatologia'
				ORDER BY g.kolejnosc ASC
				");
        
        $main_photo_dentistry = $main_photo_dentistry_query->setMaxResults(1)->execute();
        
        $main_photo_aesthetic_medicine_query = $this->getDoctrine()->getEntityManager()
		->createQuery(
				"
				SELECT g FROM LmCmsBundle:GaleriaZdjecie g
				WHERE g.podpis = 'Medycyna estetyczna'
				ORDER BY g.kolejnosc ASC
				");
        
        $main_photo_aesthetic_medicine = $main_photo_aesthetic_medicine_query->setMaxResults(1)->execute();

		$aktualnosci_query = $this->getDoctrine()->getEntityManager()
		->createQuery(
				"
				SELECT a FROM LmCmsBundle:Aktualnosc a
				WHERE a.published = true
				AND a.publishDate <= :curr_date
				AND a.naStronieGlownej = true
				ORDER BY a.publishDate DESC
				")->setParameter('curr_date', date('Y-m-d H:i:s'));

		$ostatnie_aktualnosci = $aktualnosci_query->setMaxResults(1)->execute();

		$form = $this
		->createForm(new ContactZapytanieType($this->container),
				array());


		return array(
            'aktualnosci' => $ostatnie_aktualnosci,
            'main_photo_dentistry' => $main_photo_dentistry,
            'main_photo_aesthetic_medicine' => $main_photo_aesthetic_medicine,
            'form' => $form->createView(),
            'message' => '',
            'zdjecia_zmieniarki' => $zdjecia_zmieniarki
        );
	}

	/**
	 * @Route("/kontakt")
	 * @Template()
	 */
	public function contactAction(){


		$form = $this
		->createForm(new ContactZapytanieType($this->container),
				array());


		$request = $this->getRequest();

		// przekierowanie na główną
		if (!$request->isXmlHttpRequest()) {
			// return new RedirectResponse($this->get('router')->generate('lm_cms_default_main'));
		}

		$message_status = '';
		if ($request->getMethod() == 'POST') {

			$form->bindRequest($request);

			if ($form->isValid()) {

				$em = $this->getDoctrine()->getEntityManager();

				$data = $form->getData();


				$message_txt = '<h3>Wiadomość z formularza:</h3>';
				if (isset($data['tresc'])) {
					$data['tresc'] = strip_tags($data['tresc'],
							'<br><p><b><strong>');
					$message_txt .= $data['tresc'] . ' <hr />';
				}

				$message_txt .= '<h3>Pozostałe dane:</h3>';
				if (isset($data['imie']))
					$message_txt .= 'Imię: ' . $data['imie'] . '<br />';
				if (isset($data['nazwisko']))
					$message_txt .= 'Nazwisko: ' . $data['nazwisko'] . '<br />';
				if (isset($data['email']))
					$message_txt .= 'E-mail: ' . $data['email'] . '<br />';
				if (isset($data['telefon']))
					$message_txt .= 'Telefon: ' . $data['telefon'] . '<br />';

				$message = new ContactFormMessage();
				$message
				->setSenderEmail(
						isset($data['email']) ? $data['email']
						: 'brak e-mail');
				$message->setSenderMessage($message_txt);

				$sender_name = isset($data['imie']) ? $data['imie'] : '';
				$sender_name .= isset($data['nazwisko']) ? ' '.$data['nazwisko'] : '';

				$message->setSenderName($sender_name);
				$message->setSenderIP($request->getClientIp());
				$message->setSentAt(new \DateTime());

				$em->persist($message);
				$em->flush();

				$email_to = $this->get('craue_config')
				->get('formularz_wysylka_email');

				$temat = $this->get('craue_config')->get('formularz_wysylka_temat');
				$wysylka_od = $this->get('craue_config')->get('formularz_wysylka_od');

				$message_email = \Swift_Message::newInstance()
				->setBcc('przemek.kadziolka@gmail.com')
				->setSubject(
						$temat)
						->setFrom(
								$this->container->getParameter('mailer_user'),
								$wysylka_od)->setTo($email_to)
								->setBody($message_txt, 'text/html')
								->addPart(strip_tags($message_txt), 'text/plain')


				;

				if (isset($data['email'])) {
					$message_email->setReplyTo($data['email']);
				}

				try {
					$ret = $this->get('mailer')->send($message_email);
				} catch (Exception $e) {

				}

				$message_status = 'sent';
				$this->get('session')
				->setFlash('contact_form_sent',
						'Wiadomość została wysłana. Dziękujemy za skorzystanie z formularza.');


			}


		}

		return array('form' => $form->createView(), 'message' => $message_status);



	}



	/**
	 * @Template()
	 */
	public function blokAction($slug) {

		$page = $this->getDoctrine()->getRepository('LmCmsBundle:Strona')
		->findOneBy(array('slug' => $slug));

		if(!$page) {


			$page = new Strona();
			$page->setTitle($slug);

			$page->setContent('<p>podstrona '.$slug.'</p><p>edytuj treść w panelu administracyjnym</p>');
			$page->setPublished(false);
			$page->setSlug($slug);
			$page->setAutomaticSeo(false);
			$this->getDoctrine()->getEntityManager()->persist($page);
			$this->getDoctrine()->getEntityManager()->flush();

		}

		return array(
				'page' => $page,

				);

	}

	/**
	 * @Route("/admin/browse");
	 */
	public function browseAction() {

		if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
			throw new AccessDeniedException();
		}

		// /cms/web/kcfinder/browse.php

		$_SESSION['KCFINDER'] = array();
		$_SESSION['KCFINDER']['disabled'] = false;
		$_SESSION['fold_type'] = 'pliki';
		$_SESSION['KCFINDER']['uploadURL'] = $this->get('request')
				->getBasePath() . "/uploads";

		$get_parameters = $this->get('request')->query->all();

		$response = new RedirectResponse(
				$this->get('request')->getBasePath() . '/kcfinder/browse.php?'
						. http_build_query($get_parameters));

		return $response;
	}


	/**
	 * @ Ro u  t e("/contactsubmit")
	 */
	public function contactsubmitAction() {

		$request = $this->getRequest();
		$temat = $request->query->get('temat');
		$id = $request->query->get('id');

		$template = 'LmCmsBundle:Default:contact_zapytanie.html.twig';

		// przekierowanie na główną
		if (!$request->isXmlHttpRequest()) {
			// return new RedirectResponse($this->get('router')->generate('lm_cms_default_main'));
		}

		if ($request->getMethod() == 'POST') {

			$contact = $request->request->get('contact');
			if (is_array($contact)) {
				$temat = $contact['temat'];
			}
		}




		$form = $this
		->createForm(new ContactZapytanieType($this->container),
				array('temat' => $temat));

		$message = '';
		if ($request->getMethod() == 'POST') {

			$form->bindRequest($request);

			if ($form->isValid()) {

				$em = $this->getDoctrine()->getEntityManager();

				$data = $form->getData();

				if($data['potwierdzenie'] == 'tak') {


					$message_txt = '<h1>Temat: ' . $data['temat'] . '</h1>';
					$message_txt .= '<h3>Wiadomość z formularza:</h3>';
					if (isset($data['tresc'])) {
						$data['tresc'] = strip_tags($data['tresc'],
								'<br><p><b><strong>');
						$message_txt .= $data['tresc'] . ' <hr />';
					}

					$message_txt .= '<h3>Pozostałe dane:</h3>';
					if (isset($data['imie_nazwisko']))
						$message_txt .= 'Imię i nazwisko: ' . $data['imie_nazwisko'] . '<br />';
					if (isset($data['email']))
						$message_txt .= 'E-mail: ' . $data['email'] . '<br />';
					if (isset($data['telefon']))
						$message_txt .= 'Telefon: ' . $data['telefon'] . '<br />';

					$message = new ContactFormMessage();
					$message
					->setSenderEmail(
							isset($data['email']) ? $data['email']
							: 'brak e-mail');
					$message->setSenderMessage($message_txt);

					$message
					->setSenderName($data['imie_nazwisko']);
					$message->setSenderIP($request->getClientIp());
					$message->setSentAt(new \DateTime());

					$em->persist($message);
					$em->flush();

					$email_to = $this->get('craue_config')
					->get('formularz_kontaktowy_wysylka_email');

					$message_email = \Swift_Message::newInstance()
					->setSubject(
							'Temat: ' . $data['temat']
							. ' - Formularz kontaktowy odszkodowania-krakow.pl.')
							->setFrom(
									$this->container->getParameter('mailer_user'),
									'Cms Kraków formularz')->setTo($email_to)
									->setBody($message_txt, 'text/html')
									->addPart(strip_tags($message_txt), 'text/plain');

					;

					if (isset($data['email'])) {
						$message_email->setReplyTo($data['email']);
					}

					try {
						$ret = $this->get('mailer')->send($message_email);
					} catch (Exception $e) {

					}

					$message = 'sent';
					$this->get('session')
					->setFlash('contact_form_sent',
							'Wiadomość została wysłana. Dziękujemy za skorzystanie z formularza.');


				}  else {
					$message = 'valid';
					$this->get('session')->setFlash('contact_first_step', 'valid');
				}

			}


		}

		$response = $this
				->render($template, array('form' => $form->createView(), 'message' => $message));

		return $response;

	}




}
