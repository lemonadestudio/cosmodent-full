<?php

namespace Lm\CmsBundle\Controller;
use XD\CmsBundle\Menu\Builder;

use Lm\CmsBundle\Entity\Strona;
use Lm\CmsBundle\Entity\Specjalizacja;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class StronyController extends Controller {

	/**
	 * @Route("/{slug}.html");
	 * @Template();
	 */
	public function showAction($slug) {

		$page = $this->getDoctrine()->getRepository('LmCmsBundle:Strona')
				->findOneBy(array('slug' => $slug, 'published' => true));

		if (!$page) {
			throw new NotFoundHttpException();
		}

		$breadcrumbs = $this->get("white_october_breadcrumbs");

		if($page->getStronaNadrzedna()) {
			$nadrz = $page->getStronaNadrzedna();
			$breadcrumbs->addItem($nadrz->getTitle(), $this->get('router')->generate('lm_cms_strony_show', array('slug' => $nadrz->getSlug())));

		}

		$breadcrumbs->addItem($page->getTitle(), '');

		return array('page' => $page, 'content_title' => $page->getTitle(),
		// 'leftMenus' => $this->leftmenus(array('page' => $page)),
		);

	}

	private function leftmenus($current = array()) {

		$router = $this->get('router');

		if (array_key_exists('page', $current)
				&& $current['page'] instanceof Strona) {

			$current_page = $current['page'];
		} else {

			return null;
		}

		// znalezienie węzła menu w którym znajduje się dana strona
		$items = $this->getDoctrine()->getEntityManager()
				->createQuery(
						"
						SELECT m
						FROM XDCmsBundle:MenuItem m
						WHERE m.route = 'lm_cms_strony_show'
						  AND m.routeParameters = :routeParameters
						ORDER BY m.depth ASC
						")->setMaxResults(1)
				->execute(
						array(
								'routeParameters' => json_encode(
										array(
												'slug' => $current_page
														->getSlug()))));

		if (count($items) >= 1) {

			$item = $items[0];

		} else {

			return null;
		}

		if ($item->getDepth() == 1) {
			$root_item = $item;
		} else {
			$root_item = $item->getParentItem();
		}

		$children = $root_item->getChildItems();

		$menuItems = array();

		$builder = new Builder();
		$builder->setContainer($this->container);

		foreach ($children as $menuItem) {

			$item_arr = $builder->getMenuItemArray($menuItem);

			if (!$item_arr) {
				continue;
			}

			switch ($item->getType()) {

			case 'url':
				$menu
						->addChild($item_arr['title'],
								array('uri' => $item_arr['uri'],
										'linkAttributes' => $item_arr['linkAttributes']));

				$menuItems[] = array('title' => $item_arr['title'],
						'url' => $item_arr['uri'], 'subitems' => array(),
						'current' => 0, // todo: w przypadku wypluwania tego w innych modułach spróbować wykryć aktywny element
				);

				break;

			case 'route':
				$menuItems[] = array('title' => $item_arr['title'],
						'url' => $router
								->generate($item_arr['route'],
										$item_arr['routeParameters']),
						'subitems' => array(),
						'current' => ($item_arr['route']
								== 'lm_cms_strony_show'
								&& $item_arr['routeParameters']['slug']
										== $current_page->getSlug()) ? 1 : 0,);

				break;
			default:
				break;

			}

		}

		$leftMenus = array(
				array('title' => $root_item->getTitle(), 'items' => $menuItems));

		return $leftMenus;

	}

}
