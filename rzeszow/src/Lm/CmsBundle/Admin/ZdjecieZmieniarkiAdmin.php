<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class ZdjecieZmieniarkiAdmin extends Admin {


	protected $translationDomain = 'KlinikaAdmin';

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('kadruj', '{id}/kadruj');
		$collection->add('kadrujZapisz', '{id}/kadruj-zapisz');
	}


	public function getEditTemplate() {

		return 'LmCmsBundle:Admin\\ZdjecieZmieniarki:edit.html.twig';
	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('id')
	        	->add('obrazek', null, array('template' => 'LmCmsBundle:Slider:obrazek.html.twig'))
	        	->add('kolejnosc')


         ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper
        	->addIdentifier('id', null, array())
        	->add('obrazek', null, array('template' => 'LmCmsBundle:Slider:obrazek.html.twig'))
        	->add('kolejnosc')


                ;

                $listMapper->add('_action', 'actions', array(
                		$this->trans('actions') => array(
                				'edit' => array(),
                				'delete' => array(),
                				'view' => array(),
                		)
                	)
				);

    }



    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdatedAt( new \DateTime('now'));

        $formMapper

        	->add('_file', 'file', array('property_path' => null, 'label' => 'File'))
            ->add('title', null, array('required' => true))
            ->add('content', null, array('required' => true))
        	->add('kolejnosc')

			->add('updated_at', null, array( 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')))
        ;

        $formMapper->setHelps(array(
                    '_file' => $this->trans('Zdjęcie nie mniejsze niż 650x405px'),
        		));


    }

}