<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EmployeeAdmin extends Admin {

    protected $translationDomain = 'KlinikaAdmin';

    public function getBatchActions() {
        $actions = array();

        // turn off batch actions
        return $actions;
    }

    public function getEditTemplate() {
        return 'LmCmsBundle:Admin\Employee:edit.html.twig';
    }

    public function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('name')
                ->add('description')
                ->add('position')
                ->add('specializations')
                ->add('isActive')
                ->add('entityOrder')
        ;
    }

    public function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('name', null, array())
                ->add('position')
                ->add('isActive')
                ->add('entityOrder')
                ->add('createdAt')
        ;

        $listMapper->add('_action', 'actions', array(
            $this->trans('actions') => array(
                'view' => array(),
                'edit' => array(),
                'delete' => array()
            )
        ));
    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
        ;
    }

    public function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('name', null, array('required' => true))
                ->add('description', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('isActive', 'checkbox', array('required' => false))
                ->add('_file', 'file', array('required' => false, 'label' => 'Miniaturka w listowaniu'))
                ->add('_delete_file', 'checkbox', array('required' => false, 'label' => 'Usunąć miniaturkę?'))
                ->add('_largePhotoFile', 'file', array('required' => false, 'label' => 'Dużę zdjęcie'))
                ->add('_deleteLargePhotoFile', 'checkbox', array('required' => false, 'label' => 'Usunąć duże zdjęcie?'))
                ->add('position', 'textarea', array('required' => false))
                ->add('entityOrder', null, array('required' => true))
                ->add('specializations', 'textarea', array('required' => false))
        ;
    }

}
