<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping\OrderBy;
use XD\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Lm\CmsBundle\Utils\Tools;

/**
 *
 * @ORM\Table();
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class ZdjecieZmieniarki
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
	 * @ORM\Column(type="string", length=255, nullable="true")
	 */
	private $title;
    
    /**
	 * @ORM\Column(type="string", length=255, nullable="true")
	 */
	private $content;

    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    private $kolejnosc;

    /**
     * @ORM\Column(name="obrazek", type="string", length=255, nullable="true")
     */
    private $obrazek;

    /**
     * @ORM\Column(type="datetime", nullable="true")
     */
    private $updated_at;



    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;

    protected $desktopWidth = 650;
    protected $desktopHeight = 405;

    protected $tabletWidth = 650;
    protected $tabletHeight = 405;

    protected $mobileWidth = 320;
    protected $mobileHeight = 290;

    public function __construct()
    {
        $this->kolejnosc=0;
        $this->updated_at = new \DateTime();

    }

    public function __toString() {
    	return ''.$this->getId();
    }

    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	$this->updated_at = new \DateTime("now");

    	if (null !== $this->_file) {

    		// do whatever you want to generate a unique name
    		$this->oldFile = $this->getObrazek();

    		$this->obrazek = $this->generateObrazekName().'.'.$this->_file->guessExtension();

    	}

    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->_file) {
    		return;
    	}

    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->_file->move($this->getUploadRootDir(), $this->obrazek);

    	if($this->oldFile) {
    		@unlink($this->getUploadRootDir() .'/'.$this->oldFile);
    	}

    	unset($this->_file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	if ($file = $this->getAbsolutePath()) {
    		@unlink($file);
    	}
    }

    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'desktop':
                $size['width'] = $this->desktopWidth;
                $size['height'] = $this->desktopHeight;
                break;
            case 'tablet':
                $size['width'] = $this->tabletWidth;
                $size['height'] = $this->tabletHeight;
                break;
            case 'mobile':
                $size['width'] = $this->mobileWidth;
                $size['height'] = $this->mobileHeight;
                break;
        }

        return $size;
    }
    
    public function getThumbWebPath($type)
    {
        if (empty($this->obrazek)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'desktop':
                    $sThumbName = Tools::thumbName($this->obrazek, '_d');
                    break;
                case 'tablet':
                    $sThumbName = Tools::thumbName($this->obrazek, '_t');
                    break;
                case 'mobile':
                    $sThumbName = Tools::thumbName($this->obrazek, '_m');
                    break;
            }
            
            return $this->getUploadDir() . '/' . $sThumbName;
        }
    }
    
    public function getThumbAbsolutePath($type)
    {
        if (empty($this->obrazek)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'desktop':
                    $sThumbName = Tools::thumbName($this->obrazek, '_d');
                    break;
                case 'tablet':
                    $sThumbName = Tools::thumbName($this->obrazek, '_t');
                    break;
                case 'mobile':
                    $sThumbName = Tools::thumbName($this->obrazek, '_m');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }
    
    public function getObrazekSize()
    {
        $temp = getimagesize($this->getAbsolutePath());
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }
    
    public function createThumbs()
    {
        if (null !== $this->getAbsolutePath()) {
            $sFileName = $this->getObrazek();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
            require_once __DIR__ . '/../../../../vendor/weotch/phpthumb/src/ThumbLib.inc.php';
            
            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameD = Tools::thumbName($sSourceName, '_d');
            $aThumbSizeD = $this->getThumbSize('desktop');
            $thumb->adaptiveResize($aThumbSizeD['width'] + 2, $aThumbSizeD['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeD['width'], $aThumbSizeD['height']);
            $thumb->save($sThumbNameD);
            
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameT = Tools::thumbName($sSourceName, '_t');
            $aThumbSizeT = $this->getThumbSize('tablet');
            $thumb->adaptiveResize($aThumbSizeT['width'] + 2, $aThumbSizeT['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeT['width'], $aThumbSizeT['height']);
            $thumb->save($sThumbNameT);
            
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameM = Tools::thumbName($sSourceName, '_m');
            $aThumbSizeM = $this->getThumbSize('mobile');
            $thumb->adaptiveResize($aThumbSizeM['width'] + 2, $aThumbSizeM['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
            $thumb->save($sThumbNameM);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        $sSourceName = $this->getUploadRootDir() . "/" . $this->getObrazek();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        
        require_once __DIR__ . '/../../../../vendor/weotch/phpthumb/src/ThumbLib.inc.php';
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

    public function getAbsolutePath()
    {
    	return !$this->getObrazek() ? null : $this->getUploadRootDir().'/'.$this->getObrazek();
    }

    public function getWebPath()
    {
    	return !$this->getObrazek() ? null : $this->getUploadDir().'/'.$this->getObrazek();
    }

    public function generateObrazekName() {

    	$cms = new Cms();
    	return $cms->make_url($this->getId().' '.uniqid());
    }

    public function getUploadRootDir()
    {

    	// the absolute directory path where uploaded documents should be saved
    	return __DIR__.'/../../../../web'.$this->getUploadDir();
    }

    public function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
    	return '/uploads/zdjecie_zmieniarki';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     */
    public function setKolejnosc($kolejnosc)
    {
        $this->kolejnosc = $kolejnosc;
    }

    /**
     * Get kolejnosc
     *
     * @return integer
     */
    public function getKolejnosc()
    {
        return $this->kolejnosc;
    }

    /**
     * Set obrazek
     *
     * @param string $obrazek
     */
    public function setObrazek($obrazek)
    {
        $this->obrazek = $obrazek;
    }

    /**
     * Get obrazek
     *
     * @return string
     */
    public function getObrazek()
    {
        return $this->obrazek;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
}