<?php

namespace Lm\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use XD\CmsBundle\Helper\Cms;
use XD\CmsBundle\Model\Text;


/**
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks();
 */
class Strona extends Text {

	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	public function getId() {

		return $this->id;

	}

	/**
	 * @ORM\Column(name="obrazek", type="string", length=255, nullable="true")
	 */
	private $obrazek;

	/**
	 *
	 * @Assert\File(maxSize="6000000")
	 */
	public $_file;

	public $_delete_file = false;

	/**
	 * @ORM\Column(type="datetime", nullable="true")
	 */
	private $updated_at;

	/**
	 * @ORM\ManyToOne(targetEntity="Galeria", inversedBy="strony");
	 * @ORM\JoinColumn(onDelete="SET NULL", onUpdate="SET NULL")
	 */
	private $galeria;

	/**
	 * @ORM\Column(type="string", length=255, nullable="true")
	 */
	private $title2;


	/**
	 * @ORM\OneToMany(targetEntity="Strona", mappedBy="strona_nadrzedna");
	 * @var unknown
	 */
	private $strony_podrzedne;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="Strona", inversedBy="strony_podrzedne");
	 */
	private $strona_nadrzedna;


	public function getTitle2() {
		return $this->title2;
	}

	public function setTitle2($title) {
		  $this->title2 = $title;
	}

	public function getTitleJoined() {
		if ($this->title2) {
			return $this->title.' '.$this->title2;
		}

		return $this->title;
	}


	/**
	 *
	 * @ORM\PrePersist();
	 * @ORM\PreUpdate();
	 */
	public function preUpload()
	{

		parent::setCreatedValue();

		if (null !== $this->_file) {

			// do whatever you want to generate a unique name
			$this->oldFile = $this->getAbsolutePath();

			$this->obrazek = uniqid().'.'.$this->_file->guessExtension();

		} elseif (true == $this->_delete_file ) {

			@unlink($this->getAbsolutePath());
			$this->setObrazek(null);

		}

	}

	/**
	 *
	 * @ORM\PostPersist();
	 * @ORM\PostUpdate();
	 */
	public function upload()
	{
		if (null === $this->_file) {
			return;
		}

		// if there is an error when moving the file, an exception will
		// be automatically thrown by move(). This will properly prevent
		// the entity from being persisted to the database on error
		$this->_file->move($this->getUploadRootDir(), $this->obrazek);

		if($this->oldFile) {
			@unlink($this->oldFile);
		}

		unset($this->_file);
	}

	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload()
	{
		if ($file = $this->getAbsolutePath()) {
			@unlink($file);
		}
	}


	public function getAbsolutePath()
	{
		return !$this->getObrazek() ? null : $this->getUploadRootDir().'/'.$this->getObrazek();
	}

	public function getWebPath()
	{
		return !$this->getObrazek() ? null : $this->getUploadDir().'/'.$this->getObrazek();
	}

	protected function getUploadRootDir()
	{

		// the absolute directory path where uploaded documents should be saved
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}

	protected function getUploadDir()
	{

		// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
		return 'uploads/strony';
	}


    /**
     * Set obrazek
     *
     * @param string $obrazek
     */
    public function setObrazek($obrazek)
    {
        $this->obrazek = $obrazek;
    }

    /**
     * Get obrazek
     *
     * @return string
     */
    public function getObrazek()
    {
        return $this->obrazek;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set galeria
     *
     * @param Lm\CmsBundle\Entity\Galeria $galeria
     */
    public function setGaleria($galeria)
    {
        $this->galeria = $galeria;
    }

    /**
     * Get galeria
     *
     * @return Lm\CmsBundle\Entity\Galeria
     */
    public function getGaleria()
    {
        return $this->galeria;
    }


    /**asdfg*/



    /**
     * Add strony_podrzedne
     *
     * @param Lm\CmsBundle\Entity\Strona $stronyPodrzedne
     */
    public function addStrona( $stronyPodrzedne)
    {
        $this->strony_podrzedne[] = $stronyPodrzedne;
    }

    /**
     * Get strony_podrzedne
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getStronyPodrzedne()
    {
        return $this->strony_podrzedne;
    }

    /**
     * Set strona_nadrzedna
     *
     * @param Lm\CmsBundle\Entity\Strona $stronaNadrzedna
     */
    public function setStronaNadrzedna( $stronaNadrzedna)
    {
        $this->strona_nadrzedna = $stronaNadrzedna;
    }

    /**
     * Get strona_nadrzedna
     *
     * @return Lm\CmsBundle\Entity\Strona
     */
    public function getStronaNadrzedna()
    {
        return $this->strona_nadrzedna;
    }
}