var next_item = 0;
var curr_item = 0;
var prev_item = 0;
var items = null;
var items_count = 0;
var zmien_timer = null;

$(function() {
	// slider
    items = $('#main .slider .photos img');
	items_count = items.size();

	if(items_count > 0) {

		curr_item = items_count -1;
		zmieniarkaZmien();
		zmien_timer = setInterval(function(){
			//  zmieniarkaZmien();
		}, 3000);

	}

	$('body > header .menu > ul > li:first-child').addClass('home').find('a').html('&nbsp;');
    $('.colorbox-init').colorbox();
    //showPromoVideo(false);
});

function submitContactForm() {
	_id_parent = $('#kontakt-form').parent().attr('id');

	$('#kontakt-form').ajaxSubmit({
		target: '#' + _id_parent,
	    url : contact_form_url,
	    success: function(){

	    },
	    beforeSubmit: function(){
	    	$('#'+_id_parent).html('<div class="div-loading"></div>');
	    }
	});
}


function showPromoVideo(showOnlyOnce) {
	if (!$('#main.home').length) {
		return;
	}

	if (showOnlyOnce) {
		var cookieName = 'promo-video-watched';

		if (!$.cookie(cookieName)) {
			showVideo();
			$.cookie(cookieName, 1, { expires: 365, path: '/' });
		}
	} else {
		showVideo();
	}

	function showVideo() {
		$.colorbox({ 
			iframe: true, 
			innerWidth:640, 
			innerHeight:390,
			href: 'https://www.youtube.com/embed/eJN7DfJ2LVg?rel=0&wmode=transparent&autoplay=1'
		});
	}
}

$(document).ready(function () {
    var bxSlider = $('.bx-slider').bxSlider({
        auto: true,
        mode: 'fade',
        pager: true
    });
});

// mobile menu

var mobileMenu = {
    selector: $('#mobile-menu'),
    state: false
};

function changeMenuState() {
    if (!mobileMenu.state) {
        $('#menu-list').fadeIn();
    } else {
        $('#menu-list').fadeOut();
    }
    
    mobileMenu.state = !mobileMenu.state;
}

// mobile menu -> submenu

$(document).on('click', '#menu-list > div.menu-container > ul > li > a', function (event) {
    if (window.innerWidth <= 1019) {
        if ($(this).parent('li').find('ul').length > 0) {
            event.preventDefault();
            
            var submenu = $(this).parent('li').find('ul'); // menu element - <ul> selector
            
            submenu.fadeIn(); // show submenu
            
            if (submenu.find('.submenu-title').length === 0) {
                submenu.prepend('<div class="submenu-title">' + $(this).text() + '</div>');
                
                submenu.append('<div class="back-to-list-wrap"><div class="back-to-list">Powrót do menu</div></div>');

                $(document).off('click', '.back-to-list');
                
                $(document).on('click', '.back-to-list', function () {
                    submenu.fadeOut();
                });
            }
        }
    }
});

// resizing

function resizeItems() {
    if ($(window).innerWidth() >= 1020) {
        $('#menu-list').show();
        $('ul .submenu-title').remove();
        $('ul .back-to-list-wrap').remove();
        $('#menu-list > div.menu-container > ul > li > ul').attr('style',  '');
    } else if ($(window).innerWidth() <= 1019 && $(window).innerWidth() >= 740) {
        mobileMenu.state ? $('#menu-list').show() : $('#menu-list').hide();
    } else if ($(window).innerWidth() <= 739) {
        mobileMenu.state ? $('#menu-list').show() : $('#menu-list').hide();
    }
}

$(window).on('resize orientationchange', function() {
    resizeItems();
});