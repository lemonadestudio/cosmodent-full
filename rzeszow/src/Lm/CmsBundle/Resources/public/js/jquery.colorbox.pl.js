$.colorbox.settings.current = 'Obraz {current} z {total}';
$.colorbox.settings.previous = "Poprzedni";
$.colorbox.settings.next = "Następny";
$.colorbox.settings.close = "Zamknij";
$.colorbox.settings.xhrError = "Zawartość nie mogła być załadowana.";
$.colorbox.settings.imgError = "Obrazek nie mógł być załadowany.";
$.colorbox.settings.slideshowStart = "Rozpocznij pokaz slajdów";
$.colorbox.settings.slideshowStop = "Zatrzymaj pokaz slajdów";