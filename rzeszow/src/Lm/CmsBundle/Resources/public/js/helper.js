﻿//pseudo lazyLoader
$(window).load(function () {
	$("#main .slider").find(".waitImage").fadeOut(2000, function () {
		$(this).remove();
	});
});
	
$(function () {
    var menuLiElem, slider;

    menuUlElem = $("header");

    menuUlElem.hover(function () {
        $("#main").css("z-index", "-1");
    }, function () {
        $("#main").css("z-index", "0");
    });

    //Colorbox for gallery
    if ($(".photos .row .mask").length > 0)
        $(".photos .row .mask").colorbox({ rel: 'gal' });

    //Colorbox for contactForm
    $("a#linkToContact").colorbox({
        href: contact_form_url,
        title: "Formularz kontaktowy",
        height: '480px',
        width: '485px',
        className: 'formContact'
    });

    if ($("#main .slider").length > 0) {
        slider = $("#main .slider");
			
		//slider
		function Slider() {
			$(slider.find('img')).last().fadeOut(1500, function () {
				var firstImg = slider.find('img').first();
				var lastImg = slider.find("img").last();
				lastImg.remove();
				lastImg.css('display', 'block');
				slider.find("img").first().before(lastImg);
			});
		};

		setInterval(Slider, 5000);
    }
});