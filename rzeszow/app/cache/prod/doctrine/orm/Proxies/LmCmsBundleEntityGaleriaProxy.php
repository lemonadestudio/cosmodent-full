<?php

namespace Proxies;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class LmCmsBundleEntityGaleriaProxy extends \Lm\CmsBundle\Entity\Galeria implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }
    
    
    public function __toString()
    {
        $this->__load();
        return parent::__toString();
    }

    public function preUpload()
    {
        $this->__load();
        return parent::preUpload();
    }

    public function upload()
    {
        $this->__load();
        return parent::upload();
    }

    public function removeUpload()
    {
        $this->__load();
        return parent::removeUpload();
    }

    public function getAbsolutePath()
    {
        $this->__load();
        return parent::getAbsolutePath();
    }

    public function getWebPath()
    {
        $this->__load();
        return parent::getWebPath();
    }

    public function getWebPathCrop()
    {
        $this->__load();
        return parent::getWebPathCrop();
    }

    public function generateObrazekName()
    {
        $this->__load();
        return parent::generateObrazekName();
    }

    public function getUploadRootDir()
    {
        $this->__load();
        return parent::getUploadRootDir();
    }

    public function getUploadDir()
    {
        $this->__load();
        return parent::getUploadDir();
    }

    public function getId()
    {
        $this->__load();
        return parent::getId();
    }

    public function setNazwa($nazwa)
    {
        $this->__load();
        return parent::setNazwa($nazwa);
    }

    public function getNazwa()
    {
        $this->__load();
        return parent::getNazwa();
    }

    public function setOpis($opis)
    {
        $this->__load();
        return parent::setOpis($opis);
    }

    public function getOpis()
    {
        $this->__load();
        return parent::getOpis();
    }

    public function setSlug($slug)
    {
        $this->__load();
        return parent::setSlug($slug);
    }

    public function getSlug()
    {
        $this->__load();
        return parent::getSlug();
    }

    public function setKolejnosc($kolejnosc)
    {
        $this->__load();
        return parent::setKolejnosc($kolejnosc);
    }

    public function getKolejnosc()
    {
        $this->__load();
        return parent::getKolejnosc();
    }

    public function setObrazek($obrazek)
    {
        $this->__load();
        return parent::setObrazek($obrazek);
    }

    public function getObrazek()
    {
        $this->__load();
        return parent::getObrazek();
    }

    public function setWyswietlana($wyswietlana)
    {
        $this->__load();
        return parent::setWyswietlana($wyswietlana);
    }

    public function getWyswietlana()
    {
        $this->__load();
        return parent::getWyswietlana();
    }

    public function setZalaczalna($zalaczalna)
    {
        $this->__load();
        return parent::setZalaczalna($zalaczalna);
    }

    public function getZalaczalna()
    {
        $this->__load();
        return parent::getZalaczalna();
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->__load();
        return parent::setUpdatedAt($updatedAt);
    }

    public function getUpdatedAt()
    {
        $this->__load();
        return parent::getUpdatedAt();
    }

    public function addGaleriaZdjecie(\Lm\CmsBundle\Entity\GaleriaZdjecie $zdjecie)
    {
        $this->__load();
        return parent::addGaleriaZdjecie($zdjecie);
    }

    public function getZdjecia()
    {
        $this->__load();
        return parent::getZdjecia();
    }

    public function setZdjecia($zdjecia)
    {
        $this->__load();
        return parent::setZdjecia($zdjecia);
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'nazwa', 'opis', 'slug', 'kolejnosc', 'obrazek', 'wyswietlana', 'zalaczalna', 'updated_at', 'zdjecia', 'aktualnosci', 'strony');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields AS $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}