<?php

namespace Proxies;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class XDCmsBundleEntityPageProxy extends \XD\CmsBundle\Entity\Page implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }
    
    
    public function getId()
    {
        $this->__load();
        return parent::getId();
    }

    public function getPrice()
    {
        $this->__load();
        return parent::getPrice();
    }

    public function setPrice($price)
    {
        $this->__load();
        return parent::setPrice($price);
    }

    public function addTag(\XD\CmsBundle\Entity\Tag $tag)
    {
        $this->__load();
        return parent::addTag($tag);
    }

    public function getTags()
    {
        $this->__load();
        return parent::getTags();
    }

    public function setTags(\Doctrine\Common\Collections\ArrayCollection $tags)
    {
        $this->__load();
        return parent::setTags($tags);
    }

    public function addUploadedFile(\XD\CmsBundle\Entity\UploadedFile $file)
    {
        $this->__load();
        return parent::addUploadedFile($file);
    }

    public function getFiles()
    {
        $this->__load();
        return parent::getFiles();
    }

    public function setFiles(\Doctrine\Common\Collections\ArrayCollection $files)
    {
        $this->__load();
        return parent::setFiles($files);
    }

    public function setTitle($title)
    {
        $this->__load();
        return parent::setTitle($title);
    }

    public function getTitle()
    {
        $this->__load();
        return parent::getTitle();
    }

    public function setPublishDate($publish_date)
    {
        $this->__load();
        return parent::setPublishDate($publish_date);
    }

    public function getPublishDate()
    {
        $this->__load();
        return parent::getPublishDate();
    }

    public function setKeywords($keywords)
    {
        $this->__load();
        return parent::setKeywords($keywords);
    }

    public function getKeywords()
    {
        $this->__load();
        return parent::getKeywords();
    }

    public function setDescription($description)
    {
        $this->__load();
        return parent::setDescription($description);
    }

    public function getDescription()
    {
        $this->__load();
        return parent::getDescription();
    }

    public function setSlug($slug)
    {
        $this->__load();
        return parent::setSlug($slug);
    }

    public function getSlug()
    {
        $this->__load();
        return parent::getSlug();
    }

    public function setContent($content)
    {
        $this->__load();
        return parent::setContent($content);
    }

    public function getContent()
    {
        $this->__load();
        return parent::getContent();
    }

    public function setPublished($published)
    {
        $this->__load();
        return parent::setPublished($published);
    }

    public function isPublished()
    {
        $this->__load();
        return parent::isPublished();
    }

    public function getPublished()
    {
        $this->__load();
        return parent::getPublished();
    }

    public function setAutomaticSeo($automaticSeo)
    {
        $this->__load();
        return parent::setAutomaticSeo($automaticSeo);
    }

    public function getAutomaticSeo()
    {
        $this->__load();
        return parent::getAutomaticSeo();
    }

    public function __toString()
    {
        $this->__load();
        return parent::__toString();
    }

    public function setCreatedValue()
    {
        $this->__load();
        return parent::setCreatedValue();
    }

    public function setTopImage($topImage)
    {
        $this->__load();
        return parent::setTopImage($topImage);
    }

    public function getTopImage()
    {
        $this->__load();
        return parent::getTopImage();
    }

    public function getAbsolutePath()
    {
        $this->__load();
        return parent::getAbsolutePath();
    }

    public function getWebPath()
    {
        $this->__load();
        return parent::getWebPath();
    }

    public function preUpload()
    {
        $this->__load();
        return parent::preUpload();
    }

    public function upload()
    {
        $this->__load();
        return parent::upload();
    }

    public function removeUpload()
    {
        $this->__load();
        return parent::removeUpload();
    }

    public function setOnMainPage($onMainPage)
    {
        $this->__load();
        return parent::setOnMainPage($onMainPage);
    }

    public function getOnMainPage()
    {
        $this->__load();
        return parent::getOnMainPage();
    }

    public function setOnMainPageOrder($onMainPageOrder)
    {
        $this->__load();
        return parent::setOnMainPageOrder($onMainPageOrder);
    }

    public function getOnMainPageOrder()
    {
        $this->__load();
        return parent::getOnMainPageOrder();
    }

    public function getUniqid()
    {
        $this->__load();
        return parent::getUniqid();
    }

    public function hasContactForm()
    {
        $this->__load();
        return parent::hasContactForm();
    }

    public function setType($type)
    {
        $this->__load();
        return parent::setType($type);
    }

    public function getType()
    {
        $this->__load();
        return parent::getType();
    }

    public function getPublishDatePL()
    {
        $this->__load();
        return parent::getPublishDatePL();
    }

    public function addGalleryPhoto($photo)
    {
        $this->__load();
        return parent::addGalleryPhoto($photo);
    }

    public function addPhotos($photo)
    {
        $this->__load();
        return parent::addPhotos($photo);
    }

    public function getPhotos()
    {
        $this->__load();
        return parent::getPhotos();
    }

    public function setPhotos($photos)
    {
        $this->__load();
        return parent::setPhotos($photos);
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'title', 'published', 'publishDate', 'keywords', 'description', 'price', 'slug', 'content', 'automaticSeo', 'topImage', 'onMainPage', 'onMainPageOrder', 'type', 'photos', 'files');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields AS $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}