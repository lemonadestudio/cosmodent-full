<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appprodUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // lm_cms_employee_index
        if ($pathinfo === '/nasz-zespol') {
            return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\EmployeeController::indexAction',  '_route' => 'lm_cms_employee_index',);
        }

        // lm_cms_aktualnosci_lista
        if ($pathinfo === '/aktualnosci') {
            return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\AktualnosciController::listaAction',  '_route' => 'lm_cms_aktualnosci_lista',);
        }

        // lm_cms_aktualnosci_show
        if (0 === strpos($pathinfo, '/aktualnosci') && preg_match('#^/aktualnosci/(?P<slug>[^/\\.]+?)\\.html$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\AktualnosciController::showAction',)), array('_route' => 'lm_cms_aktualnosci_show'));
        }

        // lm_cms_strony_show
        if (preg_match('#^/(?P<slug>[^/\\.]+?)\\.html$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\StronyController::showAction',)), array('_route' => 'lm_cms_strony_show'));
        }

        // lm_cms_default_main
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'lm_cms_default_main');
            }
            return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\DefaultController::mainAction',  '_route' => 'lm_cms_default_main',);
        }

        // lm_cms_default_contact
        if ($pathinfo === '/kontakt') {
            return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\DefaultController::contactAction',  '_route' => 'lm_cms_default_contact',);
        }

        // lm_cms_default_browse
        if ($pathinfo === '/admin/browse') {
            return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\DefaultController::browseAction',  '_route' => 'lm_cms_default_browse',);
        }

        // xd_cms_page_show
        if (preg_match('#^/(?P<slug>[^/\\.]+?)\\.html$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\PageController::showAction',)), array('_route' => 'xd_cms_page_show'));
        }

        // contact_form
        if ($pathinfo === '/formularz-kontaktowy') {
            return array (  '_controller' => 'XD\\CmsBundle\\Controller\\ContactFormController::showAction',  '_route' => 'contact_form',);
        }

        if (0 === strpos($pathinfo, '/admin')) {
            // sonata_admin_retrieve_form_element
            if ($pathinfo === '/admin/core/get-form-field-element') {
                return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
            }

            // sonata_admin_append_form_element
            if ($pathinfo === '/admin/core/append-form-field-element') {
                return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
            }

            // sonata_admin_short_object_information
            if ($pathinfo === '/admin/core/get-short-object-description') {
                return array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_route' => 'sonata_admin_short_object_information',);
            }

            // sonata_admin_set_object_field_value
            if ($pathinfo === '/admin/core/set-object-field-value') {
                return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
            }

            // admin_lm_cms_galeria_list
            if ($pathinfo === '/admin/lm/cms/galeria/list') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::listAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_list',  '_route' => 'admin_lm_cms_galeria_list',);
            }

            // admin_lm_cms_galeria_create
            if ($pathinfo === '/admin/lm/cms/galeria/create') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::createAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_create',  '_route' => 'admin_lm_cms_galeria_create',);
            }

            // admin_lm_cms_galeria_batch
            if ($pathinfo === '/admin/lm/cms/galeria/batch') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::batchAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_batch',  '_route' => 'admin_lm_cms_galeria_batch',);
            }

            // admin_lm_cms_galeria_edit
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeria') && preg_match('#^/admin/lm/cms/galeria/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::editAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_edit',)), array('_route' => 'admin_lm_cms_galeria_edit'));
            }

            // admin_lm_cms_galeria_delete
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeria') && preg_match('#^/admin/lm/cms/galeria/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::deleteAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_delete',)), array('_route' => 'admin_lm_cms_galeria_delete'));
            }

            // admin_lm_cms_galeria_show
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeria') && preg_match('#^/admin/lm/cms/galeria/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::showAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_show',)), array('_route' => 'admin_lm_cms_galeria_show'));
            }

            // admin_lm_cms_galeria_export
            if ($pathinfo === '/admin/lm/cms/galeria/export') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::exportAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_export',  '_route' => 'admin_lm_cms_galeria_export',);
            }

            // admin_lm_cms_galeria_edit_zdjecia
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeria') && preg_match('#^/admin/lm/cms/galeria/(?P<id>[^/]+?)/edit_zdjecia$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaAdminController::editZdjeciaAction',  '_sonata_admin' => 'lm.cms.admin.galeria',  '_sonata_name' => 'admin_lm_cms_galeria_edit_zdjecia',)), array('_route' => 'admin_lm_cms_galeria_edit_zdjecia'));
            }

            // admin_lm_cms_galeriazdjecie_list
            if ($pathinfo === '/admin/lm/cms/galeriazdjecie/list') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::listAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_list',  '_route' => 'admin_lm_cms_galeriazdjecie_list',);
            }

            // admin_lm_cms_galeriazdjecie_create
            if ($pathinfo === '/admin/lm/cms/galeriazdjecie/create') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::createAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_create',  '_route' => 'admin_lm_cms_galeriazdjecie_create',);
            }

            // admin_lm_cms_galeriazdjecie_batch
            if ($pathinfo === '/admin/lm/cms/galeriazdjecie/batch') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::batchAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_batch',  '_route' => 'admin_lm_cms_galeriazdjecie_batch',);
            }

            // admin_lm_cms_galeriazdjecie_edit
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeriazdjecie') && preg_match('#^/admin/lm/cms/galeriazdjecie/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::editAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_edit',)), array('_route' => 'admin_lm_cms_galeriazdjecie_edit'));
            }

            // admin_lm_cms_galeriazdjecie_delete
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeriazdjecie') && preg_match('#^/admin/lm/cms/galeriazdjecie/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::deleteAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_delete',)), array('_route' => 'admin_lm_cms_galeriazdjecie_delete'));
            }

            // admin_lm_cms_galeriazdjecie_show
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeriazdjecie') && preg_match('#^/admin/lm/cms/galeriazdjecie/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::showAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_show',)), array('_route' => 'admin_lm_cms_galeriazdjecie_show'));
            }

            // admin_lm_cms_galeriazdjecie_export
            if ($pathinfo === '/admin/lm/cms/galeriazdjecie/export') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::exportAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_export',  '_route' => 'admin_lm_cms_galeriazdjecie_export',);
            }

            // admin_lm_cms_galeriazdjecie_kadruj
            if (0 === strpos($pathinfo, '/admin/lm/cms/galeriazdjecie') && preg_match('#^/admin/lm/cms/galeriazdjecie/(?P<id>[^/]+?)/kadruj$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\GaleriaZdjecieAdminController::kadrujAction',  '_sonata_admin' => 'lm.cms.admin.galeria_zdjecie',  '_sonata_name' => 'admin_lm_cms_galeriazdjecie_kadruj',)), array('_route' => 'admin_lm_cms_galeriazdjecie_kadruj'));
            }

            // admin_lm_cms_zdjeciezmieniarki_list
            if ($pathinfo === '/admin/lm/cms/zdjeciezmieniarki/list') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::listAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_list',  '_route' => 'admin_lm_cms_zdjeciezmieniarki_list',);
            }

            // admin_lm_cms_zdjeciezmieniarki_create
            if ($pathinfo === '/admin/lm/cms/zdjeciezmieniarki/create') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::createAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_create',  '_route' => 'admin_lm_cms_zdjeciezmieniarki_create',);
            }

            // admin_lm_cms_zdjeciezmieniarki_batch
            if ($pathinfo === '/admin/lm/cms/zdjeciezmieniarki/batch') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::batchAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_batch',  '_route' => 'admin_lm_cms_zdjeciezmieniarki_batch',);
            }

            // admin_lm_cms_zdjeciezmieniarki_edit
            if (0 === strpos($pathinfo, '/admin/lm/cms/zdjeciezmieniarki') && preg_match('#^/admin/lm/cms/zdjeciezmieniarki/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::editAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_edit',)), array('_route' => 'admin_lm_cms_zdjeciezmieniarki_edit'));
            }

            // admin_lm_cms_zdjeciezmieniarki_delete
            if (0 === strpos($pathinfo, '/admin/lm/cms/zdjeciezmieniarki') && preg_match('#^/admin/lm/cms/zdjeciezmieniarki/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::deleteAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_delete',)), array('_route' => 'admin_lm_cms_zdjeciezmieniarki_delete'));
            }

            // admin_lm_cms_zdjeciezmieniarki_show
            if (0 === strpos($pathinfo, '/admin/lm/cms/zdjeciezmieniarki') && preg_match('#^/admin/lm/cms/zdjeciezmieniarki/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::showAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_show',)), array('_route' => 'admin_lm_cms_zdjeciezmieniarki_show'));
            }

            // admin_lm_cms_zdjeciezmieniarki_export
            if ($pathinfo === '/admin/lm/cms/zdjeciezmieniarki/export') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::exportAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_export',  '_route' => 'admin_lm_cms_zdjeciezmieniarki_export',);
            }

            // admin_lm_cms_zdjeciezmieniarki_kadruj
            if (0 === strpos($pathinfo, '/admin/lm/cms/zdjeciezmieniarki') && preg_match('#^/admin/lm/cms/zdjeciezmieniarki/(?P<id>[^/]+?)/kadruj$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::kadrujAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_kadruj',)), array('_route' => 'admin_lm_cms_zdjeciezmieniarki_kadruj'));
            }

            // admin_lm_cms_zdjeciezmieniarki_kadrujZapisz
            if (0 === strpos($pathinfo, '/admin/lm/cms/zdjeciezmieniarki') && preg_match('#^/admin/lm/cms/zdjeciezmieniarki/(?P<id>[^/]+?)/kadruj\\-zapisz$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\ZdjecieZmieniarkiAdminController::kadrujZapiszAction',  '_sonata_admin' => 'lm.cms.admin.zdjecie_zmieniarki',  '_sonata_name' => 'admin_lm_cms_zdjeciezmieniarki_kadrujZapisz',)), array('_route' => 'admin_lm_cms_zdjeciezmieniarki_kadrujZapisz'));
            }

            // admin_lm_cms_strona_list
            if ($pathinfo === '/admin/lm/cms/strona/list') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::listAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_list',  '_route' => 'admin_lm_cms_strona_list',);
            }

            // admin_lm_cms_strona_create
            if ($pathinfo === '/admin/lm/cms/strona/create') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::createAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_create',  '_route' => 'admin_lm_cms_strona_create',);
            }

            // admin_lm_cms_strona_batch
            if ($pathinfo === '/admin/lm/cms/strona/batch') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::batchAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_batch',  '_route' => 'admin_lm_cms_strona_batch',);
            }

            // admin_lm_cms_strona_edit
            if (0 === strpos($pathinfo, '/admin/lm/cms/strona') && preg_match('#^/admin/lm/cms/strona/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::editAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_edit',)), array('_route' => 'admin_lm_cms_strona_edit'));
            }

            // admin_lm_cms_strona_delete
            if (0 === strpos($pathinfo, '/admin/lm/cms/strona') && preg_match('#^/admin/lm/cms/strona/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::deleteAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_delete',)), array('_route' => 'admin_lm_cms_strona_delete'));
            }

            // admin_lm_cms_strona_show
            if (0 === strpos($pathinfo, '/admin/lm/cms/strona') && preg_match('#^/admin/lm/cms/strona/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::showAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_show',)), array('_route' => 'admin_lm_cms_strona_show'));
            }

            // admin_lm_cms_strona_export
            if ($pathinfo === '/admin/lm/cms/strona/export') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\StronaAdminController::exportAction',  '_sonata_admin' => 'lm.cms.admin.strona',  '_sonata_name' => 'admin_lm_cms_strona_export',  '_route' => 'admin_lm_cms_strona_export',);
            }

            // admin_lm_cms_aktualnosc_list
            if ($pathinfo === '/admin/lm/cms/aktualnosc/list') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::listAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_list',  '_route' => 'admin_lm_cms_aktualnosc_list',);
            }

            // admin_lm_cms_aktualnosc_create
            if ($pathinfo === '/admin/lm/cms/aktualnosc/create') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::createAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_create',  '_route' => 'admin_lm_cms_aktualnosc_create',);
            }

            // admin_lm_cms_aktualnosc_batch
            if ($pathinfo === '/admin/lm/cms/aktualnosc/batch') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::batchAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_batch',  '_route' => 'admin_lm_cms_aktualnosc_batch',);
            }

            // admin_lm_cms_aktualnosc_edit
            if (0 === strpos($pathinfo, '/admin/lm/cms/aktualnosc') && preg_match('#^/admin/lm/cms/aktualnosc/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::editAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_edit',)), array('_route' => 'admin_lm_cms_aktualnosc_edit'));
            }

            // admin_lm_cms_aktualnosc_delete
            if (0 === strpos($pathinfo, '/admin/lm/cms/aktualnosc') && preg_match('#^/admin/lm/cms/aktualnosc/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::deleteAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_delete',)), array('_route' => 'admin_lm_cms_aktualnosc_delete'));
            }

            // admin_lm_cms_aktualnosc_show
            if (0 === strpos($pathinfo, '/admin/lm/cms/aktualnosc') && preg_match('#^/admin/lm/cms/aktualnosc/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::showAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_show',)), array('_route' => 'admin_lm_cms_aktualnosc_show'));
            }

            // admin_lm_cms_aktualnosc_export
            if ($pathinfo === '/admin/lm/cms/aktualnosc/export') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\AktualnoscAdminController::exportAction',  '_sonata_admin' => 'lm.cms.admin.aktualnosc',  '_sonata_name' => 'admin_lm_cms_aktualnosc_export',  '_route' => 'admin_lm_cms_aktualnosc_export',);
            }

            // admin_lm_cms_employee_list
            if ($pathinfo === '/admin/lm/cms/employee/list') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::listAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_list',  '_route' => 'admin_lm_cms_employee_list',);
            }

            // admin_lm_cms_employee_create
            if ($pathinfo === '/admin/lm/cms/employee/create') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::createAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_create',  '_route' => 'admin_lm_cms_employee_create',);
            }

            // admin_lm_cms_employee_batch
            if ($pathinfo === '/admin/lm/cms/employee/batch') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::batchAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_batch',  '_route' => 'admin_lm_cms_employee_batch',);
            }

            // admin_lm_cms_employee_edit
            if (0 === strpos($pathinfo, '/admin/lm/cms/employee') && preg_match('#^/admin/lm/cms/employee/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::editAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_edit',)), array('_route' => 'admin_lm_cms_employee_edit'));
            }

            // admin_lm_cms_employee_delete
            if (0 === strpos($pathinfo, '/admin/lm/cms/employee') && preg_match('#^/admin/lm/cms/employee/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::deleteAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_delete',)), array('_route' => 'admin_lm_cms_employee_delete'));
            }

            // admin_lm_cms_employee_show
            if (0 === strpos($pathinfo, '/admin/lm/cms/employee') && preg_match('#^/admin/lm/cms/employee/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::showAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_show',)), array('_route' => 'admin_lm_cms_employee_show'));
            }

            // admin_lm_cms_employee_export
            if ($pathinfo === '/admin/lm/cms/employee/export') {
                return array (  '_controller' => 'Lm\\CmsBundle\\Controller\\Admin\\EmployeeAdminController::exportAction',  '_sonata_admin' => 'lm.cms.admin.employee',  '_sonata_name' => 'admin_lm_cms_employee_export',  '_route' => 'admin_lm_cms_employee_export',);
            }

            // admin_xd_cms_menuitem_list
            if ($pathinfo === '/admin/xd/cms/menuitem/list') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::listAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_list',  '_route' => 'admin_xd_cms_menuitem_list',);
            }

            // admin_xd_cms_menuitem_create
            if ($pathinfo === '/admin/xd/cms/menuitem/create') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::createAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_create',  '_route' => 'admin_xd_cms_menuitem_create',);
            }

            // admin_xd_cms_menuitem_batch
            if ($pathinfo === '/admin/xd/cms/menuitem/batch') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::batchAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_batch',  '_route' => 'admin_xd_cms_menuitem_batch',);
            }

            // admin_xd_cms_menuitem_edit
            if (0 === strpos($pathinfo, '/admin/xd/cms/menuitem') && preg_match('#^/admin/xd/cms/menuitem/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::editAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_edit',)), array('_route' => 'admin_xd_cms_menuitem_edit'));
            }

            // admin_xd_cms_menuitem_delete
            if (0 === strpos($pathinfo, '/admin/xd/cms/menuitem') && preg_match('#^/admin/xd/cms/menuitem/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::deleteAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_delete',)), array('_route' => 'admin_xd_cms_menuitem_delete'));
            }

            // admin_xd_cms_menuitem_show
            if (0 === strpos($pathinfo, '/admin/xd/cms/menuitem') && preg_match('#^/admin/xd/cms/menuitem/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::showAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_show',)), array('_route' => 'admin_xd_cms_menuitem_show'));
            }

            // admin_xd_cms_menuitem_export
            if ($pathinfo === '/admin/xd/cms/menuitem/export') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::exportAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_export',  '_route' => 'admin_xd_cms_menuitem_export',);
            }

            // admin_xd_cms_menuitem_saveorder
            if ($pathinfo === '/admin/xd/cms/menuitem/saveorder') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::saveorderAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_saveorder',  '_route' => 'admin_xd_cms_menuitem_saveorder',);
            }

            // admin_xd_cms_menuitem_routeparameters
            if ($pathinfo === '/admin/xd/cms/menuitem/routeparameters') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\MenuItemAdminController::routeparametersAction',  '_sonata_admin' => 'xd.cms.admin.menuitem',  '_sonata_name' => 'admin_xd_cms_menuitem_routeparameters',  '_route' => 'admin_xd_cms_menuitem_routeparameters',);
            }

            // admin_xd_cms_contactformmessage_list
            if ($pathinfo === '/admin/xd/cms/contactformmessage/list') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\ContactFormMessageAdminController::listAction',  '_sonata_admin' => 'xd.cms.admin.contactformmessage',  '_sonata_name' => 'admin_xd_cms_contactformmessage_list',  '_route' => 'admin_xd_cms_contactformmessage_list',);
            }

            // admin_xd_cms_contactformmessage_batch
            if ($pathinfo === '/admin/xd/cms/contactformmessage/batch') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\ContactFormMessageAdminController::batchAction',  '_sonata_admin' => 'xd.cms.admin.contactformmessage',  '_sonata_name' => 'admin_xd_cms_contactformmessage_batch',  '_route' => 'admin_xd_cms_contactformmessage_batch',);
            }

            // admin_xd_cms_contactformmessage_delete
            if (0 === strpos($pathinfo, '/admin/xd/cms/contactformmessage') && preg_match('#^/admin/xd/cms/contactformmessage/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\ContactFormMessageAdminController::deleteAction',  '_sonata_admin' => 'xd.cms.admin.contactformmessage',  '_sonata_name' => 'admin_xd_cms_contactformmessage_delete',)), array('_route' => 'admin_xd_cms_contactformmessage_delete'));
            }

            // admin_xd_cms_contactformmessage_show
            if (0 === strpos($pathinfo, '/admin/xd/cms/contactformmessage') && preg_match('#^/admin/xd/cms/contactformmessage/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\ContactFormMessageAdminController::showAction',  '_sonata_admin' => 'xd.cms.admin.contactformmessage',  '_sonata_name' => 'admin_xd_cms_contactformmessage_show',)), array('_route' => 'admin_xd_cms_contactformmessage_show'));
            }

            // admin_xd_cms_contactformmessage_export
            if ($pathinfo === '/admin/xd/cms/contactformmessage/export') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\ContactFormMessageAdminController::exportAction',  '_sonata_admin' => 'xd.cms.admin.contactformmessage',  '_sonata_name' => 'admin_xd_cms_contactformmessage_export',  '_route' => 'admin_xd_cms_contactformmessage_export',);
            }

            // admin_craue_config_setting_list
            if ($pathinfo === '/admin/craue/config/setting/list') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\SettingsAdminController::listAction',  '_sonata_admin' => 'xd.cms.admin.settings',  '_sonata_name' => 'admin_craue_config_setting_list',  '_route' => 'admin_craue_config_setting_list',);
            }

            // admin_craue_config_setting_create
            if ($pathinfo === '/admin/craue/config/setting/create') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\SettingsAdminController::createAction',  '_sonata_admin' => 'xd.cms.admin.settings',  '_sonata_name' => 'admin_craue_config_setting_create',  '_route' => 'admin_craue_config_setting_create',);
            }

            // admin_craue_config_setting_batch
            if ($pathinfo === '/admin/craue/config/setting/batch') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\SettingsAdminController::batchAction',  '_sonata_admin' => 'xd.cms.admin.settings',  '_sonata_name' => 'admin_craue_config_setting_batch',  '_route' => 'admin_craue_config_setting_batch',);
            }

            // admin_craue_config_setting_edit
            if (0 === strpos($pathinfo, '/admin/craue/config/setting') && preg_match('#^/admin/craue/config/setting/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'XD\\CmsBundle\\Controller\\SettingsAdminController::editAction',  '_sonata_admin' => 'xd.cms.admin.settings',  '_sonata_name' => 'admin_craue_config_setting_edit',)), array('_route' => 'admin_craue_config_setting_edit'));
            }

            // admin_craue_config_setting_export
            if ($pathinfo === '/admin/craue/config/setting/export') {
                return array (  '_controller' => 'XD\\CmsBundle\\Controller\\SettingsAdminController::exportAction',  '_sonata_admin' => 'xd.cms.admin.settings',  '_sonata_name' => 'admin_craue_config_setting_export',  '_route' => 'admin_craue_config_setting_export',);
            }

            // admin_xd_user_user_list
            if ($pathinfo === '/admin/xd/user/user/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_list',  '_route' => 'admin_xd_user_user_list',);
            }

            // admin_xd_user_user_create
            if ($pathinfo === '/admin/xd/user/user/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_create',  '_route' => 'admin_xd_user_user_create',);
            }

            // admin_xd_user_user_batch
            if ($pathinfo === '/admin/xd/user/user/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_batch',  '_route' => 'admin_xd_user_user_batch',);
            }

            // admin_xd_user_user_edit
            if (0 === strpos($pathinfo, '/admin/xd/user/user') && preg_match('#^/admin/xd/user/user/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_edit',)), array('_route' => 'admin_xd_user_user_edit'));
            }

            // admin_xd_user_user_delete
            if (0 === strpos($pathinfo, '/admin/xd/user/user') && preg_match('#^/admin/xd/user/user/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_delete',)), array('_route' => 'admin_xd_user_user_delete'));
            }

            // admin_xd_user_user_show
            if (0 === strpos($pathinfo, '/admin/xd/user/user') && preg_match('#^/admin/xd/user/user/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_show',)), array('_route' => 'admin_xd_user_user_show'));
            }

            // admin_xd_user_user_export
            if ($pathinfo === '/admin/xd/user/user/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_xd_user_user_export',  '_route' => 'admin_xd_user_user_export',);
            }

            // admin_xd_user_group_list
            if ($pathinfo === '/admin/xd/user/group/list') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_list',  '_route' => 'admin_xd_user_group_list',);
            }

            // admin_xd_user_group_create
            if ($pathinfo === '/admin/xd/user/group/create') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_create',  '_route' => 'admin_xd_user_group_create',);
            }

            // admin_xd_user_group_batch
            if ($pathinfo === '/admin/xd/user/group/batch') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_batch',  '_route' => 'admin_xd_user_group_batch',);
            }

            // admin_xd_user_group_edit
            if (0 === strpos($pathinfo, '/admin/xd/user/group') && preg_match('#^/admin/xd/user/group/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_edit',)), array('_route' => 'admin_xd_user_group_edit'));
            }

            // admin_xd_user_group_delete
            if (0 === strpos($pathinfo, '/admin/xd/user/group') && preg_match('#^/admin/xd/user/group/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_delete',)), array('_route' => 'admin_xd_user_group_delete'));
            }

            // admin_xd_user_group_show
            if (0 === strpos($pathinfo, '/admin/xd/user/group') && preg_match('#^/admin/xd/user/group/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_show',)), array('_route' => 'admin_xd_user_group_show'));
            }

            // admin_xd_user_group_export
            if ($pathinfo === '/admin/xd/user/group/export') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_xd_user_group_export',  '_route' => 'admin_xd_user_group_export',);
            }

            // craue_config_settings_modify
            if ($pathinfo === '/admin/settings_old') {
                return array (  '_controller' => 'Craue\\ConfigBundle\\Controller\\SettingsController::modifyAction',  '_route' => 'craue_config_settings_modify',);
            }

            // sonata_user_admin_security_login
            if ($pathinfo === '/admin/login') {
                return array (  '_controller' => 'XD\\UserBundle\\Controller\\AdminSecurityController::loginAction',  '_route' => 'sonata_user_admin_security_login',);
            }

            // sonata_user_admin_security_check
            if ($pathinfo === '/admin/login_check') {
                return array (  '_controller' => 'XD\\UserBundle\\Controller\\AdminSecurityController::checkAction',  '_route' => 'sonata_user_admin_security_check',);
            }

            // sonata_user_admin_security_logout
            if ($pathinfo === '/admin/logout') {
                return array (  '_controller' => 'XD\\UserBundle\\Controller\\AdminSecurityController::logoutAction',  '_route' => 'sonata_user_admin_security_logout',);
            }

            // sonata_admin_dashboard
            if ($pathinfo === '/admin/pulpit') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
            }

            // sonata_admin_dashboard_short
            if ($pathinfo === '/admin') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard_short',);
            }

            // sonata_admin_dashboard_short2
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonata_admin_dashboard_short2');
                }
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard_short2',);
            }

        }

        // _imagine_thumb1
        if (0 === strpos($pathinfo, '/cache/thumb1') && preg_match('#^/cache/thumb1/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_thumb1;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'thumb1',)), array('_route' => '_imagine_thumb1'));
        }
        not__imagine_thumb1:

        // _imagine_thumb_admin
        if (0 === strpos($pathinfo, '/cache/thumb_admin') && preg_match('#^/cache/thumb_admin/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_thumb_admin;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'thumb_admin',)), array('_route' => '_imagine_thumb_admin'));
        }
        not__imagine_thumb_admin:

        // _imagine_thumb_admin_small
        if (0 === strpos($pathinfo, '/cache/thumb_admin_small') && preg_match('#^/cache/thumb_admin_small/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_thumb_admin_small;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'thumb_admin_small',)), array('_route' => '_imagine_thumb_admin_small'));
        }
        not__imagine_thumb_admin_small:

        // _imagine_galeria_thumbnail
        if (0 === strpos($pathinfo, '/cache/galeria_thumbnail') && preg_match('#^/cache/galeria_thumbnail/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_galeria_thumbnail;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'galeria_thumbnail',)), array('_route' => '_imagine_galeria_thumbnail'));
        }
        not__imagine_galeria_thumbnail:

        // _imagine_page_thumbnail
        if (0 === strpos($pathinfo, '/cache/page_thumbnail') && preg_match('#^/cache/page_thumbnail/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_page_thumbnail;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'page_thumbnail',)), array('_route' => '_imagine_page_thumbnail'));
        }
        not__imagine_page_thumbnail:

        // _imagine_zmieniarka
        if (0 === strpos($pathinfo, '/cache/zmieniarka') && preg_match('#^/cache/zmieniarka/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_zmieniarka;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'zmieniarka',)), array('_route' => '_imagine_zmieniarka'));
        }
        not__imagine_zmieniarka:

        // _imagine_aktualnosc_foto
        if (0 === strpos($pathinfo, '/cache/aktualnosc_foto') && preg_match('#^/cache/aktualnosc_foto/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_aktualnosc_foto;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'aktualnosc_foto',)), array('_route' => '_imagine_aktualnosc_foto'));
        }
        not__imagine_aktualnosc_foto:

        // _imagine_podstrona_foto
        if (0 === strpos($pathinfo, '/cache/podstrona_foto') && preg_match('#^/cache/podstrona_foto/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_podstrona_foto;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'podstrona_foto',)), array('_route' => '_imagine_podstrona_foto'));
        }
        not__imagine_podstrona_foto:

        // _imagine_employee_index
        if (0 === strpos($pathinfo, '/cache/employee_index') && preg_match('#^/cache/employee_index/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_employee_index;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'liip_imagine.controller:filterAction',  'filter' => 'employee_index',)), array('_route' => '_imagine_employee_index'));
        }
        not__imagine_employee_index:

        // _internal
        if (preg_match('#^/_internal/(?P<controller>[^/]+?)/(?P<path>.+)\\.(?P<_format>[^.]+)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\InternalController::indexAction',)), array('_route' => '_internal'));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
