-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 20 Sty 2024, 11:27
-- Wersja serwera: 10.1.48-MariaDB
-- Wersja PHP: 7.2.24-0ubuntu0.18.04.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_10`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Aktualnosc`
--

CREATE TABLE `Aktualnosc` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `naStronieGlownej` tinyint(1) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publishDate` datetime DEFAULT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` longtext,
  `automaticSeo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Aktualnosc`
--

INSERT INTO `Aktualnosc` (`id`, `galeria_id`, `obrazek`, `updated_at`, `naStronieGlownej`, `title`, `published`, `publishDate`, `keywords`, `description`, `slug`, `content`, `automaticSeo`) VALUES
(1, NULL, NULL, NULL, 1, 'Nowa strona internetowa Cosmodent\'u', 1, '2013-02-11 23:10:00', 'Nowa, strona, Cosmodent,  	Tupacsum, Ipsum, nearly, gave, life,, raise, right.', 'Jest nam niezmierniło miło powitać Państwa na naszej stronie WWW. Serdecznie zapraszamy do naszego gabinetu w Łańcucie. \r\n\r\nMamy nadzieję, że dzięki nam będą się Państwo częściej uśmiechać :)', 'nowa-strona-cosmodent', '<p>\r\n	Jest nam<strong> niezmiernie miło </strong>powitać Państwa na naszej <span class=\"pink\">stronie WWW</span>. Serdecznie zapraszamy do naszego gabinetu w Łańcucie.</p>\r\n<p>\r\n	<span class=\"link\">Mamy nadzieję, że dzięki nam będą się Państwo częściej uśmiechać :)</span></p>', 0),
(2, NULL, NULL, NULL, 1, 'Gabinet w Rzeszowie już otwarty', 1, '2013-10-24 15:50:00', 'Gabinet, Rzeszowie, wkrótce, otwarty, Już, listopada, będą, mogli, Państwo, leczyć', 'Już od 10 listopada mogą Państwo leczyć swoje zęby w nowym, rzeszowskim gabinecie. Wszystkich bardzo serdecznie zapraszamy na ul. Strażacką 12d/2 (1. piętro).', 'gabinet-w-rzeszowie-wkrotce-otwarty', '<p>\r\n	Już od <strong>10 listopada </strong>mogą Państwo leczyć swoje zęby w nowym, rzeszowskim gabinecie. Wszystkich bardzo serdecznie zapraszamy na<strong> ul. Strażacką 12d/2 (1. piętro).</strong></p>\r\n<p>\r\n	<span class=\"pink\">Warto nas odwiedzić dla pięknego uśmiechu :).</span></p>', 0),
(3, 7, NULL, NULL, 1, 'Uroczyste otwarcie gabinetu na Strażackiej 12d', 1, '2013-12-30 22:10:00', 'Uroczyste, otwarcie, gabinetu, Strażackiej, dniu, 14.12.2013, odbyło, się, uroczyste, Cosmodent', 'W dniu 14.12.2013 r. odbyło się uroczyste otwarcie gabinetu Cosmodent w Rzeszowie...', 'uroczyste-otwarcie-gabinetu-na-strazackiej-12d', '<p>\r\n	W dniu 14.12.2013 r. odbyło się uroczyste otwarcie gabinetu Cosmodent w Rzeszowie. Imprezę można z czystym sumieniem zaliczyć do udanych, zaproszeni goście bawili się do p&oacute;źnych godzin nocnych :)</p>', 0),
(4, NULL, '52c6faa34a68a.jpg', NULL, 1, 'Rozpocznij Nowy Rok z pięknym uśmiechem', 1, '2014-01-03 18:58:00', 'Rozpocznij, Nowy, pięknym, uśmiechem, 2014r', 'Rok 2014r. zaczynamy od NOWOROCZNEJ PROMOCJI, w ramach której pragniemy zaoferować Państwu wspaniały, śnieżnobiały uśmiech :)', 'rozpocznij-nowy-rok-z-pieknym-usmiechem', '<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<b>Rok 2014r.</b>&nbsp;zaczynamy od&nbsp;<span class=\"pink\">NOWOROCZNEJ PROMOCJI</span>, w ramach kt&oacute;rej pragniemy zaoferować Państwu&nbsp;<strong>wspaniały, śnieżnobiały uśmiech :)</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		<strong>PRZY WYBIELANIU</strong>&nbsp;- usuwanie kamienia i piaskowanie gratis</li>\r\n	<li>\r\n		<strong>BEZ WYBIELANIA</strong>&nbsp;- usuwanie kamienia i piaskowanie za 50% ceny</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	<span class=\"link\">Zapraszamy do naszej, rzeszowskiej plac&oacute;wki przy ul. Strażackiej 12d/2 (1. piętro) na bezpłatny przegląd.</span></p>\r\n<p style=\"text-align: center;\">\r\n	tel.&nbsp;<strong>698 902 233</strong></p>', 0),
(5, NULL, '530cb9ca45b7b.jpg', NULL, 1, 'Konsultacje ortodontyczne', 1, '2014-02-25 16:36:00', 'Konsultacje, ortodontyczne, Serdecznie, Państwa, zapraszamy, konsultacje, ortodontyczne,, zar', 'Serdecznie Państwa zapraszamy na konsultacje ortodontyczne, zarówno dla dzieci jak i dorosłych.', 'konsultacje-ortodontyczne', '<p>\r\n	<strong>Serdecznie Państwa zapraszamy na konsultacje ortodontyczne, zar&oacute;wno dla dzieci jak i dorosłych.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Zadzwoń i zapisz się już teraz!</strong></p>\r\n<h2 class=\"title1\">\r\n	tel. 698 902 233</h2>', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ContactFormMessage`
--

CREATE TABLE `ContactFormMessage` (
  `id` int(11) NOT NULL,
  `senderName` varchar(255) NOT NULL,
  `senderEmail` varchar(255) NOT NULL,
  `senderMessage` longtext NOT NULL,
  `sentAt` datetime NOT NULL,
  `senderIP` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ContactFormMessage`
--

INSERT INTO `ContactFormMessage` (`id`, `senderName`, `senderEmail`, `senderMessage`, `sentAt`, `senderIP`) VALUES
(5, 'Marcin W', 'marcin@lemonadestudio.pl', '<h3>Wiadomość z formularza:</h3>TEST <hr /><h3>Pozostałe dane:</h3>Imię: Marcin<br />Nazwisko: W<br />E-mail: marcin@lemonadestudio.pl<br />Telefon: 609212060<br />', '2013-02-18 21:48:42', '91.225.135.254'),
(6, 'magdalena dubiel', 'mada.db@interia.pl', '<h3>Wiadomość z formularza:</h3>witam\n\nchciałabym umówić się na wizytę kontrolną zębów po ciąży np w sobotę do południa, jaki bedzie to koszt \n\npozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: magdalena<br />Nazwisko: dubiel<br />E-mail: mada.db@interia.pl<br />Telefon: 512290456<br />', '2013-05-08 19:54:55', '89.230.88.99'),
(7, 'Roksana Glab', 'roksanaglab@gmail.com', '<h3>Wiadomość z formularza:</h3>witam, chyba mam paradontoze, czy mozna to wyleczyc w jeden dzien ? i dodatkowo wyrwac 2 zeby i zalozyc korony tymczasowe? ile by to wszystko kosztowalo mniej wiecej ? czy moglabym sie umowic na 12 czerwca? <hr /><h3>Pozostałe dane:</h3>Imię: Roksana<br />Nazwisko: Glab<br />E-mail: roksanaglab@gmail.com<br />Telefon: 781104330<br />', '2013-05-20 22:48:50', '94.231.249.237'),
(8, 'Edyta Rakuś', 'edi18091992@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry. Chciałabym zapytać o cennik Pani usług stomatologicznych. <hr /><h3>Pozostałe dane:</h3>Imię: Edyta<br />Nazwisko: Rakuś<br />E-mail: edi18091992@wp.pl<br />Telefon: 886022138<br />', '2013-09-17 07:03:39', '91.150.173.2'),
(9, 'Ewa Szpunar', 'eszpunar@onet.pl', '<h3>Wiadomość z formularza:</h3>chciałabym zapisać moją córkę tj.Kinga Szpunar na wizytę. <hr /><h3>Pozostałe dane:</h3>Imię: Ewa<br />Nazwisko: Szpunar<br />E-mail: eszpunar@onet.pl<br />Telefon: 698790345<br />', '2013-10-21 14:38:25', '83.6.107.98'),
(10, 'Agata Kobylarz', 'agata1188@op.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nczy mogłabym uzyskać cennik Państwa usług?\nBędę wdzięczna.\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Agata<br />Nazwisko: Kobylarz<br />E-mail: agata1188@op.pl<br />Telefon: 785976287<br />', '2013-11-18 21:16:00', '91.223.64.254'),
(11, 'ewa kot', 'ewa.oliwia7@wp.pl', '<h3>Wiadomość z formularza:</h3>dzien dobry ja mam takie pytanie bo dzisiaj rano wypadla mi plaba z zeba z 2 u gory ja juz mialam go kanalowo leczonego i chcialam sie zapytac czy wystarczy tylko plabe zalozyc i ile kosztuje taka plaba <hr /><h3>Pozostałe dane:</h3>Imię: ewa<br />Nazwisko: kot<br />E-mail: ewa.oliwia7@wp.pl<br />Telefon: 531344592<br />', '2013-11-29 08:26:05', '31.130.100.124'),
(12, 'Mateusz Gębski', 'gebski.mateusz@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\n\nchciałbym zapytać, ile kosztuje u Państwa wizyta kontrolna? <hr /><h3>Pozostałe dane:</h3>Imię: Mateusz<br />Nazwisko: Gębski<br />E-mail: gebski.mateusz@gmail.com<br />Telefon: 788855516<br />', '2013-12-01 17:58:29', '83.6.93.46'),
(13, 'dorota woźniak', 'jagodad5@tlen.pl', '<h3>Wiadomość z formularza:</h3>proszę o  info  czy  gabinet  jest czynny w soboty  i w jakich godzinach? <hr /><h3>Pozostałe dane:</h3>Imię: dorota<br />Nazwisko: woźniak<br />E-mail: jagodad5@tlen.pl<br />Telefon: 501519630<br />', '2013-12-06 21:18:27', '185.26.182.36'),
(14, 'Beata Gil', 'gilbeata1@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nChciałabym  usunąć w bezpieczny sposób (obawiam się oparów rtęći przy usuwaniu) wypełnienia amalgamatowe które posiadam w kilku zębach. Czy jest to u Państwa możliwe? <hr /><h3>Pozostałe dane:</h3>Imię: Beata<br />Nazwisko: Gil<br />E-mail: gilbeata1@gmail.com<br />Telefon: 518326147<br />', '2013-12-15 21:40:41', '91.150.184.98'),
(15, 'Monika Kubasik', 'monikak_z@poczta.onet.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,   czy macie w swojej ofercie i czy wykonujecie Mosty Adhezyjne na włóknach szklanych ? Mam taki mostek (górna jedynka) już od ok 8 lat (wykonywane w Katowicach) i chciałabym aby zajęła się tym wyspecjalizowana osoba, tzn chcę sprawdzić czy wszystko jest ok i czy trzeba zrobić ewentualnie jaką poprawkę lub delikatne czyszczenie po tylu latach,   czy możecie państwo podać cenę takiego mostu oraz czas wykonania ?  z góry uprzejmie dziękuję za odpowiedź. <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Kubasik<br />E-mail: monikak_z@poczta.onet.pl<br />Telefon: 663539099<br />', '2013-12-30 17:06:35', '31.128.8.17'),
(16, 'Monika Serafin', 'm.serafin.revision@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam interesuje mnie wizyta dnia 3 .01.2013 , najlepiej w przedziale godzin 12:00-20:00.\n Celem wizyty jest uzupeninie ubytku , w górnej jedynce -to pilne ponieważ w dniu dzsiejszym ukruszył mi się ząb.\nJednoczesnie konsultacja w celu okreslenia planu leczenia, implantow i licówek mojego uzębienia.\nPozdrawiam i czekam na kontakt. \nMonika Serafin <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Serafin<br />E-mail: m.serafin.revision@gmail.com<br />Telefon: 534753000<br />', '2014-01-01 10:10:41', '91.223.64.254'),
(17, 'Monika Serafin', 'm.serafin.revision@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam Pani dr. \nDziekuję za dzisiejszą wizytę , jednocześnie pomyślałam o wybieleniu zębów skoro mamy 11.01.2014 poprawiać ich kształt materiałem stomatologicznym, wtedy wmieniłybyśmy stare żółte pląby na bielsze. Czy jest możliwość wybielenia zębów przed 11.01.2014  . Z mojej strony moge się dostosować do kazdej godziny ewentualnie wyjdę z pracy na tą wizytę  .Pozdrawiam Monika Serafin <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Serafin<br />E-mail: m.serafin.revision@gmail.com<br />Telefon: 534753000<br />', '2014-01-03 18:37:37', '91.223.64.254'),
(18, 'kinga jakiela', 'dynastia@op.pl', '<h3>Wiadomość z formularza:</h3>witam ,\njaki bylby koszt i czy bylo by miejsce dzisiaj po 15 dla dziecka  z bolem zęba ?na ul;Strazackiej <hr /><h3>Pozostałe dane:</h3>Imię: kinga<br />Nazwisko: jakiela<br />E-mail: dynastia@op.pl<br />Telefon: 600921351<br />', '2014-01-10 09:44:23', '83.15.2.2'),
(19, 'Kamil Rowicki', 'rowikus_13@o2.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, ja chciałbym się zapytać ile by kosztowało wyrwanie zęba stałego (górna 4 lub 5) z nieczuleniem. <hr /><h3>Pozostałe dane:</h3>Imię: Kamil<br />Nazwisko: Rowicki<br />E-mail: rowikus_13@o2.pl<br />Telefon: brak<br />', '2014-01-19 05:18:35', '80.48.180.40'),
(20, 'Karolina Kozioł', 'karolinakoziol87@tlen.pl', '<h3>Wiadomość z formularza:</h3>Witam, \nna górze chciałabym mieć ładne zęby i zastanawiam się czy założyć implanty czy mogą być korony myślałam o sześciu sztukach pytanie brzmi jaki byłby koszt ewentualny koszt założenia implantów lub koron.\npozdrawiam\n Karolina <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Kozioł<br />E-mail: karolinakoziol87@tlen.pl<br />Telefon: 605379823<br />', '2014-01-27 10:23:53', '31.6.162.106'),
(21, 'Urszula Adamczyk', 'urszuladamczyk@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam, chciałam zapytać czy przyjmują Państwo na NFZ, jeśli nie to jaki jest koszt wizyty. Dziękuje. Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Urszula<br />Nazwisko: Adamczyk<br />E-mail: urszuladamczyk@o2.pl<br />Telefon: 889563124<br />', '2014-02-04 10:43:31', '37.72.122.133'),
(22, 'Joanna Gargas-Jaźwiecka', 'joanna.gargas.jazwiecka@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam serdecznie,\n\nchciałabym się zapoznać z Państwa ofertą dotyczącą leczenia ortodontycznego. Uprzejmie proszę o przesłanie mi na adres poczty elektronicznej cennika usług. Nie ukrywam, iż leczenie ortodontyczne jest dla mnie sporym wydatkiem, dlatego chciałabym porównać oferty  na rzeszowskim rynku.\n\npozdrawiam \nJoanna Gargas-Jażwiecka <hr /><h3>Pozostałe dane:</h3>Imię: Joanna<br />Nazwisko: Gargas-Jaźwiecka<br />E-mail: joanna.gargas.jazwiecka@gmail.com<br />Telefon: 609653005<br />', '2014-02-07 18:49:33', '91.224.193.5'),
(23, 'sylwia lecznar', 'sylwia.83@onet.eu', '<h3>Wiadomość z formularza:</h3>dzien dobry\nczy jest mozliwosc u państwa uśpienie całkowite?i jaka jest cena? <hr /><h3>Pozostałe dane:</h3>Imię: sylwia<br />Nazwisko: lecznar<br />E-mail: sylwia.83@onet.eu<br />Telefon: 172247187<br />', '2014-02-08 13:52:31', '83.9.112.146'),
(24, 'Krzysztof K', 'kkuzry@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam. Mam wadę zgryzu i jest to prawdopodobnie przodozgryz. Ciekaw jestem ile kosztuje pierwsza wizyta, konsultacja? <hr /><h3>Pozostałe dane:</h3>Imię: Krzysztof<br />Nazwisko: K<br />E-mail: kkuzry@gmail.com<br />Telefon: brak<br />', '2014-02-24 18:37:29', '109.196.121.121'),
(25, 'Anna Pelinko-Data', 'anna.pelinko@wp.p', '<h3>Wiadomość z formularza:</h3>Witam jaki jest koszt koszt piaskowania oraz koszt plomby światło utwardzalnej . Pozdrawiam . <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Pelinko-Data<br />E-mail: anna.pelinko@wp.p<br />Telefon: 178654143<br />', '2014-03-11 14:42:15', '84.38.162.253'),
(26, 'Magdalena Maciołek', 'emmaciolek@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam , czy poszukujecie \npaństwo lekarzy dentystów do pracy w Waszej klinice? <hr /><h3>Pozostałe dane:</h3>Imię: Magdalena<br />Nazwisko: Maciołek<br />E-mail: emmaciolek@wp.pl<br />Telefon: 781834661<br />', '2014-03-20 11:48:06', '88.156.161.177'),
(27, 'Klaudia Potoczek', 'kareeena_18@hotmail.com', '<h3>Wiadomość z formularza:</h3>Witam, interesuje mnie wybielanie zębów .Jaki byłby koszt ? pozdrawiam. <hr /><h3>Pozostałe dane:</h3>Imię: Klaudia<br />Nazwisko: Potoczek<br />E-mail: kareeena_18@hotmail.com<br />Telefon: 505204845<br />', '2014-03-20 12:52:45', '62.133.139.180'),
(28, 'Joanna siemieniak', 'joanna.siemi@libero.it', '<h3>Wiadomość z formularza:</h3>Witam, prosze o informacje ile kosztuje aparat lingwalny. Dziekuje i pozdrawiam. Joanna siemieniak <hr /><h3>Pozostałe dane:</h3>Imię: Joanna<br />Nazwisko: siemieniak<br />E-mail: joanna.siemi@libero.it<br />Telefon: 000000000000<br />', '2014-03-22 16:47:10', '151.70.22.1'),
(29, 'Piotr Pytlak', 'bumpp@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam i proszę o odpowiedź. Mam 51 lat, zgryz krzyżowy. Obecnie utraciłem zęby tronowe z każdej strony 6,7,8 na skutek wysunięcia się ich, problemów z przyzębiem i z degradacją kości. Planuję uzupełnić braki implantami tj 6 i 7 - razem 8 sztuk. Ale być może wcześniej konieczna jest korekcja zgryzu. Moje pytanie tego właśnie dotyczy - czy wcześniej w takiej sytuacji wykonuje się implanty czy raczej korekcję zgryzu ? Proszę o odpowiedź która pozwoliłaby mi wyrobić sobie zdanie. Wolałbym najpierw wykonać implanty ale być może gdy planuje się korekcję zgryzu należy wcześniej właśnie wykonać korekcję, z tym że ona na pewno jest leczeniem długotrwałym które odsunie termin założenia implantów na czym mi szczególnie zależy. Mieszkam w okolicach Rzeszowa i rozglądam się za wykonawcą w/w prac. Dziękuję i pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Piotr<br />Nazwisko: Pytlak<br />E-mail: bumpp@wp.pl<br />Telefon: 601499430<br />', '2014-03-26 07:37:05', '77.255.226.61'),
(30, 'Katarzyna Hylczuk', 'katarzynahylczuk@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nMam pytanie czy można umówić się na konsultacje ortodontyczną? i jaki byłby termin i koszt takiej konsultacji ? <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Hylczuk<br />E-mail: katarzynahylczuk@gmail.com<br />Telefon: 507069128<br />', '2014-05-14 16:52:30', '212.160.172.87'),
(31, 'Juliusz Sieczkowski', 'jsi@interia.eu', '<h3>Wiadomość z formularza:</h3>W wyniku zaniedbań w młodości mam znaczne i postępujące braki w uzębieniu. Lekarz, z którego pomocy korzystam proponuje mi protezy i twierdzi, że inplanty nie są możliwe w moim przypadku. Dokąd braki uzębienia ograniczały się do kilku zębów trzonowych nie odczuwałem dyskomfortu, bo mostki i protezki szybko zaakceptowałem, teraz niestety rozpadają mi się zęby przednie i tu jest problem. Dlatego chciałbym skonsultować się z innym lekarzem stomatologiem. <hr /><h3>Pozostałe dane:</h3>Imię: Juliusz<br />Nazwisko: Sieczkowski<br />E-mail: jsi@interia.eu<br />Telefon: 603750025<br />', '2014-05-19 20:27:43', '159.205.189.93'),
(32, 'Agnieszka Niedzielska', 'agnieszka.niedzielska2@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\n\nChciałabym zapytać o możliwość podjęcia leczenia zębów - być może 2 kanałowe. ok 10-15 zębów.\n\nJaki byłby przybliżony koszt leczenia oraz najszybszy termin rozpoczęcia leczenia w godz. od. 17.00\n\nBędę wdzięczna za szybką odpowiedź.\n\nPozdrawiam,\n\nAgnieszka Niedzielska <hr /><h3>Pozostałe dane:</h3>Imię: Agnieszka<br />Nazwisko: Niedzielska<br />E-mail: agnieszka.niedzielska2@gmail.com<br />Telefon: 783471491<br />', '2014-06-03 10:42:17', '80.52.135.9'),
(33, 'Karina Kopeć', 'karina.kopec@op.pl', '<h3>Wiadomość z formularza:</h3>Proszę o zapisanie mojego dziecka -Róży Kopeć na bezpłatne konsultacje ortodontyczne. <hr /><h3>Pozostałe dane:</h3>Imię: Karina<br />Nazwisko: Kopeć<br />E-mail: karina.kopec@op.pl<br />Telefon: 512-220-588<br />', '2014-06-16 14:36:01', '178.43.36.195'),
(34, 'Magdalena Mazur', 'magfdaqueen@op.pl', '<h3>Wiadomość z formularza:</h3>Witam, może najpierw opowiem o mojej dolegliwości. 15 lat temu wyleczyłam kanałowo zęba piątkę prawą górną. Z 10 lat temu pękła mi korona tegoż zęba od strony wewnętrznej, dentysta zacementował mi w kanale kawałek metalu i odbudował utraconą częśc zęba. W tym roku straciłam zewnętrzną koronę zęba i znowu została odbudowana tymczasowo chyba za pomocą światłoutwardzacza. MOje pytanie brzmi ile będzie kosztowała obudowa całości zęba trwalszą koroną np porcelanową i ile wizyt potrzebuje? dziekuję <hr /><h3>Pozostałe dane:</h3>Imię: Magdalena<br />Nazwisko: Mazur<br />E-mail: magfdaqueen@op.pl<br />Telefon: nie mam<br />', '2014-07-22 18:23:10', '95.45.75.62'),
(35, 'Monika Serafin', 'm.serafin.revision@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam Pani dr .\nPilnie zalezy mi na wizycie w Pani gabinecie poniedzialek/ wtorek z powdu ukruszenia zęba 2 .Jestem dostepna w poniedziałek po 12:00. \nProszę o informację o wolnym terminie.\nPozdrawiam M.Serafin <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Serafin<br />E-mail: m.serafin.revision@gmail.com<br />Telefon: 534753000<br />', '2014-08-02 16:04:06', '91.223.64.254'),
(36, 'KatarZyna Wais', 'kasiawais@icloud.com', '<h3>Wiadomość z formularza:</h3>Dzien dobry. Bardzo pilnie potrzebuje wizyty u dentysty- prawdopodobnie leczenie kanalowe. Plomba mi wypadla i zab bardzo boli. Czy jest mozliwosc wizyty jutro w poniedzialek pomiedzy 12 a 2a? Prosze o potwierdzenia droga mailowa. Z powazaniem. Katarzyna Wais <hr /><h3>Pozostałe dane:</h3>Imię: KatarZyna<br />Nazwisko: Wais<br />E-mail: kasiawais@icloud.com<br />Telefon: 502678508<br />', '2014-08-10 13:47:51', '94.197.121.62'),
(37, 'Konrad Nanaszko', 'knanaszko@vp.pl', '<h3>Wiadomość z formularza:</h3>Witam od 3 dni mam straszny bol zeba, bez tabletek ciezko.. czy bylaby mozliwosc wizyty w niedziele ? Pozdrawiam i prosze o kontakt <hr /><h3>Pozostałe dane:</h3>Imię: Konrad<br />Nazwisko: Nanaszko<br />E-mail: knanaszko@vp.pl<br />Telefon: 724632912<br />', '2014-09-20 23:18:06', '178.43.80.143'),
(38, 'Marta czachor', 'cmarta@wp.pl', '<h3>Wiadomość z formularza:</h3>dzień dobry chce wizyte <hr /><h3>Pozostałe dane:</h3>Imię: Marta<br />Nazwisko: czachor<br />E-mail: cmarta@wp.pl<br />Telefon: 608 479 721<br />', '2014-10-27 13:25:09', '91.225.132.65'),
(39, 'Elżbieta M.', 'emarkowska@onet.eu', '<h3>Wiadomość z formularza:</h3>Witam, czy istnieje możliwość zapisania sie na bezpłatne konsultacje w dniu 10.11.2014 r.?\n\nBędę wdzięczna za informację, pozdrawiam. <hr /><h3>Pozostałe dane:</h3>Imię: Elżbieta<br />Nazwisko: M.<br />E-mail: emarkowska@onet.eu<br />Telefon: 787582294<br />', '2014-11-05 22:42:29', '91.189.219.147'),
(40, 'E. Markowska', 'emarkowska@onet.eu', '<h3>Wiadomość z formularza:</h3>Witam,\n\nCzy jest możliwość zapisania się na bezpłatną konsultację w dniu 10.11.2014 r.?\n\nZ góry dziękuję za odpowiedź, pozdrawiam. <hr /><h3>Pozostałe dane:</h3>Imię: E.<br />Nazwisko: Markowska<br />E-mail: emarkowska@onet.eu<br />Telefon: 787 582 294<br />', '2014-11-05 22:55:47', '91.189.219.147'),
(41, 'Joanna Szenk', 'joana_19@o2.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nMoją siedmioletnią córkę boli ząb. W związku z tym chciałabym zapytać kiedy można by było z nią przyjść do gabinetu. <hr /><h3>Pozostałe dane:</h3>Imię: Joanna<br />Nazwisko: Szenk<br />E-mail: joana_19@o2.pl<br />Telefon: 600828445<br />', '2014-12-06 16:13:16', '91.225.135.12'),
(42, 'Malgorzata Kowal', 'gosiakowal17@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry\n Proszę o przesłanie cennika Państwa usług. <hr /><h3>Pozostałe dane:</h3>Imię: Malgorzata<br />Nazwisko: Kowal<br />E-mail: gosiakowal17@gmail.com<br />Telefon: 000000<br />', '2015-01-15 18:05:19', '193.19.165.24'),
(43, 'Krzysztof Zdrada', 'krzysztof.zdrada1@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam\nChciałbym sprawdzić sobie zęby mam mała opuchliznę na dziąśle. <hr /><h3>Pozostałe dane:</h3>Imię: Krzysztof<br />Nazwisko: Zdrada<br />E-mail: krzysztof.zdrada1@gmail.com<br />Telefon: 798424054<br />', '2015-02-09 08:46:42', '193.19.165.38'),
(44, 'Marta Kusz', 'marta.kusz@gmail.com', '<h3>Wiadomość z formularza:</h3>Jestem zainteresowana założeniem 2 implantów. Chciałam zorientować się co do możliwości i ceny.\nPozdrawiam. M.Kusz <hr /><h3>Pozostałe dane:</h3>Imię: Marta<br />Nazwisko: Kusz<br />E-mail: marta.kusz@gmail.com<br />Telefon: 501-045-576<br />', '2015-03-24 13:27:46', '188.191.200.90'),
(45, 'Grazyna Lemecha', 'grazyna82@poczta.fm', '<h3>Wiadomość z formularza:</h3>Witam,\nprosze o informacje czy za leczenie ortodontyczne i implanty mozna placic w ratach. Dziekuje <hr /><h3>Pozostałe dane:</h3>Imię: Grazyna<br />Nazwisko: Lemecha<br />E-mail: grazyna82@poczta.fm<br />Telefon: 694722367<br />', '2015-04-11 09:43:56', '62.133.130.167'),
(46, 'Polacy w Skandynawii, Niemczech, Austrii, Anglii zapytanie', 'info@wirtualnaskandynawia.pl', '<h3>Wiadomość z formularza:</h3>Witamy.\n\nZwracamy się z pytaniem, czy możemy przesłać Państwu informacje o:\n\nPublikacji materiałów reklamowych na łamach naszych serwisów informacyjnych w całości poświęconych Skandynawii, Niemczech, Austrii, Anglii. Czytelnikami serwisów są Polacy żyjący za granicą jak i chcący wyjechać do wyżej wymienionych państw. Czytelnikami są również rodowici mieszkańcy tych że państw, którzy chcą zaznajomić się z polską mentalnością, kulturą, historią oraz planujących odwiedzić Polskę.\n\nNiniejsze zapytanie nie jest ofertą handlową, a jedynie zapytaniem o zgodę na przesłanie oferty handlowej drogą elektroniczną zgodnie z art. 10 ustawy z dnia 18 lipca 2002r. o świadczeniu usług drogą elektroniczną (Dz.U. z 2002r. Nr 144, poz 1204 z późn. zm.).\n\nJeżeli wyrażają Państwo zgodę proszę o odpowiedź na niniejszego maila.\n\nZ poważaniem zespół Polacy w Skandynawii, Niemczech, Austrii, Anglii <hr /><h3>Pozostałe dane:</h3>Imię: Polacy w Skandynawii, Niemczech, Austrii, Anglii<br />Nazwisko: zapytanie<br />E-mail: info@wirtualnaskandynawia.pl<br />Telefon: 607895631<br />', '2015-04-28 18:06:13', '89.65.230.60'),
(47, 'Beata Drążek', 'beatadrazek4@wp.pl', '<h3>Wiadomość z formularza:</h3>Szanowni Państwo!\n\nStudiuję filologię polską na Uniwersytecie Rzeszowski. Obecnie piszę pracę magisterską na temat nazw gabinetów stomatologicznych w województwie podkarpackim. Proszę o informację skąd pomysł na nazwę Państwa gabinetu. Bardzo zależy mi na odpowiedzi, ponieważ chciałabym, żeby moja praca miała jak najwięcej materiału badawczego.\n\nZ pozdrowieniami\nBeata Drążek <hr /><h3>Pozostałe dane:</h3>Imię: Beata<br />Nazwisko: Drążek<br />E-mail: beatadrazek4@wp.pl<br />Telefon: 534041653<br />', '2015-05-11 20:14:04', '88.199.108.128'),
(48, 'Sylwia Rzeszutek', 'szlamaniec@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nw jakiej cenie jest wypełnienie ubytku w Państwa gabinecie? I czy mają Państwo ogólny cennik usług? Chciałabym się mniej więcej zorientować w ew. kosztach. Z góry dziękuję za odp. <hr /><h3>Pozostałe dane:</h3>Imię: Sylwia<br />Nazwisko: Rzeszutek<br />E-mail: szlamaniec@gmail.com<br />Telefon: 663728456<br />', '2015-05-28 09:05:22', '62.133.130.43'),
(49, 'Jakub Kielar', 'kubakiller8@o2.pl', '<h3>Wiadomość z formularza:</h3>Po ukruszeniu zęba na migdale  strasznie mnie boli.  Proszę o pomoc i kontakt <hr /><h3>Pozostałe dane:</h3>Imię: Jakub<br />Nazwisko: Kielar<br />E-mail: kubakiller8@o2.pl<br />Telefon: 889673643<br />', '2015-06-04 23:31:50', '91.196.10.3'),
(50, 'Ewelina Machala', 'eewel@poczta.fm', '<h3>Wiadomość z formularza:</h3>Witam, pisze de panstwa poniewaz jestem zainteresewana cena implantu, zeba n°46 i 36. Czy mogliby panstwo podac mi calkowity koszt lacznie z korona. Pozdrawiam i dziekuje za odpowiedz. <hr /><h3>Pozostałe dane:</h3>Imię: Ewelina<br />Nazwisko: Machala<br />E-mail: eewel@poczta.fm<br />Telefon: 0033603729361<br />', '2015-07-17 14:19:26', '90.2.167.229'),
(51, 'Iwona Domino-Ożóg', 'shila666@o2.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, proszę o informację czy w placówce w Rzeszowie podczas zabiegu można skorzystać z znieczulenia komputerowego ? Proszę o podanie ceny wizyty z znieczuleniem (chodzi o protetykę).\nBędę wdzięczna za informacje. <hr /><h3>Pozostałe dane:</h3>Imię: Iwona<br />Nazwisko: Domino-Ożóg<br />E-mail: shila666@o2.pl<br />Telefon: 609127582<br />', '2015-08-01 15:04:10', '95.49.235.135'),
(52, 'LIDIA ZYBURA', 'india-77@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam ,chciałanym umówić się na wizytę na termin 30 lub 31 sierpień na ranną porę o ile jest to możliwe ,chciłabym zabląbować ząb i usunąć kamień .\n\nBardzo prosze o kontakt ,czy ten termin jest dostępny . <hr /><h3>Pozostałe dane:</h3>Imię: LIDIA<br />Nazwisko: ZYBURA<br />E-mail: india-77@o2.pl<br />Telefon: 00353 877823152<br />', '2015-08-11 19:36:37', '185.26.182.34'),
(53, 'LIDIA ZYBURA', 'india-77@o2.pl', '<h3>Wiadomość z formularza:</h3>WITAM \n\nChciałabym umówić sie na wizytę na 30 lub 31 sieroień na ranną porę o ile jest termin dostępny ,chciałabym zabląbować ząb i usunąć kamień .\nProszę o kontakt i dziękuje bardzo.\n\nLIDIA ZYBURA <hr /><h3>Pozostałe dane:</h3>Imię: LIDIA<br />Nazwisko: ZYBURA<br />E-mail: india-77@o2.pl<br />Telefon: 00353877823152<br />', '2015-08-11 19:41:20', '185.26.182.34'),
(54, 'LIDIA ZYBURA', 'india-77@o2.pl', '<h3>Wiadomość z formularza:</h3>witam \nchciałabym się umówić na wizytę na blombowanie ząbka i sciągnięcie kamienia na termin 3 wrzesien na ranną porę jeśli można \nprosze o informacje <hr /><h3>Pozostałe dane:</h3>Imię: LIDIA<br />Nazwisko: ZYBURA<br />E-mail: india-77@o2.pl<br />Telefon: 00353877823152<br />', '2015-08-18 16:25:09', '185.26.182.30'),
(55, 'Paweł Petryniak', 'p.pawel@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam, mam pytanie jaki były koszt leczenia zęba kanałowo, jest to ząb trzonowy (6) oraz na ile wizyt musiałbym się umówić, proszę o wiadomość. Pozdrawiam Paweł <hr /><h3>Pozostałe dane:</h3>Imię: Paweł<br />Nazwisko: Petryniak<br />E-mail: p.pawel@onet.pl<br />Telefon: 000000000<br />', '2015-09-01 18:57:40', '82.147.37.218'),
(56, 'Pawel Petryniak', 'pawey@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam, jaki byłby koszt leczenia zęba kanałowo, jest to ząb trzonowy (6) oraz ile wizyt potrzeba na leczenie, Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Pawel<br />Nazwisko: Petryniak<br />E-mail: pawey@onet.pl<br />Telefon: 512253213<br />', '2015-09-01 19:00:43', '82.147.37.218'),
(57, 'Marzena Mazurek', 'marzena.mazurek@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam, Chciałabym umówić się na konsultację do ortodonty na poniedziałek (07.09), pierwsza wizyta. Czy jest to możliwe? Interesuje mnie jak najszybsza wizyta ponieważ 22.09 wyjeżdżam za granicę. Z góry dziękuję za odpowiedź <hr /><h3>Pozostałe dane:</h3>Imię: Marzena<br />Nazwisko: Mazurek<br />E-mail: marzena.mazurek@o2.pl<br />Telefon: 66942484<br />', '2015-09-03 14:33:12', '185.24.199.50'),
(58, 'Marzena Mazurek', 'marzena.mazurek@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam, czy jest mozliwosc umówieia sie na pierwszą wizytę do ortodonty, konsultację na poniedziałek (07.09). Zależy mi na jak najszybszej wizycie ponieważ 22.09 wyjezdzam za granice. \nz góry dziękuję za odpowiedź.\nMarzena Mazurek\n 669 412 484 <hr /><h3>Pozostałe dane:</h3>Imię: Marzena<br />Nazwisko: Mazurek<br />E-mail: marzena.mazurek@o2.pl<br />Telefon: 66942484<br />', '2015-09-03 14:51:09', '185.24.199.50'),
(59, 'Pawel Petryniak', 'p.pawel@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam, mam pytanie jaki byłby koszt leczenia kanałowego zęba trzonowego (6) oraz ile wizyt potrzeba na takie leczenie, pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Pawel<br />Nazwisko: Petryniak<br />E-mail: p.pawel@onet.pl<br />Telefon: 512253213<br />', '2015-09-03 15:39:55', '193.213.32.242'),
(60, 'Justyna Zegar', 'justizegar@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry\nChciałam się dowiedzieć jaki jest koszt aparatu ortodontycznego, ile kosztują wizyty i czy dodatkowo płaci się za np. wymianę gumek.\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Justyna<br />Nazwisko: Zegar<br />E-mail: justizegar@gmail.com<br />Telefon: 791344769<br />', '2015-09-08 10:39:02', '89.231.6.19'),
(61, 'Katarzyna Hylczuk - Gmyz', 'katarzynahylczuk@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, nosze się z zamiarem założenia aparatu i mam wiele pytań , odnośnie samego leczenia, aparatu itp <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Hylczuk - Gmyz<br />E-mail: katarzynahylczuk@gmail.com<br />Telefon: 507069128<br />', '2015-10-04 10:14:07', '83.18.59.63'),
(62, 'Paweł Filip', 'pablo1581@vp.pl', '<h3>Wiadomość z formularza:</h3>Witam\nChbiałem zapytać jaki u Państwa jest orientacyjny cennik leczenia kanałowego? <hr /><h3>Pozostałe dane:</h3>Imię: Paweł<br />Nazwisko: Filip<br />E-mail: pablo1581@vp.pl<br />Telefon: 6005897724<br />', '2015-11-08 08:58:34', '31.1.62.160'),
(63, 'Karolina Piotrowska', 'karolinalola@interia.pl', '<h3>Wiadomość z formularza:</h3>Witam . Chcialabym sie dowiedziec jaki jest u Panstwa koszt aparatu stalego ? <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Piotrowska<br />E-mail: karolinalola@interia.pl<br />Telefon: 782257361<br />', '2015-11-13 16:27:39', '83.31.235.191'),
(64, 'monika Świątek', 'monikaswiatek22@op.pl', '<h3>Wiadomość z formularza:</h3>Ile kosztuje u Panstwa leczenie kanalowe i na kiedy jest najblizszy mozliwy temin?? na najlepiej w sobote jesi jest otwarte?? <hr /><h3>Pozostałe dane:</h3>Imię: monika<br />Nazwisko: Świątek<br />E-mail: monikaswiatek22@op.pl<br />Telefon: 507031253<br />', '2015-11-17 15:56:33', '62.133.150.167'),
(65, 'Karolina Grzesik', 'Karolcia201@interia.pl', '<h3>Wiadomość z formularza:</h3>Dobry wieczór mam pytanie. Chciałabym sie dowiedziec jaka jest cena zalozenie plomby do zeba białej gdyz wypadła mi i zab mnie boli.\nProszę o kontakt <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Grzesik<br />E-mail: Karolcia201@interia.pl<br />Telefon: 787390866<br />', '2015-12-04 20:24:19', '89.230.68.200'),
(66, 'Patrycja F', 'patka.sz13@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam.\nPoszukuję stomatologa z ogromną cierpliwością , który bezboleśnie leczy zęby:P\nMam bardzo zniszczonę zęby , a okropnie boję sie gabinetów dentystycznych;/\nCzy znajdzie się dla mnie ktoś taki?:)) <hr /><h3>Pozostałe dane:</h3>Imię: Patrycja<br />Nazwisko: F<br />E-mail: patka.sz13@wp.pl<br />Telefon: 535812600<br />', '2015-12-17 12:36:32', '31.130.96.179'),
(67, 'Ewa Szajowska', 'eszajowska@gmail.com', '<h3>Wiadomość z formularza:</h3>dzień dobry,\njestem zainteresowana założeniem aparatu ruchomego.\n\nProszę o informacje odnośnie możliwie najszybszego terminu wizyty.\n\nPozdrawiam\nEwa Szajowska <hr /><h3>Pozostałe dane:</h3>Imię: Ewa<br />Nazwisko: Szajowska<br />E-mail: eszajowska@gmail.com<br />Telefon: 781961451<br />', '2015-12-28 19:57:10', '95.160.40.193'),
(68, 'Łucja Oleś', 'l.oles@op.pl', '<h3>Wiadomość z formularza:</h3>Witam, mam pytanie odnośnie wizyty ortodontycznej, \njak długo czekać  trzeba na pierwszą wizytę i jaki jest jej \nkoszt, pozdrawiam! <hr /><h3>Pozostałe dane:</h3>Imię: Łucja<br />Nazwisko: Oleś<br />E-mail: l.oles@op.pl<br />Telefon: 692196242<br />', '2016-02-08 20:39:11', '83.31.73.224'),
(69, 'Jakub Długoń', 'jakub.dlugon@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam potrzebuje umówić się na pilną wizyte najlepiej w niedziele(jutro) po godzinie 9:00 z powodu ukruszenia górnej jedynki. <hr /><h3>Pozostałe dane:</h3>Imię: Jakub<br />Nazwisko: Długoń<br />E-mail: jakub.dlugon@gmail.com<br />Telefon: 696108142<br />', '2016-03-12 21:16:52', '62.133.138.243'),
(70, 'Jakub Długoń', 'jakub.dlugon@gmail.com', '<h3>Wiadomość z formularza:</h3>Potrzebuje pilnego leczenia górnej jedynki z powodu ukruszenia, preferowana wizyta najlepiej w dniu jutrzejszym(niedziela) po godzinie 9:30, będę jeszcze jutro rano dzwonił. z góry dziękuje <hr /><h3>Pozostałe dane:</h3>Imię: Jakub<br />Nazwisko: Długoń<br />E-mail: jakub.dlugon@gmail.com<br />Telefon: 696108142<br />', '2016-03-12 21:21:07', '62.133.138.243'),
(71, 'Monika Świątek', 'monikaswiatek22@op.pl', '<h3>Wiadomość z formularza:</h3>Witam, chciałabym sie umowic na leczenie kanałowe szóstki?? Jaki byłby koszt i kiedy jest najbliższy termin?? <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Świątek<br />E-mail: monikaswiatek22@op.pl<br />Telefon: 507031253<br />', '2016-03-31 10:14:15', '62.133.129.4'),
(72, 'Monika Świątek', 'monikaswiatek22@op.pl', '<h3>Wiadomość z formularza:</h3>Chciałabym sie umowic na wizyte, leczenie kanałowe tak na godzinke( nie moge dłuzej zostawic córki) najwyzej bedzie na 2 lub 3 wizytach zab leczony. Kiedy najblizszy wolny termin popołudniu lub na weekendzie?? Ząb troche boli?? <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Świątek<br />E-mail: monikaswiatek22@op.pl<br />Telefon: 507031253<br />', '2016-04-16 19:25:34', '62.133.129.4'),
(73, 'Sylwia Rzeszutek', 'szlamaniec@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, zapomniałam podczas rozmowy spytać, czy można u Państwa w gabinecie płacić kartą? Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Sylwia<br />Nazwisko: Rzeszutek<br />E-mail: szlamaniec@gmail.com<br />Telefon: 663728456<br />', '2016-04-22 19:33:31', '37.190.229.73'),
(74, 'kinga jakiela', 'dynastia@op.pl', '<h3>Wiadomość z formularza:</h3>potrzebuje b.pilnej konsultacji/wizyty protetycznej - zab sie zlamal. <hr /><h3>Pozostałe dane:</h3>Imię: kinga<br />Nazwisko: jakiela<br />E-mail: dynastia@op.pl<br />Telefon: 600921351<br />', '2016-04-25 08:04:42', '91.225.135.12'),
(75, 'kinga jakiela', 'dynastia@op.pl', '<h3>Wiadomość z formularza:</h3>potrzebuje pilnej konsultacji -zlamany zab. <hr /><h3>Pozostałe dane:</h3>Imię: kinga<br />Nazwisko: jakiela<br />E-mail: dynastia@op.pl<br />Telefon: 600921351<br />', '2016-04-25 08:11:51', '91.225.135.12'),
(76, 'Krystyna Knutel', 'anna547@poczta.onet.pl', '<h3>Wiadomość z formularza:</h3>Potrzebuję zrobić górną protezkę szkieletową w terminie od16.06.16-do dwóch tygodni-proszę o kontakt <hr /><h3>Pozostałe dane:</h3>Imię: Krystyna<br />Nazwisko: Knutel<br />E-mail: anna547@poczta.onet.pl<br />Telefon: 730 138 704<br />', '2016-05-23 07:39:08', '91.246.66.126'),
(77, 'Marcin Podgórski', 'marcin.podgorski77@onet.pl', '<h3>Wiadomość z formularza:</h3>Szanowni Państwo,\npilnie potrzebuję dostać sie do lekarza stomatologa ponieważ miałem wypadek na rowerze i złamała mi się jedynka i dwójka.\nCzy napis czynne codziennie dotyczy tez niedzieli?\nJeśli nie potrzebuję zarejestrować się na jutro najwcześniej jak to tylko możliwe .\nPozdrawiam,\nMarcin Podgórski\nMarcin Podgórski <hr /><h3>Pozostałe dane:</h3>Imię: Marcin<br />Nazwisko: Podgórski<br />E-mail: marcin.podgorski77@onet.pl<br />Telefon: 730 300 079<br />', '2016-09-04 09:23:17', '62.133.142.51'),
(78, 'Marcin Podgórski', 'marcin.podgorski77@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nSzanowni Państwo, potrzebuje pilnie umówić się na wizytę (najlepiej jutro, jak najwcześniej). Miałem wypadek na rowerze i ułamała mi się jedynka i dwójka.\nPozdrawiam,\nMarcin Podgórski <hr /><h3>Pozostałe dane:</h3>Imię: Marcin<br />Nazwisko: Podgórski<br />E-mail: marcin.podgorski77@onet.pl<br />Telefon: 730300079<br />', '2016-09-04 09:37:23', '62.133.142.51'),
(79, 'Marcin Podgórski', 'marcin.podgorski@onet.pl', '<h3>Wiadomość z formularza:</h3>Czy jest mozliwość przyjścia jutro i przyjęcia przez Państwa o godzinie 8.00? Uległem wypadkowi na rowerze i potrzebuje pilnie o przyjęcie. <hr /><h3>Pozostałe dane:</h3>Imię: Marcin<br />Nazwisko: Podgórski<br />E-mail: marcin.podgorski@onet.pl<br />Telefon: 730300079<br />', '2016-09-04 13:22:29', '62.133.134.37'),
(80, 'Marcin Wróblewski', 'catedral@catedral.com.pl', '<h3>Wiadomość z formularza:</h3>Witam, zapraszamy na jedyne szkolenie dla Higienistki i asystentki stomatologicznej z Profesjonalnej asysty implantologicznej w Rzeszowie 22.10.2016 hotel Rzeszów, szczegóły na stronie http://catedral.com.pl/profesjonalna-asysta-implantologiczna-easy-implant.html <hr /><h3>Pozostałe dane:</h3>Imię: Marcin<br />Nazwisko: Wróblewski<br />E-mail: catedral@catedral.com.pl<br />Telefon: 505092395<br />', '2016-10-16 16:02:54', '81.190.220.41'),
(81, 'Iwona Ozga', 'iwonarosol@onet.eu', '<h3>Wiadomość z formularza:</h3>Witam.  Jak jest cena za wizytę z dzieckiem w wieku prawie 3 lata.  Wizyta w celu przeglądu stanu Zdrowia zębów. \nPozdrawiam \nIwona <hr /><h3>Pozostałe dane:</h3>Imię: Iwona<br />Nazwisko: Ozga<br />E-mail: iwonarosol@onet.eu<br />Telefon: 602578395<br />', '2016-11-23 19:06:36', '37.47.80.242'),
(82, 'Sebastian pirog', 'sloik89@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam\nOdczuwam dotkliwy ból zęba, kiedy była by możliwość umówienia się na wizytę? <hr /><h3>Pozostałe dane:</h3>Imię: Sebastian<br />Nazwisko: pirog<br />E-mail: sloik89@gmail.com<br />Telefon: 725117643<br />', '2017-02-15 18:50:02', '5.172.234.109'),
(83, 'Angelika Czach', 'angelika.czach@onet.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, mam diastemę, i chciałabym się dowiedzieć mniej więcej ile u Państwa kosztuje nałożenie licówki ? Dziękuje :) <hr /><h3>Pozostałe dane:</h3>Imię: Angelika<br />Nazwisko: Czach<br />E-mail: angelika.czach@onet.pl<br />Telefon: 605673378<br />', '2017-03-18 16:46:42', '195.238.168.117'),
(84, 'Łukasz Kondratko', 'kond.luka@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\njestem zainteresowany usługą piaskowania zębów i polerowania. Interesuje mnie wtorek/środa (6-7 czerwca) godziny najlepiej wieczorne - po 17:00.\n\nMiałem już piaskowanie kiedyś robione więc wiem jak to wygląda ;)\n\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Łukasz<br />Nazwisko: Kondratko<br />E-mail: kond.luka@gmail.com<br />Telefon: 668814044<br />', '2017-06-01 12:46:09', '91.246.74.106'),
(85, 'anna sęk', 'anamariabasek@poczta.fm', '<h3>Wiadomość z formularza:</h3>Witam,proszę o ustalenie wizyty w celu usunięcia kamienia. Możliwie w nadchodzącym tygodniu. \nPozdrawiam, Anna Sęk. <hr /><h3>Pozostałe dane:</h3>Imię: anna<br />Nazwisko: sęk<br />E-mail: anamariabasek@poczta.fm<br />Telefon: 799309670<br />', '2017-07-15 12:13:43', '62.133.134.194'),
(86, 'Jennifer MICHALAK', 'jennifer.michalak@yahoo.fr', '<h3>Wiadomość z formularza:</h3>Witam, \n\nchciałam zapytać czy przyjmójecie leczenie na karte europejską (EKUZ) ? \n\nBo jestem ubezbieczona we Francji. \n\nDziękuję za odpowiedz. <hr /><h3>Pozostałe dane:</h3>Imię: Jennifer<br />Nazwisko: MICHALAK<br />E-mail: jennifer.michalak@yahoo.fr<br />Telefon: 889908993<br />', '2017-11-08 11:23:11', '91.224.118.92'),
(87, 'Jennifer Michalak', 'Jennifer.michalak@yahoo.fr', '<h3>Wiadomość z formularza:</h3>Witam, \nCzy panstwo akceptuja karte europejska (EKUZ), bo jestem ubezpieczona we Francji.\n\nDziekuje za odpowiedz. <hr /><h3>Pozostałe dane:</h3>Imię: Jennifer<br />Nazwisko: Michalak<br />E-mail: Jennifer.michalak@yahoo.fr<br />Telefon: 889908993<br />', '2017-11-08 15:57:41', '91.224.118.92'),
(88, 'Karolina Cuprys', 'Karolina.cuprys@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam chciałabym sie dowiedzieć jaka jest cena wizyty w państwa gabinecie chodzi mi o konsultacje ortodontyczną oraz cenę aparatu stałego, pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Cuprys<br />E-mail: Karolina.cuprys@onet.pl<br />Telefon: 506969010<br />', '2017-11-13 20:00:48', '91.223.175.9'),
(89, 'Karolina ORLOF', 'Flaelle17@gmail.com', '<h3>Wiadomość z formularza:</h3>Dobry wieczór. Czy jest możliwość wizyty jutro tj .21 grudnia godzina 18, 18.30 ? Stan zapalny w dziasle prawdopodobnie ropa obok zęba leczonego kanałowo. Będę wdzięczna za odpowiedz. Pozdrawiam Orlof Karolina <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: ORLOF<br />E-mail: Flaelle17@gmail.com<br />Telefon: 507305005<br />', '2017-12-20 21:10:22', '5.184.232.175'),
(90, 'Karolina Orlof', 'Flaelle17@gmail.com', '<h3>Wiadomość z formularza:</h3>Dobry wieczór. Czy byłaby możliwość wizyty jutro tj. 21 grudnia 2017 o 18,18.30 u Państwa? Stan zapalny dziąsła, prawdopodobnie ropa, przy zębie leczonym kanałowo. Będę wdzieczna za informację. Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Orlof<br />E-mail: Flaelle17@gmail.com<br />Telefon: 507305005<br />', '2017-12-20 21:14:57', '193.19.165.4'),
(91, 'kamil sz', 'ropp@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam , \nCzy bylaby mozliwosc umowienia sie na wizyte do stomatologa  w ta sobote 13.01 o jakiejkolwiek porze ? <hr /><h3>Pozostałe dane:</h3>Imię: kamil<br />Nazwisko: sz<br />E-mail: ropp@o2.pl<br />Telefon: 0785678456<br />', '2018-01-09 23:05:49', '82.41.105.191'),
(92, 'Karolina Cupryś', 'Karolina.cuprys@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam ???? Chciałabym sie dowiedzieć czy maja Państwo jakieś wolne okienko w czwartek 8 lutego ? Ukruszyłam plombę w jedynce i potrzebuje pomocy, pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Cupryś<br />E-mail: Karolina.cuprys@onet.pl<br />Telefon: 506969010<br />', '2018-02-06 13:15:10', '91.223.175.9'),
(93, 'Karolina Cupryś', 'Karolina.cuprys@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam czy posiadają Państwo jakieś wolne okienko w czwarte 8 lutego ? Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Cupryś<br />E-mail: Karolina.cuprys@onet.pl<br />Telefon: 506969010<br />', '2018-02-06 13:16:56', '91.223.175.9'),
(94, 'Tafil Patrycja', 'p-tafil@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam czy przyjmują Państwo również na  NFZ? na kiedy mozna  zapisać się  z  bólem  zęba? <hr /><h3>Pozostałe dane:</h3>Imię: Tafil<br />Nazwisko: Patrycja<br />E-mail: p-tafil@wp.pl<br />Telefon: 788312399<br />', '2018-02-28 07:41:51', '62.133.139.120'),
(95, 'Izabela Brud', 'Izasizi@interia.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry.czy nie poszukują Państwo asystentek do pracy??z doświadczeniem <hr /><h3>Pozostałe dane:</h3>Imię: Izabela<br />Nazwisko: Brud<br />E-mail: Izasizi@interia.pl<br />Telefon: 733119021<br />', '2018-05-22 17:23:58', '94.254.145.71'),
(96, 'Bartłomiej Wozowicz', 'bartomir@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, chciałbym się zapytać czy była by możliwość zapisać się na wizytę w tym tygodniu lub 30, 31 lipca w godzinach popołudniowych (po 17). Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Bartłomiej<br />Nazwisko: Wozowicz<br />E-mail: bartomir@gmail.com<br />Telefon: 603234074<br />', '2018-07-24 21:02:29', '91.223.64.252'),
(97, 'Bartłomiej Wozowicz', 'bartomir@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, chciałbym się dowiedzieć, czy byłaby możliwość umówienia się na wizytę w tym tygodniu bądź też 30, 31 lipca w godzinach popołudniowych (po 17) ? Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Bartłomiej<br />Nazwisko: Wozowicz<br />E-mail: bartomir@gmail.com<br />Telefon: 603234074<br />', '2018-07-24 21:09:11', '91.223.64.252'),
(98, 'Heini Mikkelsen Mikkelsen', 'hm@covizmo.com', '<h3>Wiadomość z formularza:</h3>Hi Iwona,\n\nYou are my dentist :-)\n\ny son in UK as a problem with his front tooth. Can you fix it and what will the approx. time and cost be.\n\nPlease find this description from Sebastian Mikkelsen:\n\" I was attacked 10 years ago and had both my front teeth damaged. \nMy left front tooth was completely out. \nThe dentist removed the nerve and put the tooth back in with a type of brace on the back to hold it in place. \nIt seemed my mouth had accepted the tooth.\nOn Wednesday the 25th of July I bit in to some food and the left tooth felt as though it moved and I could hear a cracking sound. \nThe following day I went to the dentist in England and had it X-rayed. \nThere appears to be a crack further up the tooth hidden behind the gums and theres a possibility of an infection starting in the tooth. \nTheres no pain as the nerve has been completely removed. \n\nI would like to have the tooth looked at, removed and replaced with a permanent implant. But as I live in London I wanted to find out if we could make it work with me visiting Poland a few times over the course of the next few months whilst starting as soon as possible\"\n\nBest regards\nHeini Mikkelsen <hr /><h3>Pozostałe dane:</h3>Imię: Heini Mikkelsen<br />Nazwisko: Mikkelsen<br />E-mail: hm@covizmo.com<br />Telefon: +45 30554489<br />', '2018-07-27 10:27:54', '176.20.98.145'),
(99, 'Mateusz Kowal', 'mateuszjankowal@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam\n Od ponad 5 lat nosze protezę,  zęba jedynki górnej. Jest ona zrobiona w taki sposób, że wspiera się na innych zębach ( korzeń został usunięty) właśnie przez tą protezę powżynała mi się ona i porobiła dziury w 6-tkach. Jedna z nich połamała mi się tak, iż zostało mi samo dziąsło, drugiej część się jeszcze trzyma. Oprócz tego mam wyrwaną 6 i 7 dolną lewą oraz praktycznie do usunięcia resztki 7 dolnej prawej.Pytanie moje jak mogę dojść do ładu z tymi zębami po tak długim czasie ignorowania ich ? Jaki koszt usunięcia tych resztek i jakiś protez punktowych bądź mostków ? Chciałbym bardzo zacząć ogarniać ten problem, ale niestety jak najniższym kosztem, gdyż nie jestem zamożny. Pozdrawiam. <hr /><h3>Pozostałe dane:</h3>Imię: Mateusz<br />Nazwisko: Kowal<br />E-mail: mateuszjankowal@gmail.com<br />Telefon: 534780624<br />', '2018-08-03 09:27:29', '46.134.152.162'),
(100, 'Monika Klocek', 'monikaaklocek@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, w jakiej cenie są  u Państwa wykonywane licówki kompozytowe i porcelanowe? \nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Klocek<br />E-mail: monikaaklocek@gmail.com<br />Telefon: 506342742<br />', '2018-08-23 12:00:23', '91.196.10.8'),
(101, 'Monika Klocek', 'monikaaklocek@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, jaka jest cena licówek kompozytowych i porcelanowych? <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Klocek<br />E-mail: monikaaklocek@gmail.com<br />Telefon: 506342742<br />', '2018-08-23 12:08:49', '91.196.10.8'),
(102, 'AGATA MOCHNIEJ', 'mochnieja04@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nProszę o informację o najbliższym możliwym terminie wizyty u ortodonty. Pacjentem będzie mój syn w wieku 14 lat - pierwsza wizyta. Interesują mnie godziny popołudniowe (po 15.30) w dni czwartek i piątek. \nZ góry dziękuje za informację zwrotną.\n\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: AGATA<br />Nazwisko: MOCHNIEJ<br />E-mail: mochnieja04@wp.pl<br />Telefon: 518156803<br />', '2018-10-03 10:20:27', '195.38.12.141'),
(103, 'Klaudia Olijnyk', 'Firmaostaf@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, chciałabym umówić się na wizytę ortodontyczną, chce założyć aparat na zęby, kiedy jest najbliższy termin? Nigdy wcześniej nie byłam u stomatologia u Państwa <hr /><h3>Pozostałe dane:</h3>Imię: Klaudia<br />Nazwisko: Olijnyk<br />E-mail: Firmaostaf@gmail.com<br />Telefon: 536200189<br />', '2018-11-26 18:22:22', '5.173.40.77'),
(104, 'Krystian Szwarc', 'gochagosia@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam, piszę w imieniu męża czy jest dzisiaj możliwość podejść z bólem zemba. Chodzi o godziny po 17 bo mąż jest w pracy i niemoże się wcześniej zwolnić. <hr /><h3>Pozostałe dane:</h3>Imię: Krystian<br />Nazwisko: Szwarc<br />E-mail: gochagosia@o2.pl<br />Telefon: 513991875<br />', '2019-02-12 12:34:30', '172.68.50.49'),
(105, 'Grzegorz Pitera', 'grzesiekpitera@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nod tygodnia leczę się na jednostronne zapalenie zatoki szczękowej. Jednocześnie doskwiera mi tępy ból w okolicach górnej 6-ki, 7-ki. Czy jest możliwość uzyskać dzisiaj w godzinach wieczornych poradę specjalistyczną z ewentualnym leczeniem?\nPozdrawiam,\nGrzegorz Pitera <hr /><h3>Pozostałe dane:</h3>Imię: Grzegorz<br />Nazwisko: Pitera<br />E-mail: grzesiekpitera@wp.pl<br />Telefon: 609566525<br />', '2019-02-15 08:59:03', '198.41.242.87'),
(106, 'Ryszard Jawroski', 'skupsprzedaz@onet.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry chciałbym zapytać jakie najbliższe terminy są u Państwa na wizytę u stomatologa , czy są to tylko wizyty prywatne czy może refundowane przez NFZ ( jeżeli prywatne to gdzie można zapoznać się z cennikiem ) <hr /><h3>Pozostałe dane:</h3>Imię: Ryszard<br />Nazwisko: Jawroski<br />E-mail: skupsprzedaz@onet.pl<br />Telefon: 171122334<br />', '2019-10-15 22:12:59', '172.69.54.38'),
(107, 'Ryszard Jawroski', 'skupsprzedaz@onet.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry chciałbym zapytać jakie są najbiższe wolne terminy   do stomatologa , ile kosztuje wizyta czy maja państwo umowę z nfz ? czy tylko prywatne wizyty jeżeli prywatnie to gdzie znajdę cennik <hr /><h3>Pozostałe dane:</h3>Imię: Ryszard<br />Nazwisko: Jawroski<br />E-mail: skupsprzedaz@onet.pl<br />Telefon: 176234123<br />', '2019-10-16 09:08:13', '141.101.105.185'),
(108, 'Jacek Kuk', 'gsm18@op.pl', '<h3>Wiadomość z formularza:</h3>Dzien dobry chciałbum zapytać czy stomatolog przyjmuje na NFZ czy tylko prywatnie ? Jezeli prywatnie gdzie znajde cennik <hr /><h3>Pozostałe dane:</h3>Imię: Jacek<br />Nazwisko: Kuk<br />E-mail: gsm18@op.pl<br />Telefon: 888293488<br />', '2019-12-13 19:34:25', '172.69.54.32'),
(109, 'Jadwiga Skiba-Sperling', 'skiba_j@interia.eu', '<h3>Wiadomość z formularza:</h3>Witam czy można umówić się u państwa na standardowy przegląd i usuwanie kamienia ? Jaki jest koszt usługi? Kiedy dostępny jest najbliższy termin? <hr /><h3>Pozostałe dane:</h3>Imię: Jadwiga<br />Nazwisko: Skiba-Sperling<br />E-mail: skiba_j@interia.eu<br />Telefon: 791106053<br />', '2019-12-17 18:53:22', '172.69.194.39'),
(110, 'Jadwiga Skiba-Sperling', 'Skiba_j@interia.eu', '<h3>Wiadomość z formularza:</h3>Witam w jakiej cenie jest standardowy przegląd wraz z usuwaniem kamienia ? Kiedy dostępny byłby najbliższy termin? <hr /><h3>Pozostałe dane:</h3>Imię: Jadwiga<br />Nazwisko: Skiba-Sperling<br />E-mail: Skiba_j@interia.eu<br />Telefon: 791106053<br />', '2019-12-17 18:56:33', '172.69.194.51'),
(111, 'Gabriela Kiszka', 'gabrielakiszka1@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, Czy byłaby możliwość  zapisania sie na plombowanie zęba na piatek 27 grudnia w godzinach porannych? <hr /><h3>Pozostałe dane:</h3>Imię: Gabriela<br />Nazwisko: Kiszka<br />E-mail: gabrielakiszka1@gmail.com<br />Telefon: 8<br />', '2019-12-22 17:36:03', '162.158.155.19'),
(112, 'Magdalena Nowak', 'magdale.nowak96@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nZwracam się uprzejmie do Państwa z zapytaniem, czy istnieje możliwość odbywania praktyk zawodowych? Obecnie jestem na 4. roku studiów lekarsko-dentystycznych. Jestem otwarta na pogłębianie zdobywanej wiedzy i umiejętności.\n\nZ wyrazami szacunku,\nMagdalena Nowak\nTel. 530601717 <hr /><h3>Pozostałe dane:</h3>Imię: Magdalena<br />Nazwisko: Nowak<br />E-mail: magdale.nowak96@gmail.com<br />Telefon: 53601717<br />', '2020-02-12 12:24:13', '172.69.190.21'),
(113, 'Magdalena Nowak', 'magdale.nowak96@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nZwracam się uprzejmie do Państwa z zapytaniem, czy istnieje możliwość odbywania praktyk zawodowych? Obecnie jestem na 4. roku studiów lekarsko-dentystycznych. Jestem otwarta na pogłębianie zdobywanej wiedzy i umiejętności.\n\nZ wyrazami szacunku,\nMagdalena Nowak\nTel. 530601717 <hr /><h3>Pozostałe dane:</h3>Imię: Magdalena<br />Nazwisko: Nowak<br />E-mail: magdale.nowak96@gmail.com<br />Telefon: 530601717<br />', '2020-02-12 12:25:26', '172.69.190.21'),
(114, 'Dorota pelc', 'e.mach123.pl@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam .Chciałam zapytać ,czy w Łańcucie można wykonać zabieg usuwania zmarszczek .Interesuje mnie usunięcie tzw. zmarszczki uśmiechu z obu stron i jaki będzie mniej więcej koszt .Dziękuję . <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: pelc<br />E-mail: e.mach123.pl@o2.pl<br />Telefon: 787312220<br />', '2020-02-27 08:39:45', '141.101.104.106'),
(115, 'Katarzyna Adamska', 'kasia.adamska1994@gmail.com', '<h3>Wiadomość z formularza:</h3>Czy przyjmują Państwo obecnie na usuwanie ósemek? <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Adamska<br />E-mail: kasia.adamska1994@gmail.com<br />Telefon: 726917228<br />', '2020-03-23 17:00:36', '141.101.76.159'),
(116, 'Radosław Kurasz', 'radoslaw.kurasz@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam. Odczuwam ból zęba (tj. górna dwójka) oraz boli mnie ciągle dziąsło. Jak wyglądają obecnie przyjęcia w czasie pandemii? Jak z najbliższymi terminem? Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Radosław<br />Nazwisko: Kurasz<br />E-mail: radoslaw.kurasz@onet.pl<br />Telefon: 787157733<br />', '2020-04-17 06:24:10', '162.158.34.177'),
(117, 'Radosław Kurasz', 'radoslaw.kurasz@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam. Odczuwam ból zęba (tj. górna dwójka). Boli mnie również ciągle dziąsło nad tym zębem. Jak wyglądają teraz przyjęcia w czasie pandemii? Możne sie zarejestrować w najbliższym czasie? Pozdrawiam. <hr /><h3>Pozostałe dane:</h3>Imię: Radosław<br />Nazwisko: Kurasz<br />E-mail: radoslaw.kurasz@onet.pl<br />Telefon: 787157733<br />', '2020-04-17 06:27:57', '162.158.34.135');
INSERT INTO `ContactFormMessage` (`id`, `senderName`, `senderEmail`, `senderMessage`, `sentAt`, `senderIP`) VALUES
(118, 'Filip Makowski', 'biuro@adshock.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nnazywam się Filip Makowski.\n\nJestem doświadczonym specjalistą w zakresie pozycjonowania stron w wyszukiwarce Google.\n\nChętnie pomogę Państwu w tym zakresie.\nCo ciekawe, opłaty pobieram dopiero, kiedy Państwa strona znajdzie się w TOP10 wyników wyszukiwania.\nPłacą Państwo tylko za faktyczny efekt.\n\nObsługując kilkaset stron internetowych zebrałem duże doświadczenie w Państwa branży.\n\nLepsza widoczność w wyszukiwarce da Państwu większą ilość klientów i tym samym przełoży się na zysk.\n\nwww.adshock.pl\n\n--\nPozdrawiam\nFilip Makowski\ntel. 733 70 10 10\nfilip.makowski@adshock.pl <hr /><h3>Pozostałe dane:</h3>Imię: Filip<br />Nazwisko: Makowski<br />E-mail: biuro@adshock.pl<br />Telefon: 0(733)701010<br />', '2020-05-06 16:06:34', '162.158.103.188'),
(119, 'Maria Bytnar', 'mariabytnar@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nChciałabym się umówić na konsultację. Moja córka ma duża wadę zgryzu. Po prawej stronie wychodzi jej ząb (2), widać go w dziąśle, ale nie ma miejsca aby się wybić. Dentysta powiedział, że sprawa jest dość pilna. Jak długi jest czas oczekiwania na konsultację? <hr /><h3>Pozostałe dane:</h3>Imię: Maria<br />Nazwisko: Bytnar<br />E-mail: mariabytnar@wp.pl<br />Telefon: 518242566<br />', '2020-07-14 09:49:22', '162.158.103.200'),
(120, 'Oliwia Żyra', 'oliwia.zyra@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry, \n\nChciałabym się umówić na wizytę.\nPotrzebuję nowej nakładki ortodontycznej na noc. Nie byłam wcześniej Państwa pacjentką. \nInteresują mnie godziny popołudniowe (po godz 17:00) lub weekendy jeśli mają Państwo czynne\n\nProszę o kontakt\npozdrawiam\nOliwia Żyra <hr /><h3>Pozostałe dane:</h3>Imię: Oliwia<br />Nazwisko: Żyra<br />E-mail: oliwia.zyra@gmail.com<br />Telefon: 883717919<br />', '2020-12-07 22:29:24', '141.101.99.118'),
(121, 'Michał Witt', 'michu121712@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry\nChciałbym zapytać o najbliższy termin do stomatologa. Problem polega na tym, że wypadła mi plomba i pijąc chłodne napoje drażnią nerw. Czy jest możliwość zapisania na NFZ? Jakie są ceny za wizytę prywatnie? <hr /><h3>Pozostałe dane:</h3>Imię: Michał<br />Nazwisko: Witt<br />E-mail: michu121712@gmail.com<br />Telefon: 724520253<br />', '2020-12-15 08:34:08', '162.158.202.245'),
(122, 'Sebastian Matysik', 'sebastian.matysik.sm@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry \nProszę o informację jak szybko można umówić się na wizytę ukruszona górna dwójka przy samym dziąśle \nDziękuję <hr /><h3>Pozostałe dane:</h3>Imię: Sebastian<br />Nazwisko: Matysik<br />E-mail: sebastian.matysik.sm@gmail.com<br />Telefon: 730992240<br />', '2021-04-17 13:04:11', '141.101.104.16'),
(123, 'Regina Wojciechowska-Kudyba', 'regina.wojciechowska@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\njestem zainteresowana wizytą dla dwóch nastolatek. Przegląd i ewentualne niezbędne lecznie.\nCzy mogę prosić o cennik usług i dostępny termin?\nZ poważaniem\nRegina Wojciechowska-Kudyba <hr /><h3>Pozostałe dane:</h3>Imię: Regina<br />Nazwisko: Wojciechowska-Kudyba<br />E-mail: regina.wojciechowska@wp.pl<br />Telefon: 506746267<br />', '2021-08-17 10:37:06', '141.101.77.236'),
(124, 'Karol Dziura', 'Karol.dziura1@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, chciałbym się umówić na wizytę dot. założenia aparatu na zęby. Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Karol<br />Nazwisko: Dziura<br />E-mail: Karol.dziura1@wp.pl<br />Telefon: 660437021<br />', '2021-10-12 13:41:39', '141.101.97.56'),
(125, 'Grzegorz Jankisz', 'Grzegorzjankisz@gmail.com', '<h3>Wiadomość z formularza:</h3>Interesuje mnie most zebowy na 4 sztuki zebow. Jaki material i cena <hr /><h3>Pozostałe dane:</h3>Imię: Grzegorz<br />Nazwisko: Jankisz<br />E-mail: Grzegorzjankisz@gmail.com<br />Telefon: 669719192<br />', '2022-03-30 13:53:43', '162.158.203.10'),
(126, 'Małgorzata Brahimi', 'pearlhoho19@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam.\nMyślę nad założeniem aparatu na zęby i chciałam zapytać o koszt konsultacji.\nBędę wdzięczna za odpowiedź. <hr /><h3>Pozostałe dane:</h3>Imię: Małgorzata<br />Nazwisko: Brahimi<br />E-mail: pearlhoho19@gmail.com<br />Telefon: 575831881<br />', '2022-04-07 12:04:27', '162.158.103.214'),
(127, 'Maciej Leniart', 'maniexpob2016@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, chcialbym zapytac czy macie panstwo wolne terminy na poczatek badz polowe wrzesnia na kilka wizyt, moze byc dzien po dniu. <hr /><h3>Pozostałe dane:</h3>Imię: Maciej<br />Nazwisko: Leniart<br />E-mail: maniexpob2016@gmail.com<br />Telefon: +447518402177<br />', '2022-08-04 20:37:41', '172.70.85.99'),
(128, 'Danuta Pado', 'Krystyna391@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzien dobry. Czy jest mozliwosc umowienia sie na wizyte okolo 09/08/2023, godzina dowolna. Dziekuje bardzo za informacje i pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Danuta<br />Nazwisko: Pado<br />E-mail: Krystyna391@wp.pl<br />Telefon: +4407902723433<br />', '2023-07-31 12:04:49', '172.70.90.54'),
(129, 'Monika Szkutnik', 'mnkszkutnik@gmail.com', '<h3>Wiadomość z formularza:</h3>Szanowni państwo piszę do państwa, ponieważ chciałabym się umówić na wizytę stomatologiczną do pani doktor Iwonu Sławińskiej-Niedzin w dniu 09.08.2023 lub 10.08.2023. Czy jest to możliwe? Chodzi o ból zęba, który pojawia się co jakiś czas. Proszę o informację zwrotną.\nZ poważaniem\nMonika Szkutnik <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Szkutnik<br />E-mail: mnkszkutnik@gmail.com<br />Telefon: +49017666999490<br />', '2023-08-02 11:22:37', '172.68.110.183');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `craue_config_setting`
--

CREATE TABLE `craue_config_setting` (
  `name` varchar(255) NOT NULL,
  `value` varchar(10000) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `craue_config_setting`
--

INSERT INTO `craue_config_setting` (`name`, `value`, `section`) VALUES
('employees_main_photo', '/bundles/lmcms/images/top_pracownicy.png', 'Adres url do zdjęcia głównego w module \"nasz zespół\" np \"/bundles/lmcms/images/NAZWA_ZDJECIA\".'),
('formularz_wysylka_email', 'rzeszow@cosmodent.com.pl', NULL),
('formularz_wysylka_od', 'Mailer Cosmodent Rzeszów', NULL),
('formularz_wysylka_temat', 'Formularz kontaktowy COSMODENT Rzeszów', NULL),
('kod_analytics', '<script>\r\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\r\n\r\n  ga(\'create\', \'UA-38706337-2\', \'cosmodent.com.pl\');\r\n  ga(\'send\', \'pageview\');\r\n\r\n</script>\r\n\r\n<script type=\"text/javascript\">\r\n/* <![CDATA[ */\r\nvar google_conversion_id = 1022882452;\r\nvar google_custom_params = window.google_tag_params;\r\nvar google_remarketing_only = true;\r\n/* ]]> */\r\n</script>\r\n<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">\r\n</script>\r\n<noscript>\r\n<div style=\"display:inline;\">\r\n<img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//googleads.g.doubleclick.net/pagead/viewthroughconversion/1022882452/?value=0&amp;guid=ON&amp;script=0\"/>\r\n</div>\r\n</noscript>', 'Statystyki'),
('link_facebook', 'https://www.facebook.com/CosmodentStomatologia/', NULL),
('link_instagram', 'https://www.instagram.com/', NULL),
('link_lekarz_na_stronie_glownej', '/kompetencje.html', NULL),
('seo_description', 'Nowoczesna praktyka stomatologiczna Cosmodent Rzeszów. Serdecznie zapraszamy!', NULL),
('seo_keywords', 'stomatolog, stomatologia, praktyka stomatologiczna, Cosmodent, stomatologia dziecięca, Rzeszów, gabinet Rzeszów', NULL),
('seo_title', 'Cosmodent - stomatolog Rzeszów, nowoczesna praktyka stomatologiczna, medycyna estetyczna', 'SEO'),
('tekst_formularz_podziekowanie', 'Twoja wiadomość została wysłana.<br />\r\n<br />\r\nDziękuję za kontakt :)', NULL),
('tekst_haslo_strona_glowna', 'Bezbolesne leczenie\r\nPomoc w nagłych wypadkach', 'Tekst'),
('tekst_stopka_adres', 'Rzeszów, ul. Strażacka 12d/2', NULL),
('tekst_stopka_copyright', '&copy; Copyrights 2012-2013, COSMODENT - Dentysta Rzeszów, Praktyka Stomatologiczna\r\n<br />\r\n<div style=\"padding-top: 10px\">\r\nRealizacja: <a style=\"color:black\" href=\"http://www.lemonadestudio.pl\" title=\"Lemonade Studio\">Lemonade Studio</a>\r\n</div>', NULL),
('tekst_strona_glowna_telefon', '698 902 233', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Employee`
--

CREATE TABLE `Employee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext,
  `position` longtext,
  `specializations` longtext,
  `slug` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `entityOrder` int(11) NOT NULL,
  `largePhoto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Employee`
--

INSERT INTO `Employee` (`id`, `name`, `description`, `position`, `specializations`, `slug`, `photo`, `isActive`, `createdAt`, `updatedAt`, `entityOrder`, `largePhoto`) VALUES
(3, 'Maja Maciejewska-Uruska', '<div>\r\n	<strong>W 2007 roku ukończyła studia na Uniwersytecie Medycznym w Lublinie.</strong> Zajmuje się <strong>leczeniem ortodontycznym</strong> &nbsp;dzieci i dorosłych. Prowadzi leczenie zar&oacute;wno aparatami zdejmowanymi jak i stałymi w technice konwencjonalnej i bezligaturowej. Przygotowuje pacjent&oacute;w do leczenie protetycznego oraz wszczepiania implant&oacute;w.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	<strong>Preferuje nowoczesne techniki bez usuwania zęb&oacute;w.</strong></div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Należy do Polskiego Towarzystwa Ortodontycznego. Regularnie doskonali swoją wiedzę i umiejętności, uczestnicząc w szkoleniach i kursach.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Podkreśla, iż ma ogromne szczęście łączyć zaw&oacute;d z wielką pasją.</div>', 'Lekarz stomatolog', 'ortodoncja', 'maja-maciejewska-uruska', '54db2cfe3783c.jpg', 0, '2014-11-13 22:09:32', '2019-02-07 13:55:15', 2, '54db2cfe38d31.jpg'),
(6, 'Iwona Sławińska-Niedzin', '<p>\r\n	<strong>Ukończyła wydział stomatologii Pomorskiej Akademii Medycznej w Szczecinie w 1998 roku. </strong>Kontynuuje tradycje rodzinne. Jest opr&oacute;cz dw&oacute;ch si&oacute;str trzecim stomatologiem w rodzinie. Od 2001r. właściciel prywatnego gabinetu, a następnie kliniki stomatologicznej w Grudziądzu. Regularnie uczestniczy w licznych kursach i sympozjach, nieustannie podnosząc swoje kwalifikacje zawodowe w dziedzinie stomatologii estetycznej, protetyki, endodoncji oraz stomatologii dziecięcej.</p>\r\n<p>\r\n	Pani Doktor traktuje pracę jak pasję i misję.<strong> Całą uwagę poświęca pacjentom i ich problemom, kt&oacute;re wsp&oacute;lnie rozwiązują.</strong></p>', 'Właścicielka sieci gabinetów Cosmodent\r\nLekarz stomatolog', 'stomatologia estetyczna\r\nprotetyka\r\nendodoncja\r\nstomatologia dziecięca', 'iwona-slawinska-niedzin', '54da6901a4a4c.png', 1, '2014-11-13 23:52:59', '2015-04-01 10:47:30', 1, '54db49569b6c0.jpg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `username_canonical` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_canonical` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `two_step_code` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `two_step_code`, `first_name`, `last_name`) VALUES
(1, 'admin', 'admin', 'test@acme.com', 'test@acme.com', 1, 'rlp1tysey1wwc0wsw4gg8k8cw0scwcs', 'e81f4a522c90d34fd1d7748144ecd915d3e32299cd72aa8662a63935f9614766eb8be6d6b6e3a37d592125ba79034118a4187f19659739ba4e151167328e10ec', '2020-02-26 10:38:38', 0, 0, NULL, '5u5ll7wikuscgckgo0wwssowg4s40400co88oskws8kkkw4gog', NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-02-11 01:02:11', '2020-02-26 10:38:38', NULL, NULL, NULL),
(3, 'iniedzin', 'iniedzin', 'rejestracja@cosmodent.com.pl', 'rejestracja@cosmodent.com.pl', 1, 'qr1l3nwlv3kogsckscwwkgckg4gks0c', '0689bf5bf4fb949c706572bd2957c812ee90cae06f0ebe8c3d30e9cc414feda88edcd6caac12b6e010a20902f78ffb49e22e30d5e9e5888f1ebee783e5b138f0', '2015-10-21 20:37:51', 0, 0, NULL, '5zyieckw7e4ogocoowg8k0sgo44s0wcssck0o80c8wwcsoc44s', NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-02-18 21:12:21', '2015-10-21 20:37:51', NULL, NULL, NULL),
(4, 'moderator', 'moderator', 'mail@mail.com', 'mail@mail.com', 1, 'qnswrln3ya8ckwgcws8cw04g840gwg4', '02419ed35f76b37961e8b9ee234a3db23cf7b956235ad341b5e66cd79086616d244d38cc180f2f905fbc324581823c3bc3ce758f8a90d99931d7d5cff517cec3', '2015-12-14 13:23:14', 0, 0, NULL, '10p9nu7e43wg4cwg80o4w8ooc0o8go0o44o4s08kswo0kwo8wk', NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2015-10-21 18:45:52', '2015-12-14 13:23:14', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Galeria`
--

CREATE TABLE `Galeria` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(255) NOT NULL,
  `opis` longtext,
  `slug` varchar(255) NOT NULL,
  `kolejnosc` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `wyswietlana` tinyint(1) DEFAULT NULL,
  `zalaczalna` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Galeria`
--

INSERT INTO `Galeria` (`id`, `nazwa`, `opis`, `slug`, `kolejnosc`, `obrazek`, `wyswietlana`, `zalaczalna`, `updated_at`) VALUES
(2, 'Certyfikaty', NULL, 'certyfikaty', 0, NULL, 1, 1, '2013-02-13 15:25:01'),
(5, 'Sprzęt', NULL, 'sprzet', 0, NULL, 1, 1, '2013-10-28 19:01:16'),
(6, 'Gabinet', NULL, 'gabinet', 0, NULL, 1, 1, '2013-12-29 13:06:35'),
(7, 'Otwarcie kliniki', NULL, 'otwarcie-kliniki', 0, NULL, 1, 1, '2013-12-30 22:26:25'),
(8, 'Pulpit', NULL, 'pulpit', 0, NULL, 1, 0, '2019-08-28 16:36:51'),
(9, 'Nasz zespół', NULL, 'nasz-zespol', 0, NULL, 1, 0, '2019-08-28 16:53:40');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `GaleriaZdjecie`
--

CREATE TABLE `GaleriaZdjecie` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) DEFAULT NULL,
  `podpis` varchar(1024) DEFAULT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `GaleriaZdjecie`
--

INSERT INTO `GaleriaZdjecie` (`id`, `galeria_id`, `podpis`, `url`, `kolejnosc`, `obrazek`, `updated_at`) VALUES
(7, 2, NULL, NULL, NULL, '7-511ba54f0a860.jpg', '2013-02-13 15:42:47'),
(8, 2, NULL, NULL, NULL, '8-511ba54f0c7a3.jpg', '2013-02-13 15:42:47'),
(9, 2, NULL, NULL, NULL, '9-511ba54f0dfd0.jpg', '2013-02-13 15:42:47'),
(10, 2, NULL, NULL, NULL, '10-511ba54f0f64d.jpg', '2013-02-13 15:42:47'),
(11, 2, NULL, NULL, NULL, '11-511ba54f111aa.jpg', '2013-02-13 15:42:47'),
(12, 2, NULL, NULL, NULL, '12-511ba54f12912.jpg', '2013-02-13 15:42:47'),
(13, 2, NULL, NULL, NULL, '13-511ba54f14091.jpg', '2013-02-13 15:42:47'),
(14, 2, NULL, NULL, NULL, '14-511ba54f15be9.jpg', '2013-02-13 15:42:47'),
(15, 2, NULL, NULL, NULL, '15-511ba54f17744.jpg', '2013-02-13 15:42:47'),
(16, 2, NULL, NULL, NULL, '16-511ba54f18eac.jpg', '2013-02-13 15:42:47'),
(17, 2, NULL, NULL, NULL, '17-511ba54f1a284.jpg', '2013-02-13 15:42:47'),
(18, 2, NULL, NULL, NULL, '18-511ba54f1b9f6.jpg', '2013-02-13 15:42:47'),
(19, 2, NULL, NULL, NULL, '-511ba5db8984a.jpg', '2013-02-13 15:42:47'),
(20, 2, NULL, NULL, NULL, '-511ba5db8abc0.jpg', '2013-02-13 15:42:47'),
(21, 2, NULL, NULL, NULL, '-511ba5db8b77a.jpg', '2013-02-13 15:42:47'),
(22, 2, NULL, NULL, NULL, '-511ba5db8c330.jpg', '2013-02-13 15:42:47'),
(23, 2, NULL, NULL, NULL, '-511ba5db8ceea.jpg', '2013-02-13 15:42:47'),
(24, 2, NULL, NULL, NULL, '-511ba5db8daa2.jpg', '2013-02-13 15:42:47'),
(25, 2, NULL, NULL, NULL, '-511ba5db8e659.jpg', '2013-02-13 15:42:47'),
(26, 2, NULL, NULL, NULL, '-511ba5db8f218.jpg', '2013-02-13 15:42:47'),
(27, 2, NULL, NULL, NULL, '-511ba5db8fdc9.jpg', '2013-02-13 15:42:47'),
(28, 2, NULL, NULL, NULL, '-511ba5db90960.jpg', '2013-02-13 15:42:47'),
(29, 2, NULL, NULL, NULL, '-511ba5db9196a.jpg', '2013-02-13 15:42:47'),
(30, 2, NULL, NULL, NULL, '-511ba5db924e0.jpg', '2013-02-13 15:42:47'),
(31, 2, NULL, NULL, NULL, '-511ba66775c2f.jpg', '2013-02-13 15:42:47'),
(32, 2, NULL, NULL, NULL, '-511ba66776fb7.jpg', '2013-02-13 15:42:47'),
(33, 2, NULL, NULL, NULL, '-511ba66777b9f.jpg', '2013-02-13 15:42:47'),
(34, 2, NULL, NULL, NULL, '-511ba6677875b.jpg', '2013-02-13 15:42:47'),
(35, 2, NULL, NULL, NULL, '-511ba66779321.jpg', '2013-02-13 15:42:47'),
(36, 2, NULL, NULL, NULL, '-511ba66779eea.jpg', '2013-02-13 15:42:47'),
(37, 2, NULL, NULL, NULL, '-511ba6677aa7d.jpg', '2013-02-13 15:42:47'),
(38, 2, NULL, NULL, NULL, '-511ba6677b63a.jpg', '2013-02-13 15:42:47'),
(39, 2, NULL, NULL, NULL, '-511ba6677c201.jpg', '2013-02-13 15:42:47'),
(40, 2, NULL, NULL, NULL, '-511ba6677cda3.jpg', '2013-02-13 15:42:47'),
(41, 2, NULL, NULL, NULL, '-511ba6677d95a.jpg', '2013-02-13 15:42:47'),
(42, 2, NULL, NULL, NULL, '-511ba6677e515.jpg', '2013-02-13 15:42:47'),
(43, 2, NULL, NULL, NULL, '-511ba6677f0c9.jpg', '2013-02-13 15:42:47'),
(56, 5, NULL, NULL, NULL, '-526ea6b6ab7b9.jpg', '2013-10-28 19:12:34'),
(57, 5, NULL, NULL, NULL, '57-526ea912eba3f.jpg', '2013-10-28 19:12:34'),
(58, 5, NULL, NULL, NULL, '58-526ea76e21412.jpg', '2013-10-28 19:12:35'),
(59, 5, NULL, NULL, NULL, '59-526ea74d7b353.jpg', '2013-10-28 19:12:35'),
(60, 5, NULL, NULL, NULL, '60-526ea7f3becc0.jpg', '2013-10-28 19:12:35'),
(61, 5, NULL, NULL, NULL, '61-526ea7cd186f3.jpg', '2013-10-28 19:12:35'),
(62, 5, NULL, NULL, NULL, '-526ea6b6ae8a1.jpg', '2013-10-28 19:12:35'),
(63, 6, NULL, NULL, NULL, '-52c01258a4224.jpg', '2015-01-27 18:33:00'),
(65, 6, NULL, NULL, NULL, '-52c01258a6bc1.jpg', '2015-01-27 18:33:00'),
(66, 6, NULL, NULL, NULL, '-52c01258a7043.jpg', '2015-01-27 18:33:00'),
(67, 6, NULL, NULL, NULL, '-52c01258a7468.jpg', '2015-01-27 18:33:00'),
(68, 6, NULL, NULL, NULL, '-52c01258a7893.jpg', '2015-01-27 18:33:00'),
(69, 6, NULL, NULL, NULL, '-52c01258a7cab.jpg', '2015-01-27 18:33:00'),
(70, 6, NULL, NULL, NULL, '-52c01258a8094.jpg', '2015-01-27 18:33:00'),
(71, 6, NULL, NULL, NULL, '-52c01258a84a1.jpg', '2015-01-27 18:33:00'),
(72, 6, NULL, NULL, NULL, '-52c01258a8901.jpg', '2015-01-27 18:33:00'),
(73, 6, NULL, NULL, NULL, '-52c01258a8d4a.jpg', '2015-01-27 18:33:00'),
(74, 6, NULL, NULL, NULL, '-52c01258a9166.jpg', '2015-01-27 18:33:00'),
(75, 6, NULL, NULL, NULL, '-52c01258a9527.jpg', '2015-01-27 18:33:00'),
(76, 6, NULL, NULL, NULL, '-52c01258a98fd.jpg', '2015-01-27 18:33:00'),
(77, 7, NULL, NULL, 16, '-52c1e8f49c416.jpg', '2013-12-30 22:52:02'),
(78, 7, NULL, NULL, 15, '-52c1e8f4a01a3.jpg', '2013-12-30 22:52:02'),
(79, 7, NULL, NULL, 14, '-52c1e8f4a058e.jpg', '2013-12-30 22:52:02'),
(80, 7, NULL, NULL, 13, '-52c1e8f4a0a7a.jpg', '2013-12-30 22:52:02'),
(81, 7, NULL, NULL, 12, '-52c1e8f4a0ef6.jpg', '2013-12-30 22:52:02'),
(82, 7, NULL, NULL, 11, '-52c1e8f4a12c8.jpg', '2013-12-30 22:52:02'),
(83, 7, NULL, NULL, 10, '-52c1e8f4a169d.jpg', '2013-12-30 22:52:02'),
(84, 7, NULL, NULL, 9, '-52c1e8f4a1c55.jpg', '2013-12-30 22:52:02'),
(85, 7, NULL, NULL, 8, '-52c1e8f4a1ff6.jpg', '2013-12-30 22:52:02'),
(86, 7, NULL, NULL, 7, '-52c1e8f4a2456.jpg', '2013-12-30 22:52:02'),
(87, 7, NULL, NULL, 6, '-52c1e8f4a27f9.jpg', '2013-12-30 22:52:02'),
(88, 7, NULL, NULL, 5, '-52c1e8f4a2ba8.jpg', '2013-12-30 22:52:02'),
(89, 7, NULL, NULL, 4, '-52c1e8f4a2f5e.jpg', '2013-12-30 22:52:02'),
(90, 7, NULL, NULL, 3, '-52c1e8f4a3487.jpg', '2013-12-30 22:52:02'),
(91, 7, NULL, NULL, 2, '-52c1e8f4a39db.jpg', '2013-12-30 22:52:02'),
(92, 7, NULL, NULL, 1, '-52c1e8f4a3e06.jpg', '2013-12-30 22:52:02'),
(93, 6, NULL, NULL, NULL, '93-54c7cbcc462cf.jpg', '2015-01-27 18:33:00'),
(94, 8, 'Stomatologia', NULL, 0, '94-5d6694a28287e.png', '2019-08-28 16:50:10'),
(95, 8, 'Medycyna estetyczna', NULL, 1, '95-5d6694b921346.png', '2019-08-28 16:50:33'),
(96, 9, 'our_team', NULL, 0, '96-5d66969d8899d.png', '2019-08-28 16:58:37');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `anchor` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `attributes` varchar(255) DEFAULT NULL,
  `route` varchar(1024) DEFAULT NULL,
  `routeParameters` varchar(1024) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `parentItem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `anchor`, `title`, `type`, `location`, `sort_order`, `url`, `attributes`, `route`, `routeParameters`, `lft`, `rgt`, `depth`, `parentItem_id`) VALUES
(1, 'Kompetencje', 'Kompetencje', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kompetencje\"}', 4, 4, 1, NULL),
(2, 'Usługi', 'Usługi', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"uslugi\"}', 8, 8, 1, NULL),
(3, 'Aktualności', 'Aktualności', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_aktualnosci_lista', '', 10, 10, 1, NULL),
(4, 'Kontakt', 'Kontakt', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kontakt\"}', 12, 12, 1, NULL),
(5, 'Kompetencje', 'Kompetencje', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kompetencje\"}', 36, 36, 1, NULL),
(6, 'Stomatologia', 'Stomatologia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"stomatologia\"}', 2, 2, 1, NULL),
(7, 'Wybielanie zębów', 'Wybielanie zębów', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"wybielanie-zebow\"}', 7, 7, 2, 6),
(8, 'Kontakt', 'Kontakt', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kontakt\"}', 42, 42, 1, NULL),
(12, 'Stomatologia dziecięca', 'Stomatologia dziecięca', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"stomatologia-dziecieca\"}', 3, 3, 2, 6),
(13, 'Stomatologia estetyczna', 'Stomatologia estetyczna', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"stomatologia-estetyczna\"}', 5, 5, 2, 6),
(14, 'Periodontologia', 'Periodontologia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"periodontologia\"}', 15, 15, 2, 6),
(15, 'Implantologia', 'Implantologia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"implantologia\"}', 9, 9, 2, 6),
(16, 'Leczenie kanałowe', 'Leczenie kanałowe', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"leczenie-kanalowe\"}', 11, 11, 2, 6),
(17, 'Protetyka', 'Protetyka', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"protetyka\"}', 13, 13, 2, 6),
(18, 'Ozdoby na zęby', 'Ozdoby na zęby', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"ozdoby-na-zeby\"}', 17, 17, 2, 6),
(19, 'Profilaktyka', 'Profilaktyka', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"profilaktyka\"}', 21, 21, 2, 6),
(20, 'Mezoterapia igłowa', 'Mezoterapia igłowa', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"mezoterapia-iglowa\"}', 27, 27, 2, 28),
(24, 'Ortodoncja', 'Ortodoncja', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"ortodoncja\"}', 19, 19, 2, 6),
(25, 'Galeria', 'Galeria', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"gabinet\"}', 40, 40, 1, NULL),
(26, 'Wyposażenie', 'Wyposażenie', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"sprzet\"}', 6, 6, 1, NULL),
(27, 'Nasz zespół', 'Nasz zespół', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_employee_index', '', 38, 38, 1, NULL),
(28, 'Medycyna estetyczna', 'Medycyna estetyczna', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"medycyna-estetyczna\"}', 24, 24, 1, NULL),
(29, 'Peeling chemiczny', 'Peeling chemiczny', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"peeling-chemiczny\"}', 25, 25, 2, 28),
(30, 'Kwas hialuronowy', 'Kwas hialuronowy', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kwas-hialuronowy\"}', 29, 29, 2, 28),
(31, 'Botoks', 'Botoks', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"botoks\"}', 31, 31, 2, 28),
(32, 'Osocze bogatopłytkowe', 'Osocze bogatopłytkowe', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"osocze-bogatoplytkowe\"}', 33, 33, 2, 28);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext,
  `automaticSeo` tinyint(1) NOT NULL,
  `topImage` varchar(255) DEFAULT NULL,
  `onMainPage` tinyint(1) DEFAULT NULL,
  `onMainPageOrder` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `title`, `published`, `publish_date`, `keywords`, `description`, `price`, `slug`, `content`, `automaticSeo`, `topImage`, `onMainPage`, `onMainPageOrder`, `type`) VALUES
(1, 'First simple page', 1, '2013-02-11 01:02:11', 'First, simple, page, This, very, first, containing, HTML, Second, paragraph', '\n This is very first page containing raw HTML \n\n Second paragraph \n\n\n    List first element\n    List second element\n    List third element\n\n\n A link to very nice web page: google\n\n\n\n', '0.00', 'first-simple-page', '\n<p> This is very first page containing raw HTML </p>\n\n<p> Second paragraph </p>\n\n<ul>\n    <li>List first element</li>\n    <li>List second element</li>\n    <li>List third element</li>\n</ul>\n\n<p> A link to very nice web page: <a href=\"http://www.google.com\">google.com</a> (search engine) :D </p>\n\n\n\n', 0, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page_uploadedfile`
--

CREATE TABLE `page_uploadedfile` (
  `page_id` int(11) NOT NULL,
  `uploadedfile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Strona`
--

CREATE TABLE `Strona` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) DEFAULT NULL,
  `strona_nadrzedna_id` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title2` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publishDate` datetime DEFAULT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` longtext,
  `automaticSeo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Strona`
--

INSERT INTO `Strona` (`id`, `galeria_id`, `strona_nadrzedna_id`, `obrazek`, `updated_at`, `title2`, `title`, `published`, `publishDate`, `keywords`, `description`, `slug`, `content`, `automaticSeo`) VALUES
(1, 2, NULL, '52c00cc56e91f.jpg', NULL, NULL, 'Kompetencje', 1, '2013-02-11 21:20:00', 'Kompetencje,', NULL, 'kompetencje', '<p>\r\n	<span style=\"display: none;\">&nbsp;</span></p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Lek. stom. Iwona Sławińska-Niedzin</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div style=\"text-align: justify;\">\r\n	Kontynuując tradycje w swoim domu, skończyła studia na wydziale <strong>stomatologii Pomorskiej Akademii Medycznej w Szczecinie</strong>, zostając w 1998 roku już trzecim stomatologiem w swojej rodzinie. Od tamtej pory sumiennie i regularnie uczestniczy w rozmaitych kursach i sympozjach, celem nieustannego <strong>podnoszenia swoich kwalifikacji zawodowych.</strong> Sw&oacute;j prywatny gabinet otworzyła w 2001 roku, a następnia stała się właścicielem kliniki stomatologicznej w Grudziądzu.</div>\r\n<div style=\"text-align: justify;\">\r\n	&nbsp;</div>\r\n<div style=\"text-align: justify;\">\r\n	&nbsp;</div>\r\n<div style=\"text-align: justify;\">\r\n	<span class=\"pink\">Prywatnie, dumna mama Klaudyny i Filipa.</span> Uwielbia podr&oacute;że, taniec, jazdę na rolkach i nartach. Pasjonuje się stomatologią, traktując swoją pracę jak misję.</div>\r\n<div style=\"text-align: justify;\">\r\n	&nbsp;</div>\r\n<div style=\"text-align: justify;\">\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		<h2 class=\"title\" style=\"text-align: center;\">\r\n			Dobrze wykonana praca i szeroki uśmiech pacjenta sprawiają jej największą satysfakcję.<br />\r\n			&nbsp;</h2>\r\n	</div>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n	&nbsp;</div>\r\n<div style=\"text-align: justify;\">\r\n	<span class=\"link\">Poniżej znajdue się galeria dyplom&oacute;w oraz certyfikat&oacute;w.</span></div>', 0),
(2, NULL, NULL, NULL, NULL, NULL, 'Stomatologia', 1, '2014-02-11 21:22:00', 'Usługi,  	Tupacsum, Ipsum, until, that, Thug, Life, tatted, chest., seems', 'Cosmodent - praktyka stomatologiczna\r\n\r\nLista usług', 'stomatologia', '<p style=\"text-align: center;\">\r\n	<span style=\"font-size:16px;\"><strong>Kliknij na wybraną usługę</strong>, aby poznać szczeg&oacute;ły:</span></p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 500px;\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/stomatologia-dziecieca.html\"><img alt=\"Stomatologia dziecięca\" src=\"/uploads/pliki/Uslugi/1.jpg\" style=\"width: 387px; height: 65px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/stomatologia-estetyczna.html\"><img alt=\"Stomatologia estetyczna\" src=\"/uploads/pliki/Uslugi/2.jpg\" style=\"width: 391px; height: 65px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/wybielanie-zebow.html\"><img alt=\"Wybielanie zębów\" src=\"/uploads/pliki/Uslugi/3.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/implantologia.html\"><img alt=\"Implantologia\" src=\"/uploads/pliki/Uslugi/4.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/leczenie-kanalowe.html\"><img alt=\"Leczenie kanałowe\" src=\"/uploads/pliki/Uslugi/5.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/protetyka.html\"><img alt=\"Protetyka\" src=\"/uploads/pliki/Uslugi/6.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/periodontologia.html\"><img alt=\"Periodontologia\" src=\"/uploads/pliki/Uslugi/7.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/ozdoby-na-zeby.html\"><img alt=\"Ozdoby na zęby\" src=\"/uploads/pliki/Uslugi/8.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/profilaktyka.html\"><img alt=\"Profilaktyka\" src=\"/uploads/pliki/Uslugi/9.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/laseroterapia.html\"><img alt=\"Laseroterapia\" src=\"/uploads/pliki/Uslugi/laseroterapia.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/ortodoncja.html\"><img alt=\"\" src=\"/uploads/pliki/Uslugi/ortodoncja.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/implantologia.html\"><img alt=\"Implantologia\" src=\"/uploads/pliki/Uslugi/implantologia(1).jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 0),
(4, NULL, NULL, NULL, NULL, NULL, 'blok-strona-glowna', 0, '2014-02-11 21:41:00', NULL, NULL, 'blok-strona-glowna', '<ul>\r\n	<li>\r\n		<b>STOMATOLOGIA ZACHOWAWCZA&nbsp;</b> - leczenie pr&oacute;chnicy</li>\r\n	<li>\r\n		<b>ODBUDOWA&nbsp;</b>zniszczonych i złamanych koron zęb&oacute;w</li>\r\n	<li>\r\n		<b>STOMATOLOGIA DZIECIĘCA -&nbsp;</b>leczenie zęb&oacute;w mlecznych, profilaktyka wczesnodziecięca</li>\r\n	<li>\r\n		<b>ENDODONCJA</b> - nowoczesne metody opracowywania i wypełniania kanał&oacute;w korzeniowych, leczenie zmian okołowierzchołkowych</li>\r\n	<li>\r\n		<b>PROTETYKA</b>&nbsp;- wszystkie rodzaje uzupełnień</li>\r\n	<li>\r\n		<strong>IMPLANTOLOGIA</strong> - wszczepianie implant&oacute;w w miejsca brakujących zęb&oacute;w</li>\r\n	<li>\r\n		<b>PERIODONTOLOGIA&nbsp;</b> - leczenie chor&oacute;b przyzębia, zęby rozchwiane, zanik dziąseł, przykry zapach z ust, usuwanie kamienia</li>\r\n	<li>\r\n		<b>CHIRURGIA STOMATOLOGICZNA</b> - usuwanie zęb&oacute;w mlecznych i stałych</li>\r\n	<li>\r\n		<b>LECZENIE</b>&nbsp;zaburzeń stan&oacute;w s-ż (b&oacute;le ucha i głowy)</li>\r\n	<li>\r\n		<strong>ORTODONCJA&nbsp;</strong>- aparaty stałe i ruchome</li>\r\n	<li>\r\n		<b>PROFILAKTYKA&nbsp;</b>- u dzieci i dorosłych</li>\r\n	<li>\r\n		<b>WYBIELANIE I PIASKOWANIE </b>zęb&oacute;w</li>\r\n	<li>\r\n		<b>SZLACHETNE&nbsp;</b>ozdoby na zęby</li>\r\n</ul>', 0),
(6, NULL, NULL, NULL, NULL, NULL, 'Kontakt Rzeszów', 1, '2013-02-11 23:53:00', 'Kontakt, COSMODENT, Nowoczesna, Stomatologia, Łańcut,, ul	tel:, 666	e-mail:, rejestracja@cosmodent.com	Czynne, codziennie', 'COSMODENT - Nowoczesna Stomatologia Łańcut, ul\r\n	tel: 698 902 233\r\n	e-mail: rejestracja@cosmodent.com\r\n	Czynne codziennie', 'kontakt', '<h2 class=\"title\" style=\"text-align: center;\">\r\n	COSMODENT - Nowoczesna Stomatologia Rzesz&oacute;w</h2>\r\n<p style=\"text-align: center;\">\r\n	<span style=\"font-size:14px;\">Adres:</span></p>\r\n<p style=\"text-align: center;\">\r\n	<strong><span style=\"font-size: 14px;\">Rzesz&oacute;w, ul. Strażacka 12d/2 (1. piętro)</span></strong></p>\r\n<p style=\"text-align: center;\">\r\n	<br />\r\n	<span style=\"font-size:14px;\">tel: <strong>698 902 233</strong><br />\r\n	e-mail: <span class=\"pink\"><a href=\"mailto:rzeszow@cosmodent.com.pl\">rzeszow@cosmodent.com.pl</a></span></span><br />\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: center;\">\r\n	<span class=\"pink\">Czynne codziennie</span></h2>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	<iframe frameborder=\"0\" height=\"233\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2564.108689198446!2d22.003564!3d50.00931799999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xca96a476be776b08!2sCOSMODENT+-+Dentysta+Rzesz%C3%B3w!5e0!3m2!1spl!2s!4v1396982628104\" style=\"border:0\" width=\"775\"></iframe><br />', 0),
(7, NULL, 2, NULL, NULL, NULL, 'Stomatologia dziecięca', 1, '2013-02-12 18:50:00', 'Stomatologia, dziecięca, Dziecko, należy, wrażliwych, pacjent', 'Dziecko należy do wrażliwych pacjent', 'stomatologia-dziecieca', '<p style=\"text-align: justify;\">\r\n	<strong>Dziecko</strong> w gabinecie stomatologicznym zawsze <span class=\"pink\">powinno być traktowane w spos&oacute;b szczeg&oacute;lny</span> ze względu na silną reakcję na wszelkiego rodzaju bodźce &ndash; czy to fizyczne, jak i psychiczne. &nbsp;Dla małego pacjenta wizyta związana jest z uczuciem niepokoju, negatywne doznania pozostaną na długi czas w jego pamięci. Konsekwencją czego może być strach przed zabiegami oraz wizytami u lekarza stomatologa w dorosłym wieku.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Bardzo ważny jest właściwy stosunek lekarza wobec małego pacjenta.</h2>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Zadaniem lekarza</strong> jest zminimalizowanie lęku u dziecka oraz zapewnienie miłej atmosfery podczas wizyty. Maluch z pewnością będzie ciekaw wyglądu swoich ząbk&oacute;w, a stosowana kamera wewnątrzustna pozwala na zobaczenie ich w powiększeniu na ekranie monitora. Bardzo istotne jest bieżące informowanie rodzic&oacute;w o przebiegu leczenia ich malucha. Wychodząc naprzeciw oczekiwaniom naszych klient&oacute;w, dbamy o zapewnienie jak najlepszych warunk&oacute;w dziecku, wzbudzenie dobrych skojarzeń w związku z wizytą oraz poczucia bezpieczeństwa, co skutkuje <span class=\"pink\">zaufaniem pacjenta</span> oraz <span class=\"pink\">chęcią systematycznego leczenia.</span></p>', 0),
(8, NULL, 2, NULL, NULL, NULL, 'Stomatologia estetyczna', 1, '2013-02-12 18:52:00', 'Stomatologia, estetyczna, Definiując, pojęcie, stomatologii, estetycznej, należy, stwierdzić,, gł', 'Definiując pojęcie stomatologii estetycznej należy stwierdzić, że gł', 'stomatologia-estetyczna', '<p style=\"text-align: justify;\">\r\n	<strong>Stomatologia estetyczna</strong> to dziedzina stomatologii obejmująca zabiegi, kt&oacute;rych <span class=\"pink\">gł&oacute;wnym celem jest poprawa wyglądu pacjenta.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>To właśnie uśmiech</strong> ma wpływ na oceną człowieka. Jego rola jest niezwykle istotna, gdyż &bdquo;piękniejszy uśmiech&rdquo; wywołuje pozytywne bodźce w relacjach międzyludzkich, przez co zasługuje na <span class=\"pink\">szczeg&oacute;lną uwagę i troskę.</span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Należy wiedzieć, iż wraz z wiekiem pacjenta zmniejsza się widoczność zęb&oacute;w g&oacute;rnych a co za tym idzie coraz bardziej uwidaczniane są zęb&oacute;w dolne, przykładem mogą być osoby po 60 roku życia. Zatem niezmiernie ważna w stomatologii estetycznej jest wizualna dokumentacja przed i po leczeniu, na podstawie kt&oacute;rej można dokonać analizy zmian w uzębieniu.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Podczas całego leczenia obowiązuje dogmat maksymalnego efektu przy minimum integracji. Dlatego też mając do czynienia z przebarwieniami na zębach powinniśmy wziąć pod uwagę kolejno: wybielanie zęb&oacute;w, pokrycie kompozytem, lic&oacute;wki kończąc na koronach. W przypadku zęb&oacute;w obr&oacute;conych przed przystąpieniem do szlifowania należy wziąć pod uwagę ich wcześniejsze leczenie ortodontyczne.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Stomatologia estetyczna wymaga idealnego wsp&oacute;łdziałania między poszczeg&oacute;lnymi specjalnościami stomatologii oraz wsp&oacute;łpracy z pacjentem, kt&oacute;rego obowiązkiem jest sprecyzowanie swoich oczekiwań.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Lic&oacute;wki kosmetyczne</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Zastosowanie lic&oacute;wek wzbudza coraz większe zainteresowanie wśr&oacute;d pacjent&oacute;w. W stomatologii estetycznej są one stałym uzupełnieniem protetycznym używanym do pokrycia nieestetycznych powierzchni wargowych i policzkowych zęb&oacute;w g&oacute;rnych i dolnych.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	Najczęściej są one stosowane w przednim odcinku zęb&oacute;w szczęki. Są korzystną alternatywą dla stosowanych koron protetycznych, gdyż mają niewielki stopień utraty tkanek twardych zęba w skutek szlifowania.</div>\r\n<p style=\"text-align: justify;\">\r\n	Korona protetyczna narzuca konieczność inwazyjnej preparacji całego zęba, zaś w przypadku cieniutkiej lic&oacute;wki stomatolog delikatnie usuwa niewielką warstwę szkliwa o grubości 0,3-0,7 mm. Technika laminatowa należy do uzupełnień protetycznych, kt&oacute;re oszczędzają tkankę twardą zęba. Materiałem stosowanym do wykonania lic&oacute;wek jest porcelana lub kompozyt.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Jakie są wskazania do zastosowania lic&oacute;wek? Zaliczamy do nich:</strong></span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			przebarwienia nazębne trudne do wybielenia, np. tetracyklinowe, po leczeniu kanałowym,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			niedorozw&oacute;j szkliwa i zębiny (amalogenesis imperfecta),</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			nieestetyczne wypełnienia przednich zęb&oacute;w,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			ubytki brzeg&oacute;w zęb&oacute;w oraz ukruszenia kąt&oacute;w brzeg&oacute;w siecznych,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			diastemy między zębami,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			wydłużenie startych patologicznie zęb&oacute;w,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			nieakceptowalny przez pacjenta kształt zęb&oacute;w przednich.</div>\r\n	</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Przeciwwskazania do zastosowania lic&oacute;wek:</strong></span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			zła higiena jamy ustnej,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			parafuncje zwarciowe, np. bruksizm</div>\r\n	</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"link\">Przy zastosowaniu wszelkich uzupełnień protetycznych oraz lic&oacute;wek bardzo istotna jest właściwa higiena jamy ustnej. Odpowiednie szczotkowanie, nitkowanie, wykonywanie dodatkowych zabieg&oacute;w higienicznych spowoduje, że uzupełnienie uzębienia będzie służyło pacjentowi przez wiele lat.</span></p>', 0),
(9, NULL, 2, NULL, NULL, NULL, 'Wybielanie zębów', 1, '2013-02-12 19:13:00', 'Wybielanie, zębów, Dlaczego, uśmiech, jest, ważny?', 'Dlaczego uśmiech jest tak ważny?', 'wybielanie-zebow', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy uśmiech naprawdę jest tak istotny?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Nasz wygląd odgrywa wielką rolę w naszym życiu. </strong>Zdarza się, że z wielkim podziwem i odrobiną zazdrości zwracamy uwagę na osoby o pięknym, promiennie białym uśmiechu. Sami często nie zdajemy sobie sprawy jak wielką rolę pełni on w naszym życiu. Gdy uśmiechamy się do innych ludzi, wysyłamy im pozytywne sygnały, że cieszymy się ich towarzystwem, wsp&oacute;lnie spędzonymi chwilami. Jesteśmy szczęśliwi, zadowoleni z siebie i na tyle pewni, aby poszerzyć swoje horyzonty, stawić czoła wyzwaniom - tym codziennym oraz tym większym. Uśmiech jest r&oacute;wnież synonimem otwartości na innych, zbliża ludzi, emanuje pozytywną aurą. Dzięki temu wzbudzamy zaufanie i jesteśmy odbierani jako ludzie pogodni.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Miarą atrakcyjności uśmiechu jest kilka czynnik&oacute;w, ale najważniejszym z nich jest biel naszych zęb&oacute;w. To ona ma największy wpływ na jego odbi&oacute;r. Dzięki niej poprawiają się stosunki międzyludzkie, co może decydować np. o naszej pracy.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Przebarwione zęby powodują, że uśmiechamy się rzadko. Zasłaniamy nawet usta, wstydzimy, tracimy pewność siebie, wpadamy w zakłopotanie.<br />\r\n	Możemy to jednak zmienić! Wszystko dzięki profesjonalnym metodom, umożliwiającym uzyskanie śnieżnobiałego uśmiechu, a tym samym wyższej samooceny. Warto zainwestować w dobre samopoczucie drogą najtańszej &quot;operacji plastycznej&quot;, bo tak m&oacute;wi się o przeprowadzonym przez lekarza stomatologa zabiegu. Ty r&oacute;wnież możesz z niej skorzystać!</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Opalescence dostępny jest wyłącznie u lekarzy stomatolog&oacute;w, co jest gwarancją bezpieczeństwa całego procesu wybielania zęb&oacute;w.</span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Dlaczego Opalescence?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Amerykańska firma Ultradent</strong>, wytwarzająca nowoczesne materiały stomatologiczne, jest producentem preparat&oacute;w wybielających <strong>Opacescence.</strong> Preparaty te używamy w naszym gabinecie, ponieważ są to produkty najwyższej jakości. Idzie za nią długa tradycja firmy Ultradent, kt&oacute;ra w tej dziedzienie jest pionierem i liderem. Pierwszy bezpieczny żel wybielający do stosowania w komfortowych, indywidualnych nakładach został przez nią wyprodukowany już w 1990r. Na dow&oacute;d bardzo wysokiej jakości oraz bezpieczeństwa wytwarzanych materiał&oacute;w, marka została odznaczona w 1996 r. nadaniem prestiżowego certyfikatu ADA <strong>(Amerykańskie Towarzystwo Stomatologiczne</strong>). Wyprodukowany przez Ultradent preparat 15%PF jest pierwszym takim produktem, kt&oacute;ry zawiera składniki zapobiegające nadwrażliwości zęb&oacute;w. Zwiększa to znacznie komfort kuracji. Dostępność w smakach miętowym i arbuzowym czyni produkt jeszcze bardziej atrakcyjnym.</p>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	W naszym kraju preparaty firmy Ultradent stosuje się już do 1990 roku. Mogą być Państwo pewni efektu i spokojni o swoje zęby, ponieważ metoda wybielania z użyciem produkt&oacute;w Opalescence została dokładnie przebadana na akademiach medycznych. Badania potwierdziły skuteczność produktu, bezpieczeństwo i długowieczny efekt. Odpowiednie atesty i certyfikaty (zar&oacute;wno Polskie i międzynarodowe) są gwarancją najwyższej jakości.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Skąd się biorą przebarwienia na zębach?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Przebarwienia na zębach mogą powstawać z kilku powod&oacute;w. Przede wszystkim, ich przyczyną może być nieprawidłowy rozw&oacute;j uzębienia. W przypadku długotrwałego leczenia antybiotykami z grupy tetracyklin w dzieciństwie r&oacute;wnież jesteśmy na nie narażeni. Zdarze się r&oacute;wnież, że niekt&oacute;re osoby mają naturalnie ciemniejsze uzębienie, zaś z wiekiem barwa ta zmienia się na skutek picia napoj&oacute;w, sok&oacute;w, kawy czy herbaty. Palenie papieros&oacute;w r&oacute;wnież ma na to ogromny wpływ. Ostatnią przyczyną są uszkodzenia miazgi zęba oraz złe wypełnienia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy zabieg wybielania zęb&oacute;w jest bezpieczny?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Wykonywany przez lekarza stomatologa zabieg z użyciem preparatu Opalescence <strong>jest w pełni bezpieczny</strong>. Jego właściwe zastosowane usuwa trwale i bezpiecznie większość przebarwień. Zostają one wybielone drogą utlenienia (atomy tlenu wnikają w głąb szkliwa i zębiny). Metoda ta gwarantuje nienaruszenie właściwej struktury zęba.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Jak wspominaliśmy już wcześniej, gwarancją bezpieczeństwa produkt&oacute;w amerykańskiej marki są wyniki wieloletnich badań klinicznych przeprowadzonych na uczelniach w Polsce i za granicą. Ważne jest, aby stosować preparat pod czujnym okiem lekarza stomatologa.</div>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div>\r\n	W przypadku wybielania nakładkowego opieramy się na żelu z czynnikiem aktywnym w postaci <strong>nadtlenku karbamidu</strong>, kt&oacute;ry przez długie lata wykrozystywany był do dezynfekcji jamy ustnej. <span class=\"pink\">Jest to więc w pełni bezpieczny związek chemiczny, kt&oacute;ry obecnie pomaga milion os&oacute;b zyskać śnieżnobiały uśmiech.</span></div>', 0),
(10, NULL, 2, NULL, NULL, NULL, 'Implantologia', 1, '2013-02-12 19:30:00', 'Implantologia, Implanty, twoje, \"NOWE, ZĘBY\"., Dzięki, można, wielu, przypadkach, odbudować', 'Implanty to twoje \"NOWE ZĘBY\". Dzięki nim można w wielu przypadkach odbudować ząb w miejsce wcześniej usuniętego. To tak jakby wyr', 'implantologia', '<p style=\"text-align: justify;\">\r\n	<strong>Implanty to twoje &quot;NOWE ZĘBY&quot;</strong>. Dzięki nim można w wielu przypadkach odbudować ząb w miejsce wcześniej usuniętego. To tak jakby wyr&oacute;sł nowy ząb.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	<img alt=\"Implantologia Cosmodent\" dir=\"ltr\" src=\"/uploads/pliki/Implantologia/Dental_implant2-300x300.jpg\" style=\"width: 300px; height: 300px; margin-left: 10px; margin-right: 10px;\" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Implant pełni funkcję korzenia</strong> zęba, na kt&oacute;rym można zamocować korony, mosty oraz protezy. Niewątpliwą zaletą takiego rozwiązania jest komfort pacjenta, kt&oacute;ry nie czuje r&oacute;żnicy między implantem a naturalnymi zębami.&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Najczęściej implanty wykonywane są z materiału (tytan lub cyrkon), kt&oacute;ry organizm traktuje jak własną kość i zrasta się z nim r&oacute;wnie trwale, tak jak zrastają się złamane części kości. W przypadku braku dobrych warunk&oacute;w kostnych, można odbudować kość i stworzyć miejsce dla implantu zębowego.&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	W naszym gabinecie oferujemy&nbsp; implanty firmy &quot;<strong>Nobel Biocare</strong>&quot;, &quot;<strong>Straumann</strong>&quot;, &quot;<strong>AlfaBio</strong>&quot;, &quot;<strong>Osteoplant</strong>&quot;, &quot;<strong>ImTec</strong>&quot;. Dzięki szerokiemu asortymentowi możemy dobrać&nbsp;implant odpowiedni do Państwa potrzeb.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	<img alt=\"Porównanie naturalnego zęba z implantem\" src=\"/uploads/pliki/Implantologia/porownanie_zeba_i_implantu(1).jpg\" style=\"width: 650px; height: 688px;\" /></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div>\r\n	<span style=\"font-size:16px;\"><strong>Najczęściej zadawane pytania</strong></span></div>\r\n<div>\r\n	&nbsp;</div>\r\n<h2 class=\"title1\">\r\n	Czy można wprowadzić implant natychmiast po usunięciu zęba?</h2>\r\n<div>\r\n	Jest to możliwe, choć nie w każdym przypadku. Wymaga to skonsultowania się z lekarzem prowadzącym, jeszcze przed usunięciem zęba. Zostaną wtedy wykonane właściwe zdjęcia RTG i przeprowadzona analiza warunk&oacute;w.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<h2 class=\"title1\">\r\n	Jak trwałe są implanty?</h2>\r\n<div>\r\n	Wprowadzony implant po kilku miesiącach trwale zrasta się z kością i staje się jej częścią. Przy prawidłowo zaplanowanym i wykonanym rozwiązaniu protetycznym, może on spełniać swoją funkcję tak długo, jak długo będzie istnieć utrzymująca go kość. Przy prawidłowej higienie i systematycznych, co 6-miesięcznych wizytach w gabinecie, może on służyć do końca życia. Pierwsze implanty wprowadzono 40 lat temu i do teraz spełniają one swoją funkcję w jamie ustnej.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<h2 class=\"title1\">\r\n	Jakie są przeciwwskazania do założenia implantu?</h2>\r\n<div>\r\n	Obecnie nie ma przeciwwskazań do wprowadzenia implant&oacute;w. Są ograniczenia &ndash; stany, kt&oacute;re wymagają od pacjenta podjęcia określonych działań, jak np. ograniczenie palenia, ponieważ u os&oacute;b palących wzrasta ryzyko odrzucenia wszczepu. W przypadku takich chor&oacute;b, jak: cukrzyca, choroby układu krążenia, choroby staw&oacute;w, osteoporoza niezbędna może być konsultacja i wsp&oacute;łpraca z prowadzącym specjalistą.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<h2 class=\"title1\">\r\n	Jaki jest odsetek niepowodzeń?</h2>\r\n<div>\r\n	Jest to obecnie bardzo dobrze udokumentowana i wszechstronnie przebadana metoda uzupełnienia brakującego zęba. Jej skuteczność określa się w co najmniej 96% przypadk&oacute;w.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<h2 class=\"title1\">\r\n	Czy nieprzyjęcie się implantu jest groźne dla zdrowia?</h2>\r\n<div>\r\n	Nieprzyjęcie się implantu nie jest groźne dla zdrowia, nie skutkuje żadnymi innymi konsekwencjami, jak tylko tymi związanymi ze zmianą koncepcji leczenia chirurgicznego i protetycznego.</div>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	<strong>Zapraszamy Państwa na konsultację -&nbsp;698 902 233.</strong></h2>', 0),
(11, NULL, 2, NULL, NULL, NULL, 'Leczenie kanałowe', 1, '2013-02-12 19:31:00', 'Leczenie, kanałowe, Obecnie, wiele, zęb', 'Obecnie wiele zęb', 'leczenie-kanalowe', '<p style=\"text-align: justify;\">\r\n	<strong>Endodoncja</strong> do wąska dziedzina stomatologii nazywana powszechnie leczeniem kanałowym. Polega ono na usunięciu zainfekowanej lub martwej miazgi z komory i systemu kanałowego korzeni zęb&oacute;w, a następnie na mechanicznym i chemicznym opracowaniu tego systemu. Na samym końcu leczenia następuje szczelne wypełnienie specjalnymi materiałami, opracowanymi w tym celu. <span class=\"link\">Endodoncja pozwala uratować i wyleczyć zęby, kt&oacute;re w przeszłości były skazane na usunięcie.</span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Najważniejszym etapem</strong> w leczeniu kanałowym jest <span class=\"pink\">mechaniczne oraz chemiczne oczyszczenie i poszerzenie kanał&oacute;w korzeniowych</span> na całej ich długości. Umożliwiają to specjalne, precyzyjne oraz wciąż udoskonalane <strong>narzędzia kanałowe</strong>. Podczas mechanicznego opracowania systemu kanał&oacute;w korzeniowych dokonywane jest r&oacute;wnież płukanie środkami chemicznymi, do kt&oacute;rych należą: podchloryn sodu, kwas cytrynowy i alkohol izopropylowy. Ich zadaniem jest dezynfekcja i pomoc w oczyszczneiu kanału. System musi być bowiem dokładnie oczyszczony na całej długości. Nie byłoby to możliwe dzięki endometrowi, przyrządowi precyzyjnie mierzącemu długość kanału korzeniowego. Wykorzystuje on zjawisko r&oacute;żnej przewodoności prądu elektrycznego przez tkanki zębowe, jak r&oacute;wnież i te znajdujące się poza zębem. W przeszłości stosowano metodę pomiaru długości kanału opartą na zdjęciu Rentgenowskim. Jest ona niestety zawodna i wielce nieprecyzyjna.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	<strong>W leczeniu endodontycznym</strong> niezbędnymi urządzeniami są r&oacute;wnież <span class=\"pink\">lupy o dużym powiększeniu i zimnym oświetleniu oraz mikroskopy zabiegowe.</span> Z ich pomocą lokalizowane są ujścia kanałowe, co pomaga w dokładnym opracowaniu całego systemu. Warunkiem prawidłowego leczenia jest możliwość zajrzenia do wnętrza kanału, co bez wyżej wymienionych sprzęt&oacute;w nie byłoby osiągalne.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Po opracowaniu &quot;mapy&quot; kanał&oacute;w, ich oczyszczeniu i osuszeniu następuje wypełnienie za pomocą odpowiednich materiał&oacute;w. Są nimi gutaperka lub resilon. Należy je wprowadzić do kanału na zmierzoną głębokość z odrobiną uszczelniacza. Materiał upycha się po podgrzaniu i ulastycznieniu. Warunkiem dobrze wykonanego zabiegu jest niepozostawienie w kanale pęcherzyk&oacute;w powietrza.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	<span class=\"link\">Wyleczone w taki spos&oacute;b zęby będą nam jeszcze długo służyć. Zachęcamy więc do korzystania ze zdobyczy nowoczesnej stomatologii zamiast usuwania zęb&oacute;w, kt&oacute;re może być przyczyną nieprzewidzianych problem&oacute;w.</span></div>', 0),
(12, NULL, 2, NULL, NULL, NULL, 'Protetyka', 1, '2013-02-12 19:33:00', 'Protetyka, Oferujemy, następujące, typy, protez:			akrylowe			szkieletowe			mosty			korony, (pełnoceramiczne,, złocie)			lic', 'Oferujemy następujące typy protez:\r\n\r\n	\r\n		akrylowe\r\n	\r\n		szkieletowe\r\n	\r\n		mosty\r\n	\r\n		korony (pełnoceramiczne, na złocie)\r\n	\r\n		lic', 'protetyka', '<p>\r\n	<strong>Gdy &nbsp;zęby są &nbsp;bardzo zniszczone lub są braki zębowe i nie ma możliwości odbudowy zęba lub zęb&oacute;w bezpośrednio w gabinecie</strong>, lekarz przy wsp&oacute;łpracy z technikiem dentystycznym może odbudować brakujące zęby stosując <span class=\"pink\">r&oacute;żne rodz. uzupełnień:</span></p>\r\n<ul>\r\n	<li>\r\n		akrylowe</li>\r\n	<li>\r\n		szkieletowe</li>\r\n	<li>\r\n		mosty</li>\r\n	<li>\r\n		korony (pełnoceramiczne, na złocie)</li>\r\n	<li>\r\n		lic&oacute;wki</li>\r\n</ul>', 0),
(13, NULL, 2, NULL, NULL, NULL, 'Periodontologia', 1, '2013-02-12 19:37:00', 'Periodontologia, musisz, wiedzieć, chorobach, przyzębia?	Wielu, pacjent', 'Co musisz wiedzieć o chorobach przyzębia?\r\n\r\n	Wielu pacjent', 'periodontologia', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Co powinieneś wiedzieć o chorobach przyzębia?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	Pow&oacute;d krwawienia dziąseł, odsłonięcia korzeni, rozchwiania, czy utraty zeb&oacute; przez długi czas nie był znany. Dzisiejsza stomatologia odpowiada nam na to pytanie - <strong>to paradontoza.</strong></div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Schorzenie to kiedyś uważane było za nieuleczalne, na szczęście &oacute;wczesna medycyna wnosi bardziej optymistyczną diagnozę. Parodontoza to zaraz obok pr&oacute;chnicy najczęstsza przyczyna przedwczesnej utraty uzębienia, dlatego <span class=\"link\">zaliczana jest do chor&oacute;b społecznych.</span></div>\r\n<div>\r\n	&nbsp;</div>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Przyczyny schorzenia:</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"width: 100%;\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<span style=\"font-size:14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; MIEJSCOWE</span></td>\r\n			<td>\r\n				<span style=\"font-size:14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; OG&Oacute;LNE</span></td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width: 50%;\">\r\n				<ul>\r\n					<li style=\"text-align: justify;\">\r\n						bakteryjna płytka nazębna (nieprawidłowa higiena jamy ustnej),</li>\r\n					<li style=\"text-align: justify;\">\r\n						nieprawidłowe wypełnienia (nawisy),</li>\r\n					<li style=\"text-align: justify;\">\r\n						powodujące ucisk protezy,</li>\r\n					<li style=\"text-align: justify;\">\r\n						nieszczelne korony i mosty,</li>\r\n					<li style=\"text-align: justify;\">\r\n						zgrzanie zębami, zaciskanie zęb&oacute;w</li>\r\n				</ul>\r\n			</td>\r\n			<td>\r\n				<ul>\r\n					<li style=\"text-align: justify;\">\r\n						palenie papieros&oacute;w,</li>\r\n					<li style=\"text-align: justify;\">\r\n						stres,</li>\r\n					<li style=\"text-align: justify;\">\r\n						cukrzyca,</li>\r\n					<li style=\"text-align: justify;\">\r\n						niedob&oacute;r witamin,</li>\r\n					<li style=\"text-align: justify;\">\r\n						zaburzenia hormonalne,</li>\r\n					<li style=\"text-align: justify;\">\r\n						niewłaściwe odżywianie,</li>\r\n					<li style=\"text-align: justify;\">\r\n						pozostałe choroby og&oacute;lnoustrojowe</li>\r\n				</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div>\r\n	Najgroźniejszym, miejscowym czynnikiem zapaleniotw&oacute;rczym jest bakteryjna <strong>płytka nazębna</strong>. Za jej sprawą dochodzi do zapalenia dziąseł i zaostrzenia przewlekłych stan&oacute;w zapalnych w tkankach przyzębia. Tworzy się ona na wszystkich powierzchniach zęb&oacute;w, błonie śluzowej, dziąsłach, uzupełnieniach protetycznych w jamie ustnej. <span class=\"link\">Wraz z upływem czasu podlega ona mineralizacji, co skutkuje powstawaniem kamienia nazębnego.</span></div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	R&oacute;żnego rodzaju uwarunkowania mogą być powodem sprzyjających warunk&oacute;w do rozwoju chor&oacute;b przyzębia w obecności wcześniej wspominach czynnik&oacute;w.</div>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Objawy chor&oacute;b przyzębia</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong><span style=\"font-size:14px;\">Do najczęstszych należą:</span></strong></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		krwawienie z dziąseł (najistotniejszy objaw choroby)</li>\r\n	<li style=\"text-align: justify;\">\r\n		obrzęki dziąseł,</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;l dziąseł,</li>\r\n	<li style=\"text-align: justify;\">\r\n		obnażanie korzeni (odsłanianie szyjek zębowych),</li>\r\n	<li style=\"text-align: justify;\">\r\n		rozchwianie zęb&oacute;w</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	Należy zapamiętać, że&nbsp;<strong>dziąsła zdrowe nigdy nie krwawią!</strong></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"link\">Uwaga palacze: nikotyna zwęża naczynia krwionośne, co skutkuje brakiem występowania krwawienia, a co za tym idzie utrudnia wczesne wykrycie choroby!</span></p>', 0),
(14, NULL, 2, NULL, NULL, NULL, 'Ozdoby na zęby', 1, '2013-02-12 19:52:00', 'Ozdoby, zęby, umocowania, produkt', 'Czy do umocowania produkt', 'ozdoby-na-zeby', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy do umocowania produkt&oacute;w Jewels/Twizzler konieczne jest borowanie?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie. Nasze prodykty przytwierdzane są do powierzchni zęba za pomocą kleju kompozytowego. Gwarantuje to wykluczenie jakichkolwiek mechanicznych uszkodzeń powierzchni nazębnej.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy istnieje możliwość usunięcia biżuterii nazębnej?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Tak. Bez najmniejszego problemu możemy usunąć &nbsp;każdy produkt lub go zastąpić innym, nie uszkadzając powierzchni zęba.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy stosowanie biżuterii nazębnej szkodzi zdrowiu?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Oczywiście, że nie. Biżuteria wykonana jest z materiał&oacute;w całkowicie nieszkodliwych dla zdrowia, co potwierdzają specjaliści z laboratorium w Zurichu.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy biżuterią nazębną można się skaleczyć lub zranić?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie. Przez pierwsze dni musimy sie oczywiście przyzwyczaić do nowego nabytku. Gwarantujemy jednak wysoki komfort oraz niepowtarzalny uśmiech.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy produkty Twizzer / Jewels można samodzielnie zamocować na zębie?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie! Tylko kompetentny i fachowy lekarz stomatolog z odpowiednią wiedzą i kwalifikacjami jest w stanie należycie zamocować biżuterię na zębach.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy należy zachować wyjątkową ostrożność podczas mycia zęb&oacute;w z uwagi na posiadaną biżuterię?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie trzeba. Właściwe zamocowanie jest gwarancją idealnej przyczepności. Posiadanie biżuterii zachęca wręcz do częstszej higieny jamy ustnej ze wględu na wyjątkowy uśmiech.</p>', 0),
(15, NULL, 2, NULL, NULL, NULL, 'Profilaktyka', 1, '2013-02-12 19:54:00', 'Profilaktyka, Lakierowanie	Zabieg, lakierowania, wykonuje, lekarz, stomatolog, gabinecie, stomatologicznym, stosując, tego', 'Lakierowanie\r\n\r\n	Zabieg lakierowania wykonuje lekarz stomatolog w gabinecie stomatologicznym stosując do tego preparat fluoru o wysokim stężeniu. Są to preparaty bezpieczne mimo wysokiego stężenia fluoru, gdyż szybko wiążą i dobrze przylegają do powierzchni zęba, uwalniając małe ilości fluoru\r\n\r\n	Zaletą lakier', 'profilaktyka', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Lakierowanie</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Zabieg lakierowania</strong> wykonywany jest przez lekarza stomatologa z wykorzystaniem bezpiecznego preparatu fluoru o wysokim stężeniu. Substancja szybko wiąże i doskonale przylega do powierzchni zęba, uwalniając stopniowo małe ilości fluoru. <span class=\"pink\">Bardzo długo utrzymuje się na powierzchni.</span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:16px;\"><strong>Wskazania stosowania:</strong></span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			podwyższone ryzyko pr&oacute;chnicy u dzieci powyżej 6 roku życia,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			nieprawidłowe nawyki dietetyczne u dorosłych,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			stosowanie aparat&oacute;w ortodontycznych,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			używanie protez ruchomych,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			odsłonięcie szyjek zębowych,</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			pr&oacute;chnica początkowa.</div>\r\n	</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Częstotliwość stosowania zabiegu zależy od indywidualnych skłonności do pr&oacute;chnicy. Standardowo należy wykonywać go ok. dwa razy w roku. Jeśli mamy podwyższoną skłonność do pr&oacute;chnicy zalecana jest większa częstotliwość - co kwartał. O tym powinien jednak zdecydować lekarz stomatolog.</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Lakowanie</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Lakowanie bruzd</strong> należy do rutynowych zabieg&oacute;w profilaktycznych do wielu lat. Zabieg obejmuje zęby trzonowe oraz przedtrzonowe ze wględu na fakt, iż budowa anatomiczna ich powierzchni zgryzowych sprzyja zaleganiu bakterii. <span class=\"pink\">Podyktowane jest to obecnością głębokich szczelin i bruzd.</span></p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Lakowanie bruzd stosuje się celem zabezpieczenia przed inwazją pr&oacute;chnicy zęba wyrzynającego się. <span class=\"pink\">Zabieg jest bezbolesny i komfortowy</span>, zawsze wykonywany przez lekarza stomatologa.</div>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Wskazania do lakowania:</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	O wskazaniach powinien zdecydować lekarz stomatolog, biorąc pod uwagę wiele indywidualnych czynnik&oacute;w takich jak stopień ryzyka pr&oacute;chnicy, zwyczaje żywieniowe, nawyki higieniczne.</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		całkowicie wyrznięte zęby trzonowe stałe (przede wszystkim zęby trzonowe pierwsze stałe - 6, kt&oacute;re wyrzynają się jako pierwsze zęby stałe i mają nam służyć jak najdłużej),</li>\r\n	<li style=\"text-align: justify;\">\r\n		zęby trzonowe i przedtrzonowe z głębokimi bruzdami,</li>\r\n	<li style=\"text-align: justify;\">\r\n		zęby trzonowe i przedtrzonowe u dzieci niepełnosprawnych (problem w utrzymaniu prawidłowej higieny jamy ustnej).</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>O tym jak skuteczny jest zabieg decyduje kilka czynnik&oacute;w:</strong></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		technika wykonania zabiegu</li>\r\n	<li style=\"text-align: justify;\">\r\n		przygotowanie osoby wykonującej zabieg</li>\r\n	<li style=\"text-align: justify;\">\r\n		warunki anatomiczne zęba</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			stopień wyrżnięcia zęba</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			rodzaj zgryzu</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			wilgotność zęba w czasie zabiegu</div>\r\n	</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"link\">Pamiętaj, że lakowanie pierwszych zęb&oacute;w bocznych stałych powinno się wykonywać możliwie wcześnie, najlepiej w pierwszych 6 miesięcach po wyrżnięciu się zęba.</span></p>', 0),
(16, NULL, 2, NULL, NULL, NULL, 'Laseroterapia', 1, '2013-02-12 19:58:00', 'Laseroterapia, ZAKRES, ZASTOSOWAŃ, STOMATOLOGICZNYCH', 'ZAKRES ZASTOSOWAŃ STOMATOLOGICZNYCH', 'laseroterapia', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>ZAKRES ZASTOSOWAŃ STOMATOLOGICZNYCH&nbsp;LASER&Oacute;W BIOSTYMULACYJNYCH</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Dzięki laserowi biostymulującemu</strong> możemy podnieść pr&oacute;g b&oacute;lu przed opracowaniem ubytku pr&oacute;chnicowego. W konsekwencji zabieg jest o wiele mniej bolesny. Urządzenia są wielce przydatne w zabiegach przeciwb&oacute;lowych przy:</p>\r\n<ul>\r\n	<li>\r\n		stomatopatiach protetycznych, &nbsp;</li>\r\n	<li>\r\n		ropniach przyzębia,&nbsp;</li>\r\n	<li>\r\n		paradontopatiach,&nbsp;</li>\r\n	<li>\r\n		obrzękach,</li>\r\n	<li>\r\n		opryszczce wargowej,</li>\r\n	<li>\r\n		zapaleniach miazgi.</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class=\"pink\">Lasera używamy do przyśpieszania gojenia się ran </span>po zabiegach ekstrakcji<span class=\"pink\">,</span> przecinania wędzidełka i resekcji, a ponadto przy przewlekłych zapaleniach tkanki okołozębowej. Zastosowanie urządzenia jest nieocenione przy leczeniu nerwob&oacute;l&oacute;w i stan&oacute;w zapalnych. Poprawia także terapeutyczne skutki zabieg&oacute;w, zmniejsza ryzyko infekcji oraz przyśpiesza regenerację uszkodzonych tkanek.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:16px;\"><strong>Wskazania:</strong></span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		stany zapalne miazgi, zatok, zębodołu,</li>\r\n	<li style=\"text-align: justify;\">\r\n		neuralgie,</li>\r\n	<li style=\"text-align: justify;\">\r\n		choroby dziąseł, przyzębia i błony śluzowej jamy ustnej</li>\r\n	<li style=\"text-align: justify;\">\r\n		grzybica jamy ustnej</li>\r\n	<li style=\"text-align: justify;\">\r\n		neuralgia nerwu tr&oacute;jdzielnego,</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;le w stawach skroniowo-żuchwowych,</li>\r\n	<li style=\"text-align: justify;\">\r\n		odczulanie odsłoniętej zębiny</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;l i obrzęk pozabiegowy,</li>\r\n	<li style=\"text-align: justify;\">\r\n		opryszczka, afty,</li>\r\n	<li style=\"text-align: justify;\">\r\n		zapalenie ślinianek,</li>\r\n	<li style=\"text-align: justify;\">\r\n		likwidacja szczękościsku,</li>\r\n	<li style=\"text-align: justify;\">\r\n		gojenie zębodołu po ekstrakcji,</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;l i obrzęk po złamaniach szczęki</li>\r\n	<li style=\"text-align: justify;\">\r\n		przyspieszanie gojenia po tradycyjnych zabiegach chirurgicznych w jamie ustnej (np. po plastyce wędzidełek, resekcji, sterowanej regeneracji kości, zabiegach płatowych, gingiwektomiach, przeszczepach i implantach)</li>\r\n</ul>', 0);
INSERT INTO `Strona` (`id`, `galeria_id`, `strona_nadrzedna_id`, `obrazek`, `updated_at`, `title2`, `title`, `published`, `publishDate`, `keywords`, `description`, `slug`, `content`, `automaticSeo`) VALUES
(18, NULL, NULL, NULL, NULL, NULL, 'Ortodoncja', 1, '2013-10-23 10:59:00', 'Ortodoncja, pięknego, uśmiechu, ważna, jest, profilaktyka, leczenie, zgryzu, dzieci,, młodzieży', 'Dla pięknego uśmiechu ważna jest profilaktyka i leczenie wad zgryzu u dzieci, młodzieży oraz dorosłych. Skutecznie przeprowadzone leczenie ortodontyczne wpływa na poprawę wyglądu i samopoczucia, prawidłową wymowę, lepszą higienę jamy ustnej, a czasem jest podstawowym warunkiem, aby długo cieszyć się zdrowymi zębami\r\n\r\n	Czym jest wada zgryzu?\r\n\r\n	Wada zgryzu to niewłaściwe ustawienie zęb', 'ortodoncja', '<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Leczenie wad zgryzu</span> ważne jest w każdym wieku - u <strong>dzieci, młodzieży i dorosłych</strong>. Prawidłowy zgryz jest warunkiem pięknego uśmiechu, dlatego warto w razie konieczności przeprowadzić jego leczenie. Poprawa wyglądu i samopoczucia to nie jedyne korzyści jakie za tym idą. Higiena jamy ustniej jest wygodniejsza i lepsza, nasza wymowa właściwa, możemy dłużej cieszyć się zdrowymi zębami.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Czym jest wada zgryzu?</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Definicja wady zgryzu określa ją jako niewłaściwe ustawienie zęb&oacute;w w łuakch zębowych (krzywe zęby). Występują r&oacute;wnież sytuacje, kiedy zęby są proste, ale łuki zębowe zwierają się niewłaściwie (wysunięcie g&oacute;rnych zęb&oacute;w do przodu względem dolnych). Wadę tą nie zawsze można dostrzec, warto więc skorzystać z konsultacji ortodontycznych.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Czy wada zgryzu to tylko problem brzydkiego wyglądu?</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Absolutnie nie. W większości przypadk&oacute;w problem ma wymiar nie tylko estetyczny, ale r&oacute;wnież zdrowotny. Problemy z czyszczeniem zęb&oacute;w, odkładanie się płytki nazębnej to niekt&oacute;re z konsekwencji nieprawidłowego zgryzu. Pociągają one za sobą skłonność do pr&oacute;chnicy i stan&oacute;w zapalnych dziąseł oraz przyzębia. Może dochodzić do ścierania się zęb&oacute;w i ich nieosiowego obciążenia, co w najgorszym przypadku może przyczynić się do ich utraty.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Rodzaje wad zgryzu:</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>tyłozgryz</strong> - najczęściej występująca nieprawidłowość; leczenie przynosi bardzo dobre efekty, szczeg&oacute;lnie w okresie wzrostu, czyli u dzieci. W niekt&oacute;rych przypadkach wada ta może wymagać leczenia chirurgicznego</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>przodozgryz</strong> - odwrotność tyłozgryzu - zęby przednie dolne nagryzają na g&oacute;rne, widoczne jest to w rysach twarzy poprzez wysuniętą wargę dolną czy dominującą dużą brodę. Jeżeli leczenie ortodontyczne zostało przeprowadzone przed okresem intensywnego wzrostu pacjenta, możliwe jest ponowne jej pojawienie się, a nawet nasilenie wady. Konieczna jest w&oacute;wczas interwencja chirurgiczna.</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>zgryz otwarty</strong> - w tej wadzie zęby nie kontaktują się ze sobą. Wada ta ma tendencje do nawrotu u pacjent&oacute;w, kt&oacute;rzy nieprawidłowo układają język podczas spoczynku, m&oacute;wienia lub połykania</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>zgryz głęboki </strong>- charakteryzuje się głębokim zachodzeniem siekaczy g&oacute;rnych na dolne; wada, kt&oacute;ra naturalnie pogłębia się wraz z wiekiem</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>zgryz krzyżowy </strong>- występuje w&oacute;wczas, gdy zęby dolne pokrywają zęby g&oacute;rne, co może być widoczne w rysach twarzy; zęby są przeciążone i narażone na ciągły uraz</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Jak przebiega leczenie ortodontyczne w naszej klinice?</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Konsultacja ortodontyczna</strong></span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Jest to pierwsza wizyta mająca na celu szczeg&oacute;łowe poznanie problemu oraz oczekiwań pacjenta. <span class=\"pink\">Odbywa się ona w następujący spos&oacute;b:</span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			wywiad z pacjentem</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			badanie pacjenta</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			pobranie wycisk&oacute;w do wykonania gipsowych modeli diagnostycznych</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			ewentualne wykonanie zdjęć fotograficznych wewnątrzustnych i zewnątrzustnych (rys&oacute;w twarzy)</div>\r\n	</li>\r\n	<li style=\"text-align: justify;\">\r\n		<div>\r\n			wykonanie zdjęcia pantomograficznego i zdjęcia bocznego czaszki</div>\r\n	</li>\r\n</ul>\r\n<h2 class=\"title\" style=\"text-align: justify;\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Pełny komplet badań jest warunkiem koniecznym dla kompleksowego zaplanowania leczenia.</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Plan leczenia</strong></span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Podczas drugiej wizyty pacjenta w gabinecie lekarz stomatolog</span> omawia krok po kroku układ i stan uzębienia, sugeruje najlepszy spos&oacute;b leczenia oraz informuje o jego kosztach. Dokonywany jest r&oacute;wnież wyb&oacute;r rodzaju aparatu oraz szacowany jest czas leczenia.</p>', 0),
(19, 5, NULL, NULL, NULL, NULL, 'Wyposażenie', 1, '2013-10-28 17:34:00', 'Sprzęt, gabinetach, Cosmodent, dbamy, komfort, wygodę, naszych, Pacjent', 'W gabinetach Cosmodent dbamy o komfort i wygodę naszych Pacjent', 'sprzet', '<p>\r\n	W gabinetach <span class=\"pink\">Cosmodent</span> dbamy o <strong>komfort i wygodę naszych Pacjent&oacute;w</strong>. Przyjazna i bezstresowa atmosfera podczas wizyty pozwoli <strong>przeprowadzić skuteczne leczenie</strong> oraz sprawi, że zapamiętają nas Państwo na długo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Zgodnie z powyższą ideą wyposażyliśmy naszą plac&oacute;wkę w najnowocześniejszy sprzęt stomatologiczny:</h2>\r\n<ul>\r\n	<li>\r\n		Nasi pacjenci przyjmowani są na niezwykle komfortowych i wygodnych fotelach dentystycznych firm<strong> STERN WEBER i A-DEC </strong>(unit A-dec to jedyny na świecie fotel z mapowanym naciskiem pacjenta na tapicerkę zapewniający niezr&oacute;wnany komfort bez względu na długość zabiegu)</li>\r\n	<li>\r\n		<strong>Panorama CS 8100&nbsp;</strong>marki <strong>Optident </strong>pozwoli dokładnie i szybko wykonać wysokiej jakości zdjęcia w celu precyzyjnej diagnozy</li>\r\n	<li>\r\n		Aparaty punktowe <strong>RTG CS 2200&nbsp;</strong>służą nam&nbsp;do robienie zdjęć pojedynczych zęb&oacute;w na każdym etapie leczenia</li>\r\n	<li>\r\n		Leczymy bez b&oacute;lu dzięki&nbsp;nowoczesnym, sterowanym komputerowo systemom do bezbolesnego podawania znieczuleń stomatologicznych <strong>The Wand</strong></li>\r\n	<li>\r\n		Mikroskop <strong>SmartOPTIC</strong> wykorzystywany przy leczeniu kanałowym</li>\r\n	<li>\r\n		Nowoczesna lampa do wybielania zęb&oacute;w <strong>Beyond</strong></li>\r\n	<li>\r\n		Endometr <strong>RAPEX 5</strong> do precyzyjnego badania długości kanał&oacute;w korzeniowych</li>\r\n</ul>', 0),
(20, 6, NULL, NULL, NULL, NULL, 'Gabinet', 1, '2014-12-29 13:16:00', 'Gabinet, Zapraszamy, obejrzenia, zdjęć, naszej, rzeszowskiej, plac', 'Zapraszamy do obejrzenia zdjęć naszej rzeszowskiej plac', 'gabinet', '<h2 class=\"title\" style=\"text-align: center;\">\r\n	Zapraszamy do obejrzenia naszego filmu promocyjnego:</h2>\r\n<p style=\"text-align: center;\">\r\n	<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"433\" src=\"https://www.youtube.com/embed/n_BKsj-3yfU\" width=\"770\"></iframe></p>', 0),
(21, NULL, NULL, NULL, NULL, NULL, 'blok2-strona-glowna', 0, '2019-02-18 13:53:00', 'blok2-strona-glowna,  			STOMATOLOGIA, ZACHOWAWCZA , leczenie, próchnicy			ODBUDOWA zniszczonych, złamanych, koron, zębów			STOMATOLOGIA, DZIECIĘCA, - leczenie', ' \r\n\r\n	\r\n		STOMATOLOGIA ZACHOWAWCZA  - leczenie próchnicy\r\n	\r\n		ODBUDOWA zniszczonych i złamanych koron zębów\r\n	\r\n		STOMATOLOGIA DZIECIĘCA - leczenie zębów mlecznych, profilaktyka wczesnodziecięca\r\n	\r\n		ENDODONCJA - nowoczesne metody opracowywania i wypełniania kanałów korzeniowych, leczenie zmian okołowierzchołkowych\r\n	\r\n		PROTETYKA - wszystkie rodzaje uzupełnień\r\n	\r\n		IMPLANTOLOGIA - wszczepianie implantów w miejsca brakujących zębów\r\n	\r\n		PERIODONTOLOGIA  - leczenie chorób przyzębia, zęby rozchwiane, zanik dziąseł, przykry zapach z ust, usuwanie kamienia\r\n	\r\n		CHIRURGIA STOMATOLOGICZNA - usuwanie zębów mlecznych i stałych\r\n	\r\n		LECZENIE zaburzeń stanów s-ż (bóle ucha i głowy)\r\n	\r\n		ORTODONCJA - aparaty stałe i ruchome\r\n	\r\n		PROFILAKTYKA - u dzieci i dorosłych\r\n	\r\n		WYBIELANIE I PIASKOWANIE zębów\r\n	\r\n		SZLACHETNE ozdoby na zęby\r\n\r\n\r\n	 ', 'blok2-strona-glowna', '<ul>\r\n	<li>\r\n		<b>PEELING CHEMICZNY&nbsp;</b> - przyśpieszenie odnowy kom&oacute;rkowej, wygładzanie płytkich zmarszczek, likwidacja przebarwień sk&oacute;rnych, redukcja blizn, rewitalizacja sk&oacute;rna, zmniejszenie łojotoku</li>\r\n	<li>\r\n		<b>MEZOTERAPIA IGŁOWA -&nbsp;</b>rewitalizacja i odmłodzenie sk&oacute;ry, leczenie wypadania włos&oacute;w, poprawa nawilżenia zmęczonej sk&oacute;ry, blizny potrądzikowe, rozstępy i cellulit</li>\r\n	<li>\r\n		<b>WYPEŁNIENIA KWASEM HIALURONOWYM -&nbsp;</b>zmarszczek, bruzd nosowo-wargowych i ust; korekta kształtu nosa, linii marionetki, owalu twarzy, wolumetria kości policzkowych i policzk&oacute;w</li>\r\n	<li>\r\n		<b>BOTOKS</b>&nbsp;- redukcja zmarszczek i leczenie bruksizmu, unoszenie brwi, wygładzanie sk&oacute;ry szyi, leczenie nadpotliwości, pomoc w leczeniu migreny</li>\r\n	<li>\r\n		<b>OSOCZE BOGATOPŁYTKOWE</b>&nbsp;- wampirzy lifting, zabieg w 100% naturalny niwelujący oznaki starzenia się sk&oacute;ry, przebarwienia sk&oacute;rne, przyśpiesza gojenie się ran po zabiegach medycyny estetycznej</li>\r\n</ul>', 0),
(22, NULL, NULL, NULL, NULL, NULL, 'Medycyna estetyczna', 1, '2019-02-20 12:56:00', 'Usługi,  	Tupacsum, Ipsum, until, that, Thug, Life, tatted, chest., seems', 'Cosmodent - praktyka stomatologiczna\r\n\r\nLista usług', 'medycyna-estetyczna', '<p style=\"text-align: center;\">\r\n	<span style=\"font-size:16px;\"><strong>Kliknij na wybraną usługę</strong>, aby poznać szczeg&oacute;ły:</span></p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 500px;\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/peeling-chemiczny.html\"><img alt=\"Peeling chemiczny\" src=\"/uploads/pliki/Medycyna%20estetyczna/Peeling%20chemiczny.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/mezoterapia-iglowa.html\"><img alt=\"Mezoterapia igłowa\" src=\"/uploads/pliki/Medycyna%20estetyczna/Mezoterapia%20ig%C5%82owa.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/kwas-hialuronowy.html\"><img alt=\"Kwas hialuronowy\" src=\"/uploads/pliki/Medycyna%20estetyczna/Kwas%20hialuronowy.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/botoks.html\"><img alt=\"Botoks\" src=\"/uploads/pliki/Medycyna%20estetyczna/Botoks.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/osocze-bogatoplytkowe.html\"><img alt=\"Osocze bogatopłytkowe\" src=\"/uploads/pliki/Medycyna%20estetyczna/Osocze%20bogatop%C5%82ytkowe.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n			<td>\r\n				&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 0),
(23, NULL, 22, NULL, NULL, NULL, 'Peeling chemiczny', 1, '2019-08-28 10:00:00', 'Peeling, chemczny, Czym, jest, peeling, chemiczny?	Celem, peelingu, chemicznego, usunięcie, martwego', 'Czym jest peeling chemiczny?\r\n\r\n	Celem peelingu chemicznego jest usunięcie martwego naskórka, a tym samym przyśpieszenie odnowy komórkowej, za pomocą nałożenia na skórę odpowiedniej mieszanki chemicznej. Efektem zabiegu jest natychmiastowe złuszczenie skóry, prowadzące do jej długotrwałej regeneracji i produkcji nowego kolagenu i elastyny, co bezpośrednio przekłada się na poprawę jej kondycji i jakości\r\n\r\n	Zabieg ten najczęściej stosowany jest na twarz, ale coraz częściej znajduje zastosowanie w odnowie skóry dłoni, szyi oraz dekoltu\r\n\r\n	Uznawany jest za jeden z najmniej inwazyjnych zabiegów medycyny estetycznej i nie wymaga hospitalizacji\r\n\r\n	Wskazania do zastosowania peelingu chemicznego:\r\n\r\n	\r\n		przebarwienia skórne\r\n	\r\n		wygładzenie płytkich zmarszczek\r\n	\r\n		redukcja blizn (np\r\n	\r\n		starzenie skóry twarzy spowodowane ekspozycją na słońce\r\n	\r\n		rewitalizacja skórna\r\n	\r\n		zmniejszenie łojotoku\r\n\r\n\r\n	Rodzaje peelingu chemicznego:\r\n\r\n	\r\n		Peeling powierzchniowy - eliminuje drobne zmarszczki, przebarwienia i pomaga w zwalczaniu trądzika poprzez usunięcie naskórka\r\n	\r\n		Peeling średnio-głęboki - redukuje większe zmarszczki, blizny potrądzikowe i przebarwienia, obejmuje swoim działaniem naskórek i skórę właściwą\r\n	\r\n		Peeling głęboki - stosowany do eliminacji głębokich zmarszczek i blizn\r\n\r\n\r\n	Przygotowanie do zabiegu i zalecenia:\r\n\r\n	\r\n		odstawienie palenia tytoniu oraz leków na bazie kwasu acetylosalicylowego na 6 tygodni przed planowanym zabiegiem\r\n	\r\n		może zostać zalecone używanie odpowiednich kremów w celu wzmocnienia efektów działania zabiegu\r\n	\r\n		na skórze nie mogą występować opryszczki lub zakażenia\r\n\r\n\r\n	Po zabiegu:\r\n\r\n	\r\n		należy unikać ekspozycji na słońce oraz stosować ochrony przeciwsłonecznej\r\n	\r\n		nie wykonywać forsownych ćwiczeń', 'peeling-chemiczny', '<h2 class=\"title1\">\r\n	Czym jest peeling chemiczny?</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Celem peelingu chemicznego jest usunięcie martwego nask&oacute;rka, a tym samym przyśpieszenie odnowy kom&oacute;rkowej, za pomocą nałożenia na sk&oacute;rę odpowiedniej mieszanki chemicznej. Efektem zabiegu jest natychmiastowe złuszczenie sk&oacute;ry, prowadzące do jej długotrwałej regeneracji i produkcji nowego kolagenu i elastyny, co bezpośrednio przekłada się na poprawę jej kondycji i jakości.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Zabieg ten najczęściej stosowany jest na twarz, ale coraz częściej znajduje zastosowanie w odnowie sk&oacute;ry dłoni, szyi oraz dekoltu.</h2>\r\n<p>\r\n	<span class=\"link\">Uznawany jest za jeden z najmniej inwazyjnych zabieg&oacute;w medycyny estetycznej i nie wymaga hospitalizacji.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Wskazania do zastosowania peelingu chemicznego:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		przebarwienia sk&oacute;rne</li>\r\n	<li>\r\n		wygładzenie płytkich zmarszczek</li>\r\n	<li>\r\n		redukcja blizn (np. potrądzikowe)</li>\r\n	<li>\r\n		starzenie sk&oacute;ry twarzy spowodowane ekspozycją na słońce</li>\r\n	<li>\r\n		rewitalizacja sk&oacute;rna</li>\r\n	<li>\r\n		zmniejszenie łojotoku</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title1\">\r\n	<strong>Rodzaje peelingu chemicznego:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		Peeling powierzchniowy - eliminuje drobne zmarszczki, przebarwienia i pomaga w zwalczaniu trądzika poprzez usunięcie nask&oacute;rka</li>\r\n	<li>\r\n		Peeling średnio-głęboki - redukuje większe zmarszczki, blizny potrądzikowe i przebarwienia, obejmuje swoim działaniem nask&oacute;rek i sk&oacute;rę właściwą</li>\r\n	<li>\r\n		Peeling głęboki - stosowany do eliminacji głębokich zmarszczek i blizn</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title1\">\r\n	<strong>Przygotowanie do zabiegu i zalecenia:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		odstawienie palenia tytoniu oraz lek&oacute;w na bazie kwasu acetylosalicylowego na 6 tygodni przed planowanym zabiegiem</li>\r\n	<li>\r\n		może zostać zalecone używanie odpowiednich krem&oacute;w w celu wzmocnienia efekt&oacute;w działania zabiegu</li>\r\n	<li>\r\n		na sk&oacute;rze nie mogą występować opryszczki lub zakażenia</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Po zabiegu:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		należy unikać ekspozycji na słońce oraz stosować ochrony przeciwsłonecznej</li>\r\n	<li>\r\n		nie wykonywać forsownych ćwiczeń</li>\r\n</ul>', 0),
(24, NULL, 22, NULL, NULL, NULL, 'Mezoterapia igłowa', 1, '2019-08-28 10:29:00', 'Mezoterapia, igłowa, Czym, jest, mezoterapia, jakie, przynosi, rezultaty?	Mezoterapia, jednym, najpopularniejszych', 'Czym jest mezoterapia i jakie przynosi rezultaty?\r\n\r\n	Mezoterapia jest jednym z najpopularniejszych i najchętniej wybieranych zabiegów medycyny estetycznej. Jest to w pełni bezpieczny, niechirurgiczny, a przede wszystkim skuteczny sposób na regenerację i odmłodzenie skóry, poprawienie jej elastyczności i nawilżenia. Dzięki jej zastosowaniu możemy nie tylko opóźnić proces starzenia skóry poprzez jej ujędrnienie i odżywienie, ale również skutecznie walczyć z wypadaniem włosów\r\n\r\n	Podczas zabiegu mezoterapii, za pomocą strzykawki i igły, podawane są bezpośrednio do głębszych warstw skóry indywidualnie dobrane czynne substancje lecznicze\r\n\r\n	Te przygotowywane przez wykwalifikowanego specjalistę mieszanki zwane są koktajlami. Działają one błyskawicznie, wpływając na przebudowę naskórka i pobudzenie produkcji kolagenu oraz elastyny\r\n\r\n	Zabieg charakteryzuje się krótkim czasem rekonwalescencji oraz trwałymi efektami. Najczęściej stosuje się go na twarzy, ale z powodzeniem przynosi świetne rezultaty również na innych częściach ciała - dekolcie, dłoniach, brzuchu, ramionach, udach i pośladkach\r\n\r\n	Mezoterapia igłowa znajduje zastosowanie nie tylko jako zabieg leczniczy, ale również profilaktyczny, pomagający dbać o zdrową i piękną skórę\r\n\r\n	Wskazania do zastosowania:\r\n\r\n	\r\n		niwelowanie zmarszczek\r\n	\r\n		lifting owalu twarzy\r\n	\r\n		zwiotczenie mięśni twarzy i szyi\r\n	\r\n		cienie i zasinienia pod oczami\r\n	\r\n		poprawa nawilżenia zmęczonej skóry\r\n	\r\n		blizny potrądzikowe\r\n	\r\n		rozstępy i cellulit\r\n	\r\n		wypadanie włosów i łysienie\r\n	\r\n		przebarwienia\r\n\r\n\r\n	\r\n	Przygotowanie do zabiegu i zalecenia:\r\n\r\n	\r\n		niezbędna jest specjalistyczna konsultacja, której celem jest omówienie oczekiwanych efektów oraz przygotowanie odpowiedniego koktajlu\r\n	\r\n		w dniu zabiegu nie należy spożywać kawy oraz alkoholu,\r\n	\r\n		zabieg nie jest wskazany w okresie osłabienia organizmu\r\n\r\n\r\n	Po przeprowadzonym zabiegu:\r\n\r\n	\r\n		należy stosować filtry ochronne i nie wysta', 'mezoterapia-iglowa', '<h2 class=\"title1\">\r\n	<strong><span class=\"pink\">Czym jest mezoterapia i jakie przynosi rezultaty?</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mezoterapia jest jednym z najpopularniejszych i najchętniej wybieranych zabieg&oacute;w medycyny estetycznej. Jest to w pełni bezpieczny, niechirurgiczny, a przede wszystkim skuteczny spos&oacute;b na regenerację i odmłodzenie sk&oacute;ry, poprawienie jej elastyczności i nawilżenia. Dzięki jej zastosowaniu możemy nie tylko op&oacute;źnić proces starzenia sk&oacute;ry poprzez jej ujędrnienie i odżywienie, ale r&oacute;wnież skutecznie walczyć z wypadaniem włos&oacute;w.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Podczas zabiegu mezoterapii, za pomocą strzykawki i igły, podawane są bezpośrednio do głębszych warstw sk&oacute;ry indywidualnie dobrane czynne substancje lecznicze.</h2>\r\n<p>\r\n	Te przygotowywane przez wykwalifikowanego specjalistę mieszanki zwane są koktajlami. Działają one błyskawicznie, wpływając na przebudowę nask&oacute;rka i pobudzenie produkcji kolagenu oraz elastyny.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zabieg charakteryzuje się kr&oacute;tkim czasem rekonwalescencji oraz trwałymi efektami. <span class=\"link\">Najczęściej stosuje się go na twarzy, ale z powodzeniem przynosi świetne rezultaty r&oacute;wnież na innych częściach ciała - dekolcie, dłoniach, brzuchu, ramionach, udach i pośladkach.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mezoterapia igłowa znajduje zastosowanie nie tylko jako zabieg leczniczy, ale r&oacute;wnież profilaktyczny, pomagający dbać o zdrową i piękną sk&oacute;rę.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Wskazania do zastosowania:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		niwelowanie zmarszczek</li>\r\n	<li>\r\n		lifting owalu twarzy</li>\r\n	<li>\r\n		zwiotczenie mięśni twarzy i szyi</li>\r\n	<li>\r\n		cienie i zasinienia pod oczami</li>\r\n	<li>\r\n		poprawa nawilżenia zmęczonej sk&oacute;ry</li>\r\n	<li>\r\n		blizny potrądzikowe</li>\r\n	<li>\r\n		rozstępy i cellulit</li>\r\n	<li>\r\n		wypadanie włos&oacute;w i łysienie</li>\r\n	<li>\r\n		przebarwienia</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	<br />\r\n	<strong>Przygotowanie do zabiegu i zalecenia:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		niezbędna jest specjalistyczna konsultacja, kt&oacute;rej celem jest om&oacute;wienie oczekiwanych efekt&oacute;w oraz przygotowanie odpowiedniego koktajlu</li>\r\n	<li>\r\n		w dniu zabiegu nie należy spożywać kawy oraz alkoholu,</li>\r\n	<li>\r\n		zabieg nie jest wskazany w okresie osłabienia organizmu</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Po przeprowadzonym zabiegu:</strong></p>\r\n<ul>\r\n	<li>\r\n		należy stosować filtry ochronne i nie wystawiać się na ekspozycję słoneczną</li>\r\n	<li>\r\n		unikać wzmożonej aktywności fizycznej</li>\r\n	<li>\r\n		pić większą ilość wody</li>\r\n	<li>\r\n		nie dotykać okolic poddanych iniekcji</li>\r\n</ul>', 0),
(25, NULL, 22, NULL, NULL, NULL, 'Kwas hialuronowy', 1, '2019-08-28 10:31:00', 'Kwas, hialuronowy, Czym, jest, kwas, hialuronowy?	Kwas, substancją, występującą, naturalnie, naszym', 'Czym jest kwas hialuronowy?\r\n\r\n	Kwas hialuronowy jest substancją występującą naturalnie w naszym organizmie, która przyciąga i wiąże cząsteczki wody, wpływając na nawilżenie skóry. Jego ilość wraz z wiekiem stopniowo się zmniejsza, czego skutkiem jest jej wiotczenie i wysuszanie, jak również zmniejszenie objętości policzków i ust. Odpowiedni poziom nawodnienia skóry decyduje bowiem o jej sprężystości i odporności na powstawanie zmarszczek\r\n\r\n	W jaki sposób wykorzystuje się kwas hialuronowy w medycynie estetycznej?\r\n\r\n	Preparat wykorzystywany w leczeniu jest całkowicie syntetyczny, zapewnia bezpieczeństwo i wygodę, nie wymaga próby uczuleniowej. Wstrzykuje się go w konkretne miejsca na skórze właściwej, uzupełniając niedobór jego naturalnych pokładów, czego efektem jest niemal natychmiastowe jej nawilżenie i wypełnienie\r\n\r\n	W ten sposób skutecznie modelujemy kształt ust, kontur twarzy, wypełniamy zmarszczki, redukujemy bruzdy i rewitalizujemy skórę\r\n\r\n	Pobudzane są też naturalne procesy regeneracyjne skóry, dzięki czemu nawet po wchłonięciu substancji skóra wygląda znacznie lepiej niż przed zabiegiem\r\n\r\n	Efekty działania kwasu hialuronowego utrzymują się do 18 miesięcy, w zależności od indywidualnych uwarunkowań organizmu\r\n\r\n	Wskazania do zastosowania:\r\n\r\n	\r\n		wypełnienia zmarszczek i bruzd nosowo-wargowych\r\n	\r\n		kurze łapki\r\n	\r\n		opadnięte kąciki ust\r\n	\r\n		wypełnienie ust\r\n	\r\n		korekta owalu twarzy\r\n	\r\n		korekta kształtu nosa\r\n	\r\n		korekta linii marionetki\r\n	\r\n		wolumetria kości policzkowych i policzków\r\n\r\n\r\n	\r\n	Zalecenia i przygotowanie do zabiegu:\r\n\r\n	Miejsca podania preparatu smaruje się specjalnym kremem ze środkiem znieczulającym godzinę przed zabiegiem, dzięki czemu wstrzykiwanie substancji jest bezbolesne\r\n\r\n	Po przeprowadzonym leczeniu należy unikać bezpośredniej ekspozycji słonecznej oraz nagrzewania (np. w saunie) skóry przez okres 14 dni', 'kwas-hialuronowy', '<h2 class=\"title1\">\r\n	<strong>Czym jest kwas hialuronowy?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Kwas hialuronowy jest substancją występującą naturalnie w naszym organizmie, kt&oacute;ra przyciąga i wiąże cząsteczki wody, wpływając na nawilżenie sk&oacute;ry. Jego ilość wraz z wiekiem stopniowo się zmniejsza, czego skutkiem jest jej wiotczenie i wysuszanie, jak r&oacute;wnież zmniejszenie objętości policzk&oacute;w i ust. Odpowiedni poziom nawodnienia sk&oacute;ry decyduje bowiem o jej sprężystości i odporności na powstawanie zmarszczek.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>W jaki spos&oacute;b wykorzystuje się kwas hialuronowy w medycynie estetycznej?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Preparat wykorzystywany w leczeniu jest całkowicie syntetyczny, zapewnia bezpieczeństwo i wygodę, nie wymaga pr&oacute;by uczuleniowej. Wstrzykuje się go w konkretne miejsca na sk&oacute;rze właściwej, uzupełniając niedob&oacute;r jego naturalnych pokład&oacute;w, czego efektem jest niemal natychmiastowe jej nawilżenie i wypełnienie.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	<span style=\"background-color: initial;\">W ten spos&oacute;b skutecznie modelujemy kształt ust, kontur twarzy, wypełniamy zmarszczki, redukujemy bruzdy i rewitalizujemy sk&oacute;rę.</span></h2>\r\n<p>\r\n	Pobudzane są też naturalne procesy regeneracyjne sk&oacute;ry, dzięki czemu nawet po wchłonięciu substancji sk&oacute;ra wygląda znacznie lepiej niż przed zabiegiem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class=\"link\">Efekty działania kwasu hialuronowego utrzymują się do 18 miesięcy, w zależności od indywidualnych uwarunkowań organizmu.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Wskazania do zastosowania:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		wypełnienia zmarszczek i bruzd nosowo-wargowych</li>\r\n	<li>\r\n		kurze łapki</li>\r\n	<li>\r\n		opadnięte kąciki ust</li>\r\n	<li>\r\n		wypełnienie ust</li>\r\n	<li>\r\n		korekta owalu twarzy</li>\r\n	<li>\r\n		korekta kształtu nosa</li>\r\n	<li>\r\n		korekta linii marionetki</li>\r\n	<li>\r\n		wolumetria kości policzkowych i policzk&oacute;w</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	<br />\r\n	<strong>Zalecenia i przygotowanie do zabiegu:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Miejsca podania preparatu smaruje się specjalnym kremem ze środkiem znieczulającym godzinę przed zabiegiem, <span class=\"pink\">dzięki czemu wstrzykiwanie substancji jest bezbolesne.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Po przeprowadzonym leczeniu należy unikać bezpośredniej ekspozycji słonecznej oraz nagrzewania (np. w saunie) sk&oacute;ry przez okres 14 dni.</p>', 0),
(26, NULL, 22, NULL, NULL, NULL, 'Botoks', 1, '2019-08-28 10:38:00', 'Botoks, Czym, jest, toksyna, botulinowa?	Popularny, botoks, silnie, działającą, naturalną, toksyną,', 'Czym jest toksyna botulinowa?\r\n\r\n	Popularny botoks jest silnie działającą naturalną toksyną, która powoduje zahamowanie uwalniania acetylocholiny. Jest to neuroprzekaźnik odpowiedzialny za przekazywanie impulsów pomiędzy nerwami. Innymi słowy działanie tej substancji blokuje synapsy nerwowo-mięśniowe (połączenia między zakończeniami nerwów, a unerwionymi przez nie mięśniami), w skutek czego nie dochodzi do skurczów porażonego mięśnia. Dzięki takim właściwościom możliwe jest leczenie wielu chorób neurologicznych przebiegających z nadmiernym napięciem mięśni\r\n\r\n	Jakie zastosowanie ma botoks w medycynie estetycznej?\r\n\r\n	W medycynie estetycznej do najważniejszych zastosowań botoksu możemy zaliczyć przeciwdziałanie powstawiania zmarszczek, leczenie bruksizmu i nadpotliwości. W takich przypadkach podaje się niezwykle małe stężenia toksyny, które nie mają żadnego wpływu na inne elementy układu nerwowego poza miejscem, w którym bezpośrednio ją zastosowano\r\n\r\n	Zabieg z wykorzystaniem toksyny botulinowej polega na podskórnym podaniu właściwej dawki w ściśle określone miejsca za pomocą jednorazowej strzykawki z bardzo cienką igłą\r\n\r\n	Zabieg trwa bardzo krótko, w zależności od liczby miejsc poddawanych leczeniu czas ten może się wahać około 15-20 minut\r\n\r\n	Jest to jedna z najskuteczniejszych form terapii antyzmarszczkowej, zwłaszcza w górnym obszarze twarzy i okolic nosa („zmarszczki królicze”). Umożliwia również korektę jego nadmiernie zagiętego koniuszka. Jest to również szansa dla wszystkich tych, którzy cierpią na przewlekłe migreny\r\n\r\n	Efekty działania botoksu:\r\n\r\n	Kilka dni po zabiegu zobaczymy już rezultaty, zarówno w kwestii redukcji zmarszczek, jak i nadpotliwości, natomiast pełne rezultaty pojawią się po ok. 2-3 tygodniach. Dla utrzymania stałych efektów zaleca się powtarzać zabieg co 6-8 miesięcy\r\n\r\n	Wskazania do zastosowania:\r\n\r\n	\r\n		redukcja zmarszczek okolic oczu (kurze łapki)\r\n	\r\n		redukcja zmarszczek mi', 'botoks', '<h2 class=\"title1\">\r\n	<strong>Czym jest toksyna botulinowa?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Popularny botoks jest silnie działającą naturalną toksyną, kt&oacute;ra powoduje zahamowanie uwalniania acetylocholiny. Jest to neuroprzekaźnik odpowiedzialny za przekazywanie impuls&oacute;w pomiędzy nerwami. Innymi słowy działanie tej substancji blokuje synapsy nerwowo-mięśniowe (połączenia między zakończeniami nerw&oacute;w, a unerwionymi przez nie mięśniami), w skutek czego nie dochodzi do skurcz&oacute;w porażonego mięśnia. Dzięki takim właściwościom możliwe jest leczenie wielu chor&oacute;b neurologicznych przebiegających z nadmiernym napięciem mięśni.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Jakie zastosowanie ma botoks w medycynie estetycznej?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	W medycynie estetycznej do najważniejszych zastosowań botoksu możemy zaliczyć przeciwdziałanie powstawiania zmarszczek, leczenie bruksizmu i nadpotliwości. W takich przypadkach podaje się niezwykle małe stężenia toksyny, kt&oacute;re nie mają żadnego wpływu na inne elementy układu nerwowego poza miejscem, w kt&oacute;rym bezpośrednio ją zastosowano.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Zabieg z wykorzystaniem toksyny botulinowej polega na podsk&oacute;rnym podaniu właściwej dawki w ściśle określone miejsca za pomocą jednorazowej strzykawki z bardzo cienką igłą.</h2>\r\n<p>\r\n	Zabieg trwa bardzo kr&oacute;tko, w zależności od liczby miejsc poddawanych leczeniu czas ten może się wahać około 15-20 minut.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class=\"link\">Jest to jedna z najskuteczniejszych form terapii antyzmarszczkowej, zwłaszcza w g&oacute;rnym obszarze twarzy i okolic nosa (&bdquo;zmarszczki kr&oacute;licze&rdquo;).</span> Umożliwia r&oacute;wnież korektę jego nadmiernie zagiętego koniuszka. Jest to r&oacute;wnież szansa dla wszystkich tych, kt&oacute;rzy cierpią na przewlekłe migreny.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Efekty działania botoksu:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Kilka dni po zabiegu zobaczymy już rezultaty, zar&oacute;wno w kwestii redukcji zmarszczek, jak i nadpotliwości, natomiast pełne rezultaty pojawią się po ok. 2-3 tygodniach. Dla utrzymania stałych efekt&oacute;w zaleca się powtarzać zabieg co 6-8 miesięcy.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Wskazania do zastosowania:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		redukcja zmarszczek okolic oczu (kurze łapki)</li>\r\n	<li>\r\n		redukcja zmarszczek między brwiami (lwia zmarszczka)</li>\r\n	<li>\r\n		redukcja zmarszczek na czole</li>\r\n	<li>\r\n		redukcja zmarszczek na szyi i brodzie (bruzdy marionetki)</li>\r\n	<li>\r\n		unoszenie brwi</li>\r\n	<li>\r\n		leczenie bruksizmu</li>\r\n	<li>\r\n		wygładzanie sk&oacute;ry szyi</li>\r\n	<li>\r\n		leczenie nadpotliwości</li>\r\n	<li>\r\n		leczenie migreny</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Przygotowanie do zabiegu:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Podanie toksyny botulinowej nie wymaga żadnych szczeg&oacute;łowych badań diagnostycznych. Jest to zabieg bezpieczny i niezwykle skuteczny. Zanim się jednak na niego zdecydujemy zalecana jest konsultacja ze specjalistą w celu zweryfikowania wszelkich przeciwwskazań oraz om&oacute;wienia pożądanych efekt&oacute;w.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sam zabieg nie wymaga znieczulenia miejscowego, jest nieinwazyjny i prosty, jednak dla komfortu pacjenta czasem stosuje się znieczulenie kremem, kt&oacute;ry zostaje wmasowany w sk&oacute;rę ok. godzinę przed jego rozpoczęciem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong><span class=\"pink\">Zalecenie po przeprowadzonym zabiegu:</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		rekomendowane ćwiczenia mięśni mimicznych w miejscach wkłucia</li>\r\n	<li>\r\n		nie wolno masować miejsc wkłucia</li>\r\n	<li>\r\n		nie należy pochylać głowy do 4 godzin po zabiegu</li>\r\n	<li>\r\n		nie zleca się wykonywania wyczerpujących ćwiczeń fizycznych</li>\r\n	<li>\r\n		zalecane jest nie narażanie miejsc podania toksyny na bezpośrednią ekspozycję słoneczną i wysokie temperatury</li>\r\n	<li>\r\n		podr&oacute;że lotnicze nie są rekomendowane przynajmniej przez kilka dni</li>\r\n</ul>', 0),
(27, NULL, 22, NULL, NULL, NULL, 'Osocze bogatopłytkowe', 1, '2019-08-28 10:42:00', 'Osocze, bogatopłytkowe, Czym, jest, osocze, bogatopłytkowe?	Osocze, preparatem, krwiopodobnym, zawierającym, zwiększoną', 'Czym jest osocze bogatopłytkowe?\r\n\r\n	Osocze bogatopłytkowe jest preparatem krwiopodobnym zawierającym zwiększoną ilość płytek krwi. Zawiera liczne czynniki wzrostu, stymulujące procesy regeneracyjne. Jest otrzymywane z pobranej od pacjenta krwi obwodowej poddanej odpowiedniej obróbce. Materiał pobrany w ten sposób jest biozgodny z jego organizmem, co tym samym uniemożliwia wystąpienia wszelkiego typu alergii i niezgodności\r\n\r\n	Czym jest wampirzy lifting?\r\n\r\n	Wampirzy lifting, czyli terapia osoczem bogatopłytkowym w medycynie estetycznej, służy regeneracji skóry poprzez pobudzenie fibroblastów (biostymulację) do tworzenia nowego kolagenu i składa się z trzech etapów:\r\n\r\n	\r\n		pobranie krwi z żyły pacjenta\r\n	\r\n		pozyskanie w separatorze koncentratu bogatopłytkowego oraz oddzielenie osocza za pomocą wirówki\r\n	\r\n		wstrzyknięcie wcześniej pobranego materiału do krwi pacjenta w odpowiednich miejscach\r\n\r\n\r\n	Zabieg jest naturalny i w pełni bezpieczny dla organizmu pacjenta, nie wymaga rekonwalescencji. Jego efekty są trwałe i widoczne już po kilkunastu dniach\r\n\r\n	Leczenie osoczem stosowane jest z powodzeniem w różnych dziedzinach medycyny (w chirurgii plastycznej, ortopedii i stomatologii), pomaga w leczeniu ran, wspomaganiu procesu gojenia i odbudowie tkanek\r\n\r\n	Wskazania do zabiegu:\r\n\r\n	\r\n		oznaki starzenia się skóry\r\n	\r\n		zmęczona i wysuszona skóra\r\n	\r\n		przebarwienia skórne, niejednolity kolor\r\n	\r\n		przyśpieszenie gojenia się ran po zabiegach medycyny estetycznej\r\n	\r\n		leczenie łysienia\r\n\r\n\r\n	Zalecenia pozabiegowe:\r\n\r\n	\r\n		nie dotykanie skóry przez ok\r\n	\r\n		dokładne umycie skóry po wspomnianym wyżej czasie\r\n	\r\n		brak makijażu do 48 godzin po wkłuciu osocza\r\n	\r\n		stworzenie warunków do regeneracji skóry poprzez szczególną dbałość o jej higienę', 'osocze-bogatoplytkowe', '<h2 class=\"title1\">\r\n	<strong>Czym jest osocze bogatopłytkowe?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Osocze bogatopłytkowe jest preparatem krwiopodobnym zawierającym zwiększoną ilość płytek krwi. Zawiera liczne czynniki wzrostu, stymulujące procesy regeneracyjne. Jest otrzymywane z pobranej od pacjenta krwi obwodowej poddanej odpowiedniej obr&oacute;bce. <span class=\"link\">Materiał pobrany w ten spos&oacute;b jest biozgodny z jego organizmem, co tym samym uniemożliwia wystąpienia wszelkiego typu alergii i niezgodności.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Czym jest wampirzy lifting?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wampirzy lifting, czyli terapia osoczem bogatopłytkowym w medycynie estetycznej, służy regeneracji sk&oacute;ry poprzez pobudzenie fibroblast&oacute;w (biostymulację) do tworzenia nowego kolagenu i składa się z trzech etap&oacute;w:</p>\r\n<ul>\r\n	<li>\r\n		pobranie krwi z żyły pacjenta</li>\r\n	<li>\r\n		pozyskanie w separatorze koncentratu bogatopłytkowego oraz oddzielenie osocza za pomocą wir&oacute;wki</li>\r\n	<li>\r\n		wstrzyknięcie wcześniej pobranego materiału do krwi pacjenta w odpowiednich miejscach</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zabieg jest naturalny i w pełni bezpieczny dla organizmu pacjenta, nie wymaga rekonwalescencji. Jego efekty są trwałe i widoczne już po kilkunastu dniach.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Leczenie osoczem stosowane jest z powodzeniem w r&oacute;żnych dziedzinach medycyny (w chirurgii plastycznej, ortopedii i stomatologii), pomaga w leczeniu ran, wspomaganiu procesu gojenia i odbudowie tkanek.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong><span class=\"pink\">Wskazania do zabiegu:</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		oznaki starzenia się sk&oacute;ry</li>\r\n	<li>\r\n		zmęczona i wysuszona sk&oacute;ra</li>\r\n	<li>\r\n		przebarwienia sk&oacute;rne, niejednolity kolor</li>\r\n	<li>\r\n		przyśpieszenie gojenia się ran po zabiegach medycyny estetycznej</li>\r\n	<li>\r\n		leczenie łysienia</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Zalecenia pozabiegowe:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		nie dotykanie sk&oacute;ry przez ok. 2-3 godziny po zabiegu</li>\r\n	<li>\r\n		dokładne umycie sk&oacute;ry po wspomnianym wyżej czasie</li>\r\n	<li>\r\n		brak makijażu do 48 godzin po wkłuciu osocza</li>\r\n	<li>\r\n		stworzenie warunk&oacute;w do regeneracji sk&oacute;ry poprzez szczeg&oacute;lną dbałość o jej higienę</li>\r\n</ul>', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uploaded_file`
--

CREATE TABLE `uploaded_file` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `file` varchar(1024) DEFAULT NULL,
  `showOnList` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ZdjecieZmieniarki`
--

CREATE TABLE `ZdjecieZmieniarki` (
  `id` int(11) NOT NULL,
  `kolejnosc` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ZdjecieZmieniarki`
--

INSERT INTO `ZdjecieZmieniarki` (`id`, `kolejnosc`, `obrazek`, `updated_at`, `title`, `content`) VALUES
(19, 10, '19-5d668ec2acbfd.jpg', '2019-08-28 17:44:00', 'Implantologia', 'Najwyższej jakości leczenie implantologiczne w konkurencyjnych cenach'),
(23, 6, '23-5d66901b64b97.jpg', '2019-08-28 17:48:30', 'Leczymy bez bólu', 'Leczymy bez bólu oraz pomagamy w nagłych wypadkach. Po wizycie w naszym gabinecie stomatologicznym słowo dentysta będzie wywoływać już tylko miłe skojarzenia.'),
(26, 14, '26-5d668eecbc7e0.jpg', '2019-08-28 17:42:29', 'Znajdź nas na mapie', 'Zapraszamy do gabinetu w Rzeszowie przy ulicy Strażackiej 12d'),
(29, 8, '29-5d668f01d1e58.jpg', '2019-08-28 17:47:11', 'Najwyższy komfort', 'Inwestujemy w najnowocześniejszy sprzęt i komfortowe fotele dla dobra naszych pacjentów'),
(33, 12, '33-5d668f0b0e9c2.jpg', '2019-08-28 17:37:03', 'Konsultacje ortodontyczne', 'Zapraszamy serdecznie wszystkich pacjentów na specjalistyczne konsultacje ortodontyczne do naszych gabinetów'),
(34, 0, '-5d6694f8b2f6b.jpg', '2019-08-28 17:39:15', 'Medycyna estetyczna', 'Nowość w ofercie Cosmodentu! Peeling chemiczny, mezoterapia igłowa, wypełnienia kwasem hialuronowym, botoks oraz wampirzy lifting');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `Aktualnosc`
--
ALTER TABLE `Aktualnosc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D3F2B3ED31019C` (`galeria_id`);

--
-- Indeksy dla tabeli `ContactFormMessage`
--
ALTER TABLE `ContactFormMessage`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `craue_config_setting`
--
ALTER TABLE `craue_config_setting`
  ADD PRIMARY KEY (`name`);

--
-- Indeksy dla tabeli `Employee`
--
ALTER TABLE `Employee`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indeksy dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indeksy dla tabeli `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_583D1F3EA76ED395` (`user_id`),
  ADD KEY `IDX_583D1F3EFE54D947` (`group_id`);

--
-- Indeksy dla tabeli `Galeria`
--
ALTER TABLE `Galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `GaleriaZdjecie`
--
ALTER TABLE `GaleriaZdjecie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3C805D89D31019C` (`galeria_id`);

--
-- Indeksy dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543BC4663E4` (`page_id`);

--
-- Indeksy dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D5506BF24F87` (`parentItem_id`);

--
-- Indeksy dla tabeli `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `page_uploadedfile`
--
ALTER TABLE `page_uploadedfile`
  ADD PRIMARY KEY (`page_id`,`uploadedfile_id`),
  ADD KEY `IDX_35EB0A06C4663E4` (`page_id`),
  ADD KEY `IDX_35EB0A06C34C6513` (`uploadedfile_id`);

--
-- Indeksy dla tabeli `Strona`
--
ALTER TABLE `Strona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_74FC6E18D31019C` (`galeria_id`),
  ADD KEY `IDX_74FC6E18499597B3` (`strona_nadrzedna_id`);

--
-- Indeksy dla tabeli `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uploaded_file`
--
ALTER TABLE `uploaded_file`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ZdjecieZmieniarki`
--
ALTER TABLE `ZdjecieZmieniarki`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `Aktualnosc`
--
ALTER TABLE `Aktualnosc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `ContactFormMessage`
--
ALTER TABLE `ContactFormMessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT dla tabeli `Employee`
--
ALTER TABLE `Employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `Galeria`
--
ALTER TABLE `Galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `GaleriaZdjecie`
--
ALTER TABLE `GaleriaZdjecie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `Strona`
--
ALTER TABLE `Strona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT dla tabeli `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `uploaded_file`
--
ALTER TABLE `uploaded_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ZdjecieZmieniarki`
--
ALTER TABLE `ZdjecieZmieniarki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `Aktualnosc`
--
ALTER TABLE `Aktualnosc`
  ADD CONSTRAINT `FK_D3F2B3ED31019C` FOREIGN KEY (`galeria_id`) REFERENCES `Galeria` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ograniczenia dla tabeli `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD CONSTRAINT `FK_583D1F3EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_583D1F3EFE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- Ograniczenia dla tabeli `GaleriaZdjecie`
--
ALTER TABLE `GaleriaZdjecie`
  ADD CONSTRAINT `FK_3C805D89D31019C` FOREIGN KEY (`galeria_id`) REFERENCES `Galeria` (`id`);

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543BC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D5506BF24F87` FOREIGN KEY (`parentItem_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `page_uploadedfile`
--
ALTER TABLE `page_uploadedfile`
  ADD CONSTRAINT `FK_35EB0A06C34C6513` FOREIGN KEY (`uploadedfile_id`) REFERENCES `uploaded_file` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_35EB0A06C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `Strona`
--
ALTER TABLE `Strona`
  ADD CONSTRAINT `FK_74FC6E18499597B3` FOREIGN KEY (`strona_nadrzedna_id`) REFERENCES `Strona` (`id`),
  ADD CONSTRAINT `FK_74FC6E18D31019C` FOREIGN KEY (`galeria_id`) REFERENCES `Galeria` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
