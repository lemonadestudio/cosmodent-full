<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\DoctrineBundle\DoctrineBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            
            new Symfony\Bundle\DoctrineFixturesBundle\DoctrineFixturesBundle(),
        		
        	// sonata admin:
        	new Sonata\CacheBundle\SonataCacheBundle(),
        	new Sonata\BlockBundle\SonataBlockBundle(),
        	new Sonata\jQueryBundle\SonatajQueryBundle(),	        		
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
        	new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            
        	// sonata user
            new FOS\UserBundle\FOSUserBundle(),        
            new Sonata\UserBundle\SonataUserBundle(),
            // OR: new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            // new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
        	
        		
        	new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
     		new Liip\ImagineBundle\LiipImagineBundle(),
        	new Gregwar\CaptchaBundle\GregwarCaptchaBundle(),
        	new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle(),
        		
        	new XD\UserBundle\XDUserBundle(),
        	new XD\CmsBundle\XDCmsBundle(),
        		
        	new Craue\ConfigBundle\CraueConfigBundle(),
        		
            new Lm\CmsBundle\LmCmsBundle()
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();

        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
