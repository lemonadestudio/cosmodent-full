<?php

namespace Lm\CmsBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultControllerNew extends Controller {

	/**

	 * @Template()
	 */
	public function mainAction() {

		return array();

	}
}
