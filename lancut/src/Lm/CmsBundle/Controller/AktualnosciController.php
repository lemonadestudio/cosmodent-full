<?php

namespace Lm\CmsBundle\Controller;
use Lm\CmsBundle\Entity\Specjalizacja;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AktualnosciController extends Controller {

	/**
	 *
	 * @Route("/aktualnosci")
	 * @Template()
	 */
	public function listaAction($page = 1) {

		$page = abs(intval($page));

		$breadcrumbs = $this->get("white_october_breadcrumbs");
		$breadcrumbs
				->addItem(
						$this->get('translator')->trans('title_aktualnosci'),
						'');

		$aktualnosci = $this->getAktualnosciLista($page);

		return array('aktualnosci' => $aktualnosci,
				'content_title' => 'Aktualnosci');

	}

	private function getAktualnosciLista($page = 1) {

		$page = abs(intval($page));

		$aktualnosci_query = $this->getDoctrine()->getEntityManager()
				->createQuery(
						"
				SELECT a FROM LmCmsBundle:Aktualnosc a
				WHERE a.published = true
				AND a.publishDate <= :curr_date
				ORDER BY a.publishDate DESC
				")->setParameter('curr_date', date('Y-m-d H:i:s'));

		$paginator = $this->get('knp_paginator');
		$aktualnosci = $paginator
				->paginate($aktualnosci_query,
						$this->get('request')->query->get('page', $page)/*page number*/,
						3 /*limit per page*/
				);

		return $aktualnosci;

	}


	/**
	 * @Route("/aktualnosci/{slug}.html", name="lm_cms_aktualnosci_show");
	 * @Template();
	 */
	public function showAction($slug) {

		$aktualnosc = $this->getDoctrine()
				->getRepository('LmCmsBundle:Aktualnosc')
				->findOneBy(array('slug' => $slug));

		if (!$aktualnosc) {
			throw new NotFoundHttpException();
		}

		$current_route = $this->getRequest()->attributes->get('_route');



		$breadcrumbs = $this->get("white_october_breadcrumbs");
		if ($current_route == 'lm_cms_mediaonas_show') {
			$breadcrumbs
					->addItem(
							$this->get('translator')->trans('title_mediaonas'),
							$this->get('router')
									->generate(
											'lm_cms_aktualnosci_listamedia'));
		} else {
			$breadcrumbs
					->addItem(
							$this->get('translator')
									->trans('title_aktualnosci'),
							$this->get('router')
									->generate('lm_cms_aktualnosci_lista'));
		}

		$breadcrumbs->addItem($aktualnosc->getTitle(), '');

		return array('page' => $aktualnosc,
				'content_title' => $aktualnosc->getTitle());

	}

}
