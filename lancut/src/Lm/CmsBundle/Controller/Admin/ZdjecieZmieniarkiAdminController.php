<?php

namespace Lm\CmsBundle\Controller\Admin;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\RedirectController;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\HttpFoundation\File\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Imagine\Image\Box;

use Imagine\Image\Point;

use Imagine\Gd\Imagine;

class ZdjecieZmieniarkiAdminController extends Controller {    
    public function kadrujAction(Request $request, $id) {
        $type = $request->get('type');

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(
				sprintf('unable to find the object with id : %s', $id)
            );
        }
        
        if (false === $this->admin->isGranted('EDIT', $object)) {
			throw new AccessDeniedException();
		}
        
        if (null === $object->getAbsolutePath()) {
            return $this->redirect($this->generateUrl('lm.cms.admin.zdjecie_zmieniarki.edit', array('id' => $id)));
        } else {
            $size = $object->getThumbSize($type);
            $photo = $object->getObrazekSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);
                        
            return $this->render(
                'LmCmsBundle:Admin/ZdjecieZmieniarki:kadruj.html.twig',
                array (
                    'object' => $object,
                    'preview' => $preview,
                    'thumb_conf' => $thumb_conf,
                    'size' => $size,
                    'aspect' => $thumb_ratio,
                    'type' => $type,
                )
            );
        }
    }

    public function kadrujZapiszAction(Request $request, $id) {
        $type = $request->get('type');
        $x = $request->get('x');
        $y = $request->get('y');
        $x2 = $request->get('x2');
        $y2 = $request->get('y2');

        $object = $this->admin->getObject($id);

        if (!$object) {
			throw new NotFoundHttpException(
                sprintf('unable to find the object with id : %s', $id)
            );
		}
        
        if (false === $this->admin->isGranted('EDIT', $object)) {
			throw new AccessDeniedException();
		}

        $object->Thumb($x, $y, $x2, $y2, $type);

        $this->get('session')->setFlash('sonata_flash_success', 'flash_edit_success');
        
		$url = $this->admin->generateObjectUrl('edit', $object);

		return new RedirectResponse($url);
    }
}
