<?php

namespace Lm\CmsBundle\Controller;

use Lm\CmsBundle\Entity\Specjalizacja;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class EmployeeController extends Controller {

    /**
     *
     * @Route("/nasz-zespol")
     * @Template()
     */
    public function indexAction() {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Nasz zespół", '');

        $employees = $this->getDoctrine()
                ->getEntityManager()
                ->createQuery("
                    SELECT e FROM LmCmsBundle:Employee e
                    WHERE e.isActive = true
                    ORDER BY e.entityOrder ASC, e.id ASC
                ")->execute();
        
        $our_team_query = $this->getDoctrine()->getEntityManager()
		->createQuery(
				"
				SELECT g FROM LmCmsBundle:GaleriaZdjecie g
				WHERE g.podpis = 'our_team'
				ORDER BY g.kolejnosc ASC
				");
        
        $our_team = $our_team_query->setMaxResults(1)->execute();

        return array(
            'employees' => $employees,
            'content_title' => 'Nasz zespół',
            'our_team' => $our_team
        );
    }

}
