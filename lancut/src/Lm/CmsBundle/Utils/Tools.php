<?php

namespace Lm\CmsBundle\Utils;

class Tools {
    static public function thumbName($filename, $appendix) {
        $temp = explode('.', $filename);
        $ext = end($temp);
        $thumbname = substr($filename, 0, strlen($filename) - (strlen($ext) + 1)) . $appendix . "." . $ext;
        return $thumbname;
    }
}