<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class StronaAdmin extends Admin {

	protected $translationDomain = 'KlinikaAdmin';

	public function getBatchActions()
	{
		$actions = array();

		// turn off batch actions
		return $actions;

	}

	public function getEditTemplate() {

		 return 'LmCmsBundle:Admin\Strona:edit.html.twig';

	}



    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('titleJoined')
                ->add('keywords')
                ->add('description')
                ->add('content')
                ->add('galeria')
        ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper->addIdentifier('titleJoined', null, array())

        		->add('description', 'text', array('template' => 'LmCmsBundle:Admin\Strona:_description.html.twig'))
                ->add('obrazek', null, array('template' => 'LmCmsBundle:Slider:obrazek.html.twig'))
                ->add('keywords')
                ->add('published')
                ->add('publishDate')
                ->add('galeria')
        ;

        $listMapper->add('_action', 'actions', array(
        		$this->trans('actions') => array(
        				'view' => array(),
        				'edit' => array(),
        				'delete' => array()
        		)
        ));

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

        $formMapper

                ->add('title', null, array('required' => true))

                ->add('content', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
                ->add('published', 'checkbox', array('required' => false))
                ->add('publishDate', 'datetime')
                ->add('_file', 'file', array('required' => false, 'label' => 'File'))
                ->add('_delete_file', 'checkbox', array('required'=> false, 'label' => 'Usunąć zdjęcie?'))
                ->add('galeria', null, array('required' => false))
             	->add('automaticSeo', 'checkbox', array('required' => false))
                ->add('slug', null, array('required' => false))
                ->add('keywords', null, array('required' => false))
                ->add('description', 'textarea', array('required' => false))
                ->add('strona_nadrzedna', null, array('required' => false))

        -> setHelps(array(
                    'title' => $this->trans('Enter page title'),
        			'automaticSeo' => $this->trans('help.automaticseo'),
        			'slug' => $this->trans('help.slug'),
                    'publishDate' => $this->trans('help.publishDate'),
        			'onMainPage' => $this->trans('help.onMainPage'),
        			'onMainPageOrder' => $this->trans('help.onMainPageOrder'),


           ));
    }
}