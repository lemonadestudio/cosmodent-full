<?php

namespace Lm\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;

class GaleriaAdmin extends Admin {

	protected $translationDomain = 'KlinikaAdmin';

	protected function configureRoutes(RouteCollection $collection)
	{

		$collection->add('edit_zdjecia', '{id}/edit_zdjecia');

	}


	protected function configureSideMenu(MenuItemInterface $menu, $action, Admin $childAdmin = null)
	{

		if (!$childAdmin && !in_array($action, array('edit', 'edit_zdjecia'))) {
			return;
		}
		$admin = $this->isChild() ? $this->getParent() : $this;
		$id = $admin->getRequest()->get('id');

		$menu->addChild(
				$this->trans('Edycja'),
				array('uri' => $admin->generateUrl('edit', array('id' => $id)))
		);

		$menu->addChild(
				$this->trans('Edytuj zdjęcia'),
				array('uri' => $admin->generateUrl('edit_zdjecia', array('id' => $id)))
		);


// 		$menu->addChild(
// 				'comments',
// 				array('uri' => $admin->generateUrl('acme_comment.admin.comment.list', array('id' => $id)))
// 		);
	}



	public function getEditTemplate() {

		return 'LmCmsBundle:Admin\\Galeria:edit.html.twig';
	}

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('nazwa')
	        	->add('opis')
	        	->add('obrazek', null, array('template' => 'LmCmsBundle:Slider:obrazek.html.twig'))
	        	->add('kolejnosc')
	        	// ->add('wyswietlana')
	        	->add('zalaczalna')
	        	->add('zdjecia')
         ;
    }


    public function configureListFields(ListMapper $listMapper) {

        $listMapper
        	->addIdentifier('nazwa', null, array())
        	->add('opis', null, array('template' => 'LmCmsBundle:Admin\Pracownik:_opis.html.twig'))
        	->add('obrazek', null, array('template' => 'LmCmsBundle:Slider:obrazek.html.twig'))
        	->add('kolejnosc')
        	// ->add('wyswietlana')
        	->add('zalaczalna')
        	->add('zdjecia')

                ;

                $listMapper->add('_action', 'actions', array(
                		$this->trans('actions') => array(
                				'edit' => array(),
                				'delete' => array(),
                				'view' => array(),
                		)
                	)
				);

    }



    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nazwa')

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

    	$this->getSubject()->setUpdatedAt( new \DateTime('now'));

        $formMapper
        	->add('nazwa')
        	->add('opis', null, array('required' => false, 'attr' => array('class' => 'sonata-medium wysiwyg')))
        	->add('_file', 'file', array('label' => 'File'))
        	->add('kolejnosc')
        	// ->add('wyswietlana')
        	->add('zalaczalna')
			->add('updated_at', null, array( 'with_seconds' => 'true',  'attr'=>array('style'=>'display:none;')))
        ;

        $formMapper->setHelps(array(
        		// 'wyswietlana' => 'Widoczna w module galerii - nie wpływa na wyświetlanie gdy galeria jest załączona do strony tekstowej',
        		'zalaczalna' => 'Możliwość załączenia galerii do aktualności, podstrony tekstowej',

        ));


    }

}