
$(function(){


	fckconfig_common = {
			// extraPlugins : 'stylesheetparser',
			skin: 'BootstrapCK-Skin',
			contentsCss: [edytor_css2, edytor_css],
			// EditorAreaCSS: edytor_css,
			stylesSet: [
			     { name : 'Normalny', element : 'p', attributes : { 'class' : '' } },
			     { name : 'Zwykły', element : 'span', attributes : { 'class' : '' } },
			     { name : 'Wyróżnienie', element : 'span', attributes : { 'class' : 'link' } },
			     { name : 'Pogrubinie', element: 'span', attributes: {'class' : 'pink'}},
			     { name: 'Nagłówek 1', element: 'h2', attributes: {'class' : 'title1' } },
			     { name: 'Nagłówek 2', element: 'h2', attributes: {'class' : 'title' } },
			     { name: 'Wypunktowanie', element: 'ul', attributes: {'class' : 'wypunktowanie' } }
			],

			 filebrowserBrowseUrl : edytor_browse_url,

			 // filebrowserUploadUrl : '',

	};



	fckconfig = jQuery.extend(true,  {

	}, fckconfig_common);

	fckconfig_basic = jQuery.extend(true, {

			toolbar:
				[
					['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
					['UIColor']
				]

	}, fckconfig_common );

	fckconfig_tiny = jQuery.extend(true, {

		toolbar:
			[
				['Source', '-',  'Bold', 'Italic', 'Strike', '-', 'TextColor'], ['FontSize']

			]

}, fckconfig_common );

	$('.wysiwyg').ckeditor(fckconfig);
	$('.wysiwyg-basic').ckeditor(fckconfig_basic);
	$('.wysiwyg-tiny').ckeditor(fckconfig_tiny);


});