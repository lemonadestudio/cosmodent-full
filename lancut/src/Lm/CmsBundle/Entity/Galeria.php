<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping\OrderBy;
use XD\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;



/**
 *
 * @ORM\Table();
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class Galeria
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull();
     */
    private $nazwa;
    
    /**
     * @ORM\Column(type="text", nullable="true")
     */
    private $opis;
    
    /**
     * @ORM\Column(type="string", length=255);
     * @Assert\NotNull();
     */
    private $slug;
    
    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    private $kolejnosc;
    
    /**
     * @ORM\Column(name="obrazek", type="string", length=255, nullable="true")
     */
    private $obrazek;
    
    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    private $wyswietlana;
    
    /**
     * 
     * @ORM\Column(type="boolean", nullable="true")
     */
    private $zalaczalna;
    
    /**
     * @ORM\Column(type="datetime", nullable="true")
     */
    private $updated_at;
   
    
    /**
     * @ORM\OneToMany(targetEntity="GaleriaZdjecie", mappedBy="galeria", cascade={"persist", "remove"})
     */
    private $zdjecia;
    
    /**
     * @ORM\OneToMany(targetEntity="Aktualnosc", mappedBy="galeria", cascade={"detach"})
     * @ORM\JoinColumn(onDelete="SET NULL", onUpdate="SET NULL")
     */
    private $aktualnosci;
    
    /**
     * @ORM\OneToMany(targetEntity="Aktualnosc", mappedBy="galeria", cascade={"detach"})
     */
    private $strony;

    
    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;
    

    
    public function __construct()
    {
        $this->kolejnosc= 0;
        $this->updated_at = new \DateTime();
        $this->wyswietlana = true;
        $this->zalaczalna = true;
        $this->zdjecia = new ArrayCollection();
        $this->slug = 'nowa-galeria';
    }
    
    public function __toString() {
    	return $this->getNazwa();
    }
    
    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	$cms = new Cms();
    	$this->setSlug($cms->make_url($this->nazwa));
    	
    	$this->updated_at = new \DateTime("now");
    
    	if (null !== $this->_file) {
    
    		// do whatever you want to generate a unique name
    		$this->oldFile = $this->getObrazek();
    
    		$this->obrazek = $this->generateObrazekName().'.'.$this->_file->guessExtension();
    
    	}
    
    }
    
    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->_file) {
    		return;
    	}
    
    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->_file->move($this->getUploadRootDir(), $this->obrazek);
    
    	if($this->oldFile) {
    		@unlink($this->getUploadRootDir() .'/'.$this->oldFile);
    		@unlink($this->getUploadRootDir() .'/crop-'.$this->oldFile);
    	}
    
    	unset($this->_file);
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	if ($file = $this->getAbsolutePath()) {
    		@unlink($file);
    	}
    }
    
    
    public function getAbsolutePath()
    {
    	return !$this->getObrazek() ? null : $this->getUploadRootDir().'/'.$this->getObrazek();
    }
    
    public function getWebPath()
    {
    	return !$this->getObrazek() ? null : $this->getUploadDir().'/'.$this->getObrazek();
    }
    
    public function getWebPathCrop() {
    	if(file_exists($this->getUploadRootDir().'/crop-'.$this->getObrazek())) {
    		return $this->getUploadDir().'/crop-'.$this->getObrazek();
    	} else {
    		return $this->getWebPath();
    	}
    }
    
    public function generateObrazekName() {
    
    	$cms = new Cms();
    	return $cms->make_url($this->getNazwa().' '.uniqid());
    }
    
    public function getUploadRootDir()
    {
    
    	// the absolute directory path where uploaded documents should be saved
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    public function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
    	return 'uploads/galeria';
    }
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;
    }

    /**
     * Get nazwa
     *
     * @return string 
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set opis
     *
     * @param text $opis
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;
    }

    /**
     * Get opis
     *
     * @return text 
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     */
    public function setKolejnosc($kolejnosc)
    {
        $this->kolejnosc = $kolejnosc;
    }

    /**
     * Get kolejnosc
     *
     * @return integer 
     */
    public function getKolejnosc()
    {
        return $this->kolejnosc;
    }

    /**
     * Set obrazek
     *
     * @param string $obrazek
     */
    public function setObrazek($obrazek)
    {
        $this->obrazek = $obrazek;
    }

    /**
     * Get obrazek
     *
     * @return string 
     */
    public function getObrazek()
    {
        return $this->obrazek;
    }

    /**
     * Set wyswietlana
     *
     * @param boolean $wyswietlana
     */
    public function setWyswietlana($wyswietlana)
    {
        $this->wyswietlana = $wyswietlana;
    }

    /**
     * Get wyswietlana
     *
     * @return boolean 
     */
    public function getWyswietlana()
    {
        return $this->wyswietlana;
    }

    /**
     * Set zalaczalna
     *
     * @param boolean $zalaczalna
     */
    public function setZalaczalna($zalaczalna)
    {
        $this->zalaczalna = $zalaczalna;
    }

    /**
     * Get zalaczalna
     *
     * @return boolean 
     */
    public function getZalaczalna()
    {
        return $this->zalaczalna;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    /**
     * Add zdjecia
     *
     * @param Lm\CmsBundle\Entity\GaleriaZdjecie $zdjecia
     */
    public function addGaleriaZdjecie(\Lm\CmsBundle\Entity\GaleriaZdjecie $zdjecie)
    {
        $this->zdjecia[] = $zdjecie;
        $zdjecie->setGaleria($this);
        
    }

    /**
     * Get zdjecia
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getZdjecia()
    {
        $iterator = $this->zdjecia->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->getKolejnosc() < $b->getKolejnosc()) ? -1 : 1;
        });
        
        return new ArrayCollection(iterator_to_array($iterator));
    }
    
    public function setZdjecia($zdjecia) {
    	foreach($zdjecia as $zdjecie) {
    		$this->addGaleriaZdjecie($zdjecie);
    	}
    }
    

}
