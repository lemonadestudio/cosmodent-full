<?php

namespace Lm\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use XD\CmsBundle\Helper\Cms;
use XD\CmsBundle\Model\Text;

/**
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks();
 */
class Employee {

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull();
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    private $position;

    /**
     * @ORM\Column(type="text", nullable="true")
     */
    private $specializations;

    /**
     * @ORM\Column(type="string", length=255, nullable="true");
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    private $photo;
    
    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    private $largePhoto;

    /**
     * @ORM\Column(type="boolean", nullable="false")
     */
    private $isActive;
    
    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     */
    private $entityOrder;

    /**
     * @ORM\Column(type="datetime", nullable="false")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable="true")
     */
    private $updatedAt;

    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;
    public $_delete_file = false;
    
    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_largePhotoFile;
    public $_deleteLargePhotoFile = false;

    /**
     *
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function preUpload() {
        $this->setCreatedValue();

        if ($this->_file !== null) {
            $this->oldFile = $this->getAbsolutePath();
            $this->photo = uniqid() . '.' . $this->_file->guessExtension();
        } else if ($this->_delete_file == true) {
            @unlink($this->getAbsolutePath());
            $this->setPhoto(null);
        }
        
        if ($this->_largePhotoFile !== null) {
            $this->largePhotoOldFile = $this->getLargePhotoAbsolutePath();
            $this->largePhoto = uniqid() . '.' . $this->_largePhotoFile->guessExtension();
        } else if ($this->_deleteLargePhotoFile == true) {
            @unlink($this->getLargePhotoAbsolutePath());
            $this->setLargePhoto(null);
        }
    }

    /**
     *
     * @ORM\PostPersist();
     * @ORM\PostUpdate();
     */
    public function upload() {
        if ($this->_file !== null) {
            $this->_file->move($this->getUploadRootDir(), $this->photo);

            if ($this->oldFile) {
                @unlink($this->oldFile);
            }

            unset($this->_file);
        }
        
        if ($this->_largePhotoFile !== null) {
            $this->_largePhotoFile->move($this->getUploadRootDir(), $this->largePhoto);
            
            if ($this->largePhotoOldFile) {
                @unlink($this->largePhotoOldFile);
            }
            
            unset($this->_largePhotoFile);
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getAbsolutePath()) {
            @unlink($file);
        }
        
        if ($file = $this->getLargePhotoAbsolutePath()) {
            @unlink($file);
        }
    }

    /**
     * @ORM\PrePersist();
     * @ORM\PreUpdate();
     */
    public function setCreatedValue() {
        $cms_helper = new Cms();
        $this->setSlug($cms_helper->make_url($this->getName()));
    }

    public function getAbsolutePath() {
        return !$this->getPhoto() ? null : $this->getUploadRootDir() . '/' . $this->getPhoto();
    }

    public function getWebPath() {
        return !$this->getPhoto() ? null : $this->getUploadDir() . '/' . $this->getPhoto();
    }
    
    public function getLargePhotoAbsolutePath() {
        return !$this->getLargePhoto() ? null : $this->getUploadRootDir() . '/' . $this->getLargePhoto();
    }

    public function getLargePhotoWebPath() {
        return !$this->getLargePhoto() ? null : $this->getUploadDir() . '/' . $this->getLargePhoto();
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/employee';
    }

    public function __construct() {
        $this->createdAt = new \DateTime("now");
    }
    
    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set position
     *
     * @param text $position
     */
    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * Get position
     *
     * @return text 
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * Set specializations
     *
     * @param text $specializations
     */
    public function setSpecializations($specializations) {
        $this->specializations = $specializations;
    }

    /**
     * Get specializations
     *
     * @return text 
     */
    public function getSpecializations() {
        return $this->specializations;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug) {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set photo
     *
     * @param string $photo
     */
    public function setPhoto($photo) {
        $this->photo = $photo;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     */
    public function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive() {
        return $this->isActive;
    }

    /**
     * Set entityOrder
     *
     * @param integer $entityOrder
     */
    public function setEntityOrder($entityOrder)
    {
        $this->entityOrder = $entityOrder;
    }

    /**
     * Get entityOrder
     *
     * @return integer 
     */
    public function getEntityOrder()
    {
        return $this->entityOrder;
    }

    /**
     * Set largePhoto
     *
     * @param string $largePhoto
     */
    public function setLargePhoto($largePhoto)
    {
        $this->largePhoto = $largePhoto;
    }

    /**
     * Get largePhoto
     *
     * @return string 
     */
    public function getLargePhoto()
    {
        return $this->largePhoto;
    }
}