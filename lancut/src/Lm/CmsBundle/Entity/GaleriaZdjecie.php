<?php

namespace Lm\CmsBundle\Entity;

use Doctrine\ORM\Mapping\OrderBy;
use XD\CmsBundle\Helper\Cms;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;



/**
 *
 * @ORM\Table();
 * @ORM\Entity();
 * @ORM\HasLifecycleCallbacks();
 */
class GaleriaZdjecie
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024, nullable="true")
     */
    private $podpis;
    
    /**
     * @ORM\Column(type="string", length=1024, nullable="true")
     */
    private $url;
    
    /**
     * @ORM\Column(type="integer", nullable="true")
     */
    private $kolejnosc;
    
    /**
     * @ORM\Column(name="obrazek", type="string", length=255, nullable="true")
     */
    private $obrazek;

    /**
     * @ORM\Column(type="datetime", nullable="true")
     */
    private $updated_at;
    
    /**
     * @ORM\ManyToOne(targetEntity="Galeria", inversedBy="zdjecia")
     */
    private $galeria;    

    
    /**
     *
     * @Assert\File(maxSize="6000000")
     */
    public $_file;
    

    
    public function __construct()
    {
        $this->kolejnosc=0;
        $this->updated_at = new \DateTime();
        
    }
    
    public function __toString() {
    	return $this->getPodpis() ? $this->getPodpis() : ''.$this->getId();
    }
    
    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	$this->updated_at = new \DateTime("now");
    	
    	if (null !== $this->_file) {
    
    		// do whatever you want to generate a unique name
    		$this->oldFile = $this->getObrazek();
    
    		$this->obrazek = $this->generateObrazekName().'.'.$this->_file->guessExtension();
    
    	}
    
    }
    
    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->_file) {
    		return;
    	}
    
    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->_file->move($this->getUploadRootDir(), $this->obrazek);
    
    	if($this->oldFile) {
    		@unlink($this->getUploadRootDir() .'/'.$this->oldFile);
    		@unlink($this->getUploadRootDir() .'/crop-'.$this->oldFile);
    	}
    
    	unset($this->_file);
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	if ($file = $this->getAbsolutePath()) {
    		@unlink($file);
    	}
    }
    
    
    public function getAbsolutePath()
    {
    	return !$this->getObrazek() ? null : $this->getUploadRootDir().'/'.$this->getObrazek();
    }
    
    public function getWebPath()
    {
    	return !$this->getObrazek() ? null : $this->getUploadDir().'/'.$this->getObrazek();
    }
    
    public function getWebPathCrop() {
    	if(file_exists($this->getUploadRootDir().'/crop-'.$this->getObrazek())) {
    		return $this->getUploadDir().'/crop-'.$this->getObrazek();
    	} else {
    		return $this->getWebPath();
    	}
    }
    
    public function generateObrazekName() {
    
    	$cms = new Cms();
    	return $cms->make_url($this->getId().' '.uniqid());
    }
    
    public function getUploadRootDir()
    {
    
    	// the absolute directory path where uploaded documents should be saved
    	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    
    public function getUploadDir()
    {
    	// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
    	return 'uploads/galeria_zdjecie';
    }
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set podpis
     *
     * @param string $podpis
     */
    public function setPodpis($podpis)
    {
        $this->podpis = $podpis;
    }

    /**
     * Get podpis
     *
     * @return string 
     */
    public function getPodpis()
    {
        return $this->podpis;
    }

    /**
     * Set kolejnosc
     *
     * @param integer $kolejnosc
     */
    public function setKolejnosc($kolejnosc)
    {
        $this->kolejnosc = $kolejnosc;
    }

    /**
     * Get kolejnosc
     *
     * @return integer 
     */
    public function getKolejnosc()
    {
        return $this->kolejnosc;
    }

    /**
     * Set obrazek
     *
     * @param string $obrazek
     */
    public function setObrazek($obrazek)
    {
        $this->obrazek = $obrazek;
    }

    /**
     * Get obrazek
     *
     * @return string 
     */
    public function getObrazek()
    {
        return $this->obrazek;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    }

    /**
     * Get updated_at
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set galeria
     *
     * @param Lm\CmsBundle\Entity\Galeria $galeria
     */
    public function setGaleria(\Lm\CmsBundle\Entity\Galeria $galeria)
    {
        $this->galeria = $galeria;
    }

    /**
     * Get galeria
     *
     * @return Lm\CmsBundle\Entity\Galeria 
     */
    public function getGaleria()
    {
        return $this->galeria;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
}