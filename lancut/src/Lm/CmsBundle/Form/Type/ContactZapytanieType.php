<?php

namespace Lm\CmsBundle\Form\Type;

use XD\CmsBundle\Helper\Cms;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ContactZapytanieType extends AbstractType {

	/**
	 *
	 * @var ContainerAware
	 */
	private $container;
	private $temat;

	public function __construct(Container $container) {

		$this->container = $container;



	}

	/**
	 * @param FormBuilder $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilder $builder, array $options) {



		$builder

			->add('imie', 'text', array('label' => 'Imię'))
			->add('nazwisko', 'text', array('label' => 'Nazwisko'))
			->add('email', 'text', array('label' => 'Adres e-mail'))
			->add('telefon', 'text', array('label' => 'Numer telefonu'))
			->add('tresc', 'textarea', array('label' => 'Wiadomość'))

		;

	}

	/**
	 * @param array $options
	 * @return multitype:
	 */
	public function getDefaultOptions(array $options) {



		$collectionConstraint = new Collection(array(


			'imie' => array(new NotBlank(), new MaxLength(100)),
			'nazwisko' => array(new NotBlank(), new MaxLength(100)),
			'email' => array(new NotBlank(), new Email(array('message' => 'Nieprawidłowy adres e-mail'))),
			'telefon' => new NotBlank(),
			'tresc' => array(new NotBlank()),

		));

		return array('validation_constraint' => $collectionConstraint);

	}

	public function getName() {
		return 'contact';
	}



}