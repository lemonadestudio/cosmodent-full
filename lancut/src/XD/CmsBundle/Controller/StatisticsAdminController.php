<?php

namespace XD\CmsBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController   as Controller;
use Sonata\AdminBundle\Controller\HelperController;
use Sonata\AdminBundle\Controller\CoreController;

class StatisticsAdminController extends Controller
{
	
	var $gapi_client;
	var $gapi_analytics;
	
	private function prepareGapi() {
		
		$gapi_client = new \Google_ApiClient();
		$gapi_client->setClientId($this->container->getParameter('gapi_client_id'));
		$gapi_client->setClientSecret($this->container->getParameter('gapi_client_secret'));
		$gapi_client->setRedirectUri($this->container->getParameter('gapi_redirect_uri'));
		$gapi_client->setDeveloperKey($this->container->getParameter('gapi_developer_key'));
		
		$gapi_analytics = new \Google_ApiAnalyticsService($gapi_client);
		
		$this->gapi_client = $gapi_client;
		$this->gapi_analytics = $gapi_analytics;
		
		
	}

    public function listAction()
    {
	
    	$this->prepareGapi();
    	
    	$gapi_client = $this->gapi_client;
    	$gapi_analytics = $this->gapi_analytics;
    	$error = '';
    	$auth = false;
    	
    	$gapi_token = $this->container->get('craue_config')->get('gapi_token');
    	$gapi_ga_account = $this->container->get('craue_config')->get('gapi_ga_account');
    	$gapi_ga_profile = $this->container->get('craue_config')->get('gapi_ga_profile');
    	
    	try {
    		if (isset($_GET['code'])) {
    			 
    			$gapi_client->authenticate();
    			// $_SESSION['gapi_token'] = $gapi_client->getAccessToken();
    			
    			$gapi_token = $this->container->get('craue_config')->set('gapi_token', $gapi_client->getAccessToken());
    			
    			return $this->redirect($this->admin->generateUrl('list'));
    		}
    		
    		// if (isset($_SESSION['gapi_token'])) {
    		if (isset($gapi_token) && !empty($gapi_token)) {
    			
    			$gapi_client->setAccessToken($gapi_token);
    			
    		}
    		 
    		
    		if ($gapi_client->getAccessToken()) {
    			
    			
    			
    			$accounts = $gapi_analytics->management_accounts->listManagementAccounts();
    			$profiles = $gapi_analytics->management_profiles->listManagementProfiles('~all', '~all');
    			
    			 
    			// $_SESSION['gapi_token'] = $gapi_client->getAccessToken();
    			$gapi_token  =  $this->container->get('craue_config')->set('gapi_token', $gapi_client->getAccessToken() );
    			
    			$auth = true;
    			
    			var_dump(array(), array(), $accounts, $profiles, $gapi_token);

    		}
    		
    	} catch(\Exception $e) {
    		
    		$error = $e->getMessage();
    		
    	}
    	
    	
    	
        $response = $this->render('XDCmsBundle:StatisticsAdmin:list.html.twig', array(
        		'action' => 'list',
        		'auth_url' => $gapi_client->createAuthUrl(),
        		'auth' => $auth,
        		'error' => $error
        		
        		));
        return $response;
        
        
        
        
    }
    
    
    public function oauth2callbackAction() {
    	
    	return $this->redirect($this->admin->generateUrl('list').'?code='.$_GET['code']);
    	
    	
    	
    	//$response = $this->render('XDCmsBundle:StatisticsAdmin:oauth2callback.html.twig', array('action' => 'oauth2callback'));
    	
    	//return $response;
    	
    }
    
    
    
    
    
}
