<?php

namespace XD\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PageController extends Controller
{
    /**
     *
     * 
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $query = $em->createQuery("SELECT p FROM XDCmsBundle:Page p ORDER BY p.id DESC");
        
        $pages = $query->getResult();
        
        $response = $this->render('XDCmsBundle:Page:list.html.twig', array('pages' => $pages));
        
        $response->setSharedMaxAge(10);
        $response->setMaxAge(10);

        return $response;
        
    }
    
    /**
     *
     * @Route("/{slug}.html")
     * 
     * 
     * @param string $slug
     * @return array params 
     */
    public function showAction($slug) {
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $page = $em->getRepository("XDCmsBundle:Page")->findOneBy(array('slug' => $slug, 'published' => true));
        
        
        if (!$page) {
            throw $this->createNotFoundException('The page does not exists');
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem($page->getTitle(), '');
        
        $response = $this->render('XDCmsBundle:Page:show.html.twig', array(
        		'page'=> $page,
        		'content_title' => $page->getTitle()
        		));
        
        $response->setSharedMaxAge(40);
        $response->setMaxAge(40);
        
        return $response;        
    }
    
    
    
    
}
