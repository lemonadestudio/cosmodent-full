<?php

namespace XD\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * XD\CmsBundle\Entity\MenuItem
 */
class MenuItem
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string $anchor
     */
    private $anchor;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $type
     */
    private $type;


    /**
     * @var string $url
     */
    private $url;
    
    /**
     * @var XD\CmsBundle\Entity\Page
     */
    private $page;
    
    /**
     * @var string $location
     */
    private $location;
    
    
    /**
     * @var integer $sortOrder
     * @deprecated
     */
    private $sortOrder;
    
    /**
     * 
     * @var string
     */
    private $route;
    
    /**
     * 
     * @var string
     */
    private $routeParameters;
    
    /**
     * @var array $attributes
     */
    private $attributes;
    
    
    /**
     * @var integer $left
     */
    private $lft;
    
    /**
     * @var integer $right
     */
    private $rgt;
    
    /**
     * @var integer $depth
     */
    private $depth;
    
    /**
     * @var XD\CmsBundle\Entity\MenuItem
     */
    private $parentItem;
	
    /**
     * @var XD\CmsBundle\Entity\MenuItem
     */
    private $childItems;
    
	
	public function __construct() {
		
		$this->childItems = new ArrayCollection();
		$this->depth = 1;
		$this->lft = 400;
		$this->rgt = 400;
		
		
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anchor
     *
     * @param string $anchor
     */
    public function setAnchor($anchor)
    {
        $this->anchor = $anchor;
    }

    /**
     * Get anchor
     *
     * @return string 
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }



    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    



    /**
     * Set page
     *
     * @param XD\CmsBundle\Entity\Page $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * Get page
     *
     * @return XD\CmsBundle\Entity\Page 
     */
    public function getPage()
    {
        return $this->page;
    }




    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }



    /**
     * Set attributes
     *
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Get attributes
     *
     * @return array 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
    
    public function getAttributesArray() {
    	
    	try {
	    	 $dom = new \DOMDocument();
	    	 $dom->loadHTML('<a '.$this->getAttributes().'></a>');
	    	 $s = simplexml_import_dom($dom);
	    	 $aa=(array)$s->body->a;
	    	 
    	} catch (\Exception $e) {
    		return array();
    	}
    	
    	 if(is_array($aa) && array_key_exists('@attributes', $aa)) {
    	 	return $aa['@attributes'];
    	 }
    	 
    	 return array();
	     
    }

    /**
     * Set route
     *
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set routeParameters
     *
     * @param string $routeParameters
     */
    public function setRouteParameters($routeParameters)
    {
        $this->routeParameters = $routeParameters;
    }

    /**
     * Get routeParameters
     *
     * @return string 
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }
    
    public function getRouteParametersArray() {
    	
    	
    	try {
    		$arr = json_decode($this->routeParameters);	
    	} catch(Exception $e) {
			$arr = array();
    	}
    	
    	return (array)$arr;
    }



    /**
     * Set depth
     *
     * @param integer $depth
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    /**
     * Get depth
     *
     * @return integer 
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set parentItem
     *
     * @param XD\CmsBundle\Entity\MenuItem $parentItem
     */
    public function setParentItem(\XD\CmsBundle\Entity\MenuItem $parentItem)
    {
        $this->parentItem = $parentItem;
    }

    public function setAsRoot() {
    	$this->parentItem = null;
    }

    /**
     * Get parentItem
     *
     * @return XD\CmsBundle\Entity\MenuItem 
     */
    public function getParentItem()
    {
        return $this->parentItem;
    }



    /**
     * Add childItems
     *
     * @param XD\CmsBundle\Entity\MenuItem $childItems
     */
    public function addMenuItem(\XD\CmsBundle\Entity\MenuItem $childItems)
    {
        $this->childItems[] = $childItems;
    }

    /**
     * Get childItems
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChildItems()
    {
        return $this->childItems;
    }
    
    public function setChildItems($childItems) {
    	foreach ($childItems as $childItem) {
    		
    		$this->addMenuItem($childItems);
    	}
    }

    /**
     * Set lft
     *
     * @param integer $lft
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * Get lft
     *
     * @return integer 
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * Get rgt
     *
     * @return integer 
     */
    public function getRgt()
    {
        return $this->rgt;
    }
}