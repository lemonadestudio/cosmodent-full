<?php

namespace XD\CmsBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UploadedFileAdmin extends Admin {
	
	protected $translationDomain = 'XDCmsBundle';

	
	

	public function getBatchActions()
	{
		$actions = array();
		
		// turn off batch actions
		return $actions;
		
		if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
			$actions['delete'] = array(
					'label' => $this->trans('action_delete', array(), 'SonataAdminBundle'),
					'ask_confirmation' => true, // by default always true
			);
		}
	
		return $actions;
	}
	
	
	
	
	
    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('published')
                ->add('publishDate')
                ->add('file')
                ->add('showOnList')
        ;
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
    	
        $listMapper->addIdentifier('title', null, array())
        	
				
                

        		->add('published', 'boolean')
        		->add('publishDate')
        		->add('file', null, array('template' => 'XDCmsBundle:CRUD:page_list_file.html.twig'))
        		->add('showOnList')
        ;
        
        $listMapper->add('_action', 'actions', array(
        		$this->trans('actions') => array(
        				'view' => array(),
        				'edit' => array(),
        				'delete' => array()
        		)
        ));
        
//         $listMapper->add('_batch', 'batch', array(
//         		$this->trans('actions') => array(
//         				'view' => array(),
//         				'edit' => array(),
//         				'delete' => array()
//         		)
//         ));

        
        
      
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
                
        ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
    	
        $formMapper
            ->with('General')
                ->add('title', null, array('required' => true))
               
                ->add('published', 'checkbox', array('required' => false, 'label' => 'Pokazuj w serwisie'))
                ->add('publishDate', 'datetime')
                ->add('showOnList', 'checkbox', array('required' => false, 'label' => 'Pokaż na liście') )
               
               
                ->add('_file', 'file', array('required' => false))
                
             ->end()
//              ->with('Tags', array('collapsed' => 'true'))
//                 ->add('tags', 'sonata_type_model', array("property_path" => false, 'expanded' => true, 'multiple' => true), array('edit' => 'standard'))
//              ->end()
             
            
        -> setHelps(array(
                    'title' => $this->trans('Enter page title'),
//         			'published' => 'Pokazuj w serwisie',
                    'publishDate' => $this->trans('help.publishDate'),
        			'showOnList' => 'Pokazuj na liście plików do pobrania',
        			
                ));
        

    }
    
    

}