<?php

namespace XD\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class MenuItemAdmin extends Admin {

	protected $translationDomain = 'XDCmsBundle';
	
	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'sortOrder' // field name
	);
	
	public function getTypeChoices() {
		
		 return array(
		 		'url' => $this->trans('Manual URL'),
		 		'route' => $this->trans('Module')
		 		);
	}
	
	public function getModuleChoices() {
		
		$config_menu = $this->getConfigurationPool()->getContainer()->getParameter('xd_cms.menu');
		
		$modules = array();
		
		foreach($config_menu['modules'] as $mod_name => $module) {
			
			$modules[$module['route']] = $module['label'];
			
		}
		
		return $modules;
		
	}
	
	public function getSearchServicesJson() {
		

		return json_encode($this->getSearchServices());
		
	}
	
	public function getSearchServices() {
		
		$config_menu = $this->getConfigurationPool()->getContainer()->getParameter('xd_cms.menu');
		$services = array();
		foreach($config_menu['modules'] as $mod_name => $module) {
		
			$services[$module['route']] = $module['get_elements_service'];
		
		}
		
		return $services;
		
	}
	
	
	
	public function getBatchActions() {
		return array();
		
	}
	
	protected function configureRoutes(RouteCollection $collection) {
		$collection->add('saveorder');
		$collection->add('routeparameters');
	}
	
	/**
	 * wczytuje konfigurację dostępnych lokacji
	 */
	public function getLocationChoices() {
	
		$config_menu = $this->getConfigurationPool()->getContainer()->getParameter('xd_cms.menu');
		
		
		$locations = array();
		
		foreach($config_menu['locations'] as $loc_name => $location ) {
			$locations[$loc_name] = $this->trans($location['label']);
		}
		
		return $locations;
		
		
	}
	
	
	public function getListTemplate() {
		return 'XDCmsBundle:CRUD:menuitem_list.html.twig';
	}
	
	
	public function getEditTemplate() {
	
		$template = parent::getEditTemplate();
		// SonataAdminBundle:CRUD:edit.html.twig
		
		return 'XDCmsBundle:CRUD:menuitem_edit.html.twig';
	
	}
	
    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
        		->add('location')
                ->add('title')
                ;
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
        $listMapper->addIdentifier('title')
        ->add('location')
        ->add('currentUrl', null, array('template' => 'XDCmsBundle:CRUD:menuitem_currenturl.html.twig'))
        ;
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
        ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
    	

        $formMapper
            ->with('General')
	            ->add('anchor')
	            ->add('title', null, array('required' => false))
	            ->add('location', 'choice', array('choices' => $this->getLocationChoices(), 'expanded' => false))
	            ->add('type', 'choice', array('choices' => $this->getTypeChoices()))
	            ->add('url', 'text')
	            ->add('route', 'choice', array('choices' => $this->getModuleChoices()))
	            ->add('routeParameters', 'choice', array('choices' => array(), 'label' => 'Wybierz'))
	           // ->add('page', 'sonata_type_model', array('required' => false), array('edit' => 'normal'))
        		->add('searchServices', 'hidden', array('data' => $this->getSearchServicesJson(), 'property_path' => false), array('style'=>'') )
	        ->end()
	        ->with('Advanced')
	        	->add('attributes', 'text', array('required' => false)
	        	)
	        ->end()
              
            ->setHelps(array(
            		'anchor' => $this->trans('help.menuitem.anchor'),
            		'title' => $this->trans('help.menuitem.title'),
            		'attributes' => $this->trans('help.menuitem.attributes')
            	)
           	)
           
        ;
        

    }
    
    
    
    

}