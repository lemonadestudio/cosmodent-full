<?php

namespace XD\CmsBundle\Admin;


use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GalleryPhotoAdmin extends Admin {
	
	protected $translationDomain = 'XDCmsBundle';

	
	public function showIn($context)
	{
		return false;
		switch ($context) {
			case self::CONTEXT_DASHBOARD:
			case self::CONTEXT_MENU:
			default:
				return $this->isGranted($this->getPermissionsShow($context));
		}
	}

	public function getBatchActions()
	{
		$actions = array();
		
		// turn off batch actions
		return $actions;
		
		if ($this->hasRoute('delete') && $this->isGranted('DELETE')) {
			$actions['delete'] = array(
					'label' => $this->trans('action_delete', array(), 'SonataAdminBundle'),
					'ask_confirmation' => true, // by default always true
			);
		}
	
		return $actions;
	}
	
	
	
	
	
    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('file')
        ;
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
    	
    	
        $listMapper->addIdentifier('title', null, array())
        	
	
        	
        		->add('file', null, array('template' => 'XDCmsBundle:CRUD:page_list_file.html.twig'))
        		
        ;
        
        $listMapper->add('_action', 'actions', array(
        		$this->trans('actions') => array(
        				'view' => array(),
        				'edit' => array(),
        				'delete' => array()
        		)
        ));
        
//         $listMapper->add('_batch', 'batch', array(
//         		$this->trans('actions') => array(
//         				'view' => array(),
//         				'edit' => array(),
//         				'delete' => array()
//         		)
//         ));

        
        
      
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
                
        ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
   	
    	
        $formMapper
            ->with('General')
                
                ->add('_file', 'image', array('required' => false))
                ->add('title', null, array('required' => true))
                
                //->add('page')
                
             ->end()
//              ->with('Tags', array('collapsed' => 'true'))
//                 ->add('tags', 'sonata_type_model', array("property_path" => false, 'expanded' => true, 'multiple' => true), array('edit' => 'standard'))
//              ->end()
             
            
        -> setHelps(array(
                    'title' => $this->trans('Tytuł'),

        			
                ));
        

    }
    
    

}