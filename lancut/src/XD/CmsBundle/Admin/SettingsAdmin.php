<?php

namespace XD\CmsBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SettingsAdmin extends Admin {

	protected $translationDomain = 'XDCmsBundle';

// 	public function  showIn($context) {
// 		return false;
// 	}

	protected $datagridValues = array(
			'_page'       => 1,
			'_sort_order' => 'ASC', // sort direction
			'_sort_by' => 'section' // field name
	);


	protected function configureRoutes(RouteCollection $collection) {

				// $collection->remove('create');
// 				$collection->remove('edit');
// 				$collection->remove('batch');
				$collection->remove('show');
				$collection->remove('delete');

	}

	public function getListTemplate()
	{

		return 'XDCmsBundle:Admin\Settings:list.html.twig';
	}


    public function configureListFields(ListMapper $listMapper) {

        $listMapper->addIdentifier('name')
        ->add('value')
        ->add('section')
       ;

       $listMapper->add('_action', 'actions', array(
       		$this->trans('actions') => array(
//        				'view' => array(),
       				'edit' => array(),
//        				'delete' => array()
       		)
       ));

    }

    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('section')

        ;
    }

    public function configureFormFields(FormMapper $formMapper)
    {

    	if(!$this->getRequest()->get($this->getIdParameter())) {
    		$formMapper->add('name')
    		->add('section');
    	}

        $formMapper

             // ->add('name', null, array('attr' => array('readonly' => true, 'disabled' => true)), array())
             ->add('value', 'textarea')
//               ->add('section')



        ;




    }





}