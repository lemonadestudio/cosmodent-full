<?php

namespace XD\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TagAdmin extends Admin {

    public function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('tag')
                ;
    }

   
    public function configureListFields(ListMapper $listMapper) {
        
        $listMapper->addIdentifier('tag')
                ;
        
    }
    
    public function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('tag')
            
        ;
    }
    
    public function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            
             ->add('tag')
              
            
           
        ;
        

    }

}