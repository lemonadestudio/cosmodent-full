<?php
namespace XD\CmsBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilder;

class ImageType extends AbstractType
{
	
	public function buildForm(FormBuilder $builder, array $options) {
		
		
		
		$builder->setAttribute('src', $options['src']);
		
	}
	
	
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form)
    {
    	
    	
        $view
            ->set('multipart', true)
            ->set('type', 'file')
            ->set('value', '')
            ->set('src', $form->getAttribute('src'))
        ;

    }
    
    public function getDefaultOptions(array $options)
    {
    	
    	return array(
    			'src' => '',
    			
    	);
    }
    

    public function getParent(array $options)
    {
        return 'field';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'image';
    }
}