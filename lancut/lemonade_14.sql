-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 20 Sty 2024, 11:27
-- Wersja serwera: 10.1.48-MariaDB
-- Wersja PHP: 7.2.24-0ubuntu0.18.04.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_14`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Aktualnosc`
--

CREATE TABLE `Aktualnosc` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `naStronieGlownej` tinyint(1) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publishDate` datetime DEFAULT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` longtext,
  `automaticSeo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Aktualnosc`
--

INSERT INTO `Aktualnosc` (`id`, `galeria_id`, `obrazek`, `updated_at`, `naStronieGlownej`, `title`, `published`, `publishDate`, `keywords`, `description`, `slug`, `content`, `automaticSeo`) VALUES
(1, NULL, NULL, NULL, 1, 'Nowa strona internetowa Cosmodent\'u', 1, '2013-02-11 23:10:00', 'Nowa, strona, Cosmodent,  	Tupacsum, Ipsum, nearly, gave, life,, raise, right.', 'Jest nam niezmierniło miło powitać Państwa na naszej stronie WWW. Serdecznie zapraszamy do naszego gabinetu w Łańcucie. \r\n\r\nMamy nadzieję, że dzięki nam będą się Państwo częściej uśmiechać :)', 'nowa-strona-cosmodent', '<p>\r\n	Jest nam<strong> niezmiernie miło </strong>powitać Państwa na naszej <span class=\"pink\">stronie WWW</span>. Serdecznie zapraszamy do naszego gabinetu w Łańcucie.</p>\r\n<p>\r\n	<span class=\"link\">Mamy nadzieję, że dzięki nam będą się Państwo częściej uśmiechać :)</span></p>', 0),
(2, NULL, '530cce9d91cb3.jpg', NULL, 1, 'Zapraszamy na konsultacje ortodontyczne', 1, '2014-02-25 18:10:00', 'Zapraszamy, konsultacje, ortodontyczne, Serdecznie, Państwa, zapraszamy, ortodontyczne,, zar', 'Serdecznie Państwa zapraszamy na konsultacje ortodontyczne, zarówno dzieci jak i dorosłych.', 'zapraszamy-na-konsultacje-ortodontyczne', '<p>\r\n	<strong>Serdecznie Państwa zapraszamy na konsultacje ortodontyczne, zar&oacute;wno dla dzieci jak i dorosłych.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Zadzwoń i zapisz się już teraz!</strong></p>\r\n<h2 class=\"title1\">\r\n	tel.&nbsp;730 965 666</h2>', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ContactFormMessage`
--

CREATE TABLE `ContactFormMessage` (
  `id` int(11) NOT NULL,
  `senderName` varchar(255) NOT NULL,
  `senderEmail` varchar(255) NOT NULL,
  `senderMessage` longtext NOT NULL,
  `sentAt` datetime NOT NULL,
  `senderIP` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ContactFormMessage`
--

INSERT INTO `ContactFormMessage` (`id`, `senderName`, `senderEmail`, `senderMessage`, `sentAt`, `senderIP`) VALUES
(5, 'Marcin W', 'marcin@lemonadestudio.pl', '<h3>Wiadomość z formularza:</h3>TEST <hr /><h3>Pozostałe dane:</h3>Imię: Marcin<br />Nazwisko: W<br />E-mail: marcin@lemonadestudio.pl<br />Telefon: 609212060<br />', '2013-02-18 21:48:42', '91.225.135.254'),
(6, 'magdalena dubiel', 'mada.db@interia.pl', '<h3>Wiadomość z formularza:</h3>witam\n\nchciałabym umówić się na wizytę kontrolną zębów po ciąży np w sobotę do południa, jaki bedzie to koszt \n\npozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: magdalena<br />Nazwisko: dubiel<br />E-mail: mada.db@interia.pl<br />Telefon: 512290456<br />', '2013-05-08 19:54:55', '89.230.88.99'),
(7, 'Roksana Glab', 'roksanaglab@gmail.com', '<h3>Wiadomość z formularza:</h3>witam, chyba mam paradontoze, czy mozna to wyleczyc w jeden dzien ? i dodatkowo wyrwac 2 zeby i zalozyc korony tymczasowe? ile by to wszystko kosztowalo mniej wiecej ? czy moglabym sie umowic na 12 czerwca? <hr /><h3>Pozostałe dane:</h3>Imię: Roksana<br />Nazwisko: Glab<br />E-mail: roksanaglab@gmail.com<br />Telefon: 781104330<br />', '2013-05-20 22:48:50', '94.231.249.237'),
(8, 'Edyta Rakuś', 'edi18091992@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry. Chciałabym zapytać o cennik Pani usług stomatologicznych. <hr /><h3>Pozostałe dane:</h3>Imię: Edyta<br />Nazwisko: Rakuś<br />E-mail: edi18091992@wp.pl<br />Telefon: 886022138<br />', '2013-09-17 07:03:39', '91.150.173.2'),
(9, 'Ewa Szpunar', 'eszpunar@onet.pl', '<h3>Wiadomość z formularza:</h3>chciałabym zapisać moją córkę tj.Kinga Szpunar na wizytę. <hr /><h3>Pozostałe dane:</h3>Imię: Ewa<br />Nazwisko: Szpunar<br />E-mail: eszpunar@onet.pl<br />Telefon: 698790345<br />', '2013-10-21 14:38:25', '83.6.107.98'),
(10, 'Anna Witek', 'witulaki5@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nchciałam zapoznać się z cennikiem usług, czy jest dostępny na stronie?\nPozdrawiam\nAnna Witek <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Witek<br />E-mail: witulaki5@gmail.com<br />Telefon: 535093358<br />', '2013-12-11 12:35:13', '178.43.81.72'),
(11, 'barbara Tupaj', 'tupajb@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nProszę podesłać mi cennik Państwa usług (dokładnie leczenia ubytków, czyszczenie kamienia, piaskowanie, oraz cena zdjęcia rentgenowskiego) gdyż na stronie nie znalazłam, a jestem zainteresowana leczeniem zębów w Państwa gabinecie. Dziękuję B. Tupaj <hr /><h3>Pozostałe dane:</h3>Imię: barbara<br />Nazwisko: Tupaj<br />E-mail: tupajb@wp.pl<br />Telefon: xxx xxx xxx<br />', '2013-12-16 13:32:00', '5.173.217.114'),
(12, 'Sebastian Dąbek', 'seba5r6r7@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, chciałbym umówić się na pierwszą wizytę oraz dowiedzieć się o najbliższych wolnych terminach. <hr /><h3>Pozostałe dane:</h3>Imię: Sebastian<br />Nazwisko: Dąbek<br />E-mail: seba5r6r7@wp.pl<br />Telefon: 723109743<br />', '2014-01-15 20:57:50', '178.43.41.174'),
(13, 'Jolanta Olechowska', 'olechowska.jolka@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam\nChciałabym sie zarejestrować na pierwszą wizyte stomatologiczna. <hr /><h3>Pozostałe dane:</h3>Imię: Jolanta<br />Nazwisko: Olechowska<br />E-mail: olechowska.jolka@gmail.com<br />Telefon: 669940664<br />', '2014-01-16 18:17:42', '46.174.209.246'),
(14, 'Agnieszka Misztal', 'aga.niunia.22@wp.pl', '<h3>Wiadomość z formularza:</h3>witam,chciałabym,aby  pani doradziła mi jak uzupełnic moje braki w uzębieniu i  przy pomocy zdj.panoramicznego które posiadam powiedziała,czy po usunieciu mlecznej trójki mam szanse na swojego zeba. <hr /><h3>Pozostałe dane:</h3>Imię: Agnieszka<br />Nazwisko: Misztal<br />E-mail: aga.niunia.22@wp.pl<br />Telefon: 724484084<br />', '2014-01-28 18:28:41', '91.150.173.2'),
(15, 'agnieszka misztal', 'aga.niunia.22@wp.pl', '<h3>Wiadomość z formularza:</h3>bardzo przepraszam, ale odwoluje nasza wizyte na dzien dzisiejszy, nie rezygnuje,ale w innym terminie sie zapiszę. przepraszam <hr /><h3>Pozostałe dane:</h3>Imię: agnieszka<br />Nazwisko: misztal<br />E-mail: aga.niunia.22@wp.pl<br />Telefon: 724484084<br />', '2014-02-06 08:00:04', '91.150.173.2'),
(16, 'beata smarcinska', 'shivabeata@yahoo.fr', '<h3>Wiadomość z formularza:</h3>Dzien dobry,\n\nzwracam sie do Panstwa z pytaniem, poniewaz chcialam zaplacic proteze dentystyczna i usuniecie kilku zebow i leczenie przed zalozeniem protezy osobie chorej( nosiciel wirusa aids ale nieaktywny i leczony). Czy dokanuja Panstwa takich zabiegow i jaki jest koszt protezy + leczenie. \n\nDziekuje za odpowiedz i pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: beata<br />Nazwisko: smarcinska<br />E-mail: shivabeata@yahoo.fr<br />Telefon: 0033 6 52 80 47 39<br />', '2014-02-09 09:08:04', '93.19.194.181'),
(17, 'aneta bar', 'aneta.bar@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nPisze z zwiazku z pacjentem Matthew Lister (bylismy u Pani w grudniu oststnim razem w Rzeszowie)\nmial problem z 6- tka (gorny lewy) mial tam korone ale go zaczelo to bolec wiec mial kanalowe leczenie i teraz potrzebuje koronke.\n\nChcilibysmy zapytac ile czasu by potrzeba, zeby taka u Pani zrobic i koszt,\n\nBardzo prosze o odp na maila.\nz gory dziekuje \nAneta Bar <hr /><h3>Pozostałe dane:</h3>Imię: aneta<br />Nazwisko: bar<br />E-mail: aneta.bar@gmail.com<br />Telefon: 00447960 356333<br />', '2014-02-09 18:59:23', '5.63.148.196'),
(18, 'Dorota Cisek', 'do289@wp.pl', '<h3>Wiadomość z formularza:</h3>Czy leczą Państwo w ramach umowy z NFZ? \nJeżeli nie to jaki jest koszt porady i leczenia odnośnie zaburzeń w funkcjonowaniu stawu skroniowo-żuchwowego? <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Cisek<br />E-mail: do289@wp.pl<br />Telefon: 172260118<br />', '2014-02-13 17:18:48', '91.150.173.2'),
(19, 'Dorota Cisek', 'do289@wp.pl', '<h3>Wiadomość z formularza:</h3>Czy jest możliwość spłaty aparatu ortodontycznego na dwa łuki zębowe w ratach? <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Cisek<br />E-mail: do289@wp.pl<br />Telefon: 666-544-553<br />', '2014-03-19 17:07:28', '91.150.168.186'),
(20, 'Krzysztof K', 'chomik44@op.pl', '<h3>Wiadomość z formularza:</h3>Witam\n\nChciałem zapytać o możliwą wizytę, najlepiej w godzinach popołudniowych, otóż jedna plomba częściowo odpadła, do tego drugi ubytek może uda się uratować bez leczenia kanałowego, trzeci to nie jestem pewien czy też próchnica czy to jakiś \"nadliczbowy\" ząb i szybko się psuje. Najgorsze to pozostałość po leczeniu kanałowym, plomba i resztki zęba odpadły, pozostał korzeń, i przyznam trochę się boję bo tu już pozostaje chirurgiczne usunięcie, już ponad rok minęło od kiedy się plomba rozleciała, ale zapewne i tą pozostałość trzeba usunąć. Proszę o informację na kiedy mogę się umówić i czy całkowicie prywatnie czy może częściowa refundacja. Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Krzysztof<br />Nazwisko: K<br />E-mail: chomik44@op.pl<br />Telefon: 608640432<br />', '2014-03-23 20:08:05', '91.240.130.88'),
(21, 'Katarzyna Szwed', 'kasiulek981@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry \n Czy mogłabym poprosić o jakiś cennik Państwa usług ?\nPozdrawiam serdecznie Katarzyna Szwed <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Szwed<br />E-mail: kasiulek981@wp.pl<br />Telefon: 661711286<br />', '2014-06-07 17:28:56', '212.160.172.87'),
(22, 'Katarzyna Szwed', 'kasiulek981@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam ponownie \n Czy przyjmujecie Państwo również na ubezpieczenie ?\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Szwed<br />E-mail: kasiulek981@wp.pl<br />Telefon: 661711286<br />', '2014-06-07 17:30:28', '212.160.172.87'),
(23, 'MALGORZATA KOWAL', 'MalgorzataKowal@interia.pl', '<h3>Wiadomość z formularza:</h3>DZIEN DOBRY CHCIALAM ZAPYTAC ILE KOSZTUJE WYMIANA JEDYNKI CHOC W MOIM PRZYPADKU CHYBA CHCIALAM ZAPYTAC ILE KOSZTUJE WSTAWIENIE WSZYSTKICH GORNYCH ZEBOW I WYRYWANIE ZEBOW TYCH ZNISZCZONYCH BARDZO PROSZE O ODPOWIEDZ DZIEKUJE POZDRAWIAM <hr /><h3>Pozostałe dane:</h3>Imię: MALGORZATA<br />Nazwisko: KOWAL<br />E-mail: MalgorzataKowal@interia.pl<br />Telefon: 725303007<br />', '2014-06-18 22:13:22', '195.238.171.213'),
(24, 'Mateusz Morycz', 'mateusz.morycz@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, bolała mnie \"6tka\" na dole z lewej strony jakiś czas ale brałem ibuprom i pomagało.. Przestało boleć na jakiś tydzień i \"ćmi\" mi ten ząb od 2 dni (biore ibuprom i przechodzi),W zębie nie mam żadnych widocznych ubytków a ni dziur, nie jest też jakiś czarny ani nic, ale myślałem że jest to może od moich 8semek jakiś taki ból i że to przejdzie.. dzisiaj ten ząb czyli 6tka ukruszył mi się delikatnie, nie boli ale co zrobić, czy konieczna jest wizyta u stomatologa? Proszę o odpowiedź i podanie mi w miare wolnych terminów oraz cen.\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Mateusz<br />Nazwisko: Morycz<br />E-mail: mateusz.morycz@gmail.com<br />Telefon: 783783928<br />', '2014-06-20 14:58:50', '83.28.10.214'),
(25, 'Kasia Kochmańska', 'kati.k15@interia.pl', '<h3>Wiadomość z formularza:</h3>Witam jestem zainteresowana licówkami kompozytowymi jaka jest cena jednej licówki <hr /><h3>Pozostałe dane:</h3>Imię: Kasia<br />Nazwisko: Kochmańska<br />E-mail: kati.k15@interia.pl<br />Telefon: 665740540<br />', '2014-07-11 09:45:54', '195.116.46.46'),
(26, 'Dorota Cisek', 'do289@wp.pl', '<h3>Wiadomość z formularza:</h3>Zaskoczyły mnie ostatnio tak szybkie terminy u Państwa. Czy mogłabym prosić o termin wizyty z początkiem września? <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Cisek<br />E-mail: do289@wp.pl<br />Telefon: 666-544-553<br />', '2014-08-08 16:01:04', '193.19.167.238'),
(27, 'Dorota Cisek', 'do289@wp.pl', '<h3>Wiadomość z formularza:</h3>Muszę przełamać lęk związany z inwestycją w siebie. Boję się zainwestować w siebie z różnych przyczyn. Nie podchodzę do tego tak normalnie. Cierpię na chorobę psychiczną ,w związku z czym jestem osobą niepełnosprawną, ale obecnie w dobrym stanie zdrowia od bardzo długiego czasu. Pisałam wcześniej o termin na początek września, ponieważ  mam spore kłopoty  z zebraniem się. Najlepiej gdyby to była też pora ranna, kiedy mam najwięcej połączeń autobusowych. Przepraszam za kłopot, ale z góry też dziękuję za wyrozumiałość. <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Cisek<br />E-mail: do289@wp.pl<br />Telefon: 666-544-553<br />', '2014-08-08 18:09:22', '193.19.167.238'),
(28, 'Wioletta Kulczycka', 'robert.kulc@yahoo.no', '<h3>Wiadomość z formularza:</h3>Dzień dobry.Chciałabym się dowiedzieć czy Pani doktor Sławinska zajmuje się ortodoncją i gdzie przyjmuje w Łańcucie czy w Rzeszowie. <hr /><h3>Pozostałe dane:</h3>Imię: Wioletta<br />Nazwisko: Kulczycka<br />E-mail: robert.kulc@yahoo.no<br />Telefon: 503511740<br />', '2014-09-08 13:12:14', '188.47.68.132'),
(29, 'Daniel Curkowicz', 'daniel2ah@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nNazywam się Daniel Curkowicz i jestem zainteresowany zabiegiem wybielania zębów, gdyż obecnie jest na nich żółty nalot i niezbyt ładnie to wygląda :(  mimo, że niem piję kawy, ani nie palę papierosów.\nChciałbym zapytać o cenę i czas trwania takiego zabiegu oraz jak długo czeka się na efekt widocznej poprawy stanu uzębienia (bielszy kolor) oraz jak długo się to utrzymuje. Czy jest to zabieg odnawialny i ile razy należy go powtórzyć oraz jakie są ewentualne przeciwskazania?\nChciałbym umówić się na taki zabieg, ale nie mam możliwości być w godzinach 7-17 tylko w godzinach popołudniowych (jedynie w ten piątek tj. 12.09 mógłbym się pojawić o dowolnej godzinie).\nProszę o odpowiedź. Dziękuję. <hr /><h3>Pozostałe dane:</h3>Imię: Daniel<br />Nazwisko: Curkowicz<br />E-mail: daniel2ah@wp.pl<br />Telefon: 785-025-718<br />', '2014-09-10 10:13:03', '95.48.123.105'),
(30, 'Grzegorz Bukowy', 'grzesiek.bukowy@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam\nChodzi o moja żonę. Kilka lat temu leczyła zęba kanałowo. Ostatnio zaczął ja ten ząb boleć. U obecnego stomatologa zrobiła zdjęcie RTG i okazało się, ze kanały nie zostały dobrze wypełnione. Stomatolog poradził, żeby zgłosić się do gabinetu, który leczy endoskopowo, ponieważ stwierdził że kanały są zakrzywione i boi się złamania narzędzia podczas czyszczenia. Moje pytanie - czy na podstawie zdjęcia RTG mogliby Państwo wycenić leczenie takiego zęba? <hr /><h3>Pozostałe dane:</h3>Imię: Grzegorz<br />Nazwisko: Bukowy<br />E-mail: grzesiek.bukowy@gmail.com<br />Telefon: 695320321<br />', '2014-10-07 10:38:46', '91.246.71.106'),
(31, 'Małgorzata Filip', 'malgoskafilip@o2.pl', '<h3>Wiadomość z formularza:</h3>Witam! \nProszę podać cenę usuwania kamienia i co wchodzi : skaling ,piaskowanie,fluoryzacja? <hr /><h3>Pozostałe dane:</h3>Imię: Małgorzata<br />Nazwisko: Filip<br />E-mail: malgoskafilip@o2.pl<br />Telefon: 668601261<br />', '2014-12-14 19:40:45', '159.205.192.152'),
(32, 'Ewelina Frączek', 'ewkafraczek@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nmam synka w wieku 2latka i 8 miesięcy.\nMamy problem z przednimi jedynkami na górze, są przebarwione na brązowo oraz zaatakowane tylne trzonowce, widoczne czarne środki.\nKiedy najwcześniej można się do Państwa zarejestrować?\nczy jest możliwa sobota? <hr /><h3>Pozostałe dane:</h3>Imię: Ewelina<br />Nazwisko: Frączek<br />E-mail: ewkafraczek@gmail.com<br />Telefon: 7882461<br />', '2015-01-12 14:40:29', '80.50.149.30'),
(33, 'Elżbieta Wawrzaszek', 'elaa31@interia.pl', '<h3>Wiadomość z formularza:</h3>witam, można zarejestrować się na wizytę w sobotę? <hr /><h3>Pozostałe dane:</h3>Imię: Elżbieta<br />Nazwisko: Wawrzaszek<br />E-mail: elaa31@interia.pl<br />Telefon: 721564166<br />', '2015-04-13 18:30:14', '83.31.67.222'),
(34, 'Kinga Falandysz', 'falandyszkinga@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzien dobry, chciałam zapytać jaka jest cena usuwania kamienia nazębnego i czas oczekiwania na wizyte, z góry dziękuję za odpowiedź. <hr /><h3>Pozostałe dane:</h3>Imię: Kinga<br />Nazwisko: Falandysz<br />E-mail: falandyszkinga@gmail.com<br />Telefon: 724454446<br />', '2015-04-14 13:08:42', '145.237.2.211'),
(35, 'Monika Szkutnik', 'missszkutnik@mail.com', '<h3>Wiadomość z formularza:</h3>Witam bardzo prosze o zarezerwowanie wizyty stomatologicznej w dniu 9.05.2015 (nagly przypadek). Prosze o kontakt w tej sprawie.Monika Szkutnik. <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Szkutnik<br />E-mail: missszkutnik@mail.com<br />Telefon: 792508108<br />', '2015-05-09 01:36:17', '89.188.222.187'),
(36, 'Monika Szkutnik', 'missszkutnik@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam bardzo prosze o zarezerwowanie wizyty stomatologicznej w dniu  9.05.2015 (pilny przypadek). Prosze o kontakt. Monika Szkutnik. <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Szkutnik<br />E-mail: missszkutnik@gmail.com<br />Telefon: 792508108<br />', '2015-05-09 02:39:55', '89.188.222.187'),
(37, 'Dorota Cisek', 'do289@wp.pl', '<h3>Wiadomość z formularza:</h3>Chciałabym odwołać wizytę u ortodonty 21.05.2015 o 17:00. \n                                               Dorota Cisek <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Cisek<br />E-mail: do289@wp.pl<br />Telefon: (17)2260118<br />', '2015-05-11 13:17:09', '193.19.167.238'),
(38, 'Klaudia Grzesik', 'dusia411@op.pl', '<h3>Wiadomość z formularza:</h3>Witam,\nChyciałabym się dowiedzieć czy jest u Państwa możliwość wytworzenia nakładek na górną i dolną szczękę, w celu późniejszego wybielenia zębów. Jeśli tak, to jaki byłby koszt takich nakładek?\nPozdrawiam i czekam na odpowiedź.\nKlaudia <hr /><h3>Pozostałe dane:</h3>Imię: Klaudia<br />Nazwisko: Grzesik<br />E-mail: dusia411@op.pl<br />Telefon: 880071274<br />', '2015-06-17 14:26:06', '79.139.55.126'),
(39, 'Anna Boś-Podolec', 'Anna84@wp.eu', '<h3>Wiadomość z formularza:</h3>Dzień dobry. \nJakie rodzaje znieczulenia są dostępne w Państwa placówce? <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Boś-Podolec<br />E-mail: Anna84@wp.eu<br />Telefon: 661495680<br />', '2015-07-01 08:20:42', '83.24.76.24'),
(40, 'Anna Boś-Podolec', 'Anna84@wp.eu', '<h3>Wiadomość z formularza:</h3>Dzień dobry.\nJakie rodzaje znieczulenia są dostępne w Państwa placówce? <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Boś-Podolec<br />E-mail: Anna84@wp.eu<br />Telefon: 661495680<br />', '2015-07-01 08:22:33', '83.24.76.24'),
(41, 'Beata Polak', 'bpolak76@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, proszę o cennik jeśli jest to możliwe\nDziękuję\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Beata<br />Nazwisko: Polak<br />E-mail: bpolak76@wp.pl<br />Telefon: 791459805<br />', '2015-07-18 22:06:06', '178.42.238.144'),
(42, 'Ewelina Wrona', 'wrona.ewelina@interia.pl', '<h3>Wiadomość z formularza:</h3>Dzień Dobry!\n\nChciałabym zarejestrować 5-letnią córeczkę do Pani Dr Sławińskiej-Niedzin na leczenie ząbków. Nie mogę się dodzwinić dlatego piszę. <hr /><h3>Pozostałe dane:</h3>Imię: Ewelina<br />Nazwisko: Wrona<br />E-mail: wrona.ewelina@interia.pl<br />Telefon: 695671685<br />', '2015-08-25 09:59:56', '89.188.222.150'),
(43, 'Marlena K.', 'marlena5@autograf.pl', '<h3>Wiadomość z formularza:</h3>Witam. Czy jest już możliwość u Państwa usunięcia zęba? Jeśli tak to jaki będzie koszt usunięcia górnej ósemki? <hr /><h3>Pozostałe dane:</h3>Imię: Marlena<br />Nazwisko: K.<br />E-mail: marlena5@autograf.pl<br />Telefon: 111111111<br />', '2015-09-05 16:09:20', '31.130.96.162'),
(44, 'Monika R', 'monika_w88@op.pl', '<h3>Wiadomość z formularza:</h3>Witam jaka jest cena wybielania zębów? Czy warunkiem jest wyleczenie wszystkich ewentualnych ubytków? <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: R<br />E-mail: monika_w88@op.pl<br />Telefon: 721<br />', '2015-10-23 22:46:40', '46.77.124.39'),
(45, 'KRZYSZTOF KOPEĆ', 'rover4000@op.pl', '<h3>Wiadomość z formularza:</h3>Witam chciałem umówić moją córkę na wizytę u lekarza ortodonty najchętniej w godzinach popołudniowych .Czekam na wiadomość <hr /><h3>Pozostałe dane:</h3>Imię: KRZYSZTOF<br />Nazwisko: KOPEĆ<br />E-mail: rover4000@op.pl<br />Telefon: 665801405<br />', '2016-02-04 19:07:41', '91.223.64.254'),
(46, 'Renata Szpunar', 'renciabialy@wp.pl', '<h3>Wiadomość z formularza:</h3>Strasznie boje się dentysty od poru lat nie byłam u żadnego uraz mam z dzieciństwa. Mam dolne zeby wszystkie do wyrwania. Proszę bardzo mi napisać czy robcie tak zebym nie czuła bólu najlepiej na uśpienia. <hr /><h3>Pozostałe dane:</h3>Imię: Renata<br />Nazwisko: Szpunar<br />E-mail: renciabialy@wp.pl<br />Telefon: 781297138<br />', '2016-02-17 08:17:12', '66.102.9.117'),
(47, 'Joanna Zasuń', 'asia.wawrzaszek@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nchciałabym zapytać o wolny termin do stomatologa w przyszłym tygodniu (21-26.03.2016). Proszę o kontakt.\n\nZ poważaniem,\nJoanna Zasuń <hr /><h3>Pozostałe dane:</h3>Imię: Joanna<br />Nazwisko: Zasuń<br />E-mail: asia.wawrzaszek@gmail.com<br />Telefon: 513150051<br />', '2016-03-18 08:45:58', '159.205.22.18'),
(48, 'Jarosław Faron', 'jarek_faron@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\n\nChciałbym zamówić wizytę przegląd, ewentualna naprawa (podejrzewam jedną:) i czyszczenie kamienia. Czy mają Państwo wolny termin 14/05/2016 tj. sobota o dowolnej godzinie? Proszę o informacje. Życzę miłego dnia:) <hr /><h3>Pozostałe dane:</h3>Imię: Jarosław<br />Nazwisko: Faron<br />E-mail: jarek_faron@wp.pl<br />Telefon: 0033656760609<br />', '2016-04-20 20:44:42', '109.18.69.215'),
(49, 'Elwira Kanas', 'elwira114@onet.com.pl', '<h3>Wiadomość z formularza:</h3>Witam\n\nPilnie potrzebuję wizyty,na kiedy można by bylo się umówić /??? <hr /><h3>Pozostałe dane:</h3>Imię: Elwira<br />Nazwisko: Kanas<br />E-mail: elwira114@onet.com.pl<br />Telefon: 123456789<br />', '2016-04-26 11:12:39', '83.24.209.62'),
(50, 'Marlena Kuraś', 'marlena5@autograf.pl', '<h3>Wiadomość z formularza:</h3>Witam, czy jest u Państwa możliwość wizyty w tym tygodniu w godzinach 7-11? Ząb zaczął mnie boleć i kruszy się. Proszę o odpowiedź <hr /><h3>Pozostałe dane:</h3>Imię: Marlena<br />Nazwisko: Kuraś<br />E-mail: marlena5@autograf.pl<br />Telefon: 731471433<br />', '2016-06-21 08:27:17', '31.130.96.162'),
(51, 'Monika Jarosz', 'moniapuc@gmail.com', '<h3>Wiadomość z formularza:</h3>witam\nchciałabym umówić się na pierwszą wizytę z 3,5 letnią córką (ma próchnicę na jednym ząbku) jaki możliwy termin? pasują mi godziny poranne w każdy dzień tygodnia\n\npozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Jarosz<br />E-mail: moniapuc@gmail.com<br />Telefon: 505608704<br />', '2016-07-05 19:23:23', '91.240.130.66'),
(52, 'Bożena Sura', 'bozena@sura.eu', '<h3>Wiadomość z formularza:</h3>Witam \nproszę o informację, czy wykonujecie państwo rekonstrukcję zęba na pinie, przy zastosowaniu kształtki stomatologicznej?\nW moim przypadku dotyczy to :\ngórnej dwójki(1/3 zęba własnego ,ząb żywy ,2/3 do rekonstrukcji).\nCzy zmieszczę się w 200 zł? i w tym tygodniu?\nz poważaniem bs\n\n; <hr /><h3>Pozostałe dane:</h3>Imię: Bożena<br />Nazwisko: Sura<br />E-mail: bozena@sura.eu<br />Telefon: 784 974 668<br />', '2016-07-11 13:39:24', '89.230.77.215'),
(53, 'Kasia Mazurkiewicz', '2001kasia@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam czy przyjmuja panstwo na nfz? Jesli tak to kiedy by byl wolny termin? Pozdr <hr /><h3>Pozostałe dane:</h3>Imię: Kasia<br />Nazwisko: Mazurkiewicz<br />E-mail: 2001kasia@wp.pl<br />Telefon: 726647997<br />', '2016-07-11 16:48:04', '5.172.232.245'),
(54, 'Dawid Pelc', 'dawid1234567890128@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry ja mam pytanie czy można by się umówić na wizytę w piątek na godziną 17:00 tj. 10.09.2016r w Łańcucie.\nMam spory problem z zębami przez co jest dużo do zrobienie. <hr /><h3>Pozostałe dane:</h3>Imię: Dawid<br />Nazwisko: Pelc<br />E-mail: dawid1234567890128@wp.pl<br />Telefon: 735818670<br />', '2016-09-06 15:52:31', '85.219.196.231'),
(55, 'BOGUSŁAWA Bester', 'boguslawa.bester@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam \n  \n   Mam pytanie kiedy mogę się umówić na wizytę złamał mi się ząb 4 od gory z prawej strony do doktora \n\nProszę o kontakt \nBogusława Bester <hr /><h3>Pozostałe dane:</h3>Imię: BOGUSŁAWA<br />Nazwisko: Bester<br />E-mail: boguslawa.bester@gmail.com<br />Telefon: 603851342<br />', '2016-11-29 12:29:54', '83.28.88.161'),
(56, 'Artur Krzyżak', 'artur.krzyzak@clarus.com.pl', '<h3>Wiadomość z formularza:</h3>Witam Świątecznie,  mam pytanie czy 27-29go, będzie możliwość przeprowadzenia leczenia kanałowego 6stki górnej, przez doświadczonego endodontę. Ząb jest otwarty, przygotowany do wypełnień, zrobiłem uCT. \nWszystkiego dobrego na czas Świąt Bożego Narodzenia. <hr /><h3>Pozostałe dane:</h3>Imię: Artur<br />Nazwisko: Krzyżak<br />E-mail: artur.krzyzak@clarus.com.pl<br />Telefon: 502721555<br />', '2016-12-25 12:06:49', '159.205.197.172'),
(57, 'Anna Sroczyk', 'anuss92@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzien dobry jaka cena pierwszej wizyty kontrolnej dla 2latki? pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Sroczyk<br />E-mail: anuss92@gmail.com<br />Telefon: 783469499<br />', '2017-01-11 09:09:38', '91.237.160.1'),
(58, 'Magdalena O', 'magda.ordon101@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzien dobry.\nChcialam zapytac jaki jest u Panstwa koszt lecenia kanalowego?\nPozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Magdalena<br />Nazwisko: O<br />E-mail: magda.ordon101@gmail.com<br />Telefon: 782043519<br />', '2017-01-18 18:58:20', '94.254.147.237'),
(59, 'Mateusz A', 'mateonet@op.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry. Chciałbym się zapytać, czy stosujecie Państwo np. gaz dla dzieci w sytuacji borowania ząbka, ze względu na stres dziecka? Dziecko 4 latka. Dziękuję za odpowiedź. <hr /><h3>Pozostałe dane:</h3>Imię: Mateusz<br />Nazwisko: A<br />E-mail: mateonet@op.pl<br />Telefon: 6<br />', '2017-02-21 11:11:11', '83.31.145.254'),
(60, 'beata malicka', 'ermionka@go2.pl', '<h3>Wiadomość z formularza:</h3>Witam\nProszę o wyznaczenie pilnej wizyty dla syna Szymona Malickiego 9 lat. <hr /><h3>Pozostałe dane:</h3>Imię: beata<br />Nazwisko: malicka<br />E-mail: ermionka@go2.pl<br />Telefon: 723660653<br />', '2017-05-01 21:13:22', '91.240.130.103'),
(61, 'Renata Cieslachowska', 'robertcs@onet.eu', '<h3>Wiadomość z formularza:</h3>Witam. Chciałabym umówić się na pierwszą wizytę w celu porady dotyczącej protetyki. W tym tygodniu odpowiadają mi godziny poranne, w przyszłym popołudniowe i tak na zmianę <hr /><h3>Pozostałe dane:</h3>Imię: Renata<br />Nazwisko: Cieslachowska<br />E-mail: robertcs@onet.eu<br />Telefon: 535722718<br />', '2017-06-27 13:27:25', '91.240.139.2'),
(62, 'Aneta Stróż', 'anetlic@o2.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\n\nchciałabym się umówić na wizytę.\n\nchodzi mi o badanie ogólne oraz konsultacje ortodontyczną.\n\nnajlepiej po 17:00.\n\n\nPozdrawiam serdecznie,\nAneta Stróż <hr /><h3>Pozostałe dane:</h3>Imię: Aneta<br />Nazwisko: Stróż<br />E-mail: anetlic@o2.pl<br />Telefon: 662822715<br />', '2017-07-04 13:56:09', '46.165.168.132'),
(63, 'Angelika Dąbska', 'angelika.dabska@onet.pl', '<h3>Wiadomość z formularza:</h3>Prosze umowic mnie na wizyte u Pana Igora na usunięcie zęba. Kiedy najszybciej ma wolne terminy? <hr /><h3>Pozostałe dane:</h3>Imię: Angelika<br />Nazwisko: Dąbska<br />E-mail: angelika.dabska@onet.pl<br />Telefon: 724611037<br />', '2017-08-02 23:13:20', '91.196.10.8'),
(64, 'Amna Sowa', 'pycka1@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam przebywam na urlopie za 2tg wracam do Holandii potrzebuje leczenia wszystkich zembow oraz konsultacje co do 2zemba ponieważ stwierdzono że trzeba go usunąć a ja w pracy nie mogę sobie pozwolić na bycie bez zemną 4miesiace po dopiero wtedy będę w Polsce kolejny raz pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Amna<br />Nazwisko: Sowa<br />E-mail: pycka1@onet.pl<br />Telefon: 789439992<br />', '2017-08-04 13:52:10', '31.0.86.57'),
(65, 'Aga Grabarz', 'gosia28-29@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam czy wasz gabinet ma podpisna umowę z NFZ czy tylko prywatnie? U w jakich godzinach przyjmujecie? I czy można z bólem zęba dziecka 4 letniego przyjechać w każdej chwili? <hr /><h3>Pozostałe dane:</h3>Imię: Aga<br />Nazwisko: Grabarz<br />E-mail: gosia28-29@wp.pl<br />Telefon: 667931914<br />', '2017-08-07 15:05:28', '85.219.196.38'),
(66, 'justyna dremo', 'justyna19071982@interia.pl', '<h3>Wiadomość z formularza:</h3>jakie godziny pracy i ceny leczenia lakowania zebów.dziecko ma 3 latka i jest pierwszy raz u dentysty. <hr /><h3>Pozostałe dane:</h3>Imię: justyna<br />Nazwisko: dremo<br />E-mail: justyna19071982@interia.pl<br />Telefon: 0000000000000000000<br />', '2017-08-28 21:15:08', '5.172.238.56'),
(67, 'Dorota Grzesik', 'dorota.grzesik@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam\nPisze do Państwa w sprawie usunięcia zęba czy można by było się umówić na środę w przyszłym tygodniu  gdyż mam w tedy wolny dzień a moja Pani dentystka boi się go ruszyć <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Grzesik<br />E-mail: dorota.grzesik@onet.pl<br />Telefon: 727683764<br />', '2017-08-29 11:24:54', '5.60.28.144'),
(68, 'justyna dremo', 'justyna19071982@interia.pl', '<h3>Wiadomość z formularza:</h3>WITAM. MAM 3LETNIE DZIECKO KTORE MA DUZE UBYTKI DO LECZENIA . CZYM ZNIECZULACIE MAŁE DZIECI. <hr /><h3>Pozostałe dane:</h3>Imię: justyna<br />Nazwisko: dremo<br />E-mail: justyna19071982@interia.pl<br />Telefon: ====================================<br />', '2017-09-13 21:34:57', '5.172.235.93'),
(69, 'justyna dremo', 'justyna19071982@interia.pl', '<h3>Wiadomość z formularza:</h3>WITAM. MAM 3LETNIE DZIECKO KTÓRE MA DUZE UBYTKI W ZEBACH , NADAJA SIE DO LECZENIA . CZYM ZNIECULACIE. PROSZE O ODP. POZDRAWIAM. <hr /><h3>Pozostałe dane:</h3>Imię: justyna<br />Nazwisko: dremo<br />E-mail: justyna19071982@interia.pl<br />Telefon: 000000000000000000000<br />', '2017-09-13 21:40:29', '5.172.235.93'),
(70, 'Sabina Noga', 'sakulas1@o2.pl', '<h3>Wiadomość z formularza:</h3>Dzień Dobry, Proszę o pilne przyjęcie w poniedziałek córki wiek 5 lat powód ból zęba. Proszę o kontakt.\nZ góry dziękuję. <hr /><h3>Pozostałe dane:</h3>Imię: Sabina<br />Nazwisko: Noga<br />E-mail: sakulas1@o2.pl<br />Telefon: 788839161<br />', '2017-12-09 19:59:32', '5.172.255.44'),
(71, 'Łukasz Kuduk', 'MrKiu3@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam, chciałbym zapytać ile kosztuje aparat ortodontyczny na górę zębów ? \nDla szesnastolatka <hr /><h3>Pozostałe dane:</h3>Imię: Łukasz<br />Nazwisko: Kuduk<br />E-mail: MrKiu3@gmail.com<br />Telefon: 577 986 915<br />', '2017-12-22 17:59:30', '85.219.196.30'),
(72, 'Marta Inglot', 'inglot.marta@gmail.com', '<h3>Wiadomość z formularza:</h3>Mam bardzo pilną sprawę. Złamala mi się jedynka i pilnie potrzebuję dentysty, aby naprawił ubytek dzisiaj rano. <hr /><h3>Pozostałe dane:</h3>Imię: Marta<br />Nazwisko: Inglot<br />E-mail: inglot.marta@gmail.com<br />Telefon: 602765764<br />', '2018-01-04 06:41:09', '83.31.73.78'),
(73, 'Katarzyna Kiwała', 'kajamaruszak15124@wp.pl', '<h3>Wiadomość z formularza:</h3>Witam. Proszę o informację na kiedy w najbliższym terminie(po godz. 16)jest możliwość umówienia dziecka na wizytę z powodu bolące zęba. Pozdrawiam.  Kiwała Katarzyna <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Kiwała<br />E-mail: kajamaruszak15124@wp.pl<br />Telefon: 601985804<br />', '2018-01-16 22:23:53', '31.130.96.139'),
(74, 'justyna dremo', 'justyna19071982@interia.pl', '<h3>Wiadomość z formularza:</h3>w jakich godz przyjmujecie i tel do rejestracji <hr /><h3>Pozostałe dane:</h3>Imię: justyna<br />Nazwisko: dremo<br />E-mail: justyna19071982@interia.pl<br />Telefon: 00000000000000<br />', '2018-01-29 21:54:09', '37.248.156.169'),
(75, 'Ewelina Konieczna', 'ekonieczna@onet.pl', '<h3>Wiadomość z formularza:</h3>Witam, czy jest możliwość umówienia wizyty w celu konsultacji na miesiąc marzec? Proszę o podanie wolnego terminu po 10tym dniu miesiąca. \nZ góry dziękuję za wiadomość. <hr /><h3>Pozostałe dane:</h3>Imię: Ewelina<br />Nazwisko: Konieczna<br />E-mail: ekonieczna@onet.pl<br />Telefon: 733352771<br />', '2018-02-12 01:42:59', '94.254.144.126'),
(76, 'Natalia Michno', 'dodi123@buziaczek.pl', '<h3>Wiadomość z formularza:</h3>Witam ,  chciałabym przyjść z synem 3letnim na wizyte kontrolna. Jaka cena usługi ?;) <hr /><h3>Pozostałe dane:</h3>Imię: Natalia<br />Nazwisko: Michno<br />E-mail: dodi123@buziaczek.pl<br />Telefon: 530044511<br />', '2018-02-16 22:49:32', '91.240.139.5'),
(77, 'Martyna Lubas', 'lubas.martyna@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry, czy jest szansa na wizytę dzisiaj, tj. 12.03.2018, ponieważ w nocy zauważyłam, że bardzo podniosły mi się dziąsła górnez zewnątrz,  jedynki zaczęły mi się ruszać, a dziąsła wewnątrz jamy ustnej na górnych jednynkach prawie nie ma. <hr /><h3>Pozostałe dane:</h3>Imię: Martyna<br />Nazwisko: Lubas<br />E-mail: lubas.martyna@gmail.com<br />Telefon: 781116571<br />', '2018-03-12 06:32:48', '31.130.96.186'),
(78, 'Natalia Miąsik', 'nati-rajzer@wp.pl', '<h3>Wiadomość z formularza:</h3>Dobry wieczór:) chciałam zapytać czy maja Państwo jakiś wolny termin w przyszłym tygodniu  na piaskowanie? najlepiej od poniedziałku do czwartku,może być w godzinach porannych. Bardzo proszę o kontakt <hr /><h3>Pozostałe dane:</h3>Imię: Natalia<br />Nazwisko: Miąsik<br />E-mail: nati-rajzer@wp.pl<br />Telefon: 731846067<br />', '2018-03-13 18:12:15', '91.48.88.118'),
(79, 'Katarzyna Świętoniowska', 'kswietoniowska@interia.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry. Czy byłaby szansa na wizytę u Państwa w jak najszybszym terminie. Chodzi o bolący ząb, który był już leczony. <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Świętoniowska<br />E-mail: kswietoniowska@interia.pl<br />Telefon: 607697970<br />', '2018-03-27 19:17:53', '94.254.151.7'),
(80, 'Justyna Orzechowska', 'tysia.orzechowska@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry. Chciałabym zapisać dwójkę moich dzieci (wiek 7 i 4 lata) na wizytę/ przegląd stomatologiczny, byłaby to ich pierwsza wizyta u stomatologa, prosiłabym w razie możliwości o jeden termin dla obydwu. <hr /><h3>Pozostałe dane:</h3>Imię: Justyna<br />Nazwisko: Orzechowska<br />E-mail: tysia.orzechowska@gmail.com<br />Telefon: 503403310<br />', '2018-05-15 13:36:49', '91.246.70.4'),
(81, 'Sylwia Skomra', 'sylwiaskading@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam chcialabym sie dowiedziec czy mozna sie do Pani zapisac na leczenie zebow .W Lancucie bede od 27/07 do 03/08 .Czy bedzie miala pani jakis wolny termin w tym przedziale czasowym .\nZ gory dziekuje \nSylwia <hr /><h3>Pozostałe dane:</h3>Imię: Sylwia<br />Nazwisko: Skomra<br />E-mail: sylwiaskading@gmail.com<br />Telefon: 07803824449<br />', '2018-06-19 21:27:36', '109.156.231.66'),
(82, 'Karolina Nizioł', 'karolina810@o2.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, chciałam zapytać czy jest mozliwosc umówić sie na wizytę na 23.07. 2018r. Chodzi mi o higienizacje jamy ustnej. Z góry dziekuje za informację <hr /><h3>Pozostałe dane:</h3>Imię: Karolina<br />Nazwisko: Nizioł<br />E-mail: karolina810@o2.pl<br />Telefon: 530287257<br />', '2018-06-20 20:32:19', '84.81.199.127'),
(83, 'Kinga Mól', 'kinga.lew@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam,\nProszę o informację, czy można u Państwa założyć aparat lingwalny, jeżeli tak, to jaki jest jego koszt (łuk górny) oraz ile kosztuje po kolei: wizyty, założenie, zdjęcie (+ ewentualne inne).\nPozdrawiam\nKinga Mól <hr /><h3>Pozostałe dane:</h3>Imię: Kinga<br />Nazwisko: Mól<br />E-mail: kinga.lew@gmail.com<br />Telefon: 697273609<br />', '2018-08-04 22:52:48', '91.240.139.5'),
(84, 'Bernardeta Nosek', 'bernatka.nosek@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry\nJaki najszybszy termin mają Państwo dla osoby dorosłe ? <hr /><h3>Pozostałe dane:</h3>Imię: Bernardeta<br />Nazwisko: Nosek<br />E-mail: bernatka.nosek@wp.pl<br />Telefon: 731723224<br />', '2018-08-18 16:10:45', '89.230.80.32'),
(85, 'Sylwester Partyka', 'partykasylwek@wp.pl', '<h3>Wiadomość z formularza:</h3>Konsultacja ortodontyczna córki wiek 14 lat <hr /><h3>Pozostałe dane:</h3>Imię: Sylwester<br />Nazwisko: Partyka<br />E-mail: partykasylwek@wp.pl<br />Telefon: 692881161<br />', '2018-08-25 10:16:09', '77.114.84.223'),
(86, 'Łukasz Seniw', 'ukaszseniw@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry.\nChciałbym się dowiedzieć jaki byłby koszt usługi skalingu, piaskowania i fluoryzacji :)\nPozdrawiam Łukasz <hr /><h3>Pozostałe dane:</h3>Imię: Łukasz<br />Nazwisko: Seniw<br />E-mail: ukaszseniw@gmail.com<br />Telefon: 664188662<br />', '2018-09-01 11:17:42', '91.189.222.6'),
(87, 'Renata Rejman', 'renatka90@onet.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry chciałam się zapisać na wizytę w sprawie zębów czy można jeszcze się zapisać i czy to jest płatne czy na ubezpieczenie. I kiedy mogę przyjechać. Bardzo dziękuję proszę o kontakt. <hr /><h3>Pozostałe dane:</h3>Imię: Renata<br />Nazwisko: Rejman<br />E-mail: renatka90@onet.pl<br />Telefon: 575254480<br />', '2018-10-17 08:46:04', '5.173.96.14'),
(88, 'Anna Jagielska', 'halakalin2015@hotmail.com', '<h3>Wiadomość z formularza:</h3>Witam chciała bym sobie u państwa usunąć zęby ponieważ mam zepsute i mam dużo korzeni i chcia£a bym wstawić sobie sztuczną szczeke górną i dolną dowidzenia <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Jagielska<br />E-mail: halakalin2015@hotmail.com<br />Telefon: 693128443<br />', '2018-10-22 22:14:12', '37.7.57.214'),
(89, 'Marzena Szczęch', 'marzen.szczech@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry\nProszę umówienie na wizytę mojej córki Natalii (10 lat), do usunięcia zęby wrosnięte w dziąsło <hr /><h3>Pozostałe dane:</h3>Imię: Marzena<br />Nazwisko: Szczęch<br />E-mail: marzen.szczech@gmail.com<br />Telefon: 692652588<br />', '2018-10-25 09:07:09', '31.130.96.164'),
(90, 'Anna Sroczyk', 'Ajanas@tlen.pl', '<h3>Wiadomość z formularza:</h3>Witam. Poszukuję ortodonta dla 7latka. Czy można u Państwa umówić się  na konsultację  i jaki jest przybliżony  termin.dziękuję za odpowiedź. Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Sroczyk<br />E-mail: Ajanas@tlen.pl<br />Telefon: 600452530<br />', '2018-11-18 13:52:49', '85.219.196.31'),
(91, 'Krzysztof Panek', 'k.panek@interia.pl', '<h3>Wiadomość z formularza:</h3>Witam. Czy jest szansa dziś na wizytę z bolącym zębem u dziecka? <hr /><h3>Pozostałe dane:</h3>Imię: Krzysztof<br />Nazwisko: Panek<br />E-mail: k.panek@interia.pl<br />Telefon: 600012298<br />', '2018-12-01 09:00:25', '91.240.130.248'),
(92, 'Krzysztof Panek', 'k.panek@interia.pl', '<h3>Wiadomość z formularza:</h3>Witam. Czy j strona szansa  dziś na wizytę z bolącym zębem pięcioletniej dziewczynki ? Już mieliśmy wizytę kiedyś u Państwa <hr /><h3>Pozostałe dane:</h3>Imię: Krzysztof<br />Nazwisko: Panek<br />E-mail: k.panek@interia.pl<br />Telefon: 600012298<br />', '2018-12-01 09:03:07', '91.240.130.248'),
(93, 'Monika Jamróż', 'suzuki2014@interia.eu', '<h3>Wiadomość z formularza:</h3>Witam serdecznie. Jestem zainteresowana usługą panstwa gabinetu. Interesuje mnie ortodoncja. Mam pytanie. Czy istnieje u panstwa mozliwosc rozlozenia kosztow aparatu ortodontycznego na raty? <hr /><h3>Pozostałe dane:</h3>Imię: Monika<br />Nazwisko: Jamróż<br />E-mail: suzuki2014@interia.eu<br />Telefon: 727940956<br />', '2019-01-11 14:30:28', '188.146.160.57'),
(94, 'Maciej Bosak', 'bosakmaciej150@gmail.com', '<h3>Wiadomość z formularza:</h3>Chciałbym się zapytać cz leczenie kanałowe jest u państwa płatne jeżeli tak to ile kosztuje <hr /><h3>Pozostałe dane:</h3>Imię: Maciej<br />Nazwisko: Bosak<br />E-mail: bosakmaciej150@gmail.com<br />Telefon: 790663728<br />', '2020-02-06 13:22:15', '141.101.98.129'),
(95, 'Dawid Fleszar', 'sawid@vp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, Czy mógłbym się umówić do Pani Sławińskiej-Niedzin na usuwanie kamienia? <hr /><h3>Pozostałe dane:</h3>Imię: Dawid<br />Nazwisko: Fleszar<br />E-mail: sawid@vp.pl<br />Telefon: 503508770<br />', '2020-02-13 18:12:54', '141.101.77.91'),
(96, 'Agnieszka Pawlikowska', 'a.pawlikowska@interia.eu', '<h3>Wiadomość z formularza:</h3>Witam,\n\nSzukam gabinetu w którym byłaby możliwość wstawienia kilku zębów metodą implantologiczną. W związku z powyższym mam kilka pytań. Oto one:\n- ile wynosi przeciętny koszt wstawiania zęba poprzez implant (cena całościowa bez ukrytych kosztów)\n- czy u Państwa istnieje opcja natychmiastowego wstawienia implantu z koroną\n- jaki byłby szacowany najszybszy termin wizyty konsultacyjnej oraz jej koszt.\n\nPiszę wiadomość, bo w pracy mam bardzo słaby zasięg, więc w chwili obecnej muszę ograniczyć się jedynie do tej formy kontaktu, za co bardzo przepraszam.\n\nZ góry dziękuję za odpowiedź.\n\nPozdrawiam serdecznie i życzę miłego dnia :)\n\nA. Pawlikowska <hr /><h3>Pozostałe dane:</h3>Imię: Agnieszka<br />Nazwisko: Pawlikowska<br />E-mail: a.pawlikowska@interia.eu<br />Telefon: 724 861 348<br />', '2020-02-20 12:11:37', '162.158.158.252'),
(97, 'Jarosław Łoza', 'jaroslawloza@gmail.com', '<h3>Wiadomość z formularza:</h3>dzień dobry, chciałbym się umówić na wizyte w sprawie leczenia bolącego zęba. Czy moga mi państwo podać najbliższe wolne terminy? Pozdrawiam <hr /><h3>Pozostałe dane:</h3>Imię: Jarosław<br />Nazwisko: Łoza<br />E-mail: jaroslawloza@gmail.com<br />Telefon: 883994779<br />', '2020-03-09 10:49:52', '172.68.215.35'),
(98, 'Adrian Mokrzycki', 'adrian.mokrzycki12.04@gmail.com', '<h3>Wiadomość z formularza:</h3>Witam. Chciałbym umówić sie na wizytę. Wizyta związana z ciągłym bólem zęba. Czekam na odpowiedź <hr /><h3>Pozostałe dane:</h3>Imię: Adrian<br />Nazwisko: Mokrzycki<br />E-mail: adrian.mokrzycki12.04@gmail.com<br />Telefon: 791514929<br />', '2020-04-08 19:44:21', '162.158.103.220'),
(99, 'Anna Serafin', 'anna.serafin@wp.pl', '<h3>Wiadomość z formularza:</h3>Chciałabym sie dowiedzieć o ceny założenia korony na zęba trzonowego u góry  z którego została tylko jedna ścianka od strony policzka .  \npozdrawiam serdecznie <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Serafin<br />E-mail: anna.serafin@wp.pl<br />Telefon: +48796874104<br />', '2020-04-16 16:09:00', '172.68.215.30'),
(100, 'Katarzyna Magon', 'Kasienka111@o2.pl', '<h3>Wiadomość z formularza:</h3>Szanowni Panstwo\nChcialam zapytac czy moglabym zapisac córkę 2.5 letnia na usuwanie kamienia na zebach i jaki termin wchodzi w gre. Z gory dziekuje za informacje.\nPozdrawiam\nKatarzyna M. <hr /><h3>Pozostałe dane:</h3>Imię: Katarzyna<br />Nazwisko: Magon<br />E-mail: Kasienka111@o2.pl<br />Telefon: 785513387<br />', '2020-06-10 16:06:48', '162.158.103.156'),
(101, 'Regina Krzanicka', 'regina.krzanicka@tlen.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry \nPotrzebuje kompleksowego leczenia próchnicy zębów,  zaniedbane przez lata z uwagi na brak środków  na leczenie   które wpłynęło na moje uzębienie w tym  panicxny  strach i omdlenia\nJestem załamana i jest mi wstyd gdyż wszystkie  zęby a połowę nie mam i  muszą być leczone. Chce znaleźć  pracę a to powoduje u mnie brak pewności  siebie i mój wygląd nie zachęca do zatrudnienia. \nChce wyjść do ludzi \nBardzo liczę na Państwa pomoc <hr /><h3>Pozostałe dane:</h3>Imię: Regina<br />Nazwisko: Krzanicka<br />E-mail: regina.krzanicka@tlen.pl<br />Telefon: 793988727<br />', '2020-10-08 11:32:29', '162.158.103.176'),
(102, 'Regina Krzanicka', 'regina.krzanicka@tlen.pl', '<h3>Wiadomość z formularza:</h3>Potrzebuje kompleksowego leczenia próchnicy zębów,  zaniedbane przez lata z uwagi na brak środków  na leczenie   które wpłynęło na moje uzębienie w tym  panicxny  strach i omdlenia\nJestem załamana i jest mi wstyd gdyż wszystkie  zęby a połowę nie mam i  muszą być leczone. Chce znaleźć  pracę a to powoduje u mnie brak pewności  siebie i mój wygląd nie zachęca do zatrudnienia. \nChce wyjść do ludzi \nBardzo liczę na Państwa pomoc <hr /><h3>Pozostałe dane:</h3>Imię: Regina<br />Nazwisko: Krzanicka<br />E-mail: regina.krzanicka@tlen.pl<br />Telefon: 793988727<br />', '2020-10-08 11:54:05', '162.158.103.40'),
(103, 'Dorota łach', 'dorotalach8@wp.pl', '<h3>Wiadomość z formularza:</h3>dzień dobry\nChciałam zapytać o zabieg wybielania zębów i jego cenę <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: łach<br />E-mail: dorotalach8@wp.pl<br />Telefon: +48664407842<br />', '2021-10-12 13:58:36', '162.158.203.15'),
(104, 'Dorota Sałówka', 'd.salowka@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry ,\nChciałam zapytać czy można umówić się na wizytę w celu generalnego przeglądu zębow, leczenia ubytkow i porady dentystycznej w terminie między 4 a 11 listopada?\nDziękuję \nDorota Sałówka <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: Sałówka<br />E-mail: d.salowka@wp.pl<br />Telefon: 668406257<br />', '2021-10-27 11:57:28', '162.158.89.74'),
(105, 'Mirosława Frączek', 'fraczek.mi@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry.\nCórkę - 8 lat bardzo boli ząb. Czy jest możliwość szybkiego przyjęcia? <hr /><h3>Pozostałe dane:</h3>Imię: Mirosława<br />Nazwisko: Frączek<br />E-mail: fraczek.mi@gmail.com<br />Telefon: 726153823<br />', '2022-07-20 11:03:43', '162.158.202.249'),
(106, 'Agnieszka Li', 'apaczek5@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry, mam pytanie dotyczące aparatu ortodontycznego i całego procesu leczenia. Czy jest możliwość rozłożenia tego na raty ? Z góry dziękuję za informację . <hr /><h3>Pozostałe dane:</h3>Imię: Agnieszka<br />Nazwisko: Li<br />E-mail: apaczek5@gmail.com<br />Telefon: 574471916<br />', '2022-08-16 08:02:40', '162.158.203.144'),
(107, 'Olga Bulavinskaya', 'medvolya2011@gmail.com', '<h3>Wiadomość z formularza:</h3>Potrzebuję zapisać dziecko (9 lat) na konsultację ortodonta <hr /><h3>Pozostałe dane:</h3>Imię: Olga<br />Nazwisko: Bulavinskaya<br />E-mail: medvolya2011@gmail.com<br />Telefon: +48886968401<br />', '2022-09-07 22:13:56', '172.70.246.101'),
(108, 'Dorota PELC', 'dorotabalawender@interia.pl', '<h3>Wiadomość z formularza:</h3>Proszę o informację jaka cena jest usługi leczenia kanałowego pod mikroskopem po rewitalizacji z zębiakiem. <hr /><h3>Pozostałe dane:</h3>Imię: Dorota<br />Nazwisko: PELC<br />E-mail: dorotabalawender@interia.pl<br />Telefon: 510290821<br />', '2022-09-26 19:50:10', '172.68.51.154'),
(109, 'Anna Drozd', 'drozdanna5@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry,\nchciałam zapytać o koszt pierwszej konsultacji ortodontycznej dla dziecka oraz najbliższy wolny termin.\nZ góry dziękuje za odpowiedz. <hr /><h3>Pozostałe dane:</h3>Imię: Anna<br />Nazwisko: Drozd<br />E-mail: drozdanna5@gmail.com<br />Telefon: 509695356<br />', '2023-02-09 13:35:14', '162.158.102.121'),
(110, 'joanna kane', 'joannazkane@gmail.com', '<h3>Wiadomość z formularza:</h3>jaka cena botox? jestem zainteresowana zabiegiem przed 7 maja. Zalezy mi na czole, okolice oczu i bruzda pomiedzy brwiami. Dziekuje <hr /><h3>Pozostałe dane:</h3>Imię: joanna<br />Nazwisko: kane<br />E-mail: joannazkane@gmail.com<br />Telefon: 0017732256216<br />', '2023-04-24 15:28:31', '162.158.102.80'),
(111, 'Paweł Duliban', 'pawelbudda@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry \nChciałbym się umówić do państwa na pierwsza wizytę. Mam ubytek w zębie nr 5 na dole po mojej prawej stronie. Jaki byłby czas oczekiwania na taka wizytę? Z góry dziękuję za odpowiedź\n\nPozdrawiam\nPaweł Duliban <hr /><h3>Pozostałe dane:</h3>Imię: Paweł<br />Nazwisko: Duliban<br />E-mail: pawelbudda@wp.pl<br />Telefon: 604190420<br />', '2023-06-21 12:56:06', '172.68.138.79'),
(112, 'Justyna Słonina', 'jnykiel00@gmail.com', '<h3>Wiadomość z formularza:</h3>Dzień dobry, czy usuną Państwo ósemkę? Jestem w ciąży, obecnie 2trymestr. <hr /><h3>Pozostałe dane:</h3>Imię: Justyna<br />Nazwisko: Słonina<br />E-mail: jnykiel00@gmail.com<br />Telefon: 663665995<br />', '2023-08-16 08:48:50', '162.158.102.233'),
(113, 'Natalia Rejman', 'natalia_rejman@wp.pl', '<h3>Wiadomość z formularza:</h3>Dzień dobry, \nproszę o cennik Państwa usług z zakresu medycyny estetycznej. \nPozdrawiam \nNatalia Rejman <hr /><h3>Pozostałe dane:</h3>Imię: Natalia<br />Nazwisko: Rejman<br />E-mail: natalia_rejman@wp.pl<br />Telefon: 664963425<br />', '2024-01-08 12:37:24', '172.68.138.106');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `craue_config_setting`
--

CREATE TABLE `craue_config_setting` (
  `name` varchar(255) NOT NULL,
  `value` varchar(10000) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `craue_config_setting`
--

INSERT INTO `craue_config_setting` (`name`, `value`, `section`) VALUES
('employees_main_photo', '/bundles/lmcms/images/top_pracownicy.png', 'Adres url do zdjęcia głównego w module \"nasz zespół\" np \"/bundles/lmcms/images/NAZWA_ZDJECIA\".'),
('formularz_wysylka_email', 'lancut@cosmodent.com.pl', NULL),
('formularz_wysylka_od', 'Cosmodent Łańcut', NULL),
('formularz_wysylka_temat', 'Formularz kontaktowy COSMODENT Łańcut', NULL),
('kod_analytics', '<script type=\"text/javascript\">\r\n\r\n var _gaq = _gaq || [];\r\n _gaq.push([\'_setAccount\', \'UA-38706337-1\']);\r\n _gaq.push([\'_trackPageview\']);\r\n\r\n (function() {\r\n   var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;\r\n   ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';\r\n   var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);\r\n })();\r\n\r\n</script>', 'Statystyki'),
('link_facebook', 'https://www.facebook.com/CosmodentStomatologia/', NULL),
('link_instagram', 'https://www.instagram.com/', NULL),
('link_lekarz_na_stronie_glownej', '/kompetencje.html', NULL),
('seo_description', 'Praktyka stomatologiczna i medycyna estetyczna Cosmodent Łańcut. Zapraszamy!', NULL),
('seo_keywords', 'stomatolog, stomatologia, praktyka stomatologiczna, Cosmodent, stomatologia dziecięca', NULL),
('seo_title', 'Cosmodent - Stomatolog Łańcut | Medycyna estetyczna Łańcut', 'SEO'),
('tekst_formularz_podziekowanie', 'Twoja wiadomość została wysłana.<br />\r\n<br />\r\nDziękuję za kontakt :)', NULL),
('tekst_haslo_strona_glowna', 'Przyjmujemy nagłe przypadki!\r\nLeczymy bez bólu', 'Tekst'),
('tekst_stopka_adres', 'Łańcut, ul. Piłsudskiego 52', NULL),
('tekst_stopka_copyright', '&copy; Copyrights 2019, COSMODENT - Praktyka Stomatologiczna\r\n<br />\r\n<div style=\"padding-top: 10px\">\r\nRealizacja: <a style=\"color:black\" href=\"http://www.lemonadestudio.pl\" title=\"Lemonade Studio\">Lemonade Studio</a>\r\n</div>', NULL),
('tekst_strona_glowna_telefon', '730 965 666', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Employee`
--

CREATE TABLE `Employee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext,
  `position` longtext,
  `specializations` longtext,
  `slug` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `entityOrder` int(11) NOT NULL,
  `largePhoto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Employee`
--

INSERT INTO `Employee` (`id`, `name`, `description`, `position`, `specializations`, `slug`, `photo`, `isActive`, `createdAt`, `updatedAt`, `entityOrder`, `largePhoto`) VALUES
(3, 'Igor Wełyki', '<p>\r\n	<strong>Absolwent Uniwersytetu Medycznego we Lwowie</strong>, nostryfikował dyplom na Warszawskim Uniwersytecie Medycznym. W zawodzie pracuje od 2005 roku,&nbsp;<strong>z kliniką Cosmodent związany od roku 2014</strong>. Specjalizuje się w chirurgii stomatologicznej, endodoncji, protetyce i stomatologii estetycznej. Stale uczestniczy w kursach podnoszących kwalifikacje zawodowe. Ceni dobry kontak z pacjentem w każdym wieku. Jego hobby to kolarstwo g&oacute;rskie, zimą narty oraz snowboard. Prywatnie jest szczęśliwym mężem i ojcem.</p>', 'Lekarz stomatolog', 'chirurgia stomatologiczna\r\nendodoncja\r\nprotetyka\r\nstomatologia estetyczna', 'igor-welyki', '54db6e696f8cf.jpg', 1, '2014-11-13 23:37:44', '2015-02-11 16:47:27', 4, '54db6e6970c39.jpg'),
(4, 'Iwona Sławińska-Niedzin', '<p>\r\n	<strong>Ukończyła wydział stomatologii Pomorskiej Akademii Medycznej w Szczecinie w 1998 roku.&nbsp;</strong>Kontynuuje tradycje rodzinne. Jest opr&oacute;cz dw&oacute;ch si&oacute;str trzecim stomatologiem w rodzinie. Od 2001r. właściciel prywatnego gabinetu, a następnie kliniki stomatologicznej w Grudziądzu. Regularnie uczestniczy w licznych kursach i sympozjach, nieustannie podnosząc swoje kwalifikacje zawodowe w dziedzinie stomatologii estetycznej, protetyki, endodoncji oraz stomatologii dziecięcej.</p>\r\n<p>\r\n	Pani Doktor traktuje pracę jak pasję i misję.<strong>&nbsp;Całą uwagę poświęca pacjentom i ich problemom, kt&oacute;re wsp&oacute;lnie rozwiązują.</strong></p>', 'Właścicielka sieci gabinetów Cosmodent\r\nLekarz stomatolog', 'stomatologia estetyczna\r\nprotetyka\r\nendodoncja\r\nstomatologia dziecięca', 'iwona-slawinska-niedzin', '546533f6be93f.jpg', 1, '2014-11-13 23:43:02', '2015-04-01 10:47:00', 1, '54db6e29af6e0.jpg'),
(5, 'Marzena Kmiecik-Szal', '<p>\r\n	<strong>Absolwentka Medycznego Studium Zawodowego w Łańcucie </strong>na kierunku higienistka stomatologiczna. Naukę ukończyła w roku 2000, <strong>z gabinetem Cosmodent związana od 2013 r. </strong>Chętnie podnosi swoje kwalifikacje z dziedziny higienizacji i profilaktyki, sw&oacute;j zaw&oacute;d wykonuje z pasją i dużym zaangażowaniem. Posiada wspaniały kontakt z pacjentami. Prywatnie mama Oli. W wolnym czasie czyta ciekawe książki, uwielbia tańczyć.</p>', 'Higienistka stomatologiczna', 'Higienizacja\r\nProfilaktyka', 'marzena-kmiecik-szal', '5465351917e6b.jpg', 1, '2014-11-13 23:47:52', '2015-02-11 17:05:58', 5, '54db6e354e962.jpg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `username_canonical` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_canonical` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `two_step_code` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `two_step_code`, `first_name`, `last_name`) VALUES
(1, 'admin', 'admin', 'test@acme.com', 'test@acme.com', 1, 'rlp1tysey1wwc0wsw4gg8k8cw0scwcs', 'e81f4a522c90d34fd1d7748144ecd915d3e32299cd72aa8662a63935f9614766eb8be6d6b6e3a37d592125ba79034118a4187f19659739ba4e151167328e10ec', '2020-02-26 10:39:31', 0, 0, NULL, '5u5ll7wikuscgckgo0wwssowg4s40400co88oskws8kkkw4gog', NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-02-11 01:02:11', '2020-02-26 10:39:31', NULL, NULL, NULL),
(3, 'iniedzin', 'iniedzin', 'rejestracja@cosmodent.com.pl', 'rejestracja@cosmodent.com.pl', 1, 'qr1l3nwlv3kogsckscwwkgckg4gks0c', '0689bf5bf4fb949c706572bd2957c812ee90cae06f0ebe8c3d30e9cc414feda88edcd6caac12b6e010a20902f78ffb49e22e30d5e9e5888f1ebee783e5b138f0', '2014-12-11 22:58:52', 0, 0, NULL, '5zyieckw7e4ogocoowg8k0sgo44s0wcssck0o80c8wwcsoc44s', NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2013-02-18 21:12:21', '2014-12-11 22:58:52', NULL, NULL, NULL),
(4, 'moderator', 'moderator', 'mail@mail.com', 'mail@mail.com', 1, 'k3u1by90hj4wwk88cw0848wog880w40', '54c62eb7058194f8ec50cf09c084cddf80ff15137b29f8e18f0fb6acdfbedafc9f3f003af978a175db32fbe5b8554b8edb4cea4fa73d9ddfc5656e4bae2753fe', '2015-10-23 11:39:10', 0, 0, NULL, 'lleoetjgjsgo4okscggc4s04wc04w8c884gkgks0sgk080kcs', NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', 0, NULL, '2015-10-21 18:44:21', '2015-10-23 11:39:11', NULL, 'M', 'M');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Galeria`
--

CREATE TABLE `Galeria` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(255) NOT NULL,
  `opis` longtext,
  `slug` varchar(255) NOT NULL,
  `kolejnosc` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `wyswietlana` tinyint(1) DEFAULT NULL,
  `zalaczalna` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Galeria`
--

INSERT INTO `Galeria` (`id`, `nazwa`, `opis`, `slug`, `kolejnosc`, `obrazek`, `wyswietlana`, `zalaczalna`, `updated_at`) VALUES
(2, 'Certyfikaty', NULL, 'certyfikaty', 0, NULL, 1, 1, '2013-02-13 15:25:01'),
(3, 'Stomatologia estetyczna', NULL, 'stomatologia-estetyczna', 0, NULL, 1, 1, '2013-02-18 21:25:56'),
(5, 'Cosmodent Łańcut - galeria zdjęć', NULL, 'cosmodent-lancut-galeria-zdjec', 0, NULL, 1, 1, '2015-01-27 18:17:53'),
(6, 'Pulpit', NULL, 'pulpit', 0, NULL, 1, 0, '2019-08-28 16:39:46'),
(7, 'Nasz zespół', NULL, 'nasz-zespol', 0, NULL, 1, 0, '2019-08-28 16:52:15');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `GaleriaZdjecie`
--

CREATE TABLE `GaleriaZdjecie` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) DEFAULT NULL,
  `podpis` varchar(1024) DEFAULT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `kolejnosc` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `GaleriaZdjecie`
--

INSERT INTO `GaleriaZdjecie` (`id`, `galeria_id`, `podpis`, `url`, `kolejnosc`, `obrazek`, `updated_at`) VALUES
(7, 2, NULL, NULL, NULL, '7-511ba54f0a860.jpg', '2013-02-13 15:42:47'),
(8, 2, NULL, NULL, NULL, '8-511ba54f0c7a3.jpg', '2013-02-13 15:42:47'),
(9, 2, NULL, NULL, NULL, '9-511ba54f0dfd0.jpg', '2013-02-13 15:42:47'),
(10, 2, NULL, NULL, NULL, '10-511ba54f0f64d.jpg', '2013-02-13 15:42:47'),
(11, 2, NULL, NULL, NULL, '11-511ba54f111aa.jpg', '2013-02-13 15:42:47'),
(12, 2, NULL, NULL, NULL, '12-511ba54f12912.jpg', '2013-02-13 15:42:47'),
(13, 2, NULL, NULL, NULL, '13-511ba54f14091.jpg', '2013-02-13 15:42:47'),
(14, 2, NULL, NULL, NULL, '14-511ba54f15be9.jpg', '2013-02-13 15:42:47'),
(15, 2, NULL, NULL, NULL, '15-511ba54f17744.jpg', '2013-02-13 15:42:47'),
(16, 2, NULL, NULL, NULL, '16-511ba54f18eac.jpg', '2013-02-13 15:42:47'),
(17, 2, NULL, NULL, NULL, '17-511ba54f1a284.jpg', '2013-02-13 15:42:47'),
(18, 2, NULL, NULL, NULL, '18-511ba54f1b9f6.jpg', '2013-02-13 15:42:47'),
(19, 2, NULL, NULL, NULL, '-511ba5db8984a.jpg', '2013-02-13 15:42:47'),
(20, 2, NULL, NULL, NULL, '-511ba5db8abc0.jpg', '2013-02-13 15:42:47'),
(21, 2, NULL, NULL, NULL, '-511ba5db8b77a.jpg', '2013-02-13 15:42:47'),
(22, 2, NULL, NULL, NULL, '-511ba5db8c330.jpg', '2013-02-13 15:42:47'),
(23, 2, NULL, NULL, NULL, '-511ba5db8ceea.jpg', '2013-02-13 15:42:47'),
(24, 2, NULL, NULL, NULL, '-511ba5db8daa2.jpg', '2013-02-13 15:42:47'),
(25, 2, NULL, NULL, NULL, '-511ba5db8e659.jpg', '2013-02-13 15:42:47'),
(26, 2, NULL, NULL, NULL, '-511ba5db8f218.jpg', '2013-02-13 15:42:47'),
(27, 2, NULL, NULL, NULL, '-511ba5db8fdc9.jpg', '2013-02-13 15:42:47'),
(28, 2, NULL, NULL, NULL, '-511ba5db90960.jpg', '2013-02-13 15:42:47'),
(29, 2, NULL, NULL, NULL, '-511ba5db9196a.jpg', '2013-02-13 15:42:47'),
(30, 2, NULL, NULL, NULL, '-511ba5db924e0.jpg', '2013-02-13 15:42:47'),
(31, 2, NULL, NULL, NULL, '-511ba66775c2f.jpg', '2013-02-13 15:42:47'),
(32, 2, NULL, NULL, NULL, '-511ba66776fb7.jpg', '2013-02-13 15:42:47'),
(33, 2, NULL, NULL, NULL, '-511ba66777b9f.jpg', '2013-02-13 15:42:47'),
(34, 2, NULL, NULL, NULL, '-511ba6677875b.jpg', '2013-02-13 15:42:47'),
(35, 2, NULL, NULL, NULL, '-511ba66779321.jpg', '2013-02-13 15:42:47'),
(36, 2, NULL, NULL, NULL, '-511ba66779eea.jpg', '2013-02-13 15:42:47'),
(37, 2, NULL, NULL, NULL, '-511ba6677aa7d.jpg', '2013-02-13 15:42:47'),
(38, 2, NULL, NULL, NULL, '-511ba6677b63a.jpg', '2013-02-13 15:42:47'),
(39, 2, NULL, NULL, NULL, '-511ba6677c201.jpg', '2013-02-13 15:42:47'),
(40, 2, NULL, NULL, NULL, '-511ba6677cda3.jpg', '2013-02-13 15:42:47'),
(41, 2, NULL, NULL, NULL, '-511ba6677d95a.jpg', '2013-02-13 15:42:47'),
(42, 2, NULL, NULL, NULL, '-511ba6677e515.jpg', '2013-02-13 15:42:47'),
(43, 2, NULL, NULL, NULL, '-511ba6677f0c9.jpg', '2013-02-13 15:42:47'),
(44, 3, NULL, NULL, NULL, '-51228e1dd64ea.jpg', '2013-02-18 21:25:01'),
(45, 3, NULL, NULL, NULL, '-51228e1de206f.jpg', '2013-02-18 21:25:01'),
(46, 3, NULL, NULL, NULL, '-51228e1de3025.jpg', '2013-02-18 21:25:01'),
(47, 3, NULL, NULL, NULL, '-51228e1de378f.jpg', '2013-02-18 21:25:01'),
(67, 5, NULL, NULL, 0, '-54c7c87a062eb.jpg', '2015-01-27 18:19:51'),
(68, 5, NULL, NULL, 2, '-54c7c87a07ba3.jpg', '2015-01-27 18:19:51'),
(69, 5, NULL, NULL, 1, '-54c7c87a07f93.jpg', '2015-01-27 18:19:51'),
(70, 5, NULL, NULL, 3, '-54c7c87a08389.jpg', '2015-01-27 18:19:51'),
(71, 5, NULL, NULL, 6, '-54c7c87a0874b.jpg', '2015-01-27 18:19:51'),
(72, 5, NULL, NULL, 7, '-54c7c87a08ac3.jpg', '2015-01-27 18:19:51'),
(73, 5, NULL, NULL, 8, '-54c7c87a08e37.jpg', '2015-01-27 18:19:52'),
(74, 5, NULL, NULL, 9, '-54c7c87a09253.jpg', '2015-01-27 18:19:52'),
(75, 5, NULL, NULL, 10, '-54c7c87a0966c.jpg', '2015-01-27 18:19:52'),
(76, 5, NULL, NULL, 5, '-54c7c87a09a73.jpg', '2015-01-27 18:19:52'),
(77, 5, NULL, NULL, 4, '-54c7c87a09dda.jpg', '2015-01-27 18:19:52'),
(78, 6, 'Stomatologia', NULL, 0, '78-5d66960f59f15.png', '2019-08-28 16:56:15'),
(79, 6, 'Medycyna estetyczna', NULL, 1, '79-5d6696438b811.png', '2019-08-28 16:57:07'),
(80, 7, 'our_team', NULL, 0, '80-5d6696681c252.png', '2019-08-28 16:57:44');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `anchor` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `attributes` varchar(255) DEFAULT NULL,
  `route` varchar(1024) DEFAULT NULL,
  `routeParameters` varchar(1024) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `parentItem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `anchor`, `title`, `type`, `location`, `sort_order`, `url`, `attributes`, `route`, `routeParameters`, `lft`, `rgt`, `depth`, `parentItem_id`) VALUES
(1, 'Kompetencje', 'Kompetencje', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kompetencje\"}', 4, 4, 1, NULL),
(2, 'Usługi', 'Usługi', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"uslugi\"}', 6, 6, 1, NULL),
(3, 'Aktualności', 'Aktualności', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_aktualnosci_lista', '', 8, 8, 1, NULL),
(4, 'Kontakt', 'Kontakt', 'route', 'menu_dolne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kontakt\"}', 10, 10, 1, NULL),
(5, 'Kompetencje', 'Kompetencje', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kompetencje\"}', 38, 38, 1, NULL),
(6, 'Stomatologia', 'Stomatologia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"stomatologia\"}', 2, 2, 1, NULL),
(7, 'Wybielanie zębów', 'Wybielanie zębów', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"wybielanie-zebow\"}', 7, 7, 2, 6),
(8, 'Kontakt', 'Kontakt', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kontakt\"}', 44, 44, 1, NULL),
(12, 'Stomatologia dziecięca', 'Stomatologia dziecięca', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"stomatologia-dziecieca\"}', 3, 3, 2, 6),
(13, 'Stomatologia estetyczna', 'Stomatologia estetyczna', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"stomatologia-estetyczna\"}', 5, 5, 2, 6),
(14, 'Periodontologia', 'Periodontologia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"periodontologia\"}', 15, 15, 2, 6),
(15, 'Implantologia', 'Implantologia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"implantologia\"}', 9, 9, 2, 6),
(16, 'Leczenie kanałowe', 'Leczenie kanałowe', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"leczenie-kanalowe\"}', 11, 11, 2, 6),
(17, 'Protetyka', 'Protetyka', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"protetyka\"}', 13, 13, 2, 6),
(18, 'Ozdoby na zęby', 'Ozdoby na zęby', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"ozdoby-na-zeby\"}', 17, 17, 2, 6),
(19, 'Profilaktyka', 'Profilaktyka', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"profilaktyka\"}', 21, 21, 2, 6),
(20, 'Laseroterapia', 'Laseroterapia', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"laseroterapia\"}', 23, 23, 2, 6),
(22, 'Galeria', 'Galeria', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"gabinet\"}', 42, 42, 1, NULL),
(23, 'Ortodoncja', 'Ortodoncja', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"ortodoncja\"}', 19, 19, 2, 6),
(24, 'Nasz zespół', 'Nasz zespół', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_employee_index', '', 40, 40, 1, NULL),
(25, 'Medycyna estetyczna', 'Medycyna estetyczna', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"medycyna-estetyczna\"}', 26, 26, 1, NULL),
(26, 'Peeling chemiczny', 'Peeling chemiczny', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"peeling-chemiczny\"}', 27, 27, 2, 25),
(27, 'Mezoterapia igłowa', 'Mezoterapia igłowa', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"mezoterapia-iglowa\"}', 29, 29, 2, 25),
(28, 'Kwas haliuronowy', 'Kwas haliuronowy', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"kwas-haliuronowy\"}', 31, 31, 2, 25),
(29, 'Botoks', 'Botoks', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"botoks\"}', 33, 33, 2, 25),
(30, 'Osocze bogatopłytkowe', 'Osocze bogatopłytkowe', 'route', 'menu_gorne', NULL, NULL, NULL, 'lm_cms_strony_show', '{\"slug\":\"osocze-bogatoplytkowe\"}', 35, 35, 2, 25);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext,
  `automaticSeo` tinyint(1) NOT NULL,
  `topImage` varchar(255) DEFAULT NULL,
  `onMainPage` tinyint(1) DEFAULT NULL,
  `onMainPageOrder` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `title`, `published`, `publish_date`, `keywords`, `description`, `price`, `slug`, `content`, `automaticSeo`, `topImage`, `onMainPage`, `onMainPageOrder`, `type`) VALUES
(1, 'First simple page', 1, '2013-02-11 01:02:11', 'First, simple, page, This, very, first, containing, HTML, Second, paragraph', '\n This is very first page containing raw HTML \n\n Second paragraph \n\n\n    List first element\n    List second element\n    List third element\n\n\n A link to very nice web page: google\n\n\n\n', '0.00', 'first-simple-page', '\n<p> This is very first page containing raw HTML </p>\n\n<p> Second paragraph </p>\n\n<ul>\n    <li>List first element</li>\n    <li>List second element</li>\n    <li>List third element</li>\n</ul>\n\n<p> A link to very nice web page: <a href=\"http://www.google.com\">google.com</a> (search engine) :D </p>\n\n\n\n', 0, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page_uploadedfile`
--

CREATE TABLE `page_uploadedfile` (
  `page_id` int(11) NOT NULL,
  `uploadedfile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Strona`
--

CREATE TABLE `Strona` (
  `id` int(11) NOT NULL,
  `galeria_id` int(11) DEFAULT NULL,
  `strona_nadrzedna_id` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title2` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publishDate` datetime DEFAULT NULL,
  `keywords` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` longtext,
  `automaticSeo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Strona`
--

INSERT INTO `Strona` (`id`, `galeria_id`, `strona_nadrzedna_id`, `obrazek`, `updated_at`, `title2`, `title`, `published`, `publishDate`, `keywords`, `description`, `slug`, `content`, `automaticSeo`) VALUES
(1, 2, NULL, '52c00dc548d59.jpg', NULL, NULL, 'Kompetencje', 1, '2014-02-11 21:20:00', 'Kompetencje, Iwona, Sławińska', 'Pani Doktor traktuje pracę jak pasję i misję. Całą uwagę poświęca pacjentom i ich problemom, które wspólnie rozwiązują.', 'kompetencje', '<p>\r\n	<span style=\"display: none;\">&nbsp;</span></p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<span id=\"cke_bm_119S\" style=\"display: none;\">&nbsp;</span>Lek. stom. Iwona Sławińska-Niedzin</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<div style=\"text-align: justify;\">\r\n	Ukończyła wydział stomatologii <strong>Pomorskiej Akademii Medycznej</strong> w Szczecinie w 1998 roku. Kontynuuje tradycje rodzinne. Jest opr&oacute;cz dw&oacute;ch si&oacute;str trzecim stomatologiem w rodzinie. <strong>Od 2001r. właściciel prywatnego gabinetu</strong>, a następnie kliniki stomatologicznej w Grudziądzu. Regularnie uczestniczy w licznych kursach i sympozjach, nieustannie <strong>podnosząc swoje kwalifikacje zawodowe</strong> w dziedzinie stomatologii estetycznej, protetyki, endodoncji oraz stomatologii dziecięcej.</div>\r\n<div style=\"text-align: justify;\">\r\n	<br />\r\n	&nbsp;</div>\r\n<div style=\"text-align: justify;\">\r\n	<span class=\"pink\">Pani Doktor traktuje pracę jak pasję i misję.</span> Całą uwagę poświęca pacjentom i ich problemom, kt&oacute;re wsp&oacute;lnie rozwiązują.<br />\r\n	<br />\r\n	Prywatnie mama dw&oacute;jki dzieci Klaudyny i Filipa. W wolnych chwilach podr&oacute;żuje, uwielbia taniec, jazdę na nartach oraz rolkach.</div>\r\n<div style=\"text-align: justify;\">\r\n	&nbsp;</div>\r\n<div style=\"text-align: justify;\">\r\n	<span class=\"link\">Poniżej znajdue się galeria dyplom&oacute;w oraz certyfikat&oacute;w.</span></div>', 0),
(2, NULL, NULL, NULL, NULL, NULL, 'Stomatologia', 1, '2014-02-11 21:22:00', 'Stomatologia,  	Tupacsum, Ipsum, until, that, Thug, Life, tatted, chest., seems', 'Cosmodent - praktyka stomatologiczna\r\n\r\nLista usług', 'stomatologia', '<p style=\"text-align: center;\">\r\n	<span style=\"font-size:16px;\"><strong>Kliknij na wybraną usługę</strong>, aby poznać szczeg&oacute;ły:</span></p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 500px;\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/stomatologia-dziecieca.html\"><img alt=\"Stomatologia dziecięca\" src=\"/uploads/pliki/Uslugi/1.jpg\" style=\"width: 387px; height: 65px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/stomatologia-estetyczna.html\"><img alt=\"Stomatologia estetyczna\" src=\"/uploads/pliki/Uslugi/2.jpg\" style=\"width: 391px; height: 65px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/wybielanie-zebow.html\"><img alt=\"Wybielanie zębów\" src=\"/uploads/pliki/Uslugi/3.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/implantologia.html\"><img alt=\"Implantologia\" src=\"/uploads/pliki/Uslugi/4.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/leczenie-kanalowe.html\"><img alt=\"Leczenie kanałowe\" src=\"/uploads/pliki/Uslugi/5.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/protetyka.html\"><img alt=\"Protetyka\" src=\"/uploads/pliki/Uslugi/6.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/periodontologia.html\"><img alt=\"Periodontologia\" src=\"/uploads/pliki/Uslugi/7.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/ozdoby-na-zeby.html\"><img alt=\"Ozdoby na zęby\" src=\"/uploads/pliki/Uslugi/8.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/profilaktyka.html\"><img alt=\"Profilaktyka\" src=\"/uploads/pliki/Uslugi/9.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/laseroterapia.html\"><img alt=\"Laseroterapia\" src=\"/uploads/pliki/Uslugi/laseroterapia.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/ortodoncja.html\"><img alt=\"\" src=\"/uploads/pliki/Uslugi/ortodoncja.jpg\" style=\"width: 387px; height: 69px;\" /></a></td>\r\n			<td>\r\n				&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 0),
(4, NULL, NULL, NULL, NULL, NULL, 'blok-strona-glowna', 0, '2014-02-11 21:41:00', NULL, NULL, 'blok-strona-glowna', '<ul>\r\n	<li>\r\n		<b>Stomatologia zachowawcza&nbsp;</b> - leczenie pr&oacute;chnicy</li>\r\n	<li>\r\n		<b>Odbudowa&nbsp;</b>zniszczonych i złamanych koron zęb&oacute;w</li>\r\n	<li>\r\n		<b>Stomatologia dziecięca</b></li>\r\n	<li>\r\n		<b>Endodoncja</b> - nowoczesne metody opracowywania i wypełniania kanał&oacute;w korzeniowych, leczenie zmian okołowierzchołkowych</li>\r\n	<li>\r\n		<b>Protetyka</b> - wszystkie rodzaje uzupełnień</li>\r\n	<li>\r\n		<b>Periodontologia&nbsp;</b> - leczenie chor&oacute;b przyzębia, zęby rozchwiane, zanik dziąseł, przykry zapach z ust, usuwanie kamienia</li>\r\n	<li>\r\n		<b>Chirurgia stomatologiczna</b> - usuwanie zęb&oacute;w mlecznych i stałych</li>\r\n	<li>\r\n		<b>Leczenie</b> zaburzeń stan&oacute;w s-ż (b&oacute;le ucha i głowy)</li>\r\n	<li>\r\n		<strong>Ortodoncja&nbsp;</strong>- aparaty stałe i ruchome</li>\r\n	<li>\r\n		<b>Profilaktyka </b>- u dzieci i dorosłych</li>\r\n	<li>\r\n		<b>Wybielanie i piaskowanie zęb&oacute;w</b></li>\r\n	<li>\r\n		<b>Szlachetne&nbsp;</b>ozdoby na zęby</li>\r\n</ul>', 0),
(6, NULL, NULL, NULL, NULL, NULL, 'Kontakt Łańcut', 1, '2013-02-11 23:53:00', 'Kontakt, COSMODENT, Nowoczesna, Stomatologia, Łańcut,, ul	tel:, 666	e-mail:, rejestracja@cosmodent.com	Czynne, codziennie', 'COSMODENT - Nowoczesna Stomatologia Łańcut, ul\r\n	tel: 730 965 666\r\n	e-mail: rejestracja@cosmodent.com\r\n	Czynne codziennie', 'kontakt', '<h2 class=\"title\" style=\"text-align: center;\">\r\n	COSMODENT - Nowoczesna Stomatologia Łańcut</h2>\r\n<p style=\"text-align: center;\">\r\n	<span style=\"font-size:14px;\">Adres:</span></p>\r\n<p style=\"text-align: center;\">\r\n	<strong><span style=\"font-size: 14px;\">Łańcut, ul. Piłsudskiego 52</span></strong></p>\r\n<p style=\"text-align: center;\">\r\n	<br />\r\n	<span style=\"font-size:14px;\">tel: <strong>730 965 666</strong><br />\r\n	e-mail: <span class=\"pink\"><a href=\"mailto:lancut@cosmodent.com.pl\">lancut@cosmodent.com.pl</a></span></span><br />\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: center;\">\r\n	<span class=\"pink\">Czynne codziennie</span></h2>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	<iframe frameborder=\"0\" height=\"233\" marginheight=\"0\" marginwidth=\"0\" scrolling=\"no\" src=\"https://mapy.google.pl/maps?f=q&amp;source=s_q&amp;hl=pl&amp;geocode=&amp;q=Pi%C5%82sudskiego+52,+%C5%81a%C5%84cut&amp;aq=0&amp;oq=%C5%81a%C5%84cut,+Pi%C5%82sudskiego+52&amp;sll=51.953751,19.134379&amp;sspn=11.913584,33.815918&amp;ie=UTF8&amp;hq=&amp;hnear=Pi%C5%82sudskiego+52,+37-100+%C5%81a%C5%84cut,+%C5%82a%C5%84cucki,+podkarpackie&amp;t=m&amp;ll=50.068269,22.223883&amp;spn=0.012837,0.066433&amp;z=14&amp;iwloc=near&amp;output=embed\" style=\"width: 773px; border: 1px solid #cecece; border-radius: 10px;\" width=\"775\"></iframe><br />\r\n	<small><a class=\"pink\" href=\"https://mapy.google.pl/maps?f=q&amp;source=embed&amp;hl=pl&amp;geocode=&amp;q=Pi%C5%82sudskiego+52,+%C5%81a%C5%84cut&amp;aq=0&amp;oq=%C5%81a%C5%84cut,+Pi%C5%82sudskiego+52&amp;sll=51.953751,19.134379&amp;sspn=11.913584,33.815918&amp;ie=UTF8&amp;hq=&amp;hnear=Pi%C5%82sudskiego+52,+37-100+%C5%81a%C5%84cut,+%C5%82a%C5%84cucki,+podkarpackie&amp;t=m&amp;ll=50.068269,22.223883&amp;spn=0.012837,0.066433&amp;z=14&amp;iwloc=near\" style=\"text-align:left\">Wyświetl większą mapę</a></small></p>', 0),
(7, NULL, 2, NULL, NULL, NULL, 'Stomatologia dziecięca', 1, '2013-02-12 18:50:00', 'Stomatologia, dziecięca, Dziecko, należy, wrażliwych, pacjent', 'Dziecko należy do wrażliwych pacjent', 'stomatologia-dziecieca', '<p style=\"text-align: justify;\">\r\n	<strong>Dziecko</strong> należy do wrażliwych pacjent&oacute;w, żywo reagujących na wszelkiego rodzaju bodźce -<span class=\"pink\"> tak psychiczne jak i fizyczne</span>. Wizyta u stomatologa wiąże się zawsze z uczuciem lęku, a przykre przeżycia z takich wizyt dziecko pamięta długo. Wynikiem tego może być lęk przed zabiegiem stomatologicznym w wieku dojrzałym.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Umiejętne postępowanie lekarza wobe dziecka jest bardzo istotne.</h2>\r\n<p style=\"text-align: justify;\">\r\n	Lekarz powinien umieć zmniejszyć lęk u małego pacjenta. Maluch na pewno zainteresuje się wyglądem własnych ząbk&oacute;w. Może je zobaczyć w powiększeniu na ekranie monitora, dzięki kamerze wewnątrzustnej. Zadaniem lekarza jest wytworzenie dobrych, pozytywnych skojarzeń, rozładowanie napięcia emocjonalnego, doprowadzenie do relaksacji. Ważny jest r&oacute;wnież życzliwy stosunek lekarza do dziecka i informowanie go o przebiegu leczenia. Umiejętne podejście w pierwszych wizytach owocuje zdobyciem zaufania, co w konsekwencji doprowadzi do <strong>chęci systematycznego leczenia.</strong></p>', 0),
(8, NULL, 2, NULL, NULL, NULL, 'Stomatologia estetyczna', 1, '2013-02-12 18:52:00', 'Stomatologia, estetyczna, Definiując, pojęcie, stomatologii, estetycznej, należy, stwierdzić,, gł', 'Definiując pojęcie stomatologii estetycznej należy stwierdzić, że gł', 'stomatologia-estetyczna', '<p style=\"text-align: justify;\">\r\n	Definiując pojęcie <strong>stomatologii estetycznej</strong> należy stwierdzić, że gł&oacute;wnym celem badań i terapii tej stosunkowo nowej dziedziny stomatologii jest <strong>poprawa wyglądu estetycznego uzębienia i twarzy.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Uśmiech</strong> jest integralną częścią twarzy i osobowości człowieka. Trzeba przecież przyznać, że najpiękniejsze nawet zęby nie będą widoczne bez uśmiechu.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Widoczność zęb&oacute;w g&oacute;rnych zmniejsza się wraz z wiekiem np.: w wieku 60 lat mogą one już nie wystawać spod g&oacute;rnej wargi. Wraz z wiekiem zwiększa się natomiast widoczność dolnych zęb&oacute;w i w wieku 60 lat uwidaczniają się one na ponad 2 mm. W stomatologii estetycznej niezwykle ważna jest dokumentacja wizualna przed i po leczeniu, na kt&oacute;rej można przeanalizować dokonane zmiany.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	W trakcie całego leczenia obowiązuje zasada minimum ingerencji maksimum efektu. Zgodnie z nią w przypadku przebarwień zęb&oacute;w powinniśmy rozważyć kolejno: wybielanie zęb&oacute;w, pokrycie kompozytem, lic&oacute;wki, i dopiero na końcu korony. Przed szlifowaniem zęb&oacute;w obr&oacute;conych należy rozważyć ich ortodontyczne leczenie.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	W stomatologii estetycznej konieczne jest wsp&oacute;łdziałanie poszczeg&oacute;lnych specjalności stomatologii a co najważniejsze wsp&oacute;łpraca pacjenta, kt&oacute;ry musi dokładnie określić swoje oczekiwania.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Lic&oacute;wki kosmetyczne</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Obecnie coraz większe zainteresowanie budzi zastosowanie lic&oacute;wek. Są to stałe uzupełnienia protetyczne stosowane w stomatologii estetycznej do pokrycia nieestetycznych powierzchni wargowych i policzkowych zęb&oacute;w g&oacute;rnych i dolnych.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Częściej stosowane są w odcinku przednim zęb&oacute;w szczęki. Są korzystniejszą alternatywą niż wykonanie koron protetycznych, ze względu na niewielki stopień utraty tkanek twardych zęba w wyniku szlifowania.</p>\r\n<p style=\"text-align: justify;\">\r\n	Korona protetyczna wymaga preparacji całego zęba dookoła, natomiast w przypadku cieniutkiej lic&oacute;wki lekarz delikatnie usuwa niewielką warstwę szkliwa z powierzchni wargowej na grubość 0,3-0,7 mm. Technikę laminatową zalicza się do uzupełnień protetycznych, kt&oacute;re oszczędzają tkankę twardą zęba. Materiałem do wykonania lic&oacute;wek jest kompozyt lub porcelana.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Wskazania do zastosowania lic&oacute;wek:</strong></span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		przebarwienia zęb&oacute;w trudne do wybielenia np.: tetracyklinowe, po leczeniu kanałowym,</li>\r\n	<li style=\"text-align: justify;\">\r\n		niedorozw&oacute;j szkliwa i zębiny (amalogenesis imperfecta)</li>\r\n	<li style=\"text-align: justify;\">\r\n		niekosmetyczne wypełnienia zęb&oacute;w przednich,</li>\r\n	<li style=\"text-align: justify;\">\r\n		ubytki brzeg&oacute;w zęb&oacute;w, ukruszenia kąt&oacute;w brzeg&oacute;w siecznych</li>\r\n	<li style=\"text-align: justify;\">\r\n		diastemy między zębami,</li>\r\n	<li style=\"text-align: justify;\">\r\n		wydłużenie startych patologicznie zęb&oacute;w,</li>\r\n	<li style=\"text-align: justify;\">\r\n		zdecydowana zmiana koloru zęb&oacute;w</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Przeciwwskazania:</strong></span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		zła higiena jamy ustnej,</li>\r\n	<li style=\"text-align: justify;\">\r\n		parafunkcje zwarciowe np.: bruksizm.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"link\">W przypadku lic&oacute;wek, jak i innych uzupełnień protetycznych, ważna jest odpowiednia higiena jamy ustnej. Prawidłowe szczotkowanie, nitkowanie i korzystanie z dodatkowych zabieg&oacute;w higienicznych sprawi, że wykonane uzupełnienie będzie nam służyło przez wiele lat, a my odzyskamy długo skrywany szeroki uśmiech.</span></p>', 0),
(9, NULL, NULL, NULL, NULL, NULL, 'Wybielanie zębów', 1, '2013-02-12 19:13:00', 'Wybielanie, zębów, Dlaczego, uśmiech, jest, ważny?', 'Dlaczego uśmiech jest tak ważny?', 'wybielanie-zebow', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Dlaczego uśmiech jest tak ważny?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Wygląd zewnętrzny odgrywa niebagatelną rolę w naszym życiu. </strong>Z podziwem, a czasem nawet z zazdrością, spoglądamy na zadbane osoby o promiennym, białym uśmiechu. Czasami nawet nie zdajemy sobie sprawy jak ważną rolę odgrywa uśmiech w naszym życiu.&nbsp;Uśmiechając się wysyłamy sygnały innym, że cieszymy się każdą chwilą, jesteśmy zadowoleni, szczęśliwi, gotowi podjąć nowe wyzwania, otwarci na nowe kontakty. Białym uśmiechem przełamujemy bariery. Otoczenie odbiera nas jako osoby o pogodnym usposobieniu, a przez to wzbudzamy zaufanie.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Spośr&oacute;d czynnik&oacute;w stanowiących o atrakcyjności uśmiechu biały kolor zęb&oacute;w odgrywa najważniejszą rolę poprawiając samopoczucie, stosunki międzyludzkie, a nawet ułatwiając zdobycie pracy.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Przebarwione zęby powodują, że uśmiechamy się rzadziej, zasłaniamy usta ręką, wstydzimy się, zamykamy w sobie.<br />\r\n	Na szczęście istnieją profesjonalne metody, kt&oacute;re pomogą nam przywr&oacute;cić śnieżnobiały uśmiech, a przez to noszą pozytywną samoocenę oraz dobre samopoczucie. Zabieg wybielania zęb&oacute;w przeprowadzony przez lekarza stomatologa to najtańsza &quot;operacja plastyczna&quot;, a co najważniejsze dostępna dla wszystkich.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Opalescence dostępny jest wyłącznie u lekarzy stomatolog&oacute;w, co gwarantuje bezpieczeństwo całego procesu wybielania zęb&oacute;w.</span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Dlaczego Opalescence?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Preparaty wybielające Opalescence </strong>są wytwarzane przez amerykańską firmę Ultradent, producenta wielu nowoczesnych materiał&oacute;w stomatologicznych. Firma Ultradent jest pionierem i liderem wśr&oacute;d producent&oacute;w preparat&oacute;w do wybielania zęb&oacute;w. Już w 1990 r. Wyprodukowała pierwszy skuteczny i bezpieczny żel wybielający do stosowania w wygodnych indywidualnych nakładkach. W 1996 r. Jakość i bezpieczeństwo Opalescence zostały potwierdzone nadaniem prestiżowego certyfikatu ADA - Amerykańskiego Towarzystwa Stomatologicznego. Opalescence 15%PF jest też pierwszym preparatem do wybielania zęb&oacute;w, zawierającym odpowiednie dodatki przeciwko nadwrażliwości, zwiększające komfort kuracji. Opalescence jest dostępny w r&oacute;żnych smakach m.in. Miętowym i arbuzowym.</p>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	W Polsce preparaty Opalescence są znane i stosowane z powodzeniem od 1990 roku. Wybielanie zęb&oacute;w metodą Opalescence zostało dokładnie przebadane na akademiach medycznych i okazało się skuteczne, bezpieczne i dające trwały efekt. Preparaty do wybielania zęb&oacute;w Opalescence posiadają odpowiednie atesty i certyfikaty zar&oacute;wno w polsce, jak i międzynarodowe, także te obowiązujące w Unii Europejskiej.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Dlaczego występują przebarwienia na zębach?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Powod&oacute;w może być kilka. Przebarwienia powstają między innymi w trakcie nieprawidłowego rozwoju zęb&oacute;w. Wywołuje je r&oacute;wnież długotrwałe leczenie antybiotykami z grupy tetracyklin prowadzone w dzieciństwie. Niekt&oacute;re osoby mają zęby o naturalnie ciemnym zabarwieniu. Z wiekiem zaś zęby ciemnieją z powodu picia kawy, herbaty, sok&oacute;w czy palenia papieros&oacute;w. Także stare, często złe wypełnienia lub uszkodzenia miazgi zęba mogą mieć wpływ na kolor zęb&oacute;w.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy wybielanie zęb&oacute;w przez stomatologa jest bezpieczne?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Tak! </strong>Opatentowany przez firmę Ultradent preparat Opalescence przy właściwym stosowaniu usuwa większość przebarwień w bezpieczny i skuteczny spos&oacute;b. Przebarwienia zostają wybielone przez ich utlenienie. Atomy tlenu wnikają w głąb szkliwa i zębiny wybielając je, jednak właściwa struktura zęba pozostaje nienaruszona, a jedynie ulega ona rozjaśnieniu.</p>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	Bezpieczeństwo wybielania zęb&oacute;w systemem Opalescence zostało potwierdzone przez wieloletnie badania kliniczne prowadzone na akademiach medycznych i uniwersytetach w Polsce i na całym świecie.<br />\r\n	Gdy stosuje się preparat w spos&oacute;b zalecony przez stomatologa jest on absolutnie bezpieczny.</p>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	Opalescence do wybielania nakładkowego jest żelem, kt&oacute;rego czynnikiem aktywnym jest nadtlenek karbamidu. Jest to bezpieczny związek chemiczny, kt&oacute;ry przez dziesięciolecia był używany jako środek do dezynfekcji jamy ustnej, a obecnie umożliwia wybielanie zęb&oacute;w milionom ludzi na całym świecie.</p>', 0),
(10, NULL, 2, NULL, NULL, NULL, 'Implantologia', 1, '2013-02-12 19:30:00', 'Implantologia, Implanty, twoje, \"NOWE, ZĘBY\"., Dzięki, można, wielu, przypadkach, odbudować', 'Implanty to twoje \"NOWE ZĘBY\". Dzięki nim można w wielu przypadkach odbudować ząb w miejsce wcześniej usuniętego. To tak jakby wyr', 'implantologia', '<p style=\"text-align: justify;\">\r\n	<strong>Implanty to twoje &quot;NOWE ZĘBY&quot;</strong>. Dzięki nim można w wielu przypadkach odbudować ząb w miejsce wcześniej usuniętego. To tak jakby wyr&oacute;sł nowy ząb.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">W przypadku braku dobrych warunk&oacute;w kostnych, można odbudować kość i stworzyć miejsce dla implantu zębowego.</span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	W naszym gabinecie oferujemy&nbsp; implanty firmy &quot;<strong>Nobel Biocare</strong>&quot;, &quot;<strong>Straumann</strong>&quot;, &quot;<strong>AlfaBio</strong>&quot;, &quot;<strong>Osteoplant</strong>&quot;, &quot;<strong>ImTec</strong>&quot;. Dzięki szerokiemu asortymentowi możemy dobrać&nbsp;implant odpowiedni do Państwa potrzeb.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	<strong>Zapraszamy Państwa na konsultację -&nbsp;730 965 666.</strong></h2>', 0),
(11, NULL, 2, NULL, NULL, NULL, 'Leczenie kanałowe', 1, '2013-02-12 19:31:00', 'Leczenie, kanałowe, Obecnie, wiele, zęb', 'Obecnie wiele zęb', 'leczenie-kanalowe', '<p style=\"text-align: justify;\">\r\n	<strong>Obecnie wiele zęb&oacute;w</strong>, kt&oacute;re jeszcze nie tak dawno były skazane na usunięcie możemy <strong>LECZYĆ i URATOWAĆ</strong>. Jest to możliwe dzięki wąskiej dziedzinie stomatologii zwanej endodoncją. Leczenie endodontyczne jest powszechnie zwane leczeniem kanałowym. Leczenie kanałowe w dużym uproszczeniu polega na usunięciu zainfekowanej lub martwej miazgi z komory i systemu kanałowego korzeni zęb&oacute;w, mechanicznym i chemicznym opracowaniu tego systemu, a następnie szczelnym wypełnieniu materiałami specjalnie do tego celu stworzonymi.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Kluczem do sukcesu</strong> leczenia kanałowego jest <span class=\"pink\">mechaniczne i chemiczne oczyszczenie i poszerzenie kanał&oacute;w korzeniowych na całej ich długości.</span> Jest to możliwe dzięki specjalnie do tego celu stworzonym i ciągle udoskonalanym narzędziom kanałowym. Mechaniczne opracowanie systemu kanał&oacute;w korzeniowych jest wspomagane obfitym płukaniem kanał&oacute;w takimi środkami chemicznymi jak: podchloryn sodu, kwas cytrynowy i alkohol izopropylowy. Środki te mają za zadanie zdezynfekować i pom&oacute;c oczyścić cały system kanałowy. Bardzo ważne jest to, aby system kanał&oacute;w był opracowany na całej jego długości. Dzięki urządzeniu zwanemu endometrem możemy zmierzyć długość kanału korzeniowego z dużą dokładnością. Jest tu wykorzystywane zjawisko r&oacute;żnej przewodności prądu elektrycznego przez tkanki zębowe i znajdujące się poza zębem. Liczne badania udowodniły, że powszechnie stosowana metoda pomiaru długości kanału korzeniowego oparta tylko na zdjęciu Rentgenowskim jest bardzo często zawodna.</p>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	Nieocenione r&oacute;wnież w leczeniu endodontycznym są <strong>lupy o dużym powiększeniu</strong> z &quot;zimnym&quot; światłem lub <strong>mikroskop zabiegowy</strong>. Dzięki tym urządzeniom możemy zlokalizować liczne ujścia kanałowe, kt&oacute;re bez niego są niewidoczne, a następnie dokładnie opracować cały system kanałowy . Stosując mikroskop możemy wręcz zajrzeć do wnętrza kanału i pod kontrolą wzroku przeprowadzić prawidłowe leczenie. Po opracowaniu systemu kanałowego, oczyszczeniu a następnie osuszeniu go przystępujemy do wypełniania go. Najlepszym materiałem stosowanym do wypełniania kanał&oacute;w jest gutaperka lub resilon. Wprowadza się je do kanału z niewielką ilością uszczelniacza na wcześniej określoną głębokość, a następnie upycha się go po wcześniejszym podgrzaniu i uplastycznieniu, tak by materiał wypełnił szczelnie opracowany kanał nie pozostawiając pęcherzyk&oacute;w powietrza.</p>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	<span class=\"link\">Tak przeleczony ząb na szansę służyć jeszcze wiele, wiele lat. Alternatywne leczenie polegające na usunięciu zęba dopiero rozpoczyna długą listę problem&oacute;w, a co za tym idzie koszt&oacute;w dalszego leczenia.</span></p>', 0),
(12, NULL, 2, NULL, NULL, NULL, 'Protetyka', 1, '2013-02-12 19:33:00', 'Protetyka, Oferujemy, następujące, typy, protez:			akrylowe			szkieletowe			mosty			korony, (pełnoceramiczne,, złocie)			lic', 'Oferujemy następujące typy protez:\r\n\r\n	\r\n		akrylowe\r\n	\r\n		szkieletowe\r\n	\r\n		mosty\r\n	\r\n		korony (pełnoceramiczne, na złocie)\r\n	\r\n		lic', 'protetyka', '<p>\r\n	<strong><span style=\"font-size:14px;\">Oferujemy następujące typy protez:</span></strong></p>\r\n<ul>\r\n	<li>\r\n		akrylowe</li>\r\n	<li>\r\n		szkieletowe</li>\r\n	<li>\r\n		mosty</li>\r\n	<li>\r\n		korony (pełnoceramiczne, na złocie)</li>\r\n	<li>\r\n		lic&oacute;wki</li>\r\n</ul>', 0),
(13, NULL, 2, NULL, NULL, NULL, 'Periodontologia', 1, '2013-02-12 19:37:00', 'Periodontologia, musisz, wiedzieć, chorobach, przyzębia?	Wielu, pacjent', 'Co musisz wiedzieć o chorobach przyzębia?\r\n\r\n	Wielu pacjent', 'periodontologia', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Co musisz wiedzieć o chorobach przyzębia?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Wielu pacjent&oacute;w cierpi z powodu krwawienia dziąseł, odsłonięcia korzeni, rozchwiania czy nawet utraty zęb&oacute;w. Przez długi czas przyczyna tych dolegliwości nie była znana, dziś jednak wiemy, iż gł&oacute;wnym winowajcą jest <strong>paradontoza!</strong></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Kiedyś uważana była za nieuleczalną. Rozw&oacute;j medycyny i technik badawczych pozwolił na dokładne zbadanie tego schorzenia i bardziej optymistyczną diagnozę. Choroby przyzębia, obok pr&oacute;chnicy, są najczęstszą przyczyną przedwczesnej utraty uzębienia, dlatego zaliczane są do chor&oacute;b społecznych.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Przyczyny chor&oacute;b przyzębia:</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\" style=\"width: 100%;\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<span style=\"font-size:14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; MIEJSCOWE</span></td>\r\n			<td>\r\n				<span style=\"font-size:14px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; OG&Oacute;LNE</span></td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width: 50%;\">\r\n				<ul>\r\n					<li style=\"text-align: justify;\">\r\n						bakteryjna płytka nazębna (zła higiena jamy ustnej),</li>\r\n					<li style=\"text-align: justify;\">\r\n						wadliwe wypełnienia (nawisy),</li>\r\n					<li style=\"text-align: justify;\">\r\n						stare, uciskające protezy,</li>\r\n					<li style=\"text-align: justify;\">\r\n						nieszczelne korony i mosty,</li>\r\n					<li style=\"text-align: justify;\">\r\n						zaciskanie zęb&oacute;w czy zgrzytanie zębami</li>\r\n				</ul>\r\n			</td>\r\n			<td>\r\n				<ul>\r\n					<li style=\"text-align: justify;\">\r\n						palenie tytoniu,</li>\r\n					<li style=\"text-align: justify;\">\r\n						stres,</li>\r\n					<li style=\"text-align: justify;\">\r\n						cukrzyca,</li>\r\n					<li style=\"text-align: justify;\">\r\n						niedobory witamin,</li>\r\n					<li style=\"text-align: justify;\">\r\n						zaburzenia hormonalne,</li>\r\n					<li style=\"text-align: justify;\">\r\n						nieprawidłowe odżywianie,</li>\r\n					<li style=\"text-align: justify;\">\r\n						inne choroby og&oacute;lnoustrojowe</li>\r\n				</ul>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	Bakteryjną <strong>płytkę nazębną</strong> uważa się za najważniejszy, <strong>miejscowy czynnik</strong> zapaleniotw&oacute;rczy. Najczęściej powoduje ona zapalenie dziąseł oraz zaostrzenie istniejących, przewlekłych stan&oacute;w zapalnych w tkankach przyzębia. Płytka nazębna tworzy się na wszystkich powierzchniach zęb&oacute;w, błonie śluzowej dziąsła oraz uzupełnieniach protetycznych znajdujących się w jamie ustnej. Dojrzała płytka nazębna z upływem czasu podlega mineralizacji i powstaje z niej <strong>kamień nazębny.</strong></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Uwarunkowania <strong>og&oacute;lne</strong> mogą wywoływać lub stwarzać sprzyjające warunki do powstawania chor&oacute;b przyzębia w obecności wyżej wymienionych czynnik&oacute;w miejscowych.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Objawy</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong><span style=\"font-size:14px;\">Najczęstsze objawy chor&oacute;b przyzębia to:</span></strong></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		krwawienie z dziąseł - jest to podstawowy, najważniejszy objaw choroby,</li>\r\n	<li style=\"text-align: justify;\">\r\n		obrzęki dziąseł,</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;l dziąseł,</li>\r\n	<li style=\"text-align: justify;\">\r\n		obnażanie korzeni (odsłanianie szyjek zębowych),</li>\r\n	<li style=\"text-align: justify;\">\r\n		rozchwianie zęb&oacute;w</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	<br />\r\n	Proszę pamiętać: <strong>dziąsła zdrowe nigdy nie krwawią!</strong></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"link\">Uwaga palacze: nikotyna zwęża naczynia krwionośne, co powoduje brak występowania krwawienia, a więc utrudnia szybkie wykrycie choroby przyzębia!</span></p>\r\n<p>\r\n	&nbsp;</p>', 0),
(14, NULL, 2, NULL, NULL, NULL, 'Ozdoby na zęby', 1, '2013-02-12 19:52:00', 'Ozdoby, zęby, umocowania, produkt', 'Czy do umocowania produkt', 'ozdoby-na-zeby', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy do umocowania produkt&oacute;w Jewels/Twizzler konieczne jest borowanie?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie. Nasze prodykty mocowane są na powierzchni zęba za pomocą specjalnego kleju kompozytowego, co wyklucza mechaniczne uszkodzenia powierzchni nazębnej.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy biżuterię nazębną można ponownie usunąć?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Oczywiście. Nasze produkty mogą być bez problemu i bez uszkodzeń powierzchni zęba przez fachową osobą ponownie usunięte lub przez inny model zastąpione.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy biżuteria nazębna jest szkodliwa dla zdrowia?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie. Nasze produkty są z materiał&oacute;w nieszkodliwych dla zdrowia, co zostało potwierdzone przez specjalistyczne laboratorium z Zurichu.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy istnieje możliwość poranienia lub skaleczenia się biżuterią nazębną?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie. Początkowy kr&oacute;tki okres jest okresem przyzwyczajenia się do nowego nabytku w ustach, jednakże pierwszojakościowe opracowanie naszych produkt&oacute;w gwarantuje wysoki komfort i niepowtarzalny uśmiech.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy produkty Twizzer / Jewels można samemu umocować na zębie?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie! Tylko kompetentny i fachowy stomatolog posiadający odpowiednie &quot;know how&quot; jest w stanie profesjonalnie umocować biżuterię na zębie.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Czy posiadając biżuterię nazębną trzeba zachować szczeg&oacute;lną ostrożność podczas mycia zęb&oacute;w?</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	Nie. Fachowe umocowanie gwarantuje bardzo dobrą przyczepność, poza tym posiadanie biżuteri nazębnej zachęca do częstszej higieny jamy ustnej, gdyż ściąga ona spojrzenia innych na nasz uśmiech</p>', 0),
(15, NULL, 2, NULL, NULL, NULL, 'Profilaktyka', 1, '2013-02-12 19:54:00', 'Profilaktyka, Lakierowanie	Zabieg, lakierowania, wykonuje, lekarz, stomatolog, gabinecie, stomatologicznym, stosując, tego', 'Lakierowanie\r\n\r\n	Zabieg lakierowania wykonuje lekarz stomatolog w gabinecie stomatologicznym stosując do tego preparat fluoru o wysokim stężeniu. Są to preparaty bezpieczne mimo wysokiego stężenia fluoru, gdyż szybko wiążą i dobrze przylegają do powierzchni zęba, uwalniając małe ilości fluoru\r\n\r\n	Zaletą lakier', 'profilaktyka', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Lakierowanie</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Zabieg lakierowania</strong> wykonuje lekarz stomatolog w gabinecie stomatologicznym stosując do tego preparat fluoru o wysokim stężeniu. Są to preparaty bezpieczne mimo wysokiego stężenia fluoru, gdyż szybko wiążą i dobrze przylegają do powierzchni zęba, uwalniając małe ilości fluoru.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Zaletą lakier&oacute;w fluorkowych jest długie utrzymywanie się na powierzchni zęba i długotrwałe uwalnianie fluoru.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:16px;\"><strong>Wskazania:</strong></span></p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		wysokie ryzyko pr&oacute;chnicy u dzieci powyżej 6 roku życia,</li>\r\n	<li style=\"text-align: justify;\">\r\n		złe nawyki dietetyczne u dorosłych,</li>\r\n	<li style=\"text-align: justify;\">\r\n		noszenie aparat&oacute;w ortodontycznych,</li>\r\n	<li style=\"text-align: justify;\">\r\n		użytkowanie protez ruchomych,</li>\r\n	<li style=\"text-align: justify;\">\r\n		obnażenie szyjek zęb&oacute;w,</li>\r\n	<li style=\"text-align: justify;\">\r\n		pr&oacute;chnica początkowa.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Zabieg lakierownia wykonuje się w zależności od skłonności do pr&oacute;chnicy. Przy średniej skłonności lakierowanie wykonuje się dwa razy w roku, czyli co 6 miesięcy. Przy większej podatności do pr&oacute;chnicy zabieg wykonuje się 4 razy w roku, co trzy miesiące. Jednak zawsze należy pamiętać, że o każdym zabiegu i wskazaniach do jego wykonania decyduje lekarz.</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<br />\r\n	<strong>Lakowanie</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Zabieg lakowania bruzd </strong>jest od kilku lat rutynowym zabiegiem profilaktycznym. Dotyczy zęb&oacute;w przedtrzonowych i trzonowych, gdyż budowa anatomiczna powierzchni zgryzowych tych zęb&oacute;w ze względu na obecność głębokich szczelin i bruzd sprzyja zaleganiu bakterii, kt&oacute;re kolonizują bruzdę już w momencie wyrzynania zęba.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	W celu zabezpieczenia zęba wyrzynającego się przed inwazją pr&oacute;chnicy stosuje się zabieg profilaktyczny polegający na lakowaniu bruzd. Zabieg wykonywany jest przez lekarza stomatologa, jest zupełnie bezbolesny i komfortowy dla pacjenta.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>Wskazania do lakowania:</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Wskazania należy rozważyć indywidualnie biorąc pod uwagę stopień ryzyka pr&oacute;chnicy, zwyczaje żywieniowe, nawyki higieniczne. Jednak o tym decyduje zawsze lekarz stomatolog.</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		całkowicie wyrznięte zęby trzonowe stałe (przede wszystkim zęby trzonowe pierwsze stałe - 6, kt&oacute;re wyrzynają się jako pierwsze zęby stałe i mają nam służyć jak najdłużej),</li>\r\n	<li style=\"text-align: justify;\">\r\n		zęby trzonowe i przedtrzonowe z głębokimi bruzdami,</li>\r\n	<li style=\"text-align: justify;\">\r\n		zęby trzonowe i przedtrzonowe u dzieci niepełnosprawnych (problem w utrzymaniu prawidłowej higieny jamy ustnej).</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Skuteczność zabiegu lakowania zależy od wielu czynnik&oacute;w</span>, wśr&oacute;d kt&oacute;rych wymienia się: technikę wykonania zabiegu, przygotowanie i odpowiedzialność osoby wykonującej zabieg oraz od warunk&oacute;w anatomicznych zęba, stopnia wyrżnięcia zęba, rodzaju zgryzu, wilgotności zęba w czasie zabiegu.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<strong>Pamiętaj !</strong></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"link\">Lakowanie pierwszych zęb&oacute;w bocznych stałych powinno się wykonywać możliwie wcześnie, w okresie pierwszych 6 miesięcy po wyrżnięciu się zęba.</span></p>', 0),
(16, NULL, 2, NULL, NULL, NULL, 'Laseroterapia', 1, '2013-02-12 19:58:00', 'Laseroterapia, ZAKRES, ZASTOSOWAŃ, STOMATOLOGICZNYCH', 'ZAKRES ZASTOSOWAŃ STOMATOLOGICZNYCH', 'laseroterapia', '<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	<strong>ZAKRES ZASTOSOWAŃ STOMATOLOGICZNYCH&nbsp;LASER&Oacute;W BIOSTYMULACYJNYCH</strong></h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Zastosowanie <strong>lasera biostymulacyjnego</strong> przed rozpoczęciem opracowywania ubytku pr&oacute;chnicowego podnosi pr&oacute;g b&oacute;lu i czyni zabieg znacznie mniej bolesnym. Lasery te są bardzo użyteczne w <span class=\"pink\">zabiegach przeciwb&oacute;lowych przy stomatopatiach protetycznych, ropniach przyzębia, paradontopatiach, obrzękach, opryszczce wargowej, zapaleniach miazgi.</span> Działanie lasera powoduje przyspieszenie gojenia się ran po zabiegach ekstracji, przecinania wędzidełka i resekcji, a ponadto przy przewlekłych zapaleniach tkanki okołozębowej. Użycie lasera <strong>zapobiega stanom zapalnym</strong> i jest <strong>bardzo przydatne w leczeniu nerwob&oacute;l&oacute;w</strong>. Dzięki tym aparatom zmniejszyć można krwawienie i przyspieszyć gojenie po zabiegu, a także likwidować ogniska zapalne. Zastosowanie lasera poprawia terapeutyczne skutki zabieg&oacute;w, zmniejsza ryzyko infekcji, przyspiesza regenerację uszkodzonych tkanek.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:16px;\"><strong>Wskazania:</strong></span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		stany zapalne miazgi, zatok, zębodołu,</li>\r\n	<li style=\"text-align: justify;\">\r\n		neuralgie,</li>\r\n	<li style=\"text-align: justify;\">\r\n		choroby dziąseł, przyzębia i błony śluzowej jamy ustnej</li>\r\n	<li style=\"text-align: justify;\">\r\n		grzybica jamy ustnej</li>\r\n	<li style=\"text-align: justify;\">\r\n		neuralgia nerwu tr&oacute;jdzielnego,</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;le w stawach skroniowo-żuchwowych,</li>\r\n	<li style=\"text-align: justify;\">\r\n		odczulanie odsłoniętej zębiny</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;l i obrzęk pozabiegowy,</li>\r\n	<li style=\"text-align: justify;\">\r\n		opryszczka, afty,</li>\r\n	<li style=\"text-align: justify;\">\r\n		zapalenie ślinianek,</li>\r\n	<li style=\"text-align: justify;\">\r\n		likwidacja szczękościsku,</li>\r\n	<li style=\"text-align: justify;\">\r\n		gojenie zębodołu po ekstrakcji,</li>\r\n	<li style=\"text-align: justify;\">\r\n		b&oacute;l i obrzęk po złamaniach szczęki</li>\r\n	<li style=\"text-align: justify;\">\r\n		przyspieszanie gojenia po tradycyjnych zabiegach chirurgicznych w jamie ustnej (np. po plastyce wędzidełek, resekcji, sterowanej regeneracji kości, zabiegach płatowych, gingiwektomiach, przeszczepach i implantach)</li>\r\n</ul>', 0),
(17, 5, NULL, NULL, NULL, NULL, 'Gabinet', 1, '2014-02-18 22:41:00', 'Gabinet', NULL, 'gabinet', '<h2 class=\"title\" style=\"text-align: center;\">\r\n	Zapraszamy do obejrzenia naszego filmu promocyjnego:</h2>\r\n<p style=\"text-align: center;\">\r\n	<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"433\" src=\"https://www.youtube.com/embed/n_BKsj-3yfU\" width=\"770\"></iframe></p>', 0),
(18, NULL, NULL, NULL, NULL, NULL, 'Ortodoncja', 1, '2013-10-23 10:59:00', 'Ortodoncja, pięknego, uśmiechu, ważna, jest, profilaktyka, leczenie, zgryzu, dzieci,, młodzieży', 'Dla pięknego uśmiechu ważna jest profilaktyka i leczenie wad zgryzu u dzieci, młodzieży oraz dorosłych. Skutecznie przeprowadzone leczenie ortodontyczne wpływa na poprawę wyglądu i samopoczucia, prawidłową wymowę, lepszą higienę jamy ustnej, a czasem jest podstawowym warunkiem, aby długo cieszyć się zdrowymi zębami\r\n\r\n	Czym jest wada zgryzu?\r\n\r\n	Wada zgryzu to niewłaściwe ustawienie zęb', 'ortodoncja', '<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Dla pięknego uśmiechu </span>ważna jest <strong>profilaktyka i leczenie wad zgryzu u dzieci, młodzieży oraz dorosłych</strong>. Skutecznie przeprowadzone<strong> leczenie ortodontyczne</strong> wpływa na poprawę wyglądu i samopoczucia, prawidłową wymowę, lepszą higienę jamy ustnej, a czasem jest podstawowym warunkiem, aby długo cieszyć się zdrowymi zębami.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Czym jest wada zgryzu?</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Wada zgryzu to niewłaściwe ustawienie zęb&oacute;w w łukach zębowych, czyli tzw. &quot;krzywe zęby&quot;. Obejmuje ona r&oacute;wnież takie sytuacje, w kt&oacute;rych zęby są &quot;proste&quot;, lecz łuki zębowe zwierają się w nieprawidłowy spos&oacute;b, np. g&oacute;rne zęby są znacznie wysunięte do przodu względem dolnych. Taki nieprawidłowy układ g&oacute;rnego i dolnego łuku zębowego może być zauważalny przez samego pacjenta lub rodzic&oacute;w; może ujawniać się w rysach twarzy, ale może być też niezauważony.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Czy wada zgryzu to tylko problem brzydkiego wyglądu?</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	Niestety niekt&oacute;re wady zgryzu to nie tylko problem estetyczny. Niewłaściwie ustawione zęby sprzyjają odkładaniu płytki nazębnej, utrudniają właściwe czyszczenie zęb&oacute;w, a to sprzyja powstawaniu pr&oacute;chnicy i stan&oacute;w zapalnych dziąseł czy przyzębia. Wady zgryzu sprzyjają ścieraniu się zęb&oacute;w; powodują, że zęby są obciążane nieosiowo (niefizjologicznie, ze zbyt dużym naciskiem na nie), co w efekcie może przyczynić się do ich utraty.</p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Rodzaje wad zgryzu:</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>tyłozgryz</strong> - najczęściej występująca nieprawidłowość; leczenie przynosi bardzo dobre efekty, szczeg&oacute;lnie w okresie wzrostu, czyli u dzieci. W niekt&oacute;rych przypadkach wada ta może wymagać leczenia chirurgicznego</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>przodozgryz</strong> - odwrotność tyłozgryzu - zęby przednie dolne nagryzają na g&oacute;rne, widoczne jest to w rysach twarzy poprzez wysuniętą wargę dolną czy dominującą dużą brodę. Jeżeli leczenie ortodontyczne zostało przeprowadzone przed okresem intensywnego wzrostu pacjenta, możliwe jest ponowne jej pojawienie się, a nawet nasilenie wady. Konieczna jest w&oacute;wczas interwencja chirurgiczna.</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>zgryz otwarty</strong> - w tej wadzie zęby nie kontaktują się ze sobą. Wada ta ma tendencje do nawrotu u pacjent&oacute;w, kt&oacute;rzy nieprawidłowo układają język podczas spoczynku, m&oacute;wienia lub połykania</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>zgryz głęboki </strong>- charakteryzuje się głębokim zachodzeniem siekaczy g&oacute;rnych na dolne; wada, kt&oacute;ra naturalnie pogłębia się wraz z wiekiem</li>\r\n	<li style=\"text-align: justify;\">\r\n		<strong>zgryz krzyżowy </strong>- występuje w&oacute;wczas, gdy zęby dolne pokrywają zęby g&oacute;rne, co może być widoczne w rysach twarzy; zęby są przeciążone i narażone na ciągły uraz</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<h2 class=\"title1\" style=\"text-align: justify;\">\r\n	Jak przebiega leczenie ortodontyczne w naszej klinice?</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Konsultacja ortodontyczna</strong></span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Pierwsza wizyta pacjenta w naszej klinice ma na celu poznanie gł&oacute;wnych jego problem&oacute;w i oczekiwań dotyczących efektu leczenia.</span> Niezbędne etapy tej wizyty w naszej klinice to:</p>\r\n<ul>\r\n	<li style=\"text-align: justify;\">\r\n		wywiad z pacjentem</li>\r\n	<li style=\"text-align: justify;\">\r\n		badanie pacjenta</li>\r\n	<li style=\"text-align: justify;\">\r\n		pobranie wycisk&oacute;w do wykonania gipsowych modeli diagnostycznych</li>\r\n	<li style=\"text-align: justify;\">\r\n		ewentualne wykonanie zdjęć fotograficznych wewnątrzustnych i zewnątrzustnych (rys&oacute;w twarzy)</li>\r\n	<li style=\"text-align: justify;\">\r\n		wykonanie zdjęcia pantomograficznego i zdjęcia bocznego czaszki</li>\r\n</ul>\r\n<h2 class=\"title\" style=\"text-align: justify;\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Tylko pełny komplet badań umożliwia kompleksowe zaplanowanie leczenia dla każdego pacjenta.</h2>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span style=\"font-size:14px;\"><strong>Plan leczenia</strong></span></p>\r\n<p style=\"text-align: justify;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: justify;\">\r\n	<span class=\"pink\">Druga wizyta obejmuje rozmowę lekarza z pacjentem, podczas kt&oacute;rej krok po kroku omawiany jest układ i stan uzębienia, a także możliwe sposoby leczenia</span>. Pacjent&nbsp; akceptuje plan swojego leczenia obejmujący wszystkie istotne informacje: typ aparatu, przewidywany czas leczenia i szacunkowe koszty do akceptacji.</p>', 0);
INSERT INTO `Strona` (`id`, `galeria_id`, `strona_nadrzedna_id`, `obrazek`, `updated_at`, `title2`, `title`, `published`, `publishDate`, `keywords`, `description`, `slug`, `content`, `automaticSeo`) VALUES
(19, NULL, NULL, NULL, NULL, NULL, 'blok2-strona-glowna', 0, '2019-02-20 14:05:00', 'blok2-strona-glowna,  			STOMATOLOGIA, ZACHOWAWCZA , leczenie, próchnicy			ODBUDOWA zniszczonych, złamanych, koron, zębów			STOMATOLOGIA, DZIECIĘCA, - leczenie', ' \r\n\r\n	\r\n		STOMATOLOGIA ZACHOWAWCZA  - leczenie próchnicy\r\n	\r\n		ODBUDOWA zniszczonych i złamanych koron zębów\r\n	\r\n		STOMATOLOGIA DZIECIĘCA - leczenie zębów mlecznych, profilaktyka wczesnodziecięca\r\n	\r\n		ENDODONCJA - nowoczesne metody opracowywania i wypełniania kanałów korzeniowych, leczenie zmian okołowierzchołkowych\r\n	\r\n		PROTETYKA - wszystkie rodzaje uzupełnień\r\n	\r\n		IMPLANTOLOGIA - wszczepianie implantów w miejsca brakujących zębów\r\n	\r\n		PERIODONTOLOGIA  - leczenie chorób przyzębia, zęby rozchwiane, zanik dziąseł, przykry zapach z ust, usuwanie kamienia\r\n	\r\n		CHIRURGIA STOMATOLOGICZNA - usuwanie zębów mlecznych i stałych\r\n	\r\n		LECZENIE zaburzeń stanów s-ż (bóle ucha i głowy)\r\n	\r\n		ORTODONCJA - aparaty stałe i ruchome\r\n	\r\n		PROFILAKTYKA - u dzieci i dorosłych\r\n	\r\n		WYBIELANIE I PIASKOWANIE zębów\r\n	\r\n		SZLACHETNE ozdoby na zęby\r\n\r\n\r\n	 ', 'blok2-strona-glowna', '<ul>\r\n	<li>\r\n		<b>Peeling chemiczny&nbsp;</b> - usunięcie martwego nask&oacute;rka, a tym samym przyśpieszenie odnowy kom&oacute;rkowej, za pomocą nałożenia na sk&oacute;rę odpowiedniej mieszanki chemicznej</li>\r\n	<li>\r\n		<b>Mezoterapia igłowa -&nbsp;</b>w pełni bezpieczny, niechirurgiczny, a przede wszystkim skuteczny spos&oacute;b na regenerację i odmłodzenie sk&oacute;ry, poprawienie jej elastyczności i nawilżenia.</li>\r\n	<li>\r\n		<b>Wypełnienia kwasem haliuronowym -&nbsp;</b>skutecznie modelujemy kształt ust, kontur twarzy, wypełniamy zmarszczki, redukujemy bruzdy i rewitalizujemy sk&oacute;rę</li>\r\n	<li>\r\n		<b>Botoks</b>&nbsp;- przeciwdziałanie powstawiania zmarszczek, leczenie bruksizmu i nadpotliwości; jedna z najskuteczniejszych form terapii antyzmarszczkowej, zwłaszcza w g&oacute;rnym obszarze twarzy i okolic nosa</li>\r\n	<li>\r\n		<b>Osocze bogatopłytkowe</b>&nbsp;- wampirzy lifting, czyli terapia osoczem bogatopłytkowym, służy regeneracji sk&oacute;ry poprzez pobudzenie fibroblast&oacute;w (biostymulację) do tworzenia nowego kolagenu</li>\r\n</ul>', 0),
(21, NULL, NULL, NULL, NULL, NULL, 'Medycyna estetyczna', 1, '2019-02-22 09:43:00', 'Medycyna, estetyczna, Kliknij, wybraną, usługę,, poznać, szczegóły:	 	 																																																																																																															 ', 'Kliknij na wybraną usługę, aby poznać szczegóły:\r\n\r\n	 \r\n\r\n	 \r\n\r\n	\r\n		\r\n			\r\n				\r\n			\r\n				\r\n		\r\n		\r\n			\r\n				\r\n			\r\n				\r\n		\r\n		\r\n			\r\n				\r\n			\r\n				\r\n		\r\n		\r\n			\r\n				\r\n			\r\n				\r\n		\r\n		\r\n			\r\n				\r\n			\r\n				\r\n		\r\n		\r\n			\r\n				\r\n			\r\n				\r\n		\r\n	\r\n\r\n\r\n	 ', 'medycyna-estetyczna', '<p style=\"text-align: center;\">\r\n	<span style=\"font-size:16px;\"><strong>Kliknij na wybraną usługę</strong>, aby poznać szczeg&oacute;ły:</span></p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<p style=\"text-align: center;\">\r\n	&nbsp;</p>\r\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 500px;\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/peeling-chemiczny.html\"><img alt=\"Peeling chemiczny\" src=\"/uploads/pliki/Medycyna%20Estetyczna/Peeling%20chemiczny.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/mezoterapia-iglowa.html\"><img alt=\"Mezoterapia igłowa\" src=\"/uploads/pliki/Medycyna%20Estetyczna/Mezoterapia%20ig%C5%82owa.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/kwas-hialuronowy.html\"><img alt=\"Kwas hialuronowy\" src=\"/uploads/pliki/Medycyna%20Estetyczna/Kwas%20hialuronowy.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n			<td>\r\n				<a href=\"/botoks.html\"><img alt=\"Botoks\" src=\"/uploads/pliki/Medycyna%20Estetyczna/Botoks.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<a href=\"/osocze-bogatoplytkowe.html\"><img alt=\"Osocze bogatopłytkowe\" src=\"/uploads/pliki/Medycyna%20Estetyczna/Osocze%20bogatop%C5%82ytkowe.jpg\" style=\"width: 391px; height: 69px;\" /></a></td>\r\n			<td>\r\n				&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>', 0),
(22, NULL, NULL, NULL, NULL, NULL, 'Peeling chemiczny', 1, '2019-08-28 15:34:00', 'Peeling, chemiczny, działa, peeling, chemiczny?\r\n\r\n	 \r\n\r\n	Jego, celem, jest, pozbycie, się, martwego', '\r\n	Jak działa peeling chemiczny?\r\n\r\n	 \r\n\r\n	Jego celem jest pozbycie się martwego naskórka, a tym samym przyśpieszenie odnowy komórkowej. W efekcie nałożenia na skórę odpowiedniej mieszanki chemicznej naskórek natychmiastowo się złuszcza, pobudzając organizm do produkcji nowego kolagenu i elastyny. Tak intensywna regeneracja skutkuje wyraźną poprawą kondycji skóry\n\r\n	 \r\n\r\n	Najbardziej popularną formą zabiegu jest stosowanie go na skórę twarzy, jednak coraz częściej znajduje zwolenników wśród chętnych do odnowy skóry dłoni, szyi czy dekoltu\n\r\n	Peeling chemiczny jest jednym z najmniej inwazyjnych zabiegów medycyny estetycznej i nie wymaga hospitalizacji\n\r\n	 \r\n\r\n	Wskazania do zastosowania peelingu chemicznego:\r\n\r\n	 \r\n\r\n	\r\n		przebarwienia skórne\r\n	\r\n		wygładzenie płytkich zmarszczek\r\n	\r\n		redukcja blizn (np\n	\r\n		starzenie skóry twarzy spowodowane ekspozycją na słońce\r\n	\r\n		rewitalizacja skórna\r\n	\r\n		zmniejszenie łojotoku\r\n\r\n\r\n	 \r\n\r\n	Rodzaje peelingu chemicznego:\r\n\r\n	 \r\n\r\n	\r\n		Peeling powierzchniowy — eliminuje drobne zmarszczki, przebarwienia oraz pomaga w zwalczaniu trądziku poprzez usunięcie naskórka\r\n	\r\n		Peeling średnio-głęboki — redukuje głębsze zmarszczki, blizny potrądzikowe i przebarwienia, sięga nie tylko naskórka, ale też skóry właściwej\r\n	\r\n		Peeling głęboki — eliminuje głębokie zmarszczki i blizy\r\n\r\n\r\n	 \r\n\r\n	Przygotowanie do zabiegu i zalecenia:\r\n\r\n	 \r\n\r\n	\r\n		już na 6 tygodni przed planowanym zabiegiem należy odstawić wyroby nikotynowe oraz leki na bazie kwasu acetylosalicylowego\r\n	\r\n		w celu wzmocnienia efektów zabiegu może zostać zalecone używanie odpowiednich kosmetyków\r\n	\r\n		do dokonania zabiegu, skóra musi być wolna od opryszczki lub zakażenia\r\n\r\n\r\n	 \r\n\r\n	Po zabiegu:\r\n\r\n	 \r\n\r\n	\r\n		zaleca się maksymalnie ograniczyć ekspozycję skóry na słońce oraz stosować wysoką ochronę przeciwsłoneczną\r\n	\r\n		nie należy wykonywać intensywnych ćwiczeń\r\n', 'peeling-chemiczny', '<h2 class=\"title1\">\r\n	Jak działa peeling chemiczny?</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Jego celem jest pozbycie się martwego nask&oacute;rka, a tym samym przyśpieszenie odnowy kom&oacute;rkowej. W efekcie nałożenia na sk&oacute;rę odpowiedniej mieszanki chemicznej nask&oacute;rek natychmiastowo się złuszcza, pobudzając organizm do produkcji nowego kolagenu i elastyny. Tak intensywna regeneracja skutkuje wyraźną poprawą kondycji sk&oacute;ry.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Najbardziej popularną formą zabiegu jest stosowanie go na sk&oacute;rę twarzy, jednak coraz częściej znajduje zwolennik&oacute;w wśr&oacute;d chętnych do odnowy sk&oacute;ry dłoni, szyi czy dekoltu.</h2>\r\n<p>\r\n	<span class=\"link\">Peeling chemiczny jest jednym z najmniej inwazyjnych zabieg&oacute;w medycyny estetycznej i nie wymaga hospitalizacji.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Wskazania do zastosowania peelingu chemicznego:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		przebarwienia sk&oacute;rne</li>\r\n	<li>\r\n		wygładzenie płytkich zmarszczek</li>\r\n	<li>\r\n		redukcja blizn (np. potrądzikowe)</li>\r\n	<li>\r\n		starzenie sk&oacute;ry twarzy spowodowane ekspozycją na słońce</li>\r\n	<li>\r\n		rewitalizacja sk&oacute;rna</li>\r\n	<li>\r\n		zmniejszenie łojotoku</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title1\">\r\n	<strong>Rodzaje peelingu chemicznego:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		Peeling powierzchniowy &mdash; eliminuje drobne zmarszczki, przebarwienia oraz pomaga w zwalczaniu trądziku poprzez usunięcie nask&oacute;rka</li>\r\n	<li>\r\n		Peeling średnio-głęboki &mdash; redukuje głębsze zmarszczki, blizny potrądzikowe i przebarwienia, sięga nie tylko nask&oacute;rka, ale też sk&oacute;ry właściwej</li>\r\n	<li>\r\n		Peeling głęboki &mdash; eliminuje głębokie zmarszczki i blizy</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	&nbsp;</h2>\r\n<h2 class=\"title1\">\r\n	<strong>Przygotowanie do zabiegu i zalecenia:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		już na 6 tygodni przed planowanym zabiegiem należy odstawić wyroby nikotynowe oraz leki na bazie kwasu acetylosalicylowego</li>\r\n	<li>\r\n		w celu wzmocnienia efekt&oacute;w zabiegu może zostać zalecone używanie odpowiednich kosmetyk&oacute;w</li>\r\n	<li>\r\n		do dokonania zabiegu, sk&oacute;ra musi być wolna od opryszczki lub zakażenia</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Po zabiegu:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		zaleca się maksymalnie ograniczyć ekspozycję sk&oacute;ry na słońce oraz stosować wysoką ochronę przeciwsłoneczną</li>\r\n	<li>\r\n		nie należy wykonywać intensywnych ćwiczeń</li>\r\n</ul>', 0),
(23, NULL, 21, NULL, NULL, NULL, 'Mezoterapia igłowa', 1, '2019-08-28 15:50:00', 'Mezoterapia, igłowa, Czym, jest, mezoterapia, jakie, przynosi, rezultaty?\r\n\r\n	 \r\n\r\n	Mezoterapia, pozostaje, czołówce', '\r\n	Czym jest mezoterapia i jakie przynosi rezultaty?\r\n\r\n	 \r\n\r\n	Mezoterapia pozostaje w czołówce ulubionych zabiegów wśród klientów medycyny estetycznej. Jest to w zupełności bezpieczny, niechirurgiczny, a co najważniejsze skuteczny sposób na poprawę jakości skóry. Dzięki niej mamy możliwość, by nie tylko opóźnić proces starzenia skóry, ale także skutecznie walczyć z traceniem włosów\n\r\n	 \r\n\r\n	W trakcie zabiegu, przy użyciu strzykawki i igły specjalista podaje odpowiednio dobrane substancje lecznicze, bezpośrednio do głębokich warstw skóry\n\r\n	Tak przygotowywane mieszanki zwane są powszechnie koktajlami. Ich działanie jest błyskawiczne, pozostawiając skórę w procesie pobudzonej produkcji kolagenu oraz elastyny\n\r\n	 \r\n\r\n	Zabieg przynosi trwałe efekty, przy krótkim czasie rekonwalescencji. Najczęściej stosowany jest na twarzy, ale przynosi świetne wyniki również na innych częściach\n\r\n	 \r\n\r\n	Mezoterapię igłową stosuje się nie tylko jako zabieg leczniczy, ale również profilaktycznie, w celu dbania o zdrową i piękną skórę\n\r\n	 \r\n\r\n	Wskazania do zastosowania:\r\n\r\n	 \r\n\r\n	\r\n		niwelowanie zmarszczek\r\n	\r\n		lifting owalu twarzy\r\n	\r\n		zwiotczenie mięśni twarzy i szyi\r\n	\r\n		cienie i zasinienia pod oczami\r\n	\r\n		poprawa nawilżenia zmęczonej skóry\r\n	\r\n		blizny potrądzikowe\r\n	\r\n		rozstępy i cellulit\r\n	\r\n		wypadanie włosów i łysienie\r\n	\r\n		przebarwienia\r\n\r\n\r\n	\r\n	Przygotowanie do zabiegu i zalecenia:\r\n\r\n	 \r\n\r\n	\r\n		konieczna jest konsultacja ze specjalistą, której celem jest omówienie oczekiwań oraz dobranie odpowiedniego koktajlu\r\n	\r\n		w dniu zabiegu nie należy spożywać kawy oraz alkoholu,\r\n	\r\n		zabieg nie jest zalecany w okresie osłabienia organizmu\n\r\n\r\n	 \r\n\r\n	Po przeprowadzonym zabiegu:\r\n\r\n	\r\n		należy stosować wysokie filtry ochronne i nie wystawiać się na ekspozycję słońca\r\n	\r\n		unikać wzmożonej aktywności fizycznej\r\n	\r\n		pić większą ilość wody\r\n	\r\n		nie dotykać skóry poddanej iniekcji\r\n', 'mezoterapia-iglowa', '<h2 class=\"title1\">\r\n	<strong><span class=\"pink\">Czym jest mezoterapia i jakie przynosi rezultaty?</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mezoterapia pozostaje w czoł&oacute;wce ulubionych zabieg&oacute;w wśr&oacute;d klient&oacute;w medycyny estetycznej. Jest to w zupełności bezpieczny, niechirurgiczny, a co najważniejsze skuteczny spos&oacute;b na poprawę jakości sk&oacute;ry. Dzięki niej mamy możliwość, by nie tylko op&oacute;źnić proces starzenia sk&oacute;ry, ale także skutecznie walczyć z traceniem włos&oacute;w.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	W trakcie zabiegu, przy użyciu strzykawki i igły specjalista podaje odpowiednio dobrane substancje lecznicze, bezpośrednio do głębokich warstw sk&oacute;ry.</h2>\r\n<p>\r\n	Tak przygotowywane mieszanki zwane są powszechnie koktajlami. Ich działanie jest błyskawiczne, pozostawiając sk&oacute;rę w procesie pobudzonej produkcji kolagenu oraz elastyny.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zabieg przynosi trwałe efekty, przy kr&oacute;tkim czasie rekonwalescencji. Najczęściej stosowany jest na twarzy, ale przynosi świetne wyniki r&oacute;wnież na innych częściach.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Mezoterapię igłową stosuje się nie tylko jako zabieg leczniczy, ale r&oacute;wnież profilaktycznie, w celu dbania o zdrową i piękną sk&oacute;rę.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Wskazania do zastosowania:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		niwelowanie zmarszczek</li>\r\n	<li>\r\n		lifting owalu twarzy</li>\r\n	<li>\r\n		zwiotczenie mięśni twarzy i szyi</li>\r\n	<li>\r\n		cienie i zasinienia pod oczami</li>\r\n	<li>\r\n		poprawa nawilżenia zmęczonej sk&oacute;ry</li>\r\n	<li>\r\n		blizny potrądzikowe</li>\r\n	<li>\r\n		rozstępy i cellulit</li>\r\n	<li>\r\n		wypadanie włos&oacute;w i łysienie</li>\r\n	<li>\r\n		przebarwienia</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	<br />\r\n	<strong>Przygotowanie do zabiegu i zalecenia:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		konieczna jest konsultacja ze specjalistą, kt&oacute;rej celem jest om&oacute;wienie oczekiwań oraz dobranie odpowiedniego koktajlu</li>\r\n	<li>\r\n		w dniu zabiegu nie należy spożywać kawy oraz alkoholu,</li>\r\n	<li>\r\n		zabieg nie jest zalecany w okresie osłabienia organizmu.</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Po przeprowadzonym zabiegu:</strong></p>\r\n<ul>\r\n	<li>\r\n		należy stosować wysokie filtry ochronne i nie wystawiać się na ekspozycję słońca</li>\r\n	<li>\r\n		unikać wzmożonej aktywności fizycznej</li>\r\n	<li>\r\n		pić większą ilość wody</li>\r\n	<li>\r\n		nie dotykać sk&oacute;ry poddanej iniekcji</li>\r\n</ul>', 0),
(24, NULL, 21, NULL, NULL, NULL, 'Kwas haliuronowy', 1, '2019-08-28 15:51:00', 'Kwas, haliuronowy, działa, kwas, hialuronowy?\r\n\r\n	 \r\n\r\n	Kwas, naturalnie, występuje, naszym, ciele,, lecz', '\r\n	Jak działa kwas hialuronowy?\r\n\r\n	 \r\n\r\n	Kwas ten naturalnie występuje w naszym ciele, lecz wraz z wiekiem jego ilość w skórze maleje, skutkując jej wiotczeniem i wysuszaniem, jak również zmniejszeniem objętości policzków i ust. Kwas hialuronowy przyciąga i wiąże cząsteczki wody, decydując o nawilżeniu skóry, jej sprężystości i odporności na powstawanie zmarszczek\n\r\n	 \r\n\r\n	W jaki sposób wykorzystuje się kwas hialuronowy w medycynie estetycznej?\r\n\r\n	 \r\n\r\n	Preparat wykorzystywany w zabiegu jest całkowicie syntetyczny. Wstrzykiwany w konkretne miejsca w skórze właściwej, uzupełnia niedobór jego naturalnych pokładów, natychmiastowo ją nawilżając\n\r\n	 \r\n\r\n	Tym sposobem skutecznie modelujemy kształt twarzy i ust oraz wypełniamy zmarszczki i bruzdy\n\r\n	Pobudzane są także naturalne procesy regeneracyjne skóry. Dzięki temu nawet po wchłonięciu substancji skóra wygląda znacznie lepiej niż przed zabiegiem\n\r\n	 \r\n\r\n	Efekty działania kwasu hialuronowego utrzymują się do 18 miesięcy, w zależności od indywidualnych uwarunkowań organizmu\n\r\n	 \r\n\r\n	Wskazania do zastosowania:\r\n\r\n	 \r\n\r\n	\r\n		wypełnienia zmarszczek i bruzd nosowo-wargowych\r\n	\r\n		kurze łapki\r\n	\r\n		opadnięte kąciki ust\r\n	\r\n		wypełnienie ust\r\n	\r\n		korekta owalu twarzy\r\n	\r\n		korekta kształtu nosa\r\n	\r\n		korekta linii marionetki\r\n	\r\n		wolumetria kości policzkowych i policzków\r\n\r\n\r\n	\r\n	Zalecenia i przygotowanie do zabiegu:\r\n\r\n	 \r\n\r\n	Dbając o komfort pacjenta, godzinę przed zabiegiem miejsca nakłuć smarowane są kremem zawierającym środek znieczulający\n\r\n	 \r\n\r\n	Przez okres 14 dni po zabiegu należy unikać ekspozycji na słońce oraz sauny', 'kwas-haliuronowy', '<h2 class=\"title1\">\r\n	<strong>Jak działa kwas hialuronowy?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Kwas ten naturalnie występuje w naszym ciele, lecz wraz z wiekiem jego ilość w sk&oacute;rze maleje, skutkując jej wiotczeniem i wysuszaniem, jak r&oacute;wnież zmniejszeniem objętości policzk&oacute;w i ust. Kwas hialuronowy przyciąga i wiąże cząsteczki wody, decydując o nawilżeniu sk&oacute;ry, jej sprężystości i odporności na powstawanie zmarszczek.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>W jaki spos&oacute;b wykorzystuje się kwas hialuronowy w medycynie estetycznej?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Preparat wykorzystywany w zabiegu jest całkowicie syntetyczny. Wstrzykiwany w konkretne miejsca w sk&oacute;rze właściwej, uzupełnia niedob&oacute;r jego naturalnych pokład&oacute;w, natychmiastowo ją nawilżając.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	<span style=\"background-color: initial;\">Tym sposobem skutecznie modelujemy kształt twarzy i ust oraz wypełniamy zmarszczki i bruzdy.</span></h2>\r\n<p>\r\n	Pobudzane są także naturalne procesy regeneracyjne sk&oacute;ry. Dzięki temu nawet po wchłonięciu substancji sk&oacute;ra wygląda znacznie lepiej niż przed zabiegiem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class=\"link\">Efekty działania kwasu hialuronowego utrzymują się do 18 miesięcy, w zależności od indywidualnych uwarunkowań organizmu.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Wskazania do zastosowania:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		wypełnienia zmarszczek i bruzd nosowo-wargowych</li>\r\n	<li>\r\n		kurze łapki</li>\r\n	<li>\r\n		opadnięte kąciki ust</li>\r\n	<li>\r\n		wypełnienie ust</li>\r\n	<li>\r\n		korekta owalu twarzy</li>\r\n	<li>\r\n		korekta kształtu nosa</li>\r\n	<li>\r\n		korekta linii marionetki</li>\r\n	<li>\r\n		wolumetria kości policzkowych i policzk&oacute;w</li>\r\n</ul>\r\n<h2 class=\"title1\">\r\n	<br />\r\n	<strong>Zalecenia i przygotowanie do zabiegu:</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Dbając o komfort pacjenta, godzinę przed zabiegiem miejsca nakłuć smarowane są kremem zawierającym środek znieczulający.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Przez okres 14 dni po zabiegu należy unikać ekspozycji na słońce oraz sauny.</p>', 0),
(25, NULL, 21, NULL, NULL, NULL, 'Botoks', 1, '2019-08-28 15:51:00', 'Botoks, Czym, jest, toksyna, botulinowa?	 	Popularny, botoks, silnie, działającą, naturalną, toksyną,', 'Czym jest toksyna botulinowa?\r\n\r\n	 \r\n\r\n	Popularny botoks jest silnie działającą naturalną toksyną, która powoduje zahamowanie uwalniania acetylocholiny. Jest to neuroprzekaźnik odpowiedzialny za przekazywanie impulsów pomiędzy nerwami. Innymi słowy działanie tej substancji blokuje synapsy nerwowo-mięśniowe (połączenia między zakończeniami nerwów, a unerwionymi przez nie mięśniami), w skutek czego nie dochodzi do skurczów porażonego mięśnia. Dzięki takim właściwościom możliwe jest leczenie wielu chorób neurologicznych przebiegających z nadmiernym napięciem mięśni\r\n\r\n	 \r\n\r\n	Jakie zastosowanie ma botoks w medycynie estetycznej?\r\n\r\n	 \r\n\r\n	W medycynie estetycznej do najważniejszych zastosowań botoksu możemy zaliczyć przeciwdziałanie powstawiania zmarszczek, leczenie bruksizmu i nadpotliwości. W takich przypadkach podaje się niezwykle małe stężenia toksyny, które nie mają żadnego wpływu na inne elementy układu nerwowego poza miejscem, w którym bezpośrednio ją zastosowano\r\n\r\n	 \r\n\r\n	Zabieg z wykorzystaniem toksyny botulinowej polega na podskórnym podaniu właściwej dawki w ściśle określone miejsca za pomocą jednorazowej strzykawki z bardzo cienką igłą\r\n\r\n	Zabieg trwa bardzo krótko, w zależności od liczby miejsc poddawanych leczeniu czas ten może się wahać około 15-20 minut\r\n\r\n	 \r\n\r\n	Jest to jedna z najskuteczniejszych form terapii antyzmarszczkowej, zwłaszcza w górnym obszarze twarzy i okolic nosa („zmarszczki królicze”). Umożliwia również korektę jego nadmiernie zagiętego koniuszka. Jest to również szansa dla wszystkich tych, którzy cierpią na przewlekłe migreny\r\n\r\n	 \r\n\r\n	Efekty działania botoksu:\r\n\r\n	 \r\n\r\n	Kilka dni po zabiegu zobaczymy już rezultaty, zarówno w kwestii redukcji zmarszczek, jak i nadpotliwości, natomiast pełne rezultaty pojawią się po ok. 2-3 tygodniach. Dla utrzymania stałych efektów zaleca się powtarzać zabieg co 6-8 miesięcy\r\n\r\n	 \r\n\r\n	Wskazania do zastosowania:\r\n\r\n	 \r\n\r\n	\r\n		redukcja zma', 'botoks', '<h2 class=\"title1\">\r\n	<strong>Botoks, czyli toksyna botulinowa...</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Znany wszystkim Botoks to w rzeczywistości silna naturalna toksyna, wstrzymująca uwalnianie acetylocholiny. Jako neuroprzekaźnik odpowiada za przekazywanie asumpt&oacute;w pomiędzy nerwami, w konsekwencji zapobiega skurczom porażonego mięśnia. Poprzez czerpanie z właściwości toksyny botulinowej możliwa jest kuracja licznych chor&oacute;b neurologicznych związanych z zawyżonym napięciem mięśni.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Jakie przeznaczenie ma botoks w medycynie estetycznej?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Do najważniejszych zastosowań botoksu w medycynie estetycznej należy zaliczyć zapobieganie powstawania zmarszczek, leczenie bruksizmu i nadpotliwości. W takich sytuacjach serwuje się wyjątkowo małe stężenia toksyny, niewpływające na żadne elementy układu nerwowego poza miejscem bezpośredniego zastosowania.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title\" style=\"text-align: center;\">\r\n	Zabieg z wykorzystaniem Botoksu polega na wstrzyknięciu stosownej dawki w konkretne miejsca przy użyciu jednorazowej strzykawki zakończonej wyjątkowo cienką igłą</h2>\r\n<p>\r\n	Czas zabiegu wacha się w zależności od liczby miejsc poddawanych kuracji, szacunkowo 15 do 20 minut.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class=\"link\">Jest jednym z najskuteczniejszych rodzai terapii przeciwzmarszczkowej, przede wszystkim w g&oacute;rnej sferze twarzy i okolic nosa. Pomaga r&oacute;wnież skorygować nadmierne zagięcie jego czubka. To także szansa dla wszystkich tych, kt&oacute;rym dokuczają chroniczne migreny.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Efekty działania botoksu:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zaledwie kilka dni po zabiegu będą widoczne pierwsze efekty, natomiast pełnię możliwości botoksu musimy poczekać ok.2-3 tyg. Aby efekt trwał jak najdłużej, sugeruje się powtarzać zabieg co 6-8 miesięcy.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Wskazania do zastosowania:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		redukcja zmarszczek okolic oczu (kurze łapki)</li>\r\n	<li>\r\n		redukcja zmarszczek między brwiami (lwia zmarszczka)</li>\r\n	<li>\r\n		redukcja zmarszczek na czole</li>\r\n	<li>\r\n		redukcja zmarszczek na szyi i brodzie (bruzdy marionetki)</li>\r\n	<li>\r\n		unoszenie brwi</li>\r\n	<li>\r\n		leczenie bruksizmu</li>\r\n	<li>\r\n		wygładzanie sk&oacute;ry szyi</li>\r\n	<li>\r\n		leczenie nadpotliwości</li>\r\n	<li>\r\n		leczenie migreny</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Przygotowanie do zabiegu:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Do podania toksyny nie ma szczeg&oacute;lnych wymagań diagnostycznych. Jest to zabieg niegroźny, za to bardzo skuteczny. Przed podjęciem decyzji o zabiegu radzi się konsultacji z ekspertem w celu weryfikacji wszelkich przeciwwskazań oraz om&oacute;wienia oczekiwań.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zabieg pozostaje nieinwazyjny i prosty, jednak dla wygody pacjenta niekiedy stosuje się miejscowe znieczulenie kremem.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong><span class=\"pink\">Zalecenie po przeprowadzonym zabiegu:</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		zalecane ćwiczenia mięśni mimicznych w miejscach wkłucia</li>\r\n	<li>\r\n		ograniczyć dotyk miejsc wkłucia</li>\r\n	<li>\r\n		nie powinno się pochylać głowy do 4 godzin po zabiegu</li>\r\n	<li>\r\n		nie należy wykonywać wyczerpujących ćwiczeń fizycznych</li>\r\n	<li>\r\n		zaleca się unikać bezpośredniej ekspozycji słonecznej i wysokich temperatur w miejscach podania toksyny</li>\r\n	<li>\r\n		podr&oacute;że lotnicze nie są rekomendowane przynajmniej przez kilka dni</li>\r\n</ul>', 0),
(26, NULL, 21, NULL, NULL, NULL, 'Osocze bogatopłytkowe', 1, '2019-08-28 16:05:00', 'Osocze, bogatopłytkowe, Czym, jest, osocze, bogatopłytkowe?\r\n\r\n	 \r\n\r\n	Osocze, pochodzi, pobranej, pacjęta, krwi', '\r\n	Czym jest osocze bogatopłytkowe?\r\n\r\n	 \r\n\r\n	Osocze bogatopłytkowe pochodzi od pobranej od pacjęta krwi obwodowej. Po odpowiedniej obróbce materiał ze zwiększoną ilością płytek krwi może być wstrzyknięty do krwi pacjenta. Dzięki takiej metodzie pozyskania osocza, zyskuje się pewność braku powikłań związanych z biozodnością materiału\n\r\n	 \r\n\r\n	Czym jest wampirzy lifting?\r\n\r\n	 \r\n\r\n	Wampirzy lifting, znany inaczej jako terapia osocem bogapłytkowym, ma na celu pobudzić fibroblasty do powstawania nowego kolagenu\n\r\n	 \r\n\r\n	Składa się z trzech etapów:\r\n\r\n	\r\n		pobranie krwi z żyły pacjenta\r\n	\r\n		pozyskanie w separatorze koncentratu bogatopłytkowego oraz oddzielenie osocza za pomocą wirówki\r\n	\r\n		wstrzyknięcie wcześniej pobranego materiału do krwi pacjenta w odpowiednich miejscach\r\n\r\n\r\n	 \r\n\r\n	Zabieg jest naturalny i w pełni bezpieczny dla organizmu, nie wymaga rekonwalescencji. Jego efekty są trwałe i widoczne już po kilkunastu dniach\n\r\n	 \r\n\r\n	Leczeniu osoczem zawdzięcza się wspomaganie procesu gojenia i odbudowy tkanek, dzięki czemu stosowane jest w wielu dziedzinach medycyny\n\r\n	 \r\n\r\n	Wskazania do zabiegu:\r\n\r\n	 \r\n\r\n	\r\n		oznaki starzenia się skóry\r\n	\r\n		zmęczona i wysuszona skóra\r\n	\r\n		przebarwienia skórne, niejednolity kolor\r\n	\r\n		przyśpieszenie gojenia się ran po zabiegach medycyny estetycznej\r\n	\r\n		leczenie łysienia\r\n\r\n\r\n	 \r\n\r\n	Zalecenia pozabiegowe:\r\n\r\n	 \r\n\r\n	\r\n		przez 2-3 godziny po  wkłuciu osocza nie należy dotykać skóry\r\n	\r\n		po micięciu wspomnianego wyżej czasu zaleca się dokładne umycie skóry\r\n	\r\n		nakazany jest brak makijażu do 48 godzin po zabiegu\r\n	\r\n		dla optymalnej regeneracji skóry należy dbać o jej szczegóną higienę \r\n', 'osocze-bogatoplytkowe', '<h2 class=\"title1\">\r\n	<strong>Czym jest osocze bogatopłytkowe?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Osocze bogatopłytkowe pochodzi od pobranej od pacjęta krwi obwodowej. Po odpowiedniej obr&oacute;bce materiał ze zwiększoną ilością płytek krwi może być wstrzyknięty do krwi pacjenta. <span class=\"link\">Dzięki takiej metodzie pozyskania osocza, zyskuje się pewność braku powikłań związanych z biozodnością materiału.&nbsp;</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong>Czym jest wampirzy lifting?</strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Wampirzy lifting, znany inaczej jako terapia osocem bogapłytkowym, ma na celu pobudzić fibroblasty do powstawania nowego kolagenu.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Składa się z trzech etap&oacute;w:</strong></p>\r\n<ul>\r\n	<li>\r\n		pobranie krwi z żyły pacjenta</li>\r\n	<li>\r\n		pozyskanie w separatorze koncentratu bogatopłytkowego oraz oddzielenie osocza za pomocą wir&oacute;wki</li>\r\n	<li>\r\n		wstrzyknięcie wcześniej pobranego materiału do krwi pacjenta w odpowiednich miejscach</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zabieg jest naturalny i w pełni bezpieczny dla organizmu, nie wymaga rekonwalescencji. Jego efekty są trwałe i widoczne już po kilkunastu dniach.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Leczeniu osoczem zawdzięcza się wspomaganie procesu gojenia i odbudowy tkanek, dzięki czemu stosowane jest w wielu dziedzinach medycyny.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<strong><span class=\"pink\">Wskazania do zabiegu:</span></strong></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		oznaki starzenia się sk&oacute;ry</li>\r\n	<li>\r\n		zmęczona i wysuszona sk&oacute;ra</li>\r\n	<li>\r\n		przebarwienia sk&oacute;rne, niejednolity kolor</li>\r\n	<li>\r\n		przyśpieszenie gojenia się ran po zabiegach medycyny estetycznej</li>\r\n	<li>\r\n		leczenie łysienia</li>\r\n</ul>\r\n<p>\r\n	&nbsp;</p>\r\n<h2 class=\"title1\">\r\n	<span class=\"pink\"><strong>Zalecenia pozabiegowe:</strong></span></h2>\r\n<p>\r\n	&nbsp;</p>\r\n<ul>\r\n	<li>\r\n		przez 2-3 godziny po&nbsp; wkłuciu osocza nie należy dotykać sk&oacute;ry</li>\r\n	<li>\r\n		po micięciu wspomnianego wyżej czasu zaleca się dokładne umycie sk&oacute;ry</li>\r\n	<li>\r\n		nakazany jest brak makijażu do 48 godzin po zabiegu</li>\r\n	<li>\r\n		dla optymalnej regeneracji sk&oacute;ry należy dbać o jej szczeg&oacute;ną higienę&nbsp;</li>\r\n</ul>', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uploaded_file`
--

CREATE TABLE `uploaded_file` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `file` varchar(1024) DEFAULT NULL,
  `showOnList` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ZdjecieZmieniarki`
--

CREATE TABLE `ZdjecieZmieniarki` (
  `id` int(11) NOT NULL,
  `kolejnosc` int(11) DEFAULT NULL,
  `obrazek` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ZdjecieZmieniarki`
--

INSERT INTO `ZdjecieZmieniarki` (`id`, `kolejnosc`, `obrazek`, `updated_at`, `title`, `content`) VALUES
(5, 2, '5-5d66954b2275b.jpg', '2019-08-28 18:10:01', 'Nowoczesna stomatologia', 'Inwestujemy w najnowsze technologie, leczymy bez bólu, inwestując w dobro naszych pacjentów'),
(7, 8, '7-5d669569d6d6e.jpg', '2019-08-28 18:02:15', 'Gabinet w Łańcucie', 'Zapraszamy Państwa do gabinety w Łańcucie - mieścimy się przy ul. Piłsudskiego 52'),
(8, 4, '8-5d6695901f056.jpg', '2019-08-28 18:03:03', 'Nowoczesny gabinet', 'Najwyższe kwalifikacje naszego zespołu uzupełniamy nowoczesnym wyposażeniem gabinetu'),
(9, 6, '9-5d6695a898de7.jpg', '2019-08-28 18:01:04', 'Konsultacje ortodontyczne', 'Zapraszamy Państwa na fachowe konsultacje ortodontyczne i profesjonalne leczenie z użyciem najnowszych aparatów'),
(10, 5, '-5d669920ab754.jpg', '2019-08-28 17:56:17', 'Medycyna estetyczna', 'Zachęcamy do skorzystania z nowej oferty naszych gabinetów, zabiegów medycyny estetycznej przeprowadzanych przez certyfikowanego specjalistę');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `Aktualnosc`
--
ALTER TABLE `Aktualnosc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D3F2B3ED31019C` (`galeria_id`);

--
-- Indeksy dla tabeli `ContactFormMessage`
--
ALTER TABLE `ContactFormMessage`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `craue_config_setting`
--
ALTER TABLE `craue_config_setting`
  ADD PRIMARY KEY (`name`);

--
-- Indeksy dla tabeli `Employee`
--
ALTER TABLE `Employee`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indeksy dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indeksy dla tabeli `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_583D1F3EA76ED395` (`user_id`),
  ADD KEY `IDX_583D1F3EFE54D947` (`group_id`);

--
-- Indeksy dla tabeli `Galeria`
--
ALTER TABLE `Galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `GaleriaZdjecie`
--
ALTER TABLE `GaleriaZdjecie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3C805D89D31019C` (`galeria_id`);

--
-- Indeksy dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543BC4663E4` (`page_id`);

--
-- Indeksy dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D5506BF24F87` (`parentItem_id`);

--
-- Indeksy dla tabeli `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `page_uploadedfile`
--
ALTER TABLE `page_uploadedfile`
  ADD PRIMARY KEY (`page_id`,`uploadedfile_id`),
  ADD KEY `IDX_35EB0A06C4663E4` (`page_id`),
  ADD KEY `IDX_35EB0A06C34C6513` (`uploadedfile_id`);

--
-- Indeksy dla tabeli `Strona`
--
ALTER TABLE `Strona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_74FC6E18D31019C` (`galeria_id`),
  ADD KEY `IDX_74FC6E18499597B3` (`strona_nadrzedna_id`);

--
-- Indeksy dla tabeli `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uploaded_file`
--
ALTER TABLE `uploaded_file`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ZdjecieZmieniarki`
--
ALTER TABLE `ZdjecieZmieniarki`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `Aktualnosc`
--
ALTER TABLE `Aktualnosc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `ContactFormMessage`
--
ALTER TABLE `ContactFormMessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT dla tabeli `Employee`
--
ALTER TABLE `Employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `Galeria`
--
ALTER TABLE `Galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `GaleriaZdjecie`
--
ALTER TABLE `GaleriaZdjecie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `Strona`
--
ALTER TABLE `Strona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT dla tabeli `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `uploaded_file`
--
ALTER TABLE `uploaded_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ZdjecieZmieniarki`
--
ALTER TABLE `ZdjecieZmieniarki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `Aktualnosc`
--
ALTER TABLE `Aktualnosc`
  ADD CONSTRAINT `FK_D3F2B3ED31019C` FOREIGN KEY (`galeria_id`) REFERENCES `Galeria` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ograniczenia dla tabeli `fos_user_group`
--
ALTER TABLE `fos_user_group`
  ADD CONSTRAINT `FK_583D1F3EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_583D1F3EFE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`);

--
-- Ograniczenia dla tabeli `GaleriaZdjecie`
--
ALTER TABLE `GaleriaZdjecie`
  ADD CONSTRAINT `FK_3C805D89D31019C` FOREIGN KEY (`galeria_id`) REFERENCES `Galeria` (`id`);

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543BC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D5506BF24F87` FOREIGN KEY (`parentItem_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `page_uploadedfile`
--
ALTER TABLE `page_uploadedfile`
  ADD CONSTRAINT `FK_35EB0A06C34C6513` FOREIGN KEY (`uploadedfile_id`) REFERENCES `uploaded_file` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_35EB0A06C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `Strona`
--
ALTER TABLE `Strona`
  ADD CONSTRAINT `FK_74FC6E18499597B3` FOREIGN KEY (`strona_nadrzedna_id`) REFERENCES `Strona` (`id`),
  ADD CONSTRAINT `FK_74FC6E18D31019C` FOREIGN KEY (`galeria_id`) REFERENCES `Galeria` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
